#include "pch.h"
#include "engine.h"
#include "enginetools.h"
#include "world.h"
#include "worldgeometry.h"
#include "input.h"
#include "materialsystem\imaterial.h"
#include "materialsystem\imaterialvar.h"
#include <math.h>

SINGLETON_INSTANCE_LAZY(EngineTools);

#define HIGHLIGHT_ALPHA    0.35f

// not a class member because it causes alignment issues
mat4 mouseOverlayTransform;

EngineTools::EngineTools()
	: wasContextSwitched(false)
	  , mouseOverlay(nullptr)
	  , flatMaterial(nullptr)
	  , isHighlightVisible(true)
	  , isHighlightingCorner(true)
	  , flagOverlay(nullptr)
	  , activeEditMode(TileFlags::NONE)
{
	memset(editModeOverlays, 0, sizeof(editModeOverlays));
}

void EngineTools::Init()
{
	Assert(flatMaterial == nullptr);

	flatMaterial = materialDict->FindMaterial("debug/flat");

	Assert(!flatMaterial->IsErrorMaterial());

	flatMaterial->IncrementReferenceCount();

	// hardcoded editor controls
	Input::GetInstance()->SetBinding(SDLK_RIGHT, "+camera_right");
	Input::GetInstance()->SetBinding(SDLK_LEFT, "+camera_left");
	Input::GetInstance()->SetBinding(SDLK_UP, "+camera_up");
	Input::GetInstance()->SetBinding(SDLK_DOWN, "+camera_down");

	Input::GetInstance()->SetBinding(SDLK_LSHIFT, "+camera_speed");
	Input::GetInstance()->SetBinding(SDLK_RSHIFT, "+camera_speed");

	Input::GetInstance()->SetBinding((SDL_Keycode)512, "+camera_zoom_in");
	Input::GetInstance()->SetBinding((SDL_Keycode)513, "+camera_zoom_out");
}

void EngineTools::Shutdown()
{
	if (mouseOverlay != nullptr)
	{
		mouseOverlay->Release();
	}

	if (flatMaterial != nullptr)
	{
		flatMaterial->DecrementReferenceCount();
	}
}

void EngineTools::RequestEngineQuit()
{
	Engine::GetInstance()->ChangeState(Engine::STATE_SHUTDOWN);
}

void EngineTools::LoadMap(const char *filename)
{
	Engine::GetInstance()->LoadMap(filename);

	//Generate flag edit overlays
	for (int i = 0; i < TileFlags::COUNT; i++)
	{
		if (editModeOverlays[i] != nullptr)
		{
			editModeOverlays[i]->Release();
		}

		TileFlagId flag = (TileFlagId)(1 << i);
		editModeOverlays[i] = GenerateFlagHighlightMesh(flag);
	}
}

bool EngineTools::IntersectScreenRayWithWorld(int screenx, int screeny, int &tileXOut, int &tileYOut)
{
	if (Engine::GetInstance()->GetWorld() == nullptr)
	{
		return false;
	}

	vec3 hitPoint;

	if (::IntersectScreenRayWithWorld(screenx, screeny,
		renderContext->GetScreenWidth(), renderContext->GetScreenHeight(),
		renderContext->GetMainViewProjectionMatrix(), hitPoint))
	{
		if (isHighlightingCorner)
		{
			hitPoint.x() -= WORLD_TILE_SCALE * 0.5f;
			hitPoint.y() -= WORLD_TILE_SCALE * 0.5f;
		}

		if (Engine::GetInstance()->GetWorld()->GetTileIndexAtPosition(hitPoint, tileXOut, tileYOut))
		{
			return true;
		}
	}

	return false;
}

void EngineTools::ChangeTileUVs(int tilex, int tiley, float uvs, float uvt)
{
	((World *)Engine::GetInstance()->GetWorld())->GetGeometryForModify()->ChangeTileUVs(tilex, tiley, vec2(uvs, uvt));
}

void EngineTools::ReplaceWorldTextureAtlas(void *rgbData, void *rgbDataNormal, int width, int height)
{
	Assert(rgbData != nullptr);
	Assert(width >= 2);
	Assert(height >= 2);

	IWorld *world = Engine::GetInstance()->GetWorld();

	if (world == nullptr)
	{
		return;
	}

	IMaterial *material = world->GetMaterial();

	if ((material == nullptr) ||
		material->IsErrorMaterial())
	{
		return;
	}

	IMaterialVar *var = material->FindMaterialVar("albedotexture");
	IMaterialVar *varNormal = material->FindMaterialVar("normaltexture");

	Assert(var != nullptr &&
		   var->GetType() == MaterialVarTypes::TEXTURE);
	Assert(varNormal != nullptr &&
		   varNormal->GetType() == MaterialVarTypes::TEXTURE);

	if (var == nullptr
		|| varNormal == nullptr)
	{
		return;
	}

	ITexture *texture = var->GetTexture();
	ITexture *textureNormal = varNormal->GetTexture();

	Assert(texture != nullptr);
	Assert(textureNormal != nullptr);

	if (texture == nullptr
		|| textureNormal == nullptr)
	{
		return;
	}

	texture->SetContent(TextureFormats::RGB_8, width, height, rgbData);
	textureNormal->SetContent(TextureFormats::RGB_8, width, height, rgbDataNormal);
}

void EngineTools::LockEngine(bool switchContext)
{
	Engine::GetInstance()->GetThreadLock()->RequestLock();

	if (switchContext)
	{
		renderContext->MakeCurrent(true);

		wasContextSwitched = true;
	}
}

void EngineTools::UnlockEngine()
{
	if (wasContextSwitched)
	{
		renderContext->MakeCurrent(false);

		wasContextSwitched = false;
	}

	Engine::GetInstance()->GetThreadLock()->ReleaseLock();
}

void EngineTools::Update(float frametime)
{
	SDL_Event sdlEvent;

	while (SDL_PollEvent(&sdlEvent))
	{
		switch (sdlEvent.type)
		{
		case SDL_MOUSEMOTION:
			{
				UpdateTileHighlight(sdlEvent.motion.x, sdlEvent.motion.y);
			}
			break;
		case SDL_WINDOWEVENT:
			{
				if (sdlEvent.window.event == SDL_WINDOWEVENT_FOCUS_LOST)
				{
					// TODO: doesn't do anything?..
					Input::GetInstance()->ReleaseAllKeys();
				}
			}
			break;
		case SDL_MOUSEWHEEL:
			{
				Input::GetInstance()->MouseWheel(sdlEvent.wheel.y);
			}
			break;
		}
	}
}

void EngineTools::Render()
{
	Assert(flatMaterial != nullptr);

	if (mouseOverlay != nullptr)
	{
		renderContext->PushModelMatrix(mouseOverlayTransform);

		renderContext->BindMesh(mouseOverlay);
		flatMaterial->Draw();

		renderContext->PopModelMatrix();
	}

	if (flagOverlay != nullptr)
	{
		renderContext->BindMesh(flagOverlay);
		flatMaterial->Draw();
	}
}

void EngineTools::UpdateTileHighlight(int screenx, int screeny)
{
	int tilex, tiley;
	const bool shouldShowHighlight = isHighlightVisible	&&
									 IntersectScreenRayWithWorld(screenx, screeny, tilex, tiley);
	float scale = WORLD_TILE_SCALE;

	if (shouldShowHighlight)
	{
		if (mouseOverlay == nullptr)
		{
			vec4 color(1, 1, 0.5f, HIGHLIGHT_ALPHA);

			if (!isHighlightingCorner)
			{
				color.z() = 1.0f;
				scale *= 0.5f;	//tmp solution
			}

			mouseOverlay = renderContext->CreateStaticMesh(FaceTypes::TRIANGLE_STRIP,
														   VertexFormat::POSITION_3F | VertexFormat::COLOR_4F, 2);

			MeshBuilder builder;
			builder.Attach(mouseOverlay);

			builder.Position3f(0, 0, WORLD_POS_Z);
			builder.Color4v(color);
			builder.AdvanceVertex();
			builder.Position3f(scale, 0, WORLD_POS_Z);
			builder.Color4v(color);
			builder.AdvanceVertex();
			builder.Position3f(0, scale, WORLD_POS_Z);
			builder.Color4v(color);
			builder.AdvanceVertex();
			builder.Position3f(scale, scale, WORLD_POS_Z);
			builder.Color4v(color);
			builder.AdvanceVertex();

			builder.Detach();
		}

		vec3 offset(vec3::Zero());

		if (isHighlightingCorner)
		{
			offset = vec3(scale * 0.5f, scale * 0.5f, 0.0f);
		}
		else
		{
			offset = vec3(scale * 0.25f, scale * 0.25f, 0.0f);
		}

		mouseOverlayTransform = mat4::Identity();

		mouseOverlayTransform = translate(mouseOverlayTransform,
										  vec3(tiley * WORLD_TILE_SCALE, tilex * WORLD_TILE_SCALE, 3.0f) + offset);
	}
	else if (mouseOverlay != nullptr)
	{
		mouseOverlay->Release();
		mouseOverlay = nullptr;
	}
}

void EngineTools::SetPaintHighlight(bool visible, bool isCorner)
{
	isHighlightVisible = visible;
	isHighlightingCorner = isCorner;
	if (mouseOverlay != nullptr){
		mouseOverlay->Release();
		mouseOverlay = nullptr;
	}
}

void EngineTools::SetTileFlags(int x, int y, bool set)
{
	vec4 color(0, 255, 0, HIGHLIGHT_ALPHA);

	if (!set)
	{
		color[3] = 0;
	}

	World *world = ((World *)Engine::GetInstance()->GetWorld());
	int index = x + (y * world->GetWidth());

	MeshBuilder builder;
	builder.AttachModify(flagOverlay, index * 6, 6);
	for (int i = 0; i < 6; i++)
	{
		builder.Color4v(color);
		builder.AdvanceVertex();
	}

	builder.Detach();
}

IMesh *EngineTools::GenerateFlagHighlightMesh(TileFlagId flag)
{
	IWorld *world = Engine::GetInstance()->GetWorld();

	if (world == nullptr)
	{
		return nullptr;
	}

	vec4 color(0, 255, 0, HIGHLIGHT_ALPHA);
	IMesh *mesh = renderContext->CreateStaticMesh(FaceTypes::TRIANGLES,
												  VertexFormat::POSITION_3F | VertexFormat::COLOR_4F, world->GetTileCount() * 2);

	MeshBuilder builder;
	builder.Attach(mesh);

	float z = 1.5f;
	for (int i = 0; i < world->GetTileCount(); i++)
	{
		WorldTile tile = world->GetTile(i);

		if ((tile.flags & flag) != 0)
		{
			color[3] = HIGHLIGHT_ALPHA;
		}
		else
		{
			color[3] = 0;
		}

		float x = tile.y * WORLD_TILE_SCALE, y = tile.x * WORLD_TILE_SCALE;

		builder.Position3f(x, y, z);
		builder.Color4v(color);
		builder.AdvanceVertex();
		builder.Position3f(x + WORLD_TILE_SCALE, y + WORLD_TILE_SCALE, z);
		builder.Color4v(color);
		builder.AdvanceVertex();
		builder.Position3f(x, y + WORLD_TILE_SCALE, z);
		builder.Color4v(color);
		builder.AdvanceVertex();

		builder.Position3f(x, y, z);
		builder.Color4v(color);
		builder.AdvanceVertex();
		builder.Position3f(x + WORLD_TILE_SCALE, y, z);
		builder.Color4v(color);
		builder.AdvanceVertex();
		builder.Position3f(x + WORLD_TILE_SCALE, y + WORLD_TILE_SCALE, z);
		builder.Color4v(color);
		builder.AdvanceVertex();
	}

	builder.Detach();

	return mesh;
}

void EngineTools::SetFlagHighlight(TileFlagId flag)
{
	if (flag == TileFlags::NONE)
	{
		flagOverlay = nullptr;
		return;
	}

	int index = int(ceil(log2((int)flag)));
	flagOverlay = editModeOverlays[index];
}

void EngineTools::OnKeyDown(SDL_Keycode sdlKey)
{
	Input::GetInstance()->KeyDown(sdlKey);
}

void EngineTools::OnKeyUp(SDL_Keycode sdlKey)
{
	Input::GetInstance()->KeyUp(sdlKey);
}
