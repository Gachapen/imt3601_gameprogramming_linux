#ifndef DEBUGDRAW_H
#define DEBUGDRAW_H

#include "engine/idebugdraw.h"


class DebugDraw : public IDebugDraw
{
	DECLARE_SINGLETON(DebugDraw);

public:
	~DebugDraw();

	void Init();
	void Shutdown();

	virtual void DrawLine(const vec3 &start, const vec3 &end, const vec3 &color, float alpha, float lifetime, bool ignoreZ);
	virtual void DrawLineLoop(const vec3 *vertices, int vertexCount, const vec3 &color, float alpha, float lifetime, bool ignoreZ);
	virtual void DrawTriangleFan(const vec3 *vertices, int vertexCount, const vec3 &color, float alpha, float lifetime, bool ignoreZ);
	virtual void DrawBox(const vec3 &mins, const vec3 &maxs, const vec3 &color, float alpha, float lifetime, bool ignoreZ);

	void Render();

private:
	struct DebugObject
	{
		vec3 color;
		float alpha;

		float timer;
		bool ignoreZ;
	};

	struct DebugLine : public DebugObject
	{
		vec3 start, end;
	};

	struct DebugTriangle : public DebugObject
	{
		vec3 vertices[3];
	};

	std::vector<DebugLine> lines;
	std::vector<DebugTriangle> triangles;

	IMaterial *debugMaterial;
	IMaterial *debugMaterialNoZ;
};

#endif