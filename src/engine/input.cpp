#include "pch.h"
#include "appinterface\iappinterface.h"
#include "input.h"

SINGLETON_INSTANCE(Input);

EXPOSE_APP(INTERFACE_IINPUT_VERSION, Input::GetInstance());

Input::Input()
	: mousePosX(0)
	  , mousePosY(0)
	  , activeTextListener(nullptr)
{
}

Input::~Input()
{
	Assert(keyListeners.empty());
}

void Input::MouseMove(int x, int y)
{
	mousePosX = x;
	mousePosY = y;
}

int Input::GetMousePosX()
{
	return mousePosX;
}

int Input::GetMousePosY()
{
	return mousePosY;
}

void Input::KeyDown(SDL_Keycode sdlKey)
{
	if (VECTOR_CONTAINS(pressedBoundKeys, sdlKey))
	{
		return;
	}

	bool handledKey = false;

	for (auto l : keyListeners)
	{
		if (l->OnKeyDown(sdlKey))
		{
			handledKey = true;

			Assert(!MAP_CONTAINS(activeKeyListeners, sdlKey));
			activeKeyListeners[sdlKey] = l;
			break;
		}
	}

	if (!handledKey)
	{
		handledKey = EvaluateKeyDown(sdlKey);
	}

	if (handledKey)
	{
		pressedBoundKeys.push_back(sdlKey);
	}
}

void Input::KeyUp(SDL_Keycode sdlKey)
{
	auto itr = VECTOR_FIND(pressedBoundKeys, sdlKey);

	if (itr != pressedBoundKeys.end())
	{
		pressedBoundKeys.erase(itr);

		auto itr = activeKeyListeners.find(sdlKey);

		if (itr != activeKeyListeners.end())
		{
			itr->second->OnKeyUp(sdlKey);
			activeKeyListeners.erase(itr);
			return;
		}

		EvaluateKeyUp(sdlKey);
	}
}

void Input::OnTextInput(const char *text)
{
	if (activeTextListener != nullptr)
	{
		activeTextListener->OnTextEvent(text);
	}
}

void Input::MouseDown(int mouseButton)
{
	for (auto l : mouseListeners)
	{
		l->OnMouseDown(mouseButton);
	}
}

void Input::MouseUp(int mouseButton)
{
	for (auto l : mouseListeners)
	{
		l->OnMouseUp(mouseButton);
	}
}

void Input::MouseWheel(int delta)
{
	SDL_Keycode sdlKey = (SDL_Keycode)((delta < 0) ? 512 : 513);

	delta = abs(delta);

	for (int i = 0; i < delta; i++)
	{
		EvaluateKeyDown(sdlKey);
		EvaluateKeyUp(sdlKey);
	}
}

void Input::StartTextInput(IInputKeyListener *listener)
{
	Assert(activeTextListener == nullptr);
	Assert(listener != nullptr);

	activeTextListener = listener;
	SDL_StartTextInput();
}

void Input::StopTextInput()
{
	Assert(activeTextListener != nullptr);

	activeTextListener = nullptr;
	SDL_StopTextInput();
}

void Input::AddKeyListener(IInputKeyListener *listener)
{
	Assert(!VECTOR_CONTAINS(keyListeners, listener));

	if (!VECTOR_CONTAINS(keyListeners, listener))
	{
		keyListeners.push_back(listener);
	}
}

void Input::RemoveKeyListener(IInputKeyListener *listener)
{
	Assert(VECTOR_CONTAINS(keyListeners, listener));

	if (VECTOR_CONTAINS(keyListeners, listener))
	{
		VECTOR_REMOVE(keyListeners, listener);
	}
}

void Input::AddMouseListener(IInputMouseListener *listener)
{
	Assert(!VECTOR_CONTAINS(mouseListeners, listener));

	if (!VECTOR_CONTAINS(mouseListeners, listener))
	{
		mouseListeners.push_back(listener);
	}
}

void Input::RemoveMouseListener(IInputMouseListener *listener)
{
	Assert(VECTOR_CONTAINS(mouseListeners, listener));

	if (VECTOR_CONTAINS(mouseListeners, listener))
	{
		VECTOR_REMOVE(mouseListeners, listener);
	}
}

void Input::SetBinding(SDL_Keycode sdlKey, const char *command)
{
	bindings[sdlKey] = command;
}

const char *Input::GetBinding(SDL_Keycode sdlKey)
{
	auto itr = bindings.find(sdlKey);

	if (itr == bindings.end())
	{
		return "";
	}

	return itr->second.c_str();
}

SDL_Keycode Input::GetKeyOfBinding(const char *command)
{
	for (auto itr : bindings)
	{
		if (itr.second == command)
		{
			return itr.first;
		}
	}

	return SDLK_UNKNOWN;
}

bool Input::EvaluateKeyDown(SDL_Keycode sdlKey)
{
	const char *binding = GetBinding(sdlKey);

	if (*binding)
	{
		commandBuffer += binding;
		commandBuffer += '\n';

		return true;
	}

	return false;
}

void Input::EvaluateKeyUp(SDL_Keycode sdlKey)
{
	const char *binding = GetBinding(sdlKey);

	if (*binding == '+')
	{
		char releaseBinding[64];

		G_StrNCpy(releaseBinding, binding, sizeof(releaseBinding));
		*releaseBinding = '-';

		commandBuffer += releaseBinding;
		commandBuffer += '\n';
	}
}

void Input::ReleaseAllKeys()
{
	std::vector<SDL_Keycode> keys = pressedBoundKeys;

	for (auto key : keys)
	{
		KeyUp(key);
	}

	Assert(pressedBoundKeys.empty());
}

void Input::ExecuteCommandBuffer()
{
	if (commandBuffer.empty())
	{
		return;
	}

	cvar->ExecuteBuffer(commandBuffer.c_str());

	commandBuffer.clear();
}
