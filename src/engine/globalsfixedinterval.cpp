#include "pch.h"
#include "globalsfixedinterval.h"

GlobalsFixedInterval::GlobalsFixedInterval()
	: intervaltime(0.0f)
	, framecount(0)
	, simulatedTime(0.0)
	, currentTime(0.0)
	, simulatedTimeFloat(0.0f)
{
}

void GlobalsFixedInterval::SetFixedInterval(float interval)
{
	intervaltime = interval;
}

float GlobalsFixedInterval::GetFrametime()
{
	return intervaltime;
}

int GlobalsFixedInterval::GetFrameCount()
{
	return framecount;
}

float GlobalsFixedInterval::GetTime()
{
	return simulatedTimeFloat;
}

double GlobalsFixedInterval::GetTimeHighPrecision()
{
	return simulatedTime;
}

int GlobalsFixedInterval::AdvanceTime(float frametime)
{
	Assert(frametime > 0.0f); // Not set up properly
	Assert(((currentTime - simulatedTime) / intervaltime) < 1); // Was advanced without processing steps

	currentTime += frametime;

	return int((currentTime - simulatedTime) / intervaltime);
}

void GlobalsFixedInterval::AdvanceStep()
{
	simulatedTime += intervaltime;
	simulatedTimeFloat = float(simulatedTime);
	framecount++;
}

void GlobalsFixedInterval::Reset()
{
	framecount = 0;
	simulatedTime = 0.0;
	simulatedTimeFloat = 0.0f;
	currentTime = 0.0;
}

void GlobalsFixedInterval::SetFrametime(float frametime)
{
	this->intervaltime = frametime;
}

void GlobalsFixedInterval::SetFrameCount(int framecount)
{
	this->framecount = framecount;
}

void GlobalsFixedInterval::SetTime(double time)
{
	this->simulatedTime = time;
}