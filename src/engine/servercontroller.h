#ifndef SERVERCONTROLLER_H
#define SERVERCONTROLLER_H

class INetworkServer;

class ServerController
{
	DECLARE_SINGLETON(ServerController);

public:
	~ServerController();

	bool HandlePackage(const NetworkAddress &source, const int& clientReference, Uint8 type, ByteBuffer &buffer);

	void Simulate();

	void ShutdownServer();

	/// Takes ownership
	void SendMessageTo(const NetworkAddress &destAddress, const int &destIndex, Byte *data, int sizeInBytes);

	/// Takes ownership
	void SendMessageToClient(int clientIndex, Byte *data, int sizeInBytes);

	/// Takes ownership
	void SendMessageToAllClients(Byte *data, int sizeInBytes, bool bufferIsStatic = false);

	Message *PollMessage();

	// Interaction with network server
	bool StartNetworkServer(unsigned short port);
	void DropNetworkClient(const int clientIndex);
	void DispatchOutgoingMessage(const int clientReference, ByteBuffer& data, bool reliable = false);
	Byte* PollIncomingMessages(int clientReferenceOutput, Uint32& sizeOutput);

private:

	void QueueClientMessage(int clientIndex, Message *message);

	void HandleClientConnectionAttempt(const NetworkAddress &source, const int& clientReference, ByteBuffer &buffer);
	void HandleClientConnectionPrecache(const NetworkAddress &source, const int& clientReference, ByteBuffer &buffer);
	void HandleClientConnectionFinished(const NetworkAddress &source, const int& clientReference, ByteBuffer &buffer);
	void HandleClientCommand(const NetworkAddress &source, const int& clientReference, ByteBuffer &buffer);

	int GetClientIndexFromReference(const int &reference);

	struct ClientData
	{
		ClientData()
			: state(ClientStates::DISCONNECTED)
			, clientReference(-1)
		{
		}

		~ClientData()
		{
			for (auto m : queuedMessages)
			{
				delete m;
			}
		}

		bool IsFree() const
		{
			return state == ClientStates::DISCONNECTED;
		}

		bool IsLocal() const
		{
			return (clientReference == -1);
		}

		bool IsFullyConnected() const
		{
			return state == ClientStates::CONNECTED;
		}

		void Reset()
		{
			state = ClientStates::DISCONNECTED;
			address.Reset();
			clientReference = -1;
		}

		ClientStateId state;
		NetworkAddress address;
		int clientReference;

		std::vector<Message *> queuedMessages;
	};

	ClientData clients[MAX_PLAYERS];

	std::vector<Message *> messages;
	INetworkServer *networkServer;
};

#endif
