#ifndef WORLDGEOMETRY_H
#define WORLDGEOMETRY_H

#include "engine\iworld.h"

class World;
class IMesh;

class WorldGeometry : public IWorldGeometry
{
	NO_COPY(WorldGeometry);
public:
	WorldGeometry();
	~WorldGeometry();

	struct ListEntry
	{
		IMesh *mesh;
		int startIndex_x;
		int startIndex_y;
		int width;
		int height;
		vec3 min;
		vec3 max;
	};

	void AddWorldMesh(ListEntry &entry); // takes ownership
	void AddWaterMesh(ListEntry &entry); // takes ownership

	void ChangeTileUVs(int tile_x, int tile_y, vec2 uvs);

	// IWorldGeometry
	virtual int GetWorldListCount() const;
	virtual const IMesh *GetWorldListMesh(int index) const;
	virtual void GetWorldListBounds(int index, vec3 &min, vec3 &max) const;

	virtual int GetWaterListCount() const;
	virtual const IMesh *GetWaterListMesh(int index) const;
	virtual void GetWaterListBounds(int index, vec3 &min, vec3 &max) const;

private:
	std::vector<ListEntry> worldList;
	std::vector<ListEntry> waterList;

};

#endif