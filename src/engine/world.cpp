#include "pch.h"

#include "debugdraw.h"
#include "engine.h"
#include "world.h"
#include "worldgeometry.h"

World::World()
	: name(nullptr)
	  , tiles(nullptr)
	  , geometry(nullptr)
	  , material(nullptr)
	  , waterMaterial(nullptr)
	  , version(0)
	  , tileNeighbours(nullptr)
	  , tileVertices(nullptr)
	  , vertexCount(0)
{
}

World::~World()
{
	Assert(material == nullptr);
	Assert(waterMaterial == nullptr);

	delete geometry;
	delete [] name;
	delete [] tiles;
	delete [] tileNeighbours;
	delete [] tileVertices;
}

void World::Release()
{
	Shutdown();

	delete this;
}

World *World::LoadFromFile(const char *filename)
{
	char pathShort[MAX_PATH_GAME];
	char pathFull[MAX_PATH_GAME];
	char pathTmp[MAX_PATH_GAME];
	char pathMaterial[MAX_PATH_GAME];

	G_StrAbsContentPath(ContentDirectories::MAPS, filename, pathShort, sizeof(pathShort));
	G_StrFixSlashes(pathShort);
	G_StrAddOrReplaceExtension(pathShort, WORLD_EXT, pathFull, sizeof(pathFull));

	G_StrSnPrintf(pathTmp, sizeof(pathTmp), "maps" PATHSEPARATOR_STRING "%s" WORLD_ATLAS_SUFFIX, filename);
	G_StrAbsContentPath(ContentDirectories::MATERIALS, pathTmp, pathMaterial, sizeof(pathMaterial));
	G_StrFixSlashes(pathMaterial);

	std::string file = LoadFile(pathFull);

	Assert(file.length() > 0);

	if (file.empty())
	{
		Assert(0);
		return nullptr;
	}

	World *world = new World;

	ByteBuffer reader((Byte *)file.data(), file.length());

	if (reader.GetReadLeft() < 6)
	{
		delete world;
		return nullptr;
	}

	world->version = reader.ReadUInt();

	// only supported version currently
	Assert(world->version == 2);

	int nameLength = reader.ReadUChar();

	if (reader.GetReadLeft() < nameLength)
	{
		delete world;
		return nullptr;
	}

	world->name = new char[nameLength + 1];
	reader.Read(nameLength, world->name);
	world->name[nameLength] = 0;

	if (reader.GetReadLeft() < 8)
	{
		delete world;
		return nullptr;
	}

	world->width = reader.ReadUShort();
	world->height = reader.ReadUShort();
	world->camWidth = reader.ReadUShort();
	world->camHeight = reader.ReadUShort();

	world->tileCount = world->width * world->height;

	if (reader.GetReadLeft() < 10)
	{
		delete world;
		return nullptr;
	}

	world->globalLight.color[0] = reader.ReadUShort() / 255.0f;
	world->globalLight.color[1] = reader.ReadUShort() / 255.0f;
	world->globalLight.color[2] = reader.ReadUShort() / 255.0f;

	euler angles = euler::Zero();
	angles[1] = reader.ReadUShort();
	angles[0] = reader.ReadUShort();

	AngleVectors(angles, &world->globalLight.direction);

	if (reader.GetReadLeft() < 14)
	{
		delete world;
		return nullptr;
	}

	world->fog.color[0] = reader.ReadUShort();
	world->fog.color[0] = CLAMP(world->fog.color[0], 0, 255) / 255.0f;
	world->fog.color[1] = reader.ReadUShort();
	world->fog.color[1] = CLAMP(world->fog.color[1], 0, 255) / 255.0f;
	world->fog.color[2] = reader.ReadUShort();
	world->fog.color[2] = CLAMP(world->fog.color[2], 0, 255) / 255.0f;
	world->fog.start = reader.ReadUShort();
	world->fog.range = reader.ReadUShort();
	world->fog.amount = reader.ReadFloat();

	//default tile type
	world->defaultTileType = reader.ReadUChar();

	const int tileCount = world->width * world->height;

	if (reader.GetReadLeft() < tileCount * 10)
	{
		delete world;
		return nullptr;
	}

	world->tiles = new WorldTile[tileCount];

	for (int i = 0; i < tileCount; i++)
	{
		world->tiles[i].uvs = reader.ReadFloat();
		world->tiles[i].uvt = reader.ReadFloat();
		world->tiles[i].flags = reader.ReadUChar();
		world->tiles[i].tileType = reader.ReadUChar();

		world->tiles[i].index = i;

		world->tiles[i].x = i % world->width;
		world->tiles[i].y = i / world->width;
	}

	if (reader.GetReadLeft() < 2)
	{
		delete world;
		return nullptr;
	}

	const int waveCount = reader.ReadUShort();

	if (reader.GetReadLeft() < waveCount * 5 + 5)
	{
		delete world;
		return nullptr;
	}

	world->waveDuration = reader.ReadUShort();
	world->pauseDuration = reader.ReadUShort();
	world->waveFlags = reader.ReadUChar();

	for (int i = 0; i < waveCount; i++)
	{
		const int unitTypeCount = reader.ReadUShort();
		WorldWave wave;

		for (int u = 0; u < unitTypeCount; u++)
		{
			WorldWaveUnitType unitType;
			unitType.type = reader.ReadUChar();
			unitType.count = reader.ReadUShort();

			wave.unitTypes.push_back(unitType);
		}

		world->waves.push_back(wave);
	}

	// TODO: world entities

	Assert(reader.GetReadLeft() > 0);
	
	Assert(world->tileNeighbours == nullptr);
	Assert(world->tileVertices == nullptr);

	world->tileNeighbours = new int[tileCount];
	memset(world->tileNeighbours, 0, sizeof(int) * tileCount);

	world->vertexCount = (world->width + 1) * (world->height + 1);

	world->tileVertices = new WorldVertex[world->vertexCount];
	memset(world->tileVertices, 0, sizeof(WorldVertex) * world->vertexCount);

	world->GenerateNeighbours();
	world->GenerateVertexPositions();
	world->GenerateWorldGeometry();

	world->material = materialDict->FindMaterial(pathMaterial);
	world->waterMaterial = materialDict->FindMaterial("maps" PATHSEPARATOR_STRING "water_simple");

	Assert(world->material != nullptr && !world->material->IsErrorMaterial());
	Assert(world->waterMaterial != nullptr && !world->waterMaterial->IsErrorMaterial());

	if (world->material != nullptr)
	{
		world->material->IncrementReferenceCount();
	}

	if (world->waterMaterial != nullptr)
	{
		world->waterMaterial->IncrementReferenceCount();
	}

	world->Init();

	return world;
}

int World::GetVersion() const
{
	return version;
}

const char *World::GetName() const
{
	return name;
}

int World::GetWidth() const
{
	return width;
}

int World::GetHeight() const
{
	return height;
}

int World::GetCameraWidth() const
{
	return camWidth;
}

int World::GetCameraHeight() const
{
	return camHeight;
}

const GlobalLightParams &World::GetGlobalLightParams() const
{
	return globalLight;
}

const FogParams &World::GetFogParams() const
{
	return fog;
}

int World::GetTileCount() const
{
	return tileCount;
}

const WorldTile &World::GetTile(int index) const
{
	Assert(index >= 0 && index < tileCount);
	return tiles[index];
}

int World::GetWaveDuration() const
{
	return waveDuration;
}

int World::GetPauseDuration() const
{
	return pauseDuration;
}

int World::GetWaveFlags() const
{
	return waveFlags;
}

int World::GetWaveCount() const
{
	return waves.size();
}

const WorldWave &World::GetWave(int index) const
{
	Assert(index >= 0 && index < (int)waves.size());
	return waves[index];
}

WorldGeometry *World::GetGeometryForModify()
{
	return geometry;
}

const IWorldGeometry *World::GetGeometry() const
{
	return geometry;
}

IMaterial *World::GetMaterial() const
{
	return material;
}

IMaterial *World::GetWaterMaterial() const
{
	return waterMaterial;
}

void World::GenerateWorldGeometry()
{
	Assert(geometry == nullptr);
	Assert(renderContext != nullptr);

	geometry = new WorldGeometry();

	const int tileCountWRemainder = width % WORLD_CLUSTER_SIZE;
	const int tileCountHRemainder = height % WORLD_CLUSTER_SIZE;
	const int clusterCountW = (width / WORLD_CLUSTER_SIZE) + ((tileCountWRemainder != 0) ? 1 : 0);
	const int clusterCountH = (height / WORLD_CLUSTER_SIZE) + ((tileCountHRemainder != 0) ? 1 : 0);

	const vec3 worldNormal(0, 0, 1.0f);

	const float worldPosZ = WORLD_POS_Z;
	const float waterPosZ = -15.0f;
	const float waterUVScale = 0.002f;

	const bool enableWater = Engine::GetInstance()->IsInitializationMode(Engine::INIT_GAME);

	for (int w = 0; w < clusterCountW; w++)
	{
		int tileCountW = WORLD_CLUSTER_SIZE;

		if (w == clusterCountW - 1
			&& tileCountWRemainder != 0)
			tileCountW = tileCountWRemainder;

		for (int h = 0; h < clusterCountH; h++)
		{
			int tileCountH = WORLD_CLUSTER_SIZE;

			if (h == clusterCountH - 1
				&& tileCountHRemainder != 0)
				tileCountH = tileCountHRemainder;

			const int clusterTileCount = tileCountH * tileCountW;

			if (clusterTileCount < 1)
			{
				Assert(0);
				continue;
			}

			const int tileStartIndex = w * WORLD_CLUSTER_SIZE + h * WORLD_CLUSTER_SIZE * width;
			const int tileEndIndex = tileStartIndex + tileCountW - 1 + (tileCountH - 1) * width;

			Assert(tileStartIndex < tileCount);
			Assert(tileEndIndex < tileCount);

			IMesh *mesh = renderContext->CreateStaticMesh(FaceTypes::TRIANGLES,
				VertexFormat::POSITION_3F | VertexFormat::TEXTURE_COORD_2F
				| VertexFormat::NORMAL_3F | VertexFormat::BINORMAL_3F | VertexFormat::TANGENT_3F,
				clusterTileCount * 2);

			MeshBuilder builder;
			builder.Attach(mesh);

			vec3 mins = vec3(tiles[tileStartIndex].y * WORLD_TILE_SCALE,
				tiles[tileStartIndex].x * WORLD_TILE_SCALE,
				worldPosZ - 1.0f);

			vec3 maxs = vec3(tiles[tileEndIndex].y * WORLD_TILE_SCALE + WORLD_TILE_SCALE,
				tiles[tileEndIndex].x * WORLD_TILE_SCALE + WORLD_TILE_SCALE,
				worldPosZ);

			std::vector<const WorldTile*> waterTiles;

			for (int tile = 0; tile < clusterTileCount; tile++)
			{
				const int globalIndex = tile % tileCountW
					+ tile / tileCountW * width
					+ tileStartIndex;

				Assert(globalIndex < tileCount);

				const WorldTile &info = tiles[globalIndex];

				if ((tileNeighbours[globalIndex] & TN_WATER_MASK) != 0)
				{
					waterTiles.push_back(&info);
				}

				vec4 elevation;
				GetElevationPoints(globalIndex, elevation);

				const int base = GetTileVertexIndex(globalIndex);

				Assert(base < vertexCount - width - 2);

				// the y index is along the x world axis
				// the x index is along the y world axis
				vec2 uvBase(info.uvs, info.uvt);

				builder.Position3v(tileVertices[base].position);
				builder.Texcoord2f(uvBase.x() + WORLD_TILE_UV_INSET, uvBase.y() + WORLD_TILE_UV_INSET);
				builder.Normal3p(worldNormal.data());
				builder.AdvanceVertex();
				builder.Position3v(tileVertices[base + width + 1].position);
				builder.Texcoord2f(uvBase.x() + WORLD_TILE_UV_INSET, uvBase.y() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET);
				builder.Normal3p(worldNormal.data());
				builder.AdvanceVertex();
				builder.Position3v(tileVertices[base + 1].position);
				builder.Texcoord2f(uvBase.x() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET, uvBase.y() + WORLD_TILE_UV_INSET);
				builder.Normal3p(worldNormal.data());
				builder.AdvanceVertex();

				builder.Position3v(tileVertices[base + width + 2].position);
				builder.Texcoord2f(uvBase.x() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET, uvBase.y() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET);
				builder.Normal3p(worldNormal.data());
				builder.AdvanceVertex();
				builder.Position3v(tileVertices[base + 1].position);
				builder.Texcoord2f(uvBase.x() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET, uvBase.y() + WORLD_TILE_UV_INSET);
				builder.Normal3p(worldNormal.data());
				builder.AdvanceVertex();
				builder.Position3v(tileVertices[base + width + 1].position);
				builder.Texcoord2f(uvBase.x() + WORLD_TILE_UV_INSET, uvBase.y() + WORLD_TILE_UV_SIZE - WORLD_TILE_UV_INSET);
				builder.Normal3p(worldNormal.data());
				builder.AdvanceVertex();
			}

			builder.GenerateNormals();
			builder.SmoothNormals();
			builder.GenerateTangentSpace();
			builder.Detach();

			WorldGeometry::ListEntry entry;
			entry.mesh = mesh;
			entry.min = mins;
			entry.max = maxs;
			entry.width = tileCountW;
			entry.height = tileCountH;
			entry.startIndex_x = tiles[tileStartIndex].x;
			entry.startIndex_y = tiles[tileStartIndex].y;

			geometry->AddWorldMesh(entry);

			if (enableWater
				&& !waterTiles.empty())
			{
				WorldGeometry::ListEntry waterEntry(entry);

				IMesh *waterMesh = renderContext->CreateStaticMesh(FaceTypes::TRIANGLES,
					VertexFormat::POSITION_3F | VertexFormat::TEXTURE_COORD_2F
					| VertexFormat::NORMAL_3F | VertexFormat::BINORMAL_3F | VertexFormat::TANGENT_3F,
					waterTiles.size() * 2);

				MeshBuilder builder;
				builder.Attach(waterMesh);

				waterEntry.min = vec3(FLT_MAX, FLT_MAX, waterPosZ - 1);
				waterEntry.max = vec3(-FLT_MAX, -FLT_MAX, waterPosZ + 1);

				for (auto t : waterTiles)
				{
					vec3 positionMin(t->y * WORLD_TILE_SCALE,
						t->x * WORLD_TILE_SCALE,
						waterPosZ);
					vec3 positionMax = vec3(t->y * WORLD_TILE_SCALE + WORLD_TILE_SCALE,
						t->x * WORLD_TILE_SCALE + WORLD_TILE_SCALE,
						waterPosZ);

					for (int v = 0; v < 2; v++)
					{
						waterEntry.min[v] = MIN(waterEntry.min[v], positionMin[v]);
						waterEntry.max[v] = MAX(waterEntry.max[v], positionMax[v]);
					}

					vec2 uvMin(positionMin.x() * waterUVScale,
						positionMin.y() * waterUVScale);
					vec2 uvMax = uvMin + vec2(WORLD_TILE_SCALE * waterUVScale,
						WORLD_TILE_SCALE * waterUVScale);

					builder.Position3p(positionMin.data());
					builder.Texcoord2f(uvMin.x(), uvMin.y());
					builder.Normal3p(worldNormal.data());
					builder.AdvanceVertex();
					builder.Position3f(positionMax.x(), positionMin.y(), waterPosZ);
					builder.Texcoord2f(uvMax.x(), uvMin.y());
					builder.Normal3p(worldNormal.data());
					builder.AdvanceVertex();
					builder.Position3f(positionMin.x(), positionMax.y(), waterPosZ);
					builder.Texcoord2f(uvMin.x(), uvMax.y());
					builder.Normal3p(worldNormal.data());
					builder.AdvanceVertex();

					builder.Position3p(positionMax.data());
					builder.Texcoord2f(uvMax.x(), uvMax.y());
					builder.Normal3p(worldNormal.data());
					builder.AdvanceVertex();
					builder.Position3f(positionMin.x(), positionMax.y(), waterPosZ);
					builder.Texcoord2f(uvMin.x(), uvMax.y());
					builder.Normal3p(worldNormal.data());
					builder.AdvanceVertex();
					builder.Position3f(positionMax.x(), positionMin.y(), waterPosZ);
					builder.Texcoord2f(uvMax.x(), uvMin.y());
					builder.Normal3p(worldNormal.data());
					builder.AdvanceVertex();
				}

				builder.GenerateTangentSpace();
				builder.Detach();

				waterEntry.mesh = waterMesh;

				geometry->AddWaterMesh(waterEntry);
			}
		}
	}
}

void World::GenerateNeighbours()
{
	for (int i = 0; i < tileCount; i++)
	{
		int &neightbours = tileNeighbours[i];

		WorldTile *kernel[9] = {nullptr};

		kernel[4] = &tiles[i];

		if (i >= width)
		{
			if ((i % width) != 0)
				kernel[0] = &tiles[i - width - 1];

			kernel[1] = &tiles[i - width];

			if ((i % width) != width - 1)
				kernel[2] = &tiles[i - width + 1];
		}

		if ((i % width) != 0)
			kernel[3] = &tiles[i - 1];

		if ((i % width) != width - 1)
			kernel[5] = &tiles[i + 1];

		if (i / width < height - 1)
		{
			if ((i % width) != 0)
				kernel[6] = &tiles[i + width - 1];

			kernel[7] = &tiles[i + width];
	
			if ((i % width) != width - 1)
				kernel[8] = &tiles[i + width + 1];
		}

		for (int m = 0; m < 9; m++)
		{
			if (kernel[m] == nullptr)
				continue;

			if ((kernel[m]->flags & TileFlags::MOUNTAIN) != 0)
				neightbours |= (1 << m);

			if ((kernel[m]->flags & TileFlags::WATER) != 0)
				neightbours |= (1 << (9 + m));
		}
	}
}

void World::GenerateVertexPositions()
{
	for (int i = 0; i < tileCount; i++)
	{
		// remap to elevation array
		int xPos = i % width;
		int yPos = i / width;

		int elevationIndex = xPos + yPos * (width + 1);

		Assert(elevationIndex < vertexCount);

		tileVertices[elevationIndex].position = vec3(yPos * WORLD_TILE_SCALE, xPos * WORLD_TILE_SCALE, WORLD_POS_Z);

		if ((tileNeighbours[i]
			& (TN_MOUNTAIN_UPPER | TN_MOUNTAIN_UPPER_LEFT | TN_MOUNTAIN_LEFT | TN_MOUNTAIN)) != 0)
				tileVertices[elevationIndex].position.z() += GetMountainOffset(i, 0);
		else if ((tileNeighbours[i]
			& (TN_WATER_UPPER | TN_WATER_UPPER_LEFT | TN_WATER_LEFT | TN_WATER)) != 0)
				tileVertices[elevationIndex].position.z() += GetWaterOffset(i, 0);
	}

	for (int i = 0; i < width; i++)
	{
		int elevationIndex = (width + 1) * height + i;
		int tileIndex = tileCount - width + i;

		int xPos = tileIndex % width;
		int yPos = tileIndex / width + 1;

		Assert(elevationIndex < vertexCount);

		tileVertices[elevationIndex].position = vec3(yPos * WORLD_TILE_SCALE, xPos * WORLD_TILE_SCALE, WORLD_POS_Z);

		if ((tileNeighbours[tileIndex]
			& (/*TN_MOUNTAIN_UPPER | TN_MOUNTAIN_UPPER_LEFT |*/ TN_MOUNTAIN_LEFT | TN_MOUNTAIN)) != 0)
				tileVertices[elevationIndex].position.z() += GetMountainOffset(tileIndex, 1);
		else if ((tileNeighbours[tileIndex]
			& (/*TN_WATER_UPPER | TN_WATER_UPPER_LEFT |*/ TN_WATER_LEFT | TN_WATER)) != 0)
				tileVertices[elevationIndex].position.z() += GetWaterOffset(tileIndex, 1);
	}

	for (int i = 0; i < height; i++)
	{
		int elevationIndex = width + (width + 1) * i;
		int tileIndex = width - 1 + width * i;

		int xPos = tileIndex % width + 1;
		int yPos = tileIndex / width;

		Assert(tileIndex < tileCount);
		Assert(elevationIndex < vertexCount);

		tileVertices[elevationIndex].position = vec3(yPos * WORLD_TILE_SCALE, xPos * WORLD_TILE_SCALE, WORLD_POS_Z);

		if ((tileNeighbours[tileIndex]
			& (TN_MOUNTAIN_UPPER /*| TN_MOUNTAIN_UPPER_LEFT | TN_MOUNTAIN_LEFT*/ | TN_MOUNTAIN)) != 0)
				tileVertices[elevationIndex].position.z() += GetMountainOffset(tileIndex, 3);
		else if ((tileNeighbours[tileIndex]
			& (TN_WATER_UPPER/* | TN_WATER_UPPER_LEFT | TN_WATER_LEFT*/ | TN_WATER)) != 0)
				tileVertices[elevationIndex].position.z() += GetWaterOffset(tileIndex, 3);
	}

	tileVertices[vertexCount - 1].position = vec3(height * WORLD_TILE_SCALE, width * WORLD_TILE_SCALE, WORLD_POS_Z);

	if ((tileNeighbours[tileCount - 1] & TN_MOUNTAIN) != 0)
		tileVertices[vertexCount - 1].position.z() += GetMountainOffset(tileCount - 1, 2);
	else if ((tileNeighbours[tileCount - 1] & TN_WATER) != 0)
		tileVertices[vertexCount - 1].position.z() += GetWaterOffset(tileCount - 1, 2);

	if (Engine::GetInstance()->IsInitializationMode(Engine::INIT_TOOLSMODE))
	{
		for (int i = 0; i < vertexCount; i++)
		{
			tileVertices[i].position.z() = 0.0f;
		}
	}
}

float World::GetMountainOffset(int tile, int corner)
{
	switch (corner)
	{
	case 1:
		tile += width;
		break;
	case 2:
		tile += width + 1;
		break;
	case 3:
		tile += 1;
		break;
	}

	return 35.0f + fmodf((tile * 1234.0f), 2.1f) / 2.1f * 40.0f;
}

float World::GetWaterOffset(int tile, int corner)
{
	return -60;
}

void World::GetElevationPoints(int tile, vec4 &offsets) const
{
	// remap to elevation array
	const int base = tile % width
		+ (tile / width) * (width + 1);

	offsets.x() = tileVertices[base].position.z();
	offsets.y() = tileVertices[base + width + 1].position.z();
	offsets.z() = tileVertices[base + width + 2].position.z();
	offsets.w() = tileVertices[base + 1].position.z();
}

int World::GetTileIndexAtPosition(const vec3 &worldPosition) const
{
	const float tileSize = WORLD_TILE_SCALE;

	if (worldPosition.x() < 0.0f
		|| worldPosition.y() < 0.0f)
	{
		return -1;
	}

	int tileIndex = int(worldPosition.y() / tileSize);

	if (tileIndex >= width)
	{
		return -1;
	}

	tileIndex += int(worldPosition.x() / tileSize) * width;

	if (tileIndex >= width * height)
	{
		return -1;
	}

	return tileIndex;
}

bool World::GetTileIndexAtPosition(const vec3 &worldPosition, int &index_x, int &index_y) const
{
	if (worldPosition.x() < 0.0f
		|| worldPosition.y() < 0.0f)
	{
		return false;
	}

	const float tileSize = WORLD_TILE_SCALE;
	index_x = int(worldPosition.y() / tileSize);
	index_y = int(worldPosition.x() / tileSize);

	if (index_x >= width
		|| index_y >= height)
	{
		return false;
	}

	return true;
}

vec3 World::GetTilePosition(int index) const
{
	return vec3(index / width * WORLD_TILE_SCALE,
		index % width * WORLD_TILE_SCALE,
		WORLD_POS_Z);
}

void World::GetElevationHeights(int index, vec4 &heights, bool ignoreWater) const
{
	Assert(index >= 0 && index < tileCount);

	GetElevationPoints(index, heights);

	if (ignoreWater)
	{
		for (int i = 0; i < 4; i++)
		{
			heights[i] = MAX(0.0f, heights[i]);
		}
	}
}

float World::GetElevationHeight(const vec3 &worldPosition, bool ignoreWater) const
{
	if (worldPosition.x() < 0.0f || worldPosition.y() < 0.0f)
	{
		return 0.0f;
	}

	const float tileSize = WORLD_TILE_SCALE;
	const int index_x = int(worldPosition.y() / tileSize);
	const int index_y = int(worldPosition.x() / tileSize);

	int indexTile = index_x + index_y * width;

	if (indexTile < 0
		|| indexTile >= tileCount
		|| index_x >= width)
	{
		return 0.0f;
	}

	if (ignoreWater
		&& (tileNeighbours[indexTile] & TN_MOUNTAIN_MASK) == 0)
	{
		return 0.0f;
	}

	// bilinear interp
	float weights[2] = {
		(worldPosition.y() - (index_x * tileSize)) / tileSize,
		(worldPosition.x() - (index_y * tileSize)) / tileSize,
	};

	const int elevationIndex = index_x + index_y * (width + 1);

	Assert(weights[0] >= 0.0f && weights[0] <= 1.0f);
	Assert(weights[1] >= 0.0f && weights[1] <= 1.0f);

	Assert(elevationIndex < vertexCount - width - 2);

	float heightTop = Lerp(weights[0], tileVertices[elevationIndex].position.z(),
		tileVertices[elevationIndex + 1].position.z());
	float heightLow = Lerp(weights[0], tileVertices[elevationIndex + width + 1].position.z(),
		tileVertices[elevationIndex + width + 2].position.z());

	return Lerp(weights[1], heightTop, heightLow);
}

void World::Init()
{
	// create rigid bodies for non-walkable tiles

	// extends along both directions in box2d
	vec3 size(WORLD_TILE_SCALE * 0.5f, WORLD_TILE_SCALE * 0.5f, 0.0f);

	for (int i = 0; i < tileCount; i++)
	{
		const WorldTile &tile = tiles[i];

		if ((tile.flags & TileFlags::WALKABLE) == 0)
		{
			vec3 position(tile.y * WORLD_TILE_SCALE, tile.x * WORLD_TILE_SCALE, 0.0f);

			PhysicsBody rect = physics->CreateRigidRect(position + size, size);

			solidTiles.push_back(rect);
		}
	}
}

void World::Shutdown()
{
	for (auto s : solidTiles)
	{
		physics->DestroyObject(s);
	}

	if (material != nullptr)
	{
		material->DecrementReferenceCount();
		material = nullptr;
	}

	if (waterMaterial != nullptr)
	{
		waterMaterial->DecrementReferenceCount();
		waterMaterial = nullptr;
	}
}

inline mat4 orthographicWorld(float left, float top, float right, float bottom, float zNear, float zFar)
{
	mat4 matProj = mat4::Identity();

	matProj(0, 0) = -1.0f / (zFar - zNear);
	matProj(1, 1) = -2.0f / (right - left);
	matProj(2, 2) = -2.0f / (top - bottom);
	matProj(0, 3) = 0; //(zFar + zNear) / (zFar - zNear);
	matProj(1, 3) = -(right + left) / (right - left);
	matProj(2, 3) = -(top + bottom) / (top - bottom);

	mat4 screenMatrix = translate(mat4::Identity(), vec3(0.0f, 0.5f, 0.50f));

	screenMatrix = scale(screenMatrix, 1.0f, 0.5f, -0.5f);

	return screenMatrix * matProj;
}

inline bool IntersectTrianglePointBarycentric(const vec3 &v0, const vec3 &v1, const vec3 &v2, const vec3 &point,
											  float &u, float &v)
{
	vec3 delta10 = v1 - v0;
	vec3 delta20 = v2 - v0;
	vec3 deltaPoint = point - v0;

	u = deltaPoint.dot(delta10);
	v = deltaPoint.dot(delta20);

	return u >= 0.0f && u <= 1.0f
		&& v >= 0.0f && v <= 1.0f;
}

struct DecalVertex
{
	vec3 position;
	vec2 uv;
};

inline void ClipPolygonPlane(const Plane &plane, std::vector<DecalVertex> &vertices)
{
	std::vector<DecalVertex> verticesOut;

	for (unsigned int i = 0; i < vertices.size(); i++)
	{
		const int endIndex = (i < vertices.size() - 1) ? i + 1 : 0;
		const vec3 &start = vertices[i].position;
		const vec3 &end = vertices[endIndex].position;

		Ray ray;
		ray.InitFromPoints(start, end);

		vec3 point;
		float dist;

		bool startInfront = plane.normal.dot(start) - plane.distance <= 0.0f;

		if (IntersectRayWithPlane(ray, plane, &point, &dist)
			&& dist <= 1.0f)
		{
			if (startInfront)
				verticesOut.push_back(vertices[i]);

			DecalVertex vertex;
			vertex.position = point;
			vertex.uv = (vertices[endIndex].uv - vertices[i].uv) * dist + vertices[i].uv;
			verticesOut.push_back(vertex);

			if (!startInfront)
				verticesOut.push_back(vertices[endIndex]);
		}
		else if (startInfront)
		{
			verticesOut.push_back(vertices[i]);
		}
	}

	vertices = std::move(verticesOut);
}

inline void ClipDecalTriangle(const DecalVertex &v0, const DecalVertex &v1, const DecalVertex &v2,
							  std::vector<DecalVertex> &out)
{
	Plane planes[4];
	planes[0].normal = vec3(0, -1, 0);
	planes[1].normal = vec3(0, 0, -1);
	planes[2].normal = vec3(0, 1, 0);
	planes[3].normal = vec3(0, 0, 1);

	planes[2].distance = 1;
	planes[3].distance = 1;

	std::vector<DecalVertex> tri;
	tri.push_back(v0);
	tri.push_back(v1);
	tri.push_back(v2);

	for (int i = 0; i < 4; i++)
		ClipPolygonPlane(planes[i], tri);

	if (tri.size() < 3)
		return;

	for (unsigned int i = 0; i < 3; i++)
		out.push_back(tri[i]);

	//if (int(tri.size()) >= 3)
	//{
	//	vec3 baseNormal = (tri[1].position - tri[0].position).cross(tri[2].position - tri[0].position);

		for (unsigned int i = 3; i < tri.size(); i++)
		{
	//		vec3 testNormal = (tri[i].position - tri[0].position).cross(tri[i - 1].position - tri[0].position);

			//if (testNormal.dot(baseNormal) >= 0.0f)
			//{
				//out.push_back(tri[0]);
				//out.push_back(tri[i]);
				//out.push_back(tri[i - 1]);
			//}
			//else
			//{
				out.push_back(tri[0]);
				out.push_back(tri[i - 1]);
				out.push_back(tri[i]);
			//}
		}
	//}
}

IMesh *World::CreateProjectedDecal(const vec3 &origin, const euler &angles, const vec3 &size, bool ignoreWater)
{
	mat4 decalSpace(mat4::Identity());
	vec3 fwd, left, up;

	decalSpace = rotate(decalSpace, angles);

	AngleVectors(angles, &fwd, &left, &up);
	decalSpace = translate(decalSpace, -origin);

	decalSpace = orthographicWorld(-size.y(), size.z(), size.y(), -size.z(), 0.0f, size.x()) * decalSpace;

	mat4 decalSpaceInverse = decalSpace.inverse().eval();

	vec3 mins = origin;
	vec3 maxs = origin;

	//DebugDraw::GetInstance()->DrawLine(origin, origin + fwd * 200, vec3(1,0,0),1, -1, true);
	//DebugDraw::GetInstance()->DrawLine(origin, origin + left * 200, vec3(0,1,0),1, -1, true);
	//DebugDraw::GetInstance()->DrawLine(origin, origin + up * 200, vec3(0,0,1),1, -1, true);

	vec3 bounds[8] = {
		origin + fwd * size.x() + left * size.y() + up * size.z(),
		origin + fwd * size.x() + left * size.y() - up * size.z(),
		origin + fwd * size.x() - left * size.y() + up * size.z(),
		origin + fwd * size.x() - left * size.y() - up * size.z(),
		origin + left * size.y() + up * size.z(),
		origin + left * size.y() - up * size.z(),
		origin - left * size.y() + up * size.z(),
		origin - left * size.y() - up * size.z(),
	};

	for (int i = 0; i < 3; i++)
	{
		for (int x = 0; x < 8; x++)
		{
			mins[i] = MIN(mins[i], bounds[x][i]);
			maxs[i] = MAX(maxs[i], bounds[x][i]);
		}
	}

	//DebugDraw::GetInstance()->DrawBox(mins, maxs, vec3(1,0,1), 1, -1, true);

	int tileIndexMinX = int(mins.y() / WORLD_TILE_SCALE);
	int tileIndexMinY = int(mins.x() / WORLD_TILE_SCALE);
	int tileIndexMaxX = int(maxs.y() / WORLD_TILE_SCALE);
	int tileIndexMaxY = int(maxs.x() / WORLD_TILE_SCALE);

	int projectedX = tileIndexMaxX - tileIndexMinX + 1;
	int projectedY = tileIndexMaxY - tileIndexMinY + 1;

	std::vector<DecalVertex> vertices;

	for (int x = 0; x < projectedX; x++)
	{
		const int index_x = tileIndexMinX + x;

		if (index_x < 0 || index_x >= width)
			continue;

		for (int y = 0; y < projectedY; y++)
		{
			const int index_y = tileIndexMinY + y;

			if (index_y < 0 || index_y >= height)
				continue;

			int index = GetTileVertexIndex(index_x, index_y);

			// TODO: use real normal
			const vec3 normal = vec3(0, 0, 1.5f);

			DecalVertex v[4];

			v[0].position = tileVertices[index].position + normal;
			v[1].position = tileVertices[index + width + 1].position + normal;
			v[2].position = tileVertices[index + 1].position + normal;
			v[3].position = tileVertices[index + width + 2].position + normal;

			for (int i = 0; i < 4; i++)
			{
				vec4 projected = (decalSpace * vec4(XYZ(v[i].position), 1.0f));
				v[i].position = vec3(XYZ(projected));
				//v[i].uv = vec2(XY(v[i].position));
				v[i].uv.x() = v[i].position.y();
				v[i].uv.y() = 1.0f - v[i].position.z();
			}

			ClipDecalTriangle(v[0], v[1], v[2], vertices);
			ClipDecalTriangle(v[2], v[1], v[3], vertices);
		}
	}

	for (auto &itr : vertices)
	{
		vec4 pos = decalSpaceInverse * vec4(XYZ(itr.position), 1.0f);
		itr.position = vec3(XYZ(pos));

		if (ignoreWater)
		{
			itr.position.z() = MAX(WORLD_POS_Z, itr.position.z());
		}
	}

	if (vertices.empty())
	{
		return nullptr;
	}

	//for (unsigned int i = 0; i < vertices.size(); i += 3)
	//{
	//	const float lifetime = -1.0f;

	//	DebugDraw::GetInstance()->DrawLine(vertices[i].position, vertices[i + 1].position,
	//		vec3(vertices[i].uv.x(), vertices[i].uv.y(), 0), 1, lifetime, true);
	//	DebugDraw::GetInstance()->DrawLine(vertices[i + 2].position, vertices[i + 1].position,
	//		vec3(vertices[i + 2].uv.x(), vertices[i + 2].uv.y(), 0), 1, lifetime, true);
	//	DebugDraw::GetInstance()->DrawLine(vertices[i].position, vertices[i + 2].position,
	//		vec3(vertices[i].uv.x(), vertices[i].uv.y(), 0), 1, lifetime, true);
	//}

	IMesh *mesh = renderContext->CreateStaticMesh(FaceTypes::TRIANGLES,
		VertexFormat::POSITION_3F | VertexFormat::TEXTURE_COORD_2F | VertexFormat::NORMAL_3F,
		vertices.size() / 3);

	MeshBuilder builder;
	builder.Attach(mesh);

	for (auto itr : vertices)
	{
		builder.Position3v(itr.position);
		builder.Texcoord2f(itr.uv.x(), itr.uv.y());
		builder.AdvanceVertex();
	}

	builder.GenerateNormals();
	builder.SmoothNormals();

	builder.Detach();
	return mesh;
}
