#include "pch.h"
#include "globals.h"

Globals::Globals()
	: frametime(0.0f)
	, framecount(0)
	, time(0.0)
	, timeFloat(0.0f)
{
}

float Globals::GetFrametime()
{
	return frametime;
}

int Globals::GetFrameCount()
{
	return framecount;
}

float Globals::GetTime()
{
	return timeFloat;
}

double Globals::GetTimeHighPrecision()
{
	return time;
}


void Globals::Advance(float frametime)
{
	this->frametime = frametime;

	framecount++;
	time += frametime;
	timeFloat = float(time);
}

void Globals::Reset()
{
	//frametime = 0.0f;
	framecount = 0;
	time = 0.0;
	timeFloat = 0.0f;
}

void Globals::SetFrametime(float frametime)
{
	this->frametime = frametime;
}

void Globals::SetFrameCount(int framecount)
{
	this->framecount = framecount;
}

void Globals::SetTime(double time)
{
	this->time = time;
}