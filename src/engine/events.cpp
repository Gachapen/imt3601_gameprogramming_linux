
#include "pch.h"
#include "events.h"

SINGLETON_INSTANCE(Events);

EXPOSE_APP(INTERFACE_IEVENTS_VERSION, Events::GetInstance());

Events::Events()
{
}

Events::~Events()
{
}

void Events::AddListener(const char *eventName, ICallbackConcrete<KeyValues> *listener)
{
	auto &list = listeners[eventName];

	list.insert(listener);
}

void Events::RemoveListener(ICallbackConcrete<KeyValues> *listener)
{
	for (auto &itr : listeners)
	{
		itr.second.erase(listener);
	}
}

void Events::FireEvent(KeyValues *eventData)
{
	Assert(*eventData->GetName());

	auto &list = listeners[eventData->GetName()];

	for (auto listener : list)
	{
		listener->Run(eventData);
	}

	eventData->Release();
}