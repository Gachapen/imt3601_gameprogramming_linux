#ifndef GLOBALSFIXEDINTERVAL_H
#define GLOBALSFIXEDINTERVAL_H

#include "engine\iglobals.h"

/// Global time/frame information for fixed frame speed simulations.
/// Might be instantiated multiple times for different modules or contexts
class GlobalsFixedInterval : public IGlobals
{
public:
	GlobalsFixedInterval();

	void SetFixedInterval(float interval);

	// IGlobals
	/// Get time passed for last frame
	virtual float GetFrametime();
	virtual int GetFrameCount();
	virtual float GetTime();
	virtual double GetTimeHighPrecision();

	/// Advance time by specific real interval
	/// @return simulation steps processed
	int AdvanceTime(float frametime);
	/// Advance single step
	void AdvanceStep();
	/// Reset all time information
	void Reset();

	void SetFrametime(float frametime);
	void SetFrameCount(int framecount);
	void SetTime(double time);

private:
	float intervaltime;
	int framecount;

	double simulatedTime;
	float simulatedTimeFloat;
	double currentTime;
};

#endif