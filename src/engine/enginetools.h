#ifndef ENGINETOOLS_H
#define ENGINETOOLS_H

#include "engine\ienginetools.h"

class EngineTools : public IEngineTools, public SingletonLazy<EngineTools>
{
	DECLARE_SINGLETON_LAZY(EngineTools);
public:

	void Init();
	void Shutdown();

	virtual void RequestEngineQuit();
	virtual void LoadMap(const char *filename);

	virtual bool IntersectScreenRayWithWorld(int screenx, int screeny, int &tileXOut, int &tileYOut);
	virtual void ChangeTileUVs(int tilex, int tiley, float uvs, float uvt);
	virtual void ReplaceWorldTextureAtlas(void *rgbData, void *rgbDataNormal, int width, int height);

	virtual void LockEngine(bool switchContext);
	virtual void UnlockEngine();

	virtual void SetPaintHighlight(bool visible, bool isCorner);
	virtual void SetFlagHighlight(TileFlagId flag);
	virtual void SetTileFlags(int x, int y, bool set);

	virtual void OnKeyDown(SDL_Keycode sdlKey);
	virtual void OnKeyUp(SDL_Keycode sdlKey);

	void Update(float frametime);

	void Render();

private:

	void UpdateTileHighlight(int screenx, int screeny);

	bool wasContextSwitched;

	IMaterial *flatMaterial;
	IMesh *mouseOverlay;
	IMesh *flagOverlay;
	TileFlagId activeEditMode;
	IMesh *editModeOverlays[TileFlags::COUNT];

	bool isHighlightVisible;
	bool isHighlightingCorner;

	virtual IMesh* GenerateFlagHighlightMesh(TileFlagId flag);
};

#endif