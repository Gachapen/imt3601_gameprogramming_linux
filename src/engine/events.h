#ifndef EVENTS_H
#define EVENTS_H

#include "engine/ievents.h"

class Events : public IEvents
{
	DECLARE_SINGLETON(Events);
public:
	~Events();

	virtual void AddListener(const char *eventName, ICallbackConcrete<KeyValues> *listener);
	virtual void RemoveListener(ICallbackConcrete<KeyValues> *listener);

	virtual void FireEvent(KeyValues *eventData);

private:
	std::unordered_map<std::string, std::unordered_set<ICallbackConcrete<KeyValues>*>> listeners;

};

#endif