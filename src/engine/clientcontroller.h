#ifndef CLIENTCONTROLLER_H
#define CLIENTCONTROLLER_H

#include "networking\enums.h"

class INetworkClient;

class ClientController
{
	DECLARE_SINGLETON(ClientController);

public:
	~ClientController();

	bool IsInState(ClientStateId state);

	void ShowLoadingScreen();
	void HideLoadingScreen();

	void ConnectToServer(const NetworkAddress &address);
	void Disconnect();

	void SendClientCommand(const char *command);

	bool HandlePackage(const NetworkAddress &source, Uint8 type, ByteBuffer &buffer);

	void Simulate();

	/// Takes ownership
	void SendMessageToServer(Byte *data, int sizeInBytes);

	/// Takes ownership, use dynamic buffer
	void SendMessageToServer(ByteBuffer &buffer);

	Message *PollMessage();

	void ShutdownClient();

	// For interaction with network client
	bool ConnectToNetworkServer(const char* host, unsigned short port, bool block = true, size_t blockTimeout = 2000);
	bool ConnectToNetworkServer(NetworkAddress host, bool block = true, size_t blockTimeout = 2000);
	void DisconnectFromServer();

	ConnectionStatus GetConnectionStatus();
	void DispatchOutgoingMessage(ByteBuffer &data, bool reliable = false);
	Byte *PollIncomingMessages(Uint32 &sizeOutput);
	

private:

	void ChangeState(ClientStateId state);

	void HandleConnectionAttemptResponse(ByteBuffer &buffer);
	void HandleConnectionPrecacheResponse(ByteBuffer &buffer);
	void HandleConnectionFinishedResponse(ByteBuffer &buffer);
	void HandleEntityConstruction(ByteBuffer &buffer);
	void HandleWorldStateUpdate(ByteBuffer &buffer);

	ClientStateId state;
	NetworkAddress connectedServer;
	INetworkClient *networkClient;

	std::vector<Message *> messages;

	float pingTimer;
};

#endif