#include <SDL.h>

#include "materialsystem\materialsystem.h"
#include "materialsystem\irendercontext.h"
#include "util\stringutil.h"
#include "appinterface\iappinterface.h"
#include "materialsystem\irendercontext.h"
#include "materialsystem\imaterialdict.h"
#include "materialsystem\imaterial.h"
#include "materialsystem\itexturedict.h"
#include "materialsystem\itexture.h"
#include "materialsystem\imdxrenderabledict.h"
#include "materialsystem\ifbodict.h"
#include "materialsystem\imesh.h"
#include "materialsystem\meshbuilder.h"
#include "mdxlib\imdxmodeldict.h"
#include "util/util.h"
#include "util/mathutil.h"
#include "shadershared.h"

static IRenderContext *renderContext;
static IMaterialDict *materialDict;
static ITextureDict *textureDict;
static IMDXModelDict *modelDict;
static IMDXRenderableDict *renderableDict;
static IFBODict *fboDict;


#ifdef DEBUG
#define FRENZYMODE 0
#else
#define FRENZYMODE 1 // draw lotsa footmen
#endif

#if FRENZYMODE
const int shadowMapResolution = 1024;
const float shadowMapOrthoSize = 1000;
const int footmenCount = 100;
#else
const int shadowMapResolution = 256;
const float shadowMapOrthoSize = 100;
const int footmenCount = 1;
#endif

FBO_HANDLE fboGBuffer;
FBO_HANDLE lightBuffer;
FBO_HANDLE shadowmapBuffer;
FBO_HANDLE frameBufferCopy;
FBO_HANDLE frameBufferQuarter;
FBO_HANDLE frameBufferQuarter2;

IMesh *world;

void Init()
{
	const int rtFlags = TextureFlags::CLAMP_S | TextureFlags::CLAMP_T | TextureFlags::NO_MIPMAP | TextureFlags::NO_FILTERING;

	fboGBuffer = fboDict->CreateFBO();
	RT_HANDLE rt0 = fboDict->CreateRenderTarget("__framebuffer", TextureFormats::RGB_8, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);
	RT_HANDLE rt1 = fboDict->CreateRenderTarget("__framebuffer_1", TextureFormats::RGB_8, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);
	RT_HANDLE rt2 = fboDict->CreateRenderTarget("__framebuffer_depth", TextureFormats::DEPTH_16F, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);

	fboDict->PushFBO(fboGBuffer);
	fboDict->BindRenderTargetColor(rt0, 0);
	fboDict->BindRenderTargetColor(rt1, 1);
	fboDict->BindRenderTargetDepth(rt2);
	fboDict->PopFBO();


	frameBufferCopy = fboDict->CreateFBO();
	RT_HANDLE frameBufferCopyRt0 = fboDict->CreateRenderTarget("__framebuffer_copy", TextureFormats::RGB_8, FramebufferResizeModes::FULL_FRAMEBUFFER);

	fboDict->PushFBO(frameBufferCopy);
	fboDict->BindRenderTargetColor(frameBufferCopyRt0, 0);
	fboDict->PopFBO();


	frameBufferQuarter = fboDict->CreateFBO();
	RT_HANDLE frameBufferQuarterRt0 = fboDict->CreateRenderTarget("__framebuffer_quarter", TextureFormats::RGB_8, FramebufferResizeModes::QUARTER_FRAMEBUFFER);

	fboDict->PushFBO(frameBufferQuarter);
	fboDict->BindRenderTargetColor(frameBufferQuarterRt0, 0);
	fboDict->PopFBO();


	frameBufferQuarter2 = fboDict->CreateFBO();
	RT_HANDLE frameBufferQuarter2Rt0 = fboDict->CreateRenderTarget("__framebuffer_quarter_2", TextureFormats::RGB_8, FramebufferResizeModes::QUARTER_FRAMEBUFFER);

	fboDict->PushFBO(frameBufferQuarter2);
	fboDict->BindRenderTargetColor(frameBufferQuarter2Rt0, 0);
	fboDict->PopFBO();


	lightBuffer = fboDict->CreateFBO();
	RT_HANDLE lightRt0 = fboDict->CreateRenderTarget("__lightbuffer", TextureFormats::RGB_16F, FramebufferResizeModes::FULL_FRAMEBUFFER, -1, -1, rtFlags);

	fboDict->PushFBO(lightBuffer);
	fboDict->BindRenderTargetColor(lightRt0, 0);
	fboDict->PopFBO();


	shadowmapBuffer = fboDict->CreateFBO();
	RT_HANDLE shadowmapRt0 = fboDict->CreateRenderTarget("__shadowmap_global", TextureFormats::DEPTH_16F, FramebufferResizeModes::OFFSCREEN, shadowMapResolution, shadowMapResolution, rtFlags);
	
	fboDict->PushFBO(shadowmapBuffer);
	fboDict->BindRenderTargetDepth(shadowmapRt0);
	fboDict->PopFBO();


	world = renderContext->CreateStaticMesh(FaceTypes::TRIANGLE_STRIP,
		VertexFormat::POSITION_3F | VertexFormat::TEXTURE_COORD_2F |
		VertexFormat::NORMAL_3F | VertexFormat::BINORMAL_3F | VertexFormat::TANGENT_3F,
		2 );

	MeshBuilder builder;
	builder.Attach(world);

	vec3 tangent = vec3(0,-1,0);
	vec3 binormal = vec3(-1,0,0);
	const float textureScale = 25;
	const float worldSize = 2000;
	const float worldZ = 2.0f;

	builder.Position3f( worldSize, worldSize, worldZ );
	builder.Texcoord2f( 0, 0 );
	builder.Normal3f(0, 0, 1);
	builder.Tangent3p(tangent.data());
	builder.Binormal3p(binormal.data());
	builder.AdvanceVertex();

	builder.Position3f( -worldSize, worldSize, worldZ );
	builder.Texcoord2f( 0, textureScale );
	builder.Normal3f(0, 0, 1);
	builder.Tangent3p(tangent.data());
	builder.Binormal3p(binormal.data());
	builder.AdvanceVertex();

	builder.Position3f( worldSize, -worldSize, worldZ );
	builder.Texcoord2f( textureScale, 0 );
	builder.Normal3f(0, 0, 1);
	builder.Tangent3p(tangent.data());
	builder.Binormal3p(binormal.data());
	builder.AdvanceVertex();

	builder.Position3f( -worldSize, -worldSize, worldZ );
	builder.Texcoord2f( textureScale, textureScale );
	builder.Normal3f(0, 0, 1);
	builder.Tangent3p(tangent.data());
	builder.Binormal3p(binormal.data());
	builder.AdvanceVertex();

	builder.Detach();
}

void Shutdown()
{
	world->Release();
}

static IMDXRenderable *renderables[footmenCount];

void CreateRenderables()
{
	for (int i = 0; i < footmenCount; i++)
	{
		renderables[i] = renderableDict->CreateMDXRenderableInstance("units/footman.mdx");
	}
}

void DestroyRenderables()
{
	for (int i = 0; i < footmenCount; i++)
	{
		renderables[i]->Release();
	}
}

void SetRenderablesSequence(const char *name)
{
	for (int i = 0; i < footmenCount; i++)
	{
		renderables[i]->SetSequence(name);

		if (i > 0)
			renderables[i]->SetCycle(randf());
	}
}

void UpdateRenderables(float frametime)
{
	if (frametime <= 0.0f)
	{
		for (int i = 0; i < footmenCount; i++)
		{
			renderables[i]->InvalidateBoneCache();
		}
	}
	else
	{
		for (int i = 0; i < footmenCount; i++)
		{
			renderables[i]->AdvanceFrame(frametime);
		}
	}
}

void DrawScene(IMaterial *worldMaterial)
{
	renderContext->BindMesh(world);
	worldMaterial->Draw();

	renderContext->SetRenderParamVec3(RenderParams::VEC3_MODEL_TINT_COLOR, vec3(1.0f, 0.05f, 0.0f));

#if FRENZYMODE
	// draw a bunch of footmen with different xforms and cycles
	srand(5000);

	for (int i = 0; i < footmenCount; i++)
	{
		vec3 pos;
		pos[2] = 0;
		pos[0] = randf() * 1000 - 500;
		pos[1] = randf() * 1000 - 500;

		float s = randf() + 1.0f;

		mat4 model = mat4::Identity();
		model = scale(model, s,s,s);
		model = translate(model, pos);
		model = rotate(model, randf() * M_PI_F, vec3(0,0,1));

		renderContext->PushModelMatrix(model);
		//renderables[i]->InvalidateBoneCache();
		renderables[i]->Draw();
		renderContext->PopModelMatrix();
	}
#else
	// draw a single footman
	const float size = 1.0f;
	mat4 model = mat4::Identity();
	model = scale(model, size, size, size);

	renderContext->PushModelMatrix(model);

	//renderables[0]->InvalidateBoneCache();
	renderables[0]->Draw();

	renderContext->PopModelMatrix();
#endif
}

#ifdef PLATFORM_WIN32
int CALLBACK WinMain(_In_ HINSTANCE hInstance, _In_ HINSTANCE hPrevInstance,
					 _In_ LPSTR lpCmdLine, _In_ int nCmdShow)
#else
#error
#endif
{
	char dir[MAX_PATH_GAME];
	GetCWD(dir, sizeof(dir));
	G_StrStripDirectory(dir, "bin");

	_chdir(dir);

	SDL_Init(SDL_INIT_EVERYTHING);

	int windowWidth = 640;
	int windowHeight = 480;
	SDL_Window *window = SDL_CreateWindow("Materialsystem Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
										  windowWidth, windowHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

	// all shared libraries we'll use
	GetAppInterface()->LoadSharedLibrary("mdxlib");
	GetAppInterface()->LoadSharedLibrary("materialsystem");

	// pull interfaces from those libraries
	renderContext = (IRenderContext *)GetAppInterface()->QueryApp(INTERFACE_IRENDERCONTEXT_VERSION);
	materialDict = (IMaterialDict *)GetAppInterface()->QueryApp(INTERFACE_IMATERIALDICT_VERSION);
	textureDict = (ITextureDict *)GetAppInterface()->QueryApp(INTERFACE_ITEXTUREDICT_VERSION);
	modelDict = (IMDXModelDict *)GetAppInterface()->QueryApp(INTERFACE_IMDXMODELDICT_VERSION);
	renderableDict = (IMDXRenderableDict*)GetAppInterface()->QueryApp(INTERFACE_IMDXRENDERABLEDICT_VERSION);
	fboDict = (IFBODict*)GetAppInterface()->QueryApp(INTERFACE_IFBODICT_VERSION);

	Assert(renderContext);
	Assert(materialDict);
	Assert(textureDict);
	Assert(modelDict);
	Assert(renderableDict);
	Assert(fboDict);

	renderContext->Init(window, windowWidth, windowHeight);

	Init();

	// load the model
	CreateRenderables();

	// load output post processing materials
	IMaterial *debugGbuffer[] = {
		materialDict->FindMaterial("postprocessing/bloom_combine"),
		materialDict->FindMaterial("postprocessing/light_combine"),
		materialDict->FindMaterial("postprocessing/debug_bloom"),
		materialDict->FindMaterial("postprocessing/debug_gbuffer_albedo"),
		materialDict->FindMaterial("postprocessing/debug_gbuffer_normals"),
		materialDict->FindMaterial("postprocessing/debug_gbuffer_depth"),
		materialDict->FindMaterial("postprocessing/debug_lightbuffer"),
		materialDict->FindMaterial("postprocessing/debug_shadowmap"),
	};
	const int debugGbufferCount = ARRAYSIZE(debugGbuffer);
	int activeGbuffer = 0;

	// load world material
	IMaterial *worldMaterial = materialDict->FindMaterial("ground/concretetest");

	// load light material for testing
	IMaterial *lightPointWorld = materialDict->FindMaterial("postprocessing/light_point_world");

	IMaterial *downsample4 = materialDict->FindMaterial("postprocessing/downsample4");
	IMaterial *gaussblur13v = materialDict->FindMaterial("postprocessing/gaussblur13v");
	IMaterial *gaussblur13h = materialDict->FindMaterial("postprocessing/gaussblur13h");

	for ( auto m : debugGbuffer )
	{
		m->IncrementReferenceCount();
		Assert(!m->IsErrorMaterial());
	}
	worldMaterial->IncrementReferenceCount();
	lightPointWorld->IncrementReferenceCount();
	downsample4->IncrementReferenceCount();
	gaussblur13v->IncrementReferenceCount();
	gaussblur13h->IncrementReferenceCount();

	Assert(!worldMaterial->IsErrorMaterial());

	const char *sequenceNames[] = {
		"walk",
		"stand - 1",
		"stand - 4",
		"stand victory",
		"attack - 1",
		"Death",
		"Decay bone",
	};
	const int sequenceCount = ARRAYSIZE(sequenceNames);
	int activeSequence = 0;

	SetRenderablesSequence(sequenceNames[0]);

	SDL_Event event;
	bool isRunning = true;

	float ticksPerSec = (float)SDL_GetPerformanceFrequency();
	Uint64 ticksOld = SDL_GetPerformanceCounter();

	float frametime;
	float rotation_yaw = 0.0f;
	float rotation_pitch = 0.0f;
	int inputMode = 0;
	bool isAnimating = true;

	int mouseLastX = -1, mouseLastY = -1;

	vec3 directionalLightDir = vec3( -1, -1, -1 );
	directionalLightDir.normalize();

	mat4 view = mat4::Identity();
	mat4 shadow = mat4::Identity();

	while (isRunning)
	{
		{
			Uint64 ticksNow = SDL_GetPerformanceCounter();

			if (ticksNow == ticksOld)
			{
				SDL_Delay(1);
				continue;
			}

			frametime = (ticksNow - ticksOld) / ticksPerSec;
			ticksOld = ticksNow;
		}

		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				isRunning = false;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
					isRunning = false;
					break;
				case SDLK_r:
					materialDict->ReloadAllMaterials();
					break;
				case SDLK_g:
					activeGbuffer++;
					if (activeGbuffer >= debugGbufferCount)
						activeGbuffer = 0;
					break;
				case SDLK_SPACE:
					isAnimating = !isAnimating;
					break;
				case SDLK_RETURN:
					if ( (SDL_GetWindowFlags(window) & (SDL_WINDOW_FULLSCREEN|SDL_WINDOW_FULLSCREEN_DESKTOP)) != 0)
						SDL_SetWindowFullscreen(window, 0);
					else
						SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
					break;
				case SDLK_s:
					activeSequence++;
					if (activeSequence >= sequenceCount)
						activeSequence = 0;
					SetRenderablesSequence(sequenceNames[activeSequence]);
					break;
				}
				break;
			case SDL_WINDOWEVENT:
				switch (event.window.event)
				{
				case SDL_WINDOWEVENT_RESIZED:
					windowWidth = event.window.data1;
					windowHeight = event.window.data2;

					renderContext->Resize(windowWidth, windowHeight);
					break;
				}
				break;
			case SDL_MOUSEMOTION:
				{
					if (mouseLastY >= 0
						&& mouseLastX >= 0
						&& inputMode != 0)
					{
						float dx = float(event.motion.x - mouseLastX);
						float dy = float(event.motion.y - mouseLastY);

						switch (inputMode)
						{
						case 1:
							{
								rotation_yaw += dx;
								rotation_pitch -= dy;

								rotation_yaw = ClampAngle(rotation_yaw);
								rotation_pitch = ClampAngle(rotation_pitch);
							}
							break;
						case 2:
							{
								mat4 viewT = view.inverse().eval();
								mat3 rot = rotationMatrix(viewT);

								vec3 dirScreenX(0,1,0);
								vec3 dirScreenY(0,0,1);

								dirScreenX = rot * dirScreenX;
								dirScreenY = rot * dirScreenY;

								directionalLightDir = rotate(directionalLightDir, DEG2RAD(dx), dirScreenY);
								directionalLightDir = rotate(directionalLightDir, -DEG2RAD(dy), dirScreenX);
								directionalLightDir.normalize();
							}
							break;
						}
					}

					mouseLastX = event.motion.x;
					mouseLastY = event.motion.y;
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				{
					switch (event.button.button)
					{
					case SDL_BUTTON_LEFT:
						{
							if (inputMode == 0)
								inputMode = 1;
						}
						break;
					case SDL_BUTTON_RIGHT:
						{
							if (inputMode == 0)
								inputMode = 2;
						}
						break;
					}
				}
				break;
			case SDL_MOUSEBUTTONUP:
				{
					switch (event.button.button)
					{
					case SDL_BUTTON_LEFT:
						{
							if (inputMode == 1)
								inputMode = 0;
						}
						break;
					case SDL_BUTTON_RIGHT:
						{
							if (inputMode == 2)
								inputMode = 0;
						}
						break;
					}
				}
				break;
			}
		}

		if (isAnimating)
			UpdateRenderables(frametime);
		else
			UpdateRenderables(0.0f);

		view = mat4::Identity();
		shadow = mat4::Identity();

#if COMBINE_ORTHOGRAPHIC
		view = translate(view, vec3(-1000,0,0));
#else
#if FRENZYMODE
		view = translate(view, vec3(-500,0,0));
#else
		view = translate(view, vec3(-100,0,0));
#endif
#endif

		view = rotate(view, DEG2RAD(rotation_pitch), vec3(0,1,0));
		view = rotate(view, DEG2RAD(rotation_yaw), vec3(0,0,1));
		view = translate(view, vec3(0,0,40));

		shadow = translate(shadow, vec3(-500,0,0));
		shadow = rotate(shadow, quaternionFromVector(directionalLightDir));
		shadow = translate(shadow, vec3(0,0,40));

		// draw main scene
		fboDict->PushFBO(fboGBuffer);

		//renderContext->SetClearColor(0.17f, 0.18f, 0.2f, 1);
		renderContext->ClearBuffers(false, true);

		const float zFar = 2000.0f;
		const float ratio = windowWidth/float(windowHeight);
		mat4 projectionMatrix;

#if COMBINE_ORTHOGRAPHIC
		const float zNear = 0.0f;
		projectionMatrix = orthographic(-200 * ratio, 200, 200 * ratio, -200, zNear, zFar);
#else
		const float zNear = 0.1f;
		projectionMatrix = perspective(90.0f, ratio, zNear, zFar);
#endif

		renderContext->PushViewMatrix(view);
		renderContext->PushProjectionMatrix(projectionMatrix);
		renderContext->SetDepthRange(zNear, zFar);

		DrawScene(worldMaterial);

		fboDict->PopFBO();

		// draw lights
		fboDict->PushFBO(lightBuffer);

		renderContext->SetClearColor(0, 0, 0, 1);
		renderContext->ClearBuffers(true, false);

		renderContext->DrawFullscreenQuad(lightPointWorld);

		fboDict->PopFBO();

		renderContext->PopProjectionMatrix();
		renderContext->PopViewMatrix();

		// draw shadow map
		fboDict->PushFBO(shadowmapBuffer);
		renderContext->SetRenderStage(RenderStages::SHADOW);

		renderContext->PushViewport(0, 0, shadowMapResolution, shadowMapResolution);
		renderContext->PushProjectionMatrix(orthographic(-shadowMapOrthoSize, shadowMapOrthoSize,
			shadowMapOrthoSize, -shadowMapOrthoSize,
			0.0f, 2000.0f));
		renderContext->PushViewMatrix(shadow);

		renderContext->ClearBuffers(false, true);

		renderContext->UpdateMatrices();
		renderContext->SetRenderParamMat4(RenderParams::MAT4_SHADOW_VIEWPROJECTION, shadowRemapMatrix() * renderContext->GetOGLViewProjectionMatrix());
		renderContext->SetRenderParamFloat(RenderParams::FLOAT_SHADOWMAP_SIZE, (float)shadowMapResolution);
		renderContext->SetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_DIR, directionalLightDir);
		renderContext->SetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_COLOR, vec3(1.2f, 1.1f, 0.9f));
		renderContext->SetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_AMBIENT, vec3(0.08f, 0.12f, 0.14f));

		DrawScene(worldMaterial);

		renderContext->PopViewMatrix();
		renderContext->PopProjectionMatrix();
		renderContext->PopViewport();

		renderContext->SetRenderStage(RenderStages::NORMAL);
		fboDict->PopFBO();


		// compose scene
		// these matrices are used during worldspace reconstruction too
		fboDict->PushFBO(frameBufferCopy);

		renderContext->PushViewMatrix(view);
		renderContext->PushProjectionMatrix(projectionMatrix);

		renderContext->SetRenderParamFloat(RenderParams::FLOAT_FOG_AMOUNT, 1.0f);
		renderContext->SetRenderParamFloat(RenderParams::FLOAT_FOG_START, 100.0f);
		renderContext->SetRenderParamFloat(RenderParams::FLOAT_FOG_RANGE, 800.0f);
		renderContext->SetRenderParamVec3(RenderParams::VEC3_FOG_COLOR, vec3(0.039f, 0.039f, 0.04f));

		renderContext->DrawFullscreenQuad(debugGbuffer[1]);

		renderContext->PopViewMatrix();
		renderContext->PopProjectionMatrix();

		fboDict->PopFBO();

		// post processing
		renderContext->PushViewport(0,0,windowWidth/4,windowHeight/4);

		// downsample scene
		fboDict->PushFBO(frameBufferQuarter);
		renderContext->DrawFullscreenQuad(downsample4);
		fboDict->PopFBO();

		// blur vertical
		fboDict->PushFBO(frameBufferQuarter2);
		renderContext->DrawFullscreenQuad(gaussblur13v);
		fboDict->PopFBO();

		// blur horizontal and do bloom mapping
		fboDict->PushFBO(frameBufferQuarter);
		renderContext->DrawFullscreenQuad(gaussblur13h);
		fboDict->PopFBO();

		renderContext->PopViewport();

		// output result
		// combine bloom with scene
		renderContext->PushViewMatrix(view);
		renderContext->PushProjectionMatrix(projectionMatrix);

		renderContext->DrawFullscreenQuad(debugGbuffer[activeGbuffer]);

		//renderables[0]->DrawSkeleton();

		renderContext->PopViewMatrix();
		renderContext->PopProjectionMatrix();

		renderContext->SwapBuffers();

		renderContext->CheckForErrors();
	}

	Shutdown();

	gaussblur13h->DecrementReferenceCount();
	gaussblur13v->DecrementReferenceCount();
	downsample4->DecrementReferenceCount();
	lightPointWorld->DecrementReferenceCount();
	worldMaterial->DecrementReferenceCount();
	for ( auto m : debugGbuffer )
	{
		m->DecrementReferenceCount();
		Assert(!m->IsErrorMaterial());
	}
	DestroyRenderables();

	modelDict->Shutdown();

	renderContext->Shutdown();

	GetAppInterface()->Shutdown();

	SDL_Quit();

	return 0;
}
