#include "pch.h"
#include "util\testutil.h"
#include "util\threadlock.h"

#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

int threadTestState;

mutex threadMutex;
condition_variable requestVar;
condition_variable confirmVar;
volatile bool requestLock = false;


void TestThread_0()
{
	bool isRunning = true;

	// the mocked game loop
	while (isRunning)
	{
		// becomes true if someone wants to lock me
		if (requestLock)
		{
			unique_lock<mutex> threadLock(threadMutex);

			Assert(threadTestState == 1);
			threadTestState = 2;

			// wake up everybody waiting who was requesting me to lock down
			confirmVar.notify_all();

			// wait for being unlocked
			requestVar.wait(threadLock);

			Assert(threadTestState == 3);
			threadTestState = 4;

			// exit the game loop now because we just test this for one round
			isRunning = false;
		}
	}
}

void TestThread_1()
{
	// tell the mocked game loop to lock down
	{
		unique_lock<mutex> threadLock(threadMutex);

		Assert(threadTestState == 0);
		threadTestState = 1;

		requestLock = true;

		// wait for lock down confirmation
		confirmVar.wait(threadLock);

		Assert(threadTestState == 2);
		threadTestState = 3;
	}

	// do shit that needs to be synchronized with care

	{
		unique_lock<mutex> threadLock(threadMutex);

		// tell the game loop to resume processing
		requestVar.notify_all();
	}
}

ThreadLock lockClass;
bool isRunningClassThread0;

void TestClassThread_0()
{
	isRunningClassThread0 = true;

	while (isRunningClassThread0)
	{
		Assert(isRunningClassThread0);
		Assert(threadTestState == 0);

		lockClass.CheckForLock();

		if (isRunningClassThread0)
		{
			Assert(threadTestState == 0);
		}
		else
		{
			Assert(threadTestState == 1);
		}
	}

	Assert(threadTestState == 1);
	threadTestState = 2;
}

void TestClassThread_1()
{
	Assert(!lockClass.IsLocked());

	lockClass.RequestLock();

	Assert(lockClass.IsLocked());
	Assert(threadTestState == 0);

	threadTestState = 1;
	isRunningClassThread0 = false;

	lockClass.ReleaseLock();
	Assert(!lockClass.IsLocked());
}

BEGIN_TEST_FUNC(ThreadLocking)
{
	// test prototype
	threadTestState = 0;

	thread t0(TestThread_0);
	thread t1(TestThread_1);

	t0.join();
	t1.join();

	ASSERTTEST(threadTestState == 4);
}
END_TEST_FUNC()


BEGIN_TEST_FUNC(ThreadLockingUtility)
{
	// test utility class that implements former prototype
	threadTestState = 0;

	thread tc0(TestClassThread_0);
	thread tc1(TestClassThread_1);

	tc0.join();

	ASSERTTEST(threadTestState == 2);

	tc1.join();
}
END_TEST_FUNC()
