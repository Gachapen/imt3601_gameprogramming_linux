#include "pch.h"
#include "util\testutil.h"

using namespace std;

BEGIN_TEST_FUNC(ByteBufferDynamic)
{
	ByteBuffer buffer(1, 2);
	
	ASSERTTEST(buffer.GetReadLeft() == 1);

	buffer.WriteBool(false);
	buffer.WriteBool(true);
	buffer.WriteChar('a');
	buffer.WriteChar('Z');
	buffer.WriteDouble(0.9999);
	buffer.WriteFloat(-5600.06f);
	buffer.WriteInt(42);
	buffer.WriteInt(-12345);
	buffer.WritePtr(&cout);
	buffer.WriteUChar(240);
	buffer.WriteUInt(4000000005u);
	buffer.WriteShort(-30000);
	buffer.WriteUShort(60000);
	buffer.WriteString("ABC");

	ASSERTTEST(buffer.GetReadLeft() == buffer.GetSizeAllocated());

	ASSERTTEST(buffer.ReadBool() == false);
	ASSERTTEST(buffer.ReadBool() == true);
	ASSERTTEST(buffer.ReadChar() == 'a');
	ASSERTTEST(buffer.ReadChar() == 'Z');
	ASSERTTEST(buffer.ReadDouble() == 0.9999);
	ASSERTTEST(buffer.ReadFloat() == float(-5600.06f));
	ASSERTTEST(buffer.ReadInt() == 42);
	ASSERTTEST(buffer.ReadInt() == -12345);
	ASSERTTEST(buffer.ReadPtr<void *>() == (void *)&cout);
	ASSERTTEST(buffer.ReadUChar() == 240);
	ASSERTTEST(buffer.ReadUInt() == 4000000005u);
	ASSERTTEST(buffer.ReadShort() == -30000);
	ASSERTTEST(buffer.ReadUShort() == 60000);

	char str[4];
	buffer.ReadString(str, sizeof(str));
	ASSERTTEST(G_StrEq("ABC", str));

	ASSERTTEST(buffer.GetReadLeft() == buffer.GetSizeAllocated() - 41);
}
END_TEST_FUNC()


BEGIN_TEST_FUNC(ByteBufferStatic)
{
	Byte mem[3];

	ByteBuffer buffer(mem, sizeof(mem));

	buffer.WriteChar(10);
	buffer.WriteChar(20);
	buffer.WriteChar(15);
	ASSERTTEST(buffer.GetWritePosition() == buffer.GetSizeAllocated());

	ASSERTTEST(buffer.ReadChar() == 10);
	ASSERTTEST(buffer.ReadChar() == 20);
	ASSERTTEST(buffer.ReadChar() == 15);

	ASSERTTEST(buffer.GetReadPosition() == buffer.GetSizeAllocated());
}
END_TEST_FUNC()


BEGIN_TEST_FUNC(ByteBufferSwap)
{
	Byte mem0[5];
	Byte mem1[5];

	ByteBuffer buffer0(mem0, sizeof(mem0));
	ByteBuffer buffer1(mem1, sizeof(mem1));

	ASSERTTEST(buffer0.GetReadLeft() == buffer0.GetSizeAllocated());
	ASSERTTEST(buffer1.GetReadLeft() == buffer0.GetSizeAllocated());

	buffer0.WriteInt(56);
	buffer0.WriteBool(true);

	buffer1.WriteFloat(77.5f);
	buffer1.WriteChar(80);

	buffer0.Swap(buffer1);
	
	ASSERTTEST(buffer0.GetReadLeft() == buffer0.GetSizeAllocated());
	ASSERTTEST(buffer1.GetReadLeft() == buffer0.GetSizeAllocated());

	ASSERTTEST(buffer1.ReadInt() == 56);
	ASSERTTEST(buffer1.ReadBool() == true);
	ASSERTTEST(buffer0.ReadFloat() == float(77.5f));
	ASSERTTEST(buffer0.ReadChar() == 80);

	ASSERTTEST(buffer0.GetReadLeft() == buffer0.GetSizeAllocated() - 5);
	ASSERTTEST(buffer1.GetReadLeft() == buffer1.GetSizeAllocated() - 5);
}
END_TEST_FUNC()
