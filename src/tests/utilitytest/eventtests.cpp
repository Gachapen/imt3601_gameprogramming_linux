

#include "pch.h"
#include "util\testutil.h"
#include "util/eventutil.h"

static int eventTestCount = 0;

class EventObject
{
	DECLARE_CLASS_NOBASE(EventObject);
public:
	EventObject();

	EVENTHANDLER(TestListener1, int);
	EVENTHANDLER(TestListener2, int);
};

EventObject::EventObject()
	: LTestListener1(this)
	, LTestListener2(this)
{
}

void EventObject::TestListener1(int value)
{
	eventTestCount += value;
}

void EventObject::TestListener2(int value)
{
	eventTestCount += value;
}

BEGIN_TEST_FUNC(EventTest)
{
	Event<int> evt;
	EventObject obj;

	eventTestCount = 0;

	evt += obj.LTestListener1;

	ASSERTTEST(eventTestCount == 0);

	evt(6);

	ASSERTTEST(eventTestCount == 6);

	evt -= obj.LTestListener1;

	ASSERTTEST(eventTestCount == 6);

	evt += &obj.LTestListener1;
	evt += &obj.LTestListener1;

	evt(-3);

	ASSERTTEST(eventTestCount == 3);

	evt -= &obj.LTestListener1;

	evt(-2);

	ASSERTTEST(eventTestCount == 3);

	{
		EventObject objOutOfScope;
		evt += objOutOfScope.LTestListener1;

		evt(5);

		ASSERTTEST(eventTestCount == 8);
	}

	evt(5);

	ASSERTTEST(eventTestCount == 8);
}
END_TEST_FUNC()


BEGIN_TEST_FUNC(EventScopeTest)
{
	eventTestCount = 0;

	Event<int> evt;

	ASSERTTEST(eventTestCount == 0);

	{
		EventObject objOutOfScope;
		evt += objOutOfScope.LTestListener1;
		evt += objOutOfScope.LTestListener1;
		evt += objOutOfScope.LTestListener2;

		evt(5);

		ASSERTTEST(eventTestCount == 10);
	}

	evt(5);

	ASSERTTEST(eventTestCount == 10);

	{
		EventObject obj;
		evt += obj.LTestListener1;

		{
			Event<int> evtScope;

			evtScope += obj.LTestListener1;

			ASSERTTEST(eventTestCount == 10);

			evt(1);
			ASSERTTEST(eventTestCount == 11);
			evtScope(1);
			ASSERTTEST(eventTestCount == 12);
		}

		evt(1);
		ASSERTTEST(eventTestCount == 13);
	}

	evt(1);
	ASSERTTEST(eventTestCount == 13);
}
END_TEST_FUNC()


BEGIN_TEST_FUNC(EventCopyTest)
{
	eventTestCount = 0;

	Event<int> evt;
	Event<int> *evtCopy;

	{
		EventObject obj;

		evt += obj.LTestListener1;

		evtCopy = new Event<int>(evt);

		(*evtCopy)(10);
		ASSERTTEST(eventTestCount == 10);
	}

	(*evtCopy)(10);
	ASSERTTEST(eventTestCount == 10);

	delete evtCopy;

	EventObject *objCopy;

	{
		EventObject obj;

		{
			Event<int> evtScope;

			evtScope += obj.LTestListener1;
			evt += obj.LTestListener1;

			objCopy = new EventObject(obj);

			evtScope(5);
			ASSERTTEST(eventTestCount == 20);
		}

	}

	evt(5);
	ASSERTTEST(eventTestCount == 25);
	delete objCopy;
}
END_TEST_FUNC()