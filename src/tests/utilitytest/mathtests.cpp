#include "pch.h"
#include "util\testutil.h"

using namespace std;

static void PrintM4(const mat4 &m)
{
	DBGMSGF("\nm4:\nfwd: %f %f %f\n", m(0, 0), m(0, 1), m(0, 2));
	DBGMSGF("lft: %f %f %f\n", m(1, 0), m(1, 1), m(1, 2));
	DBGMSGF("up: %f %f %f\n", m(2, 0), m(2, 1), m(2, 2));
}

static void PrintM3(const mat3 &m)
{
	DBGMSGF("\nm3\nfwd: %f %f %f\n", m(0, 0), m(0, 1), m(0, 2));
	DBGMSGF("lft: %f %f %f\n", m(1, 0), m(1, 1), m(1, 2));
	DBGMSGF("up: %f %f %f\n", m(2, 0), m(2, 1), m(2, 2));
}

static void PrintBase(const vec3 &fwd, const vec3 &left, const vec3 &up)
{
	DBGMSGF("\nm3\nfwd: %f %f %f\n", XYZ(fwd));
	DBGMSGF("lft: %f %f %f\n", XYZ(left));
	DBGMSGF("up: %f %f %f\n", XYZ(up));
}


inline void MatrixGetForwardNew(const mat4 &matrix, vec3 &origin)
{
	origin.x() = matrix(0, 0);
	origin.y() = matrix(0, 1);
	origin.z() = matrix(0, 2);
}

inline void MatrixGetLeftNew(const mat4 &matrix, vec3 &origin)
{
	origin.x() = matrix(1, 0);
	origin.y() = matrix(1, 1);
	origin.z() = matrix(1, 2);
}

inline void MatrixGetUpNew(const mat4 &matrix, vec3 &origin)
{
	origin.x() = matrix(2, 0);
	origin.y() = matrix(2, 1);
	origin.z() = matrix(2, 2);
}

BEGIN_TEST_FUNC(MathTestEulerAnglesUnit)
{
	euler angles(0, 0, 0);
	vec3 fwd, left, up;

	AngleVectors(angles, &fwd);

	ASSERTTEST(fwd == vec3::UnitX());

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(fwd == vec3::UnitX());
	ASSERTTEST(left == vec3::UnitY());
	ASSERTTEST(up == vec3::UnitZ());
}
END_TEST_FUNC()



BEGIN_TEST_FUNC(MathTestEulerAnglesOffset)
{
	//mat4 m4Test(mat4::Identity());
	//mat3 m3Test(mat3::Identity());

	//PrintM4(m4Test);
	//PrintM3(m3Test);

	//m4Test = Eigen::Affine3f(Eigen::AngleAxisf(DEG2RAD(45), vec3(0, 0, 1))).matrix() * m4Test;
	//m4Test = Eigen::Affine3f(Eigen::AngleAxisf(DEG2RAD(90), vec3(0, 1, 0))).matrix() * m4Test;

	//m3Test = m3Test * Eigen::AngleAxisf(DEG2RAD(90), vec3(0, 1, 0)).matrix();
	//m3Test = m3Test * Eigen::AngleAxisf(DEG2RAD(45), vec3(0, 0, 1)).matrix();

	//PrintM4(m4Test);
	//PrintM3(m3Test);

	//vec3 fwd, left, up;
	//AngleVectors(euler(90, 45, 0), &fwd, &left, &up);

	//PrintBase(fwd, left, up);

	//MatrixGetForwardNew(m4Test, fwd);
	//MatrixGetLeftNew(m4Test, left);
	//MatrixGetUpNew(m4Test, up);

	//PrintBase(fwd, left, up);

	euler angles(0, -90, 0);
	vec3 fwd, left, up;
	const float vecQuarter = sin(M_PI_F * 0.25f);

	// test YAW

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitY()));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitY()));
	ASSERTTEST(VectorsEqual(left, -vec3::UnitX()));
	ASSERTTEST(VectorsEqual(up, vec3::UnitZ()));

	angles = euler(0, 45, 0);

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, vec3(vecQuarter, -vecQuarter, 0.0f)));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3(vecQuarter, -vecQuarter, 0.0f)));
	ASSERTTEST(VectorsEqual(left, vec3(vecQuarter, vecQuarter, 0.0f)));
	ASSERTTEST(VectorsEqual(up, vec3::UnitZ()));

	// test PITCH

	angles = euler(-90, 0, 0);

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, -vec3::UnitZ()));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, -vec3::UnitZ()));
	ASSERTTEST(VectorsEqual(left, vec3::UnitY()));
	ASSERTTEST(VectorsEqual(up, vec3::UnitX()));

	angles = euler(90, 0, 0);

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitZ()));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitZ()));
	ASSERTTEST(VectorsEqual(left, vec3::UnitY()));
	ASSERTTEST(VectorsEqual(up, -vec3::UnitX()));

	angles = euler(-45, 0, 0);

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, vec3(vecQuarter, 0.0f, -vecQuarter)));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3(vecQuarter, 0.0f, -vecQuarter)));
	ASSERTTEST(VectorsEqual(left, vec3::UnitY()));
	ASSERTTEST(VectorsEqual(up, vec3(vecQuarter, 0.0f, vecQuarter)));

	angles = euler(45, 0, 0);

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, vec3(vecQuarter, 0.0f, vecQuarter)));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3(vecQuarter, 0.0f, vecQuarter)));
	ASSERTTEST(VectorsEqual(left, vec3::UnitY()));
	ASSERTTEST(VectorsEqual(up, vec3(-vecQuarter, 0.0f, vecQuarter)));

	angles = euler(135, 0, 0);

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, vec3(-vecQuarter, 0.0f, vecQuarter)));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3(-vecQuarter, 0.0f, vecQuarter)));
	ASSERTTEST(VectorsEqual(left, vec3::UnitY()));
	ASSERTTEST(VectorsEqual(up, vec3(-vecQuarter, 0.0f, -vecQuarter)));

	// test ROLL

	angles = euler(0, 0, -90);

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitX()));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitX()));
	ASSERTTEST(VectorsEqual(left, vec3::UnitZ()));
	ASSERTTEST(VectorsEqual(up, -vec3::UnitY()));

	angles = euler(0, 0, 45);

	AngleVectors(angles, &fwd);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitX()));

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitX()));
	ASSERTTEST(VectorsEqual(left, vec3(0.0f, vecQuarter, -vecQuarter)));
	ASSERTTEST(VectorsEqual(up, vec3(0.0f, vecQuarter, vecQuarter)));

	// mixed stuff

	angles = euler(0, -90, 45);

	AngleVectors(angles, &fwd, &left, &up);

	ASSERTTEST(VectorsEqual(fwd, vec3::UnitY()));
	ASSERTTEST(VectorsEqual(left, vec3(-vecQuarter,0.0f,-vecQuarter)));
	ASSERTTEST(VectorsEqual(up, vec3(-vecQuarter,0.0f,vecQuarter)));
}
END_TEST_FUNC()
