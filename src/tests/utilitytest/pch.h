#ifndef PCH_H
#define PCH_H

#include "platform.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fstream>
#include <direct.h>
#include <vector>
#include <unordered_map>
#include <stack>
#include <memory>
#include <conio.h>
#include <iostream>

#include "util/macros.h"
#include "util/util.h"
#include "util/mathutil.h"
#include "util/fileutil.h"
#include "util/singleton.h"
#include "util/referencecounted.h"
#include "util/factory.h"
#include "util/stringutil.h"
#include "util\bytebuffer.h"

#include <shareddefs.h>

#include <cstdlib>
#include <string>



#endif