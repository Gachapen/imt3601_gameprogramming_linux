﻿namespace LevelEditor.Util{
    #region

    using System.IO;
    using LevelEditor.Properties;

    #endregion

    internal class FileUtil{
        public static void CreateMaterial(string name){
            string path = Settings.Default.LevelAtlasPath + name + "_atlas.xml";
            if (File.Exists(path)){
                return;
            }

            string content = string.Format(@"<material>
	<shader>ground</shader>
	<var name={0}>
		maps\\{1}_atlas.png
	</var>
	<var name={2}>
		normal_flat.png
	</var>
	<var name={3}>
		[0.7 0.7 0.7]
	</var>
</material>", "\"ALBEDOTEXTURE\"", name, "\"NORMALTEXTURE\"", "\"COLOR\"");

            FileStream fileStream = File.Create(path);
            StreamWriter writer = new StreamWriter(fileStream);
            writer.Write(content);
            writer.Close();
        }
    }
}