﻿namespace LevelEditor{
    #region

    using System;
    using System.IO;
    using System.Windows.Forms;
    using LevelEditor.Properties;

    #endregion

    internal static class Program{
        private static void CheckCreateDir(string path){
            if (!Directory.Exists(path)){
                Directory.CreateDirectory(path);
            }
        }

        private static void InitalizeProjectStructure(){
            CheckCreateDir(Settings.Default.LevelPath);
            CheckCreateDir(Settings.Default.TilesTexPath);
        }

        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(){
            InitalizeProjectStructure();
            TileManager.Load();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WorldEditor());
        }
    }
}