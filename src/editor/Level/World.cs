﻿namespace LevelEditor{
    #region

    using System;
    using System.IO;
    using LevelEditor.Properties;
    using LevelEditor.Util;

    #endregion

    public class World{
        private readonly Tile defaultTile;
        public string Name;
        public Tile[,] Tiles { get; private set; }
        public Atlas Atlas { get; private set; }
        public int Height { get { return Tiles.GetLength(1); } }
        public int Width { get { return Tiles.GetLength(0); } }

        public World(int width, int height, TileType defaultTileType){
            defaultTile = new Tile{ TileType = defaultTileType };
            Tiles = new Tile[width,height];

            Tile[,] defaultLevel = new Tile[width,height];
            for (int x = 0; x < defaultLevel.GetLength(0); x++){
                for (int y = 0; y < defaultLevel.GetLength(1); y++){
                    defaultLevel[x, y] = new Tile{ TileType = defaultTile.TileType };
                }
            }

            Initalize(defaultLevel);
        }

        public World(MapData mapData){
            defaultTile = new Tile{ TileType = mapData.DefaultTileType };
            Initalize(mapData.Tiles);
            Name = mapData.Name;
        }

        public event Action Resized;

        protected virtual void OnResized(){
            Action handler = Resized;
            if (handler != null){
                handler();
            }
        }

        public Tile GetTileFromPos(int x, int y){
            if (x < 0 || x >= Tiles.GetLength(0)){
                return null;
            }
            if (y < 0 || y >= Tiles.GetLength(1)){
                return null;
            }

            return Tiles[x, y];
        }

        public void RecalculateAt(int x, int y){
            Tile neighbour = GetTileFromPos(x, y);
            if (neighbour != null){
                if (Atlas != null){
                    Atlas.Remove(neighbour);
                }
                neighbour.Recalculate();
                if (Atlas != null){
                    Atlas.Add(x, y, neighbour);
                }
            }
        }

        private void CalculateNeighbours(int x, int y){
            Tile tile = GetTileFromPos(x, y);

            Tile neighbour = GetTileFromPos(x - 1, y) ?? defaultTile;
            tile.Left = new TileShareholder(TileEdge.DownLeft, neighbour);

            neighbour = GetTileFromPos(x, y - 1) ?? defaultTile;
            tile.Top = new TileShareholder(TileEdge.UpRight, neighbour);

            neighbour = GetTileFromPos(x - 1, y - 1) ?? defaultTile;
            tile.Diagonal = new TileShareholder(TileEdge.UpLeft, neighbour);
        }

        private void Initalize(Tile[,] tiles){
            Tiles = tiles;

            for (int x = 0; x < Width; x++){
                for (int y = 0; y < Height; y++){
                    Tile tile = Tiles[x, y];

                    CalculateNeighbours(x, y);
                    tile.Recalculate();
                }
            }

            Atlas = new Atlas(Tiles);
        }

        public void ForEach(Action<int, int, Tile> action){
            for (int x = 0; x < Width; x++){
                for (int y = 0; y < Height; y++){
                    action(x, y, Tiles[x, y]);
                }
            }
        }

        public bool Resize(int leftAdd, int rightAdd, int topAdd, int bottomAdd){
            int newWidth = Width + leftAdd + rightAdd;
            int newHeight = Height + topAdd + bottomAdd;

            if (newWidth < 1 || newHeight < 1){
                return false;
            }

            Tile[,] newTiles = new Tile[newWidth,newHeight];
            //Set all to default
            for (int x = 0; x < newWidth; x++){
                for (int y = 0; y < newHeight; y++){
                    newTiles[x, y] = new Tile(defaultTile.TileType);
                }
            }

            //Setup x
            int xSrc = 0, xDest = 0;
            if (leftAdd > 0){
                xDest = leftAdd;
            }
            else{
                xSrc = Math.Abs(leftAdd);
            }

            int xSrcMaxIndex;
            if (rightAdd > 0){
                xSrcMaxIndex = Width;
            }
            else{
                xSrcMaxIndex = Width + rightAdd;
            }

            //Setup y
            int ySrc = 0, yDest = 0;
            if (topAdd > 0){
                yDest = topAdd;
            }
            else{
                ySrc = Math.Abs(topAdd);
            }

            int ySrcMaxIndex;
            if (bottomAdd > 0){
                ySrcMaxIndex = Height;
            }
            else{
                ySrcMaxIndex = Height + bottomAdd;
            }

            //Copy existing world part into new world
            for (; xSrc < xSrcMaxIndex; xSrc++){
                int ySrcTmp = ySrc;
                int yDestTmp = yDest;
                for (; ySrc < ySrcMaxIndex; ySrc++){
                    newTiles[xDest, yDest] = new Tile(Tiles[xSrc, ySrc].TileType);
                    newTiles[xDest, yDest].Flags = Tiles[xSrc, ySrc].Flags;
                    yDest++;
                }
                ySrc = ySrcTmp;
                yDest = yDestTmp;
                xDest++;
            }

            Initalize(newTiles);

            Save(); //Auto-save on Resize

            OnResized();
            return true;
        }

        public void RecalculateAll(){
            Atlas = null;

            for (int x = 0; x < Width; x++){
                for (int y = 0; y < Height; y++){
                    RecalculateAt(x, y);
                }
            }

            //Just regenerate due to many tile changes
            Atlas = new Atlas(Tiles);
        }

        public void Save(){
            Atlas = new Atlas(Tiles);

            MapData data = new MapData();
            data.Name = Name;
            data.Width = (ushort)Width;
            data.Height = (ushort)Height;
            data.Tiles = Atlas.Tiles;
            data.DefaultTileType = defaultTile.TileType;

            string path = Settings.Default.LevelPath + data.Name + ".level";
            File.Delete(path);
            byte[] serialize = data.Serialize();
            FileStream fileStream = File.Open(path, FileMode.Create);
            fileStream.Write(data.Serialize(), 0, serialize.Length);
            fileStream.Close();
            SaveAtlas();
            FileUtil.CreateMaterial(Name);
        }

        public void ChangeDefaultTile(TileType selection){
            defaultTile.TileType = selection;
        }

        public void SaveAtlas(){
            Atlas.SaveAs(Settings.Default.LevelAtlasPath + "\\" + Name + "_atlas.png");
        }
    }
}