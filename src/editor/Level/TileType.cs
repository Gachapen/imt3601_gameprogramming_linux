﻿namespace LevelEditor{
    #region

    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    #endregion

    /// <summary>
    ///     0 = textures
    ///     1 = normal texture
    /// </summary>
    public class TileType{
        private const int TileHeight = 64;
        private const int TileWidth = 64;
        private static int nextId;
        private readonly List<Bitmap[]> textures = new List<Bitmap[]>{ new Bitmap[16], new Bitmap[16] };

        public int Depth;
        public byte Id;
        public string Name;
        //private Bitmap[] normalTextures = new Bitmap[16];
        // private Bitmap[] textures = new Bitmap[16];

        public TileEdge FullMask { get { return TileEdge.DownLeft | TileEdge.DownRight | TileEdge.UpLeft | TileEdge.UpRight; } }
        public static int TexTypeCount { get { return 2; } }

        public TileType(){
            Id = (byte)nextId++;
        }

        public TileType(Bitmap textureAtlas, string name){
            Name = name;
            Depth = 0;
            Id = (byte)nextId++;
            textures[0] = ExtractTiles(textureAtlas);
        }

        public Bitmap GetTexture(int type, TileEdge mask){
            return textures[type][(int)mask];
        }

        private Bitmap ExtractTile(Bitmap atlas, int x, int y){
            return atlas.Clone(new Rectangle(x * TileWidth, y * TileHeight, TileWidth, TileHeight), atlas.PixelFormat);
        }

        private Bitmap[] ExtractTiles(Bitmap wc3Atlas){
            Bitmap[] bitmaps = new Bitmap[16];

            bitmaps[(int)(TileEdge.DownLeft | TileEdge.DownRight | TileEdge.UpLeft | TileEdge.UpRight)] =
                ExtractTile(wc3Atlas, 0, 0);

            bitmaps[(int)TileEdge.UpLeft] = ExtractTile(wc3Atlas, 0, 2);
            bitmaps[(int)TileEdge.UpRight] = ExtractTile(wc3Atlas, 0, 1);
            bitmaps[(int)TileEdge.DownLeft] = ExtractTile(wc3Atlas, 2, 0);
            bitmaps[(int)TileEdge.DownRight] = ExtractTile(wc3Atlas, 1, 0);

            bitmaps[(int)(TileEdge.DownLeft | TileEdge.UpLeft)] = ExtractTile(wc3Atlas, 2, 2);
            bitmaps[(int)(TileEdge.DownRight | TileEdge.UpRight)] = ExtractTile(wc3Atlas, 1, 1);
            bitmaps[(int)(TileEdge.UpLeft | TileEdge.UpRight)] = ExtractTile(wc3Atlas, 0, 3);
            bitmaps[(int)(TileEdge.DownLeft | TileEdge.DownRight)] = ExtractTile(wc3Atlas, 3, 0);

            bitmaps[(int)(TileEdge.DownRight | TileEdge.UpLeft | TileEdge.UpRight)] = ExtractTile(wc3Atlas, 1, 3);
            bitmaps[(int)(TileEdge.DownLeft | TileEdge.DownRight | TileEdge.UpRight)] = ExtractTile(wc3Atlas, 3, 1);
            bitmaps[(int)(TileEdge.DownLeft | TileEdge.UpLeft | TileEdge.UpRight)] = ExtractTile(wc3Atlas, 2, 3);
            bitmaps[(int)(TileEdge.UpLeft | TileEdge.DownRight | TileEdge.DownLeft)] = ExtractTile(wc3Atlas, 3, 2);

            bitmaps[(int)(TileEdge.DownLeft | TileEdge.UpRight)] = ExtractTile(wc3Atlas, 2, 1);
            bitmaps[(int)(TileEdge.DownRight | TileEdge.UpLeft)] = ExtractTile(wc3Atlas, 1, 2);

            return bitmaps;
        }

        public void Load(string path){
            textures[0] = ExtractTiles(new Bitmap(path));

            string normalPath = path.Replace(".png", "");
            normalPath += "_normal.png";

            try{
                Bitmap bitmap = new Bitmap(normalPath);
                textures[1] = ExtractTiles(bitmap);
            }
            catch (Exception){
                MessageBox.Show("Could not find normal map: " + normalPath);
            }
        }
    }
}