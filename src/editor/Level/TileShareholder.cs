﻿namespace LevelEditor{
    public class TileShareholder{
        public TileEdge Edge;
        public Tile Tile;

        public TileShareholder(TileEdge edge, Tile tile){
            Edge = edge;
            Tile = tile;
        }
    }
}