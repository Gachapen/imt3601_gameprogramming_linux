﻿namespace LevelEditor{
    public enum TilePart{
        Full,
        UpFull,
        UpLeftOnly,
        UpRightOnly,
        UpLeftEmpty,
        UpRightEmpty,
    }
}