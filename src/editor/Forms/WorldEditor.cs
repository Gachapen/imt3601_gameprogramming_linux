﻿namespace LevelEditor{
    #region

    using System;
    using System.IO;
    using System.Windows.Forms;
    using LevelEditor.Forms;
    using LevelEditor.Properties;

    #endregion

    public partial class WorldEditor : Form{
        private readonly TileEditor tileEditor = new TileEditor();
        private bool levelChanged;
        private World world;

        public bool LevelChanged{
            get { return levelChanged; }
            set{
                if (levelChanged == value){
                    return;
                }
                levelChanged = value;
                if (levelChanged){
                    Text = Text + "*";
                }
                else{
                    Text = Text.Replace("*", "");
                }
            }
        }

        public WorldEditor(){
            InitializeComponent();
            if (!LoadLevel(Settings.Default.LastLevelName + ".level")){
                world = new World(5, 5, TileManager.GetFirst());
                world.Name = "Default";
                SetWorld(world);
                BSaveLevelClick(null, null);
            }

            InitWorldPanel(new CppWorldPanel(pTileSelector, world));
            //InitWorldPanel(new SharpWorldPanel(pTileSelector, newWorld));

            int style = NativeWinAPI.GetWindowLong(Handle, NativeWinAPI.GWL_EXSTYLE);
            style |= NativeWinAPI.WS_EX_COMPOSITED;
            NativeWinAPI.SetWindowLong(Handle, NativeWinAPI.GWL_EXSTYLE, style);

            TileManager.Changed += RecalculateAll;
            pWorld.Changed += () => LevelChanged = true;
            tbLevelName.TextChanged += (sender, args) => world.Name = tbLevelName.Text;

            cbEditMode.DataSource = Enum.GetValues(typeof(TileFlags));
            cbEditMode.SelectedIndex = 0;
        }

        private void BLoadLevelClick(object sender, EventArgs e){
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = Directory.GetCurrentDirectory() + "//maps";
            fileDialog.Multiselect = false;
            fileDialog.Filter = "Level file (.level)|*.level";

            DialogResult dialogResult = fileDialog.ShowDialog();
            if (dialogResult != DialogResult.OK){
                return;
            }

            LoadLevel(fileDialog.SafeFileName);
        }

        private bool LoadLevel(string name){
            if (!File.Exists(Settings.Default.LevelPath + name)){
                return false;
            }

            FileStream fileStream = File.Open(Settings.Default.LevelPath + name, FileMode.Open);
            long length = fileStream.Length;
            byte[] data = new byte[length];
            fileStream.Read(data, 0, (int)length);
            fileStream.Close();

            MapData mapData = MapData.Deserialize(data);
            SetWorld(new World(mapData));

            LevelChanged = false;
            Refresh();
            SetLastLevelUsedSetting();

            return true;
        }

        private void SetWorld(World newWorld){
            world = newWorld;
            world.Resized += () => SetWorld(world);

            tbLevelName.Text = world.Name;
            lWorldSize.Text = world.Width + "x" + world.Height;
            if (pWorld != null){
                pWorld.SetWorld(world);
            }
        }

        private void SetLastLevelUsedSetting(){
            Settings.Default.LastLevelName = world.Name;
            Settings.Default.Save();
        }

        private void BSaveLevelClick(object sender, EventArgs e){
            world.Save();
            LevelChanged = false;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData){
            if (keyData == (Keys.S | Keys.Control)){
                BSaveLevelClick(null, null);
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void MenuButtonTilesClick(object sender, EventArgs e){
            tileEditor.Show();
        }

        private void RecalculateAll(){
            world.RecalculateAll();
            BSaveLevelClick(null, null);
            pWorld.SetWorld(world);
            Refresh();
        }

        private void bResize_Click(object sender, EventArgs e){
            MarginDialog marginDialog = new MarginDialog(world.Width - 1, world.Height - 1);
            marginDialog.Text = "Resize";
            if (marginDialog.ShowDialog() == DialogResult.OK){
                world.Resize(
                    (int)marginDialog.nudLeft.Value,
                    (int)marginDialog.nudRight.Value,
                    (int)marginDialog.nudTop.Value,
                    (int)marginDialog.nudBottom.Value);
                world.RecalculateAll();

                pWorld.Refresh();
            }
        }

        private void bNewLevel_Click(object sender, EventArgs e){
            CreateLevel createLevel = new CreateLevel();
            if (createLevel.ShowDialog() == DialogResult.OK){
                world = new World(
                    (int)createLevel.nudWidth.Value,
                    (int)createLevel.nudHeight.Value,
                    createLevel.tileSelector.Selection);

                tbLevelName.Text = createLevel.tbLevelName.Text;
                world.Save();
                SetLastLevelUsedSetting();

                pWorld.SetWorld(world);
                Refresh();
            }
        }

        private void button1_Click(object sender, EventArgs e){
        }

        private void cbEditMode_SelectedIndexChanged(object sender, EventArgs e){
            pWorld.EditMode = (TileFlags)cbEditMode.SelectedItem;
        }

        private void changeDefaulToolStripMenuItem_Click(object sender, EventArgs e){
            TileSelectDialog dialog = new TileSelectDialog();
            if (dialog.ShowDialog() == DialogResult.OK){
                world.ChangeDefaultTile(dialog.tileSelector.Selection);
                RecalculateAll();
            }
        }
    }
}