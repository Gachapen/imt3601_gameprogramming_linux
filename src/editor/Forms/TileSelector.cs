﻿namespace LevelEditor{
    #region

    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    #endregion

    public class TileSelector : Panel{
        public TileType Selection;
        private Button lastSelected;

        public TileSelector(){
            Width = 640;
            Height = 64;
            TileManager.Changed += GenerateTileButtons;
            TabStop = false;

            GenerateTileButtons();
        }

        public event Action<TileType> SelectionChanged;

        public void GenerateTileButtons(){
            Controls.Clear();
            lastSelected = null;

            List<TileType> tiles = TileManager.ToList().OrderBy(tile => tile.Depth).ToList();

            int xPos = 0;

            foreach (TileType tile in tiles){
                Button b = new Button();
                b.BackgroundImage = tile.GetTexture(0, tile.FullMask);
                b.FlatStyle = FlatStyle.Flat;
                b.Location = new Point(xPos, 0);
                b.Width = 64;
                b.Height = 64;
                b.ForeColor = Color.Black;
                b.TabStop = false;
                b.Click += delegate{
                    Select(b);

                    if (Selection != tile){
                        Selection = tile;
                        OnSelectionChanged(Selection);
                    }
                };
                Controls.Add(b);

                xPos += 65;
            }

            if (tiles.Count >= 1){
                Selection = tiles[0];
                Select((Button)Controls[0]);
            }
        }

        private void Select(Button button){
            if (lastSelected != null){
                lastSelected.ForeColor = Color.Black;
            }
            button.ForeColor = Color.LightSeaGreen;
            lastSelected = button;
        }

        protected virtual void OnSelectionChanged(TileType obj){
            Action<TileType> handler = SelectionChanged;
            if (handler != null){
                handler(obj);
            }
        }
    }
}