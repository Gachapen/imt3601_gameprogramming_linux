﻿namespace LevelEditor.Forms{
    #region

    using System.Windows.Forms;

    #endregion

    public partial class MarginDialog : Form{
        public MarginDialog(int widthMax, int heightMax){
            InitializeComponent();

            nudLeft.Minimum = -widthMax;
            nudRight.Minimum = -widthMax;
            nudBottom.Minimum = -heightMax;
            nudTop.Minimum = -heightMax;
        }
    }
}