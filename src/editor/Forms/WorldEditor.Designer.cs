﻿namespace LevelEditor
{
    using System.Drawing;
    using System.Windows.Forms;
    using LevelEditor.Forms;
    using OpenTK;

    partial class WorldEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitWorldPanel(WorldPanel panel){
            if (pWorld != null){
                Controls.Remove(pWorld);
                pWorld.Dispose();
            }
            this.pWorld = panel;
            pWorld.Location = new Point(0, 92);
            pWorld.Name = "pWorld";
            pWorld.Dock = DockStyle.Fill;
            
            pWorld.SetWorld(world);
            this.Controls.Add(pWorld);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.levelEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bNewLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.bLoadLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.bSaveLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wavesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bResize = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDefaulToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuButtonTiles = new System.Windows.Forms.ToolStripMenuItem();
            this.menuButtonTower = new System.Windows.Forms.ToolStripMenuItem();
            this.menuButtonUnits = new System.Windows.Forms.ToolStripMenuItem();
            this.tbLevelName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbEditMode = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lWorldSize = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pTileSelector = new LevelEditor.TileSelector();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.levelEditorToolStripMenuItem,
            this.editToolStripMenuItem,
            this.menuButtonTiles,
            this.menuButtonTower,
            this.menuButtonUnits});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1349, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // levelEditorToolStripMenuItem
            // 
            this.levelEditorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bNewLevel,
            this.bLoadLevel,
            this.bSaveLevel});
            this.levelEditorToolStripMenuItem.Name = "levelEditorToolStripMenuItem";
            this.levelEditorToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.levelEditorToolStripMenuItem.Text = "File";
            // 
            // bNewLevel
            // 
            this.bNewLevel.Name = "bNewLevel";
            this.bNewLevel.Size = new System.Drawing.Size(100, 22);
            this.bNewLevel.Text = "New";
            this.bNewLevel.Click += new System.EventHandler(this.bNewLevel_Click);
            // 
            // bLoadLevel
            // 
            this.bLoadLevel.Name = "bLoadLevel";
            this.bLoadLevel.Size = new System.Drawing.Size(100, 22);
            this.bLoadLevel.Text = "Load";
            this.bLoadLevel.Click += new System.EventHandler(this.BLoadLevelClick);
            // 
            // bSaveLevel
            // 
            this.bSaveLevel.Name = "bSaveLevel";
            this.bSaveLevel.Size = new System.Drawing.Size(100, 22);
            this.bSaveLevel.Text = "Save";
            this.bSaveLevel.Click += new System.EventHandler(this.BSaveLevelClick);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wavesToolStripMenuItem1,
            this.bResize,
            this.changeDefaulToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // wavesToolStripMenuItem1
            // 
            this.wavesToolStripMenuItem1.Name = "wavesToolStripMenuItem1";
            this.wavesToolStripMenuItem1.Size = new System.Drawing.Size(156, 22);
            this.wavesToolStripMenuItem1.Text = "Waves";
            // 
            // bResize
            // 
            this.bResize.Name = "bResize";
            this.bResize.Size = new System.Drawing.Size(156, 22);
            this.bResize.Text = " Resize";
            this.bResize.Click += new System.EventHandler(this.bResize_Click);
            // 
            // changeDefaulToolStripMenuItem
            // 
            this.changeDefaulToolStripMenuItem.Name = "changeDefaulToolStripMenuItem";
            this.changeDefaulToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.changeDefaulToolStripMenuItem.Text = "Change Default";
            this.changeDefaulToolStripMenuItem.Click += new System.EventHandler(this.changeDefaulToolStripMenuItem_Click);
            // 
            // menuButtonTiles
            // 
            this.menuButtonTiles.Name = "menuButtonTiles";
            this.menuButtonTiles.Size = new System.Drawing.Size(43, 20);
            this.menuButtonTiles.Text = "Tiles";
            this.menuButtonTiles.Click += new System.EventHandler(this.MenuButtonTilesClick);
            // 
            // menuButtonTower
            // 
            this.menuButtonTower.Name = "menuButtonTower";
            this.menuButtonTower.Size = new System.Drawing.Size(52, 20);
            this.menuButtonTower.Text = "Tower";
            // 
            // menuButtonUnits
            // 
            this.menuButtonUnits.Name = "menuButtonUnits";
            this.menuButtonUnits.Size = new System.Drawing.Size(46, 20);
            this.menuButtonUnits.Text = "Units";
            // 
            // tbLevelName
            // 
            this.tbLevelName.Location = new System.Drawing.Point(649, 43);
            this.tbLevelName.Name = "tbLevelName";
            this.tbLevelName.Size = new System.Drawing.Size(100, 20);
            this.tbLevelName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(646, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Level name:";
            // 
            // cbEditMode
            // 
            this.cbEditMode.FormattingEnabled = true;
            this.cbEditMode.Items.AddRange(new object[] {
            "Tiles",
            "Walkable",
            "Buildable",
            "Spawnpoint",
            "Endpoint"});
            this.cbEditMode.Location = new System.Drawing.Point(782, 42);
            this.cbEditMode.Name = "cbEditMode";
            this.cbEditMode.Size = new System.Drawing.Size(121, 21);
            this.cbEditMode.TabIndex = 0;
            this.cbEditMode.TabStop = false;
            this.cbEditMode.SelectedIndexChanged += new System.EventHandler(this.cbEditMode_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(779, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Edit mode";
            // 
            // lWorldSize
            // 
            this.lWorldSize.AutoSize = true;
            this.lWorldSize.Location = new System.Drawing.Point(646, 66);
            this.lWorldSize.Name = "lWorldSize";
            this.lWorldSize.Size = new System.Drawing.Size(36, 13);
            this.lWorldSize.TabIndex = 8;
            this.lWorldSize.Text = "10x10";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(831, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(281, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "(Flag painting: Press E to \"paint enable\" and D for disable)";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gray;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.MaximumSize = new System.Drawing.Size(0, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1349, 100);
            this.panel2.TabIndex = 11;
            // 
            // pTileSelector
            // 
            this.pTileSelector.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pTileSelector.Location = new System.Drawing.Point(0, 27);
            this.pTileSelector.Name = "pTileSelector";
            this.pTileSelector.Size = new System.Drawing.Size(640, 64);
            this.pTileSelector.TabIndex = 2;
            // 
            // WorldEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1349, 795);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lWorldSize);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbEditMode);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbLevelName);
            this.Controls.Add(this.pTileSelector);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panel2);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "WorldEditor";
            this.Text = "WorldEditor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem levelEditorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bLoadLevel;
        private System.Windows.Forms.ToolStripMenuItem bSaveLevel;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wavesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem menuButtonTiles;
        private System.Windows.Forms.ToolStripMenuItem menuButtonTower;
        private System.Windows.Forms.ToolStripMenuItem menuButtonUnits;
        private LevelEditor.TileSelector pTileSelector;
        private System.Windows.Forms.TextBox tbLevelName;
        private System.Windows.Forms.Label label1;
        private WorldPanel pWorld;
        private System.Windows.Forms.ToolStripMenuItem bResize;
        private System.Windows.Forms.ToolStripMenuItem bNewLevel;
        private System.Windows.Forms.ComboBox cbEditMode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lWorldSize;
        private System.Windows.Forms.ToolStripMenuItem changeDefaulToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
    }
}