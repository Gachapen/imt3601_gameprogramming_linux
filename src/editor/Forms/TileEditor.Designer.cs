﻿namespace LevelEditor
{
    partial class TileEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuButtonImportWC3 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.lTileName = new System.Windows.Forms.Label();
            this.pBFull = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pBDownRight = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pBDownLeft = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pBUpLeft = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pBUpRight = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.pBRightFull = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pBLeftFull = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pBUpFull = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pBDownFull = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pBUREmpty = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pBULEmpty = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pBDLEmpty = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pbDREmpty = new System.Windows.Forms.PictureBox();
            this.bSave = new System.Windows.Forms.Button();
            this.pTileSelector = new TileSelector();
            this.nudDepth = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBDownRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBDownLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBUpLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBUpRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBRightFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLeftFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBUpFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBDownFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBUREmpty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBULEmpty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBDLEmpty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDREmpty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDepth)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(891, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuButtonImportWC3});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // menuButtonImportWC3
            // 
            this.menuButtonImportWC3.Name = "menuButtonImportWC3";
            this.menuButtonImportWC3.Size = new System.Drawing.Size(173, 22);
            this.menuButtonImportWC3.Text = "Import WC3 (.png)";
            this.menuButtonImportWC3.Click += new System.EventHandler(this.menuButtonImportWC3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Name:";
            // 
            // lTileName
            // 
            this.lTileName.AutoSize = true;
            this.lTileName.Location = new System.Drawing.Point(36, 94);
            this.lTileName.Name = "lTileName";
            this.lTileName.Size = new System.Drawing.Size(85, 13);
            this.lTileName.TabIndex = 2;
            this.lTileName.Text = "nothing selected";
            // 
            // pBFull
            // 
            this.pBFull.Location = new System.Drawing.Point(4, 157);
            this.pBFull.Name = "pBFull";
            this.pBFull.Size = new System.Drawing.Size(64, 64);
            this.pBFull.TabIndex = 3;
            this.pBFull.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Full";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(84, 384);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Down right";
            // 
            // pBDownRight
            // 
            this.pBDownRight.Location = new System.Drawing.Point(74, 317);
            this.pBDownRight.Name = "pBDownRight";
            this.pBDownRight.Size = new System.Drawing.Size(64, 64);
            this.pBDownRight.TabIndex = 5;
            this.pBDownRight.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 384);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Down left";
            // 
            // pBDownLeft
            // 
            this.pBDownLeft.Location = new System.Drawing.Point(4, 317);
            this.pBDownLeft.Name = "pBDownLeft";
            this.pBDownLeft.Size = new System.Drawing.Size(64, 64);
            this.pBDownLeft.TabIndex = 7;
            this.pBDownLeft.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Upper left";
            // 
            // pBUpLeft
            // 
            this.pBUpLeft.Location = new System.Drawing.Point(4, 247);
            this.pBUpLeft.Name = "pBUpLeft";
            this.pBUpLeft.Size = new System.Drawing.Size(64, 64);
            this.pBUpLeft.TabIndex = 9;
            this.pBUpLeft.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(82, 231);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Upper right";
            // 
            // pBUpRight
            // 
            this.pBUpRight.Location = new System.Drawing.Point(74, 247);
            this.pBUpRight.Name = "pBUpRight";
            this.pBUpRight.Size = new System.Drawing.Size(64, 64);
            this.pBUpRight.TabIndex = 11;
            this.pBUpRight.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(283, 231);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Right full";
            // 
            // pBRightFull
            // 
            this.pBRightFull.Location = new System.Drawing.Point(275, 247);
            this.pBRightFull.Name = "pBRightFull";
            this.pBRightFull.Size = new System.Drawing.Size(64, 64);
            this.pBRightFull.TabIndex = 19;
            this.pBRightFull.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(202, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Left full";
            // 
            // pBLeftFull
            // 
            this.pBLeftFull.Location = new System.Drawing.Point(205, 247);
            this.pBLeftFull.Name = "pBLeftFull";
            this.pBLeftFull.Size = new System.Drawing.Size(64, 64);
            this.pBLeftFull.TabIndex = 17;
            this.pBLeftFull.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(202, 384);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Up full";
            // 
            // pBUpFull
            // 
            this.pBUpFull.Location = new System.Drawing.Point(205, 317);
            this.pBUpFull.Name = "pBUpFull";
            this.pBUpFull.Size = new System.Drawing.Size(64, 64);
            this.pBUpFull.TabIndex = 15;
            this.pBUpFull.TabStop = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(285, 384);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Down full";
            // 
            // pBDownFull
            // 
            this.pBDownFull.Location = new System.Drawing.Point(275, 317);
            this.pBDownFull.Name = "pBDownFull";
            this.pBDownFull.Size = new System.Drawing.Size(64, 64);
            this.pBDownFull.TabIndex = 13;
            this.pBDownFull.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(479, 231);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "up right empty";
            // 
            // pBUREmpty
            // 
            this.pBUREmpty.Location = new System.Drawing.Point(471, 247);
            this.pBUREmpty.Name = "pBUREmpty";
            this.pBUREmpty.Size = new System.Drawing.Size(64, 64);
            this.pBUREmpty.TabIndex = 27;
            this.pBUREmpty.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(398, 231);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "up left empty";
            // 
            // pBULEmpty
            // 
            this.pBULEmpty.Location = new System.Drawing.Point(401, 247);
            this.pBULEmpty.Name = "pBULEmpty";
            this.pBULEmpty.Size = new System.Drawing.Size(64, 64);
            this.pBULEmpty.TabIndex = 25;
            this.pBULEmpty.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(398, 384);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "down left empty";
            // 
            // pBDLEmpty
            // 
            this.pBDLEmpty.Location = new System.Drawing.Point(401, 317);
            this.pBDLEmpty.Name = "pBDLEmpty";
            this.pBDLEmpty.Size = new System.Drawing.Size(64, 64);
            this.pBDLEmpty.TabIndex = 23;
            this.pBDLEmpty.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(481, 384);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(87, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "down right empty";
            // 
            // pbDREmpty
            // 
            this.pbDREmpty.Location = new System.Drawing.Point(471, 317);
            this.pbDREmpty.Name = "pbDREmpty";
            this.pbDREmpty.Size = new System.Drawing.Size(64, 64);
            this.pbDREmpty.TabIndex = 21;
            this.pbDREmpty.TabStop = false;
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(1, 431);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(75, 23);
            this.bSave.TabIndex = 29;
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Visible = false;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // pTileSelector
            // 
            this.pTileSelector.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.pTileSelector.Location = new System.Drawing.Point(0, 27);
            this.pTileSelector.Name = "pTileSelector";
            this.pTileSelector.Size = new System.Drawing.Size(640, 64);
            this.pTileSelector.TabIndex = 30;
            // 
            // nudDepth
            // 
            this.nudDepth.Location = new System.Drawing.Point(40, 110);
            this.nudDepth.Name = "nudDepth";
            this.nudDepth.Size = new System.Drawing.Size(120, 20);
            this.nudDepth.TabIndex = 31;
            this.nudDepth.ValueChanged += new System.EventHandler(this.nudDepth_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(1, 112);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Depth:";
            // 
            // TileEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 593);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.nudDepth);
            this.Controls.Add(this.pTileSelector);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pBUREmpty);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pBULEmpty);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.pBDLEmpty);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.pbDREmpty);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.pBRightFull);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pBLeftFull);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pBUpFull);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pBDownFull);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.pBUpRight);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pBUpLeft);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pBDownLeft);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pBDownRight);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pBFull);
            this.Controls.Add(this.lTileName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "TileEditor";
            this.Text = "Tile editor";
            this.Load += new System.EventHandler(this.TileEditor_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBDownRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBDownLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBUpLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBUpRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBRightFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBLeftFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBUpFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBDownFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBUREmpty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBULEmpty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pBDLEmpty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDREmpty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDepth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuButtonImportWC3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lTileName;
        private System.Windows.Forms.PictureBox pBFull;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pBDownRight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pBDownLeft;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pBUpLeft;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pBUpRight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pBRightFull;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pBLeftFull;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pBUpFull;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pBDownFull;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pBUREmpty;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pBULEmpty;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pBDLEmpty;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pbDREmpty;
        private System.Windows.Forms.Button bSave;
        private TileSelector pTileSelector;
        private System.Windows.Forms.NumericUpDown nudDepth;
        private System.Windows.Forms.Label label15;
    }
}