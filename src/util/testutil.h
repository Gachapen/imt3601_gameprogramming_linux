#ifndef TESTUTIL_H
#define TESTUTIL_H

#define __BASEFILE__    (strrchr(__FILE__, PATHSEPARATOR) ? strrchr(__FILE__, PATHSEPARATOR) + 1 : __FILE__)

#define SET_CONSOLE_COLOR(c)    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), c)

#define BEGIN_TEST_SUITE()			\
	int main(int argc, char **args)	\
	{								\
		int count = 0, failed = 0;

#define END_TEST_SUITE()												\
	std::cout << "\nTests run: " << count << ", failed: " << failed << '\n';	\
	std::cout << "Press any key to continue";								\
	_getch();															\
	return 0;															\
	}

#define BEGIN_TEST_FUNC(x) \
	bool test_ ## x()	   \
	{					   \
		std::cout << "Begin test: " << # x << '\n';

#define END_TEST_FUNC()	\
	return true;		\
	}

#define ASSERTTEST(x)																   \
	if (!(x))																		   \
	{																				   \
		std::cout << "Test assertion: " << __BASEFILE__ << " at line " << __LINE__ << '\n'; \
		Assert(0);																	   \
		return false;																   \
	}

#define RUN_TEST(x)				   \
	extern bool test_ ## x();	   \
	if (!test_ ## x())			   \
	{							   \
		SET_CONSOLE_COLOR(0x0C);   \
		std::cout << "Test failed!\n";  \
		SET_CONSOLE_COLOR(0x07);   \
		failed++;				   \
	}							   \
	else						   \
	{							   \
		SET_CONSOLE_COLOR(0x02);   \
		std::cout << "Test success.\n"; \
		SET_CONSOLE_COLOR(0x07);   \
	}							   \
	count++;
#endif
