#ifndef SINGLETON_H
#define SINGLETON_H

#include "macros.h"
#include "appinterface/iappinterface.h"

/// Creates a lazy singleton. needs to be initialized by hand
/// before calling GetInstance
template<typename T>
class SingletonLazy
{
	static T *instance;

protected:
	SingletonLazy() {}
	virtual ~SingletonLazy() { Assert(instance == nullptr); }

public:
	/// Simply allocate the singleton
	static void InitSingleton() { Assert(instance == nullptr); instance = new T(); }

	/// Allocate singleton and expose it as an interface
	static void InitSingletonInterface(const char *interfaceName)
	{ Assert(instance == nullptr); instance = new T(); GetAppInterface()->RegisterApp(interfaceName, instance); }

	/// Deallocate the singleton
	static void ShutdownSingleton() { T *del = instance; instance = nullptr; delete del; }

	/// Access the singleton instance
	static T *GetInstance() { return instance; }
};

/// Creates a private ctor/cctor, allows singleton class
/// to make an instance of this class
#define DECLARE_SINGLETON(className)					  \
public:													  \
	static className *GetInstance() { return &instance; } \
private:												  \
	static className instance;							  \
	className();										  \
	className(const className &other);


/// Allocates space for the singleton
#define SINGLETON_INSTANCE(className) \
	className className::instance

/// Creates a private ctor/cctor, allows singleton class
/// to make an instance of this class
#define DECLARE_SINGLETON_LAZY(className)  \
private:								   \
	friend class SingletonLazy<className>; \
	className();						   \
	className(const className &other);	   \
public:

/// Allocates space for the pointer to the singleton
#define SINGLETON_INSTANCE_LAZY(className) \
	className * SingletonLazy<className>::instance = nullptr
#endif
