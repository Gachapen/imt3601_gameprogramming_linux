#ifndef REFERENCECOUNTED_H
#define REFERENCECOUNTED_H

#include "macros.h"

// implements increment and decrement of an integer to
// keep track of references to shared resources (not threadsafe)
// used for materials and textures
class CReferenceCounted
{
public:
	CReferenceCounted()
		: referenceCount(0)
	{
	}

	virtual ~CReferenceCounted() {}

	inline void IncrementReferenceCount()
	{
		referenceCount++;
	}

	inline void DecrementReferenceCount()
	{
		referenceCount--;
		Assert(referenceCount >= 0);
	}

	inline bool IsReferenced() const
	{
		return referenceCount > 0;
	}

protected:
	inline bool IsReferenceCountValid() const
	{
		return referenceCount >= 0;
	}

private:

	int referenceCount;
};

// expose reference counting in an interface
#define FORWARD_REFERENCE_COUNTED_DECLARE()		\
	virtual void IncrementReferenceCount() = 0;	\
	virtual void DecrementReferenceCount() = 0

// implement naive reference counting in concrete class of interface
#define FORWARD_REFERENCE_COUNTED_DEFINE()			  \
	virtual void IncrementReferenceCount()			  \
	{ CReferenceCounted::IncrementReferenceCount(); } \
	virtual void DecrementReferenceCount()			  \
	{ CReferenceCounted::DecrementReferenceCount(); }
#endif
