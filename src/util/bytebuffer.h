#ifndef BYTEBUFFER_H
#define BYTEBUFFER_H

#include "util.h"
#include "stringutil.h"
#include <memory>

class ByteBuffer
{
	ByteBuffer(const ByteBuffer &other);

public:
	ByteBuffer(Byte *destBuffer, int destBufferSize, int writePosition = 0)
	{
		Assert(destBuffer != nullptr);
		positionRead = 0;
		position = writePosition;

		external = true;
		growSize = 0;

		size = destBufferSize;
		buffer = destBuffer;
	}

	ByteBuffer(int intialSize = 4, int growSize = 32, int writePosition = 0)
	{
		positionRead = 0;
		position = writePosition;

		external = false;
		this->growSize = growSize;

		if (intialSize > 0)
		{
			size = intialSize;
			buffer = new Byte[size];
		}
		else
		{
			size = 0;
			buffer = nullptr;
		}
	}

	~ByteBuffer()
	{
		if (!external)
		{
			delete [] buffer;
		}
	}

	inline void Swap(ByteBuffer &other)
	{
		::Swap(size, other.size);
		::Swap(growSize, other.growSize);
		::Swap(position, other.position);
		::Swap(positionRead, other.positionRead);
		::Swap(buffer, other.buffer);
		::Swap(external, other.external);
	}

	inline void Attach(Byte *destBuffer, int destBufferSize)
	{
		external = true;

		size = destBufferSize;
		buffer = destBuffer;

		position = 0;
		positionRead = 0;
		this->growSize = 0;
	}

	inline void Detach()
	{
		buffer = nullptr;
		size = 0;
		position = 0;
		positionRead = 0;
	}

	inline void Reset()
	{
		position = 0;
		positionRead = 0;
	}

	inline int GetSizeAllocated() const
	{
		return size;
	}

	inline int GetReadPosition() const
	{
		return positionRead;
	}

	inline int GetWritePosition() const
	{
		return position;
	}

	inline int GetReadLeft() const
	{
		return size - positionRead;
	}

	inline Byte *GetBase() const
	{
		return buffer;
	}

	inline void Write(const void *data, const int &size)
	{
		GrowOnDemand(size);

		Assert(size <= (this->size - position));

		memcpy(&buffer[position], data, size);
		position += size;
	}

	inline void WriteBool(const bool &v)
	{
		Write(&v, sizeof(bool));
	}

	inline void WriteChar(const char &v)
	{
		Write(&v, sizeof(char));
	}

	inline void WriteUChar(const unsigned char &v)
	{
		Write(&v, sizeof(unsigned char));
	}

	inline void WriteShort(const short &v)
	{
		Write(&v, sizeof(short));
	}

	inline void WriteUShort(const unsigned short &v)
	{
		Write(&v, sizeof(unsigned short));
	}

	inline void WriteInt(const int &v)
	{
		Write(&v, sizeof(int));
	}

	inline void WriteUInt(const unsigned int &v)
	{
		Write(&v, sizeof(unsigned int));
	}

	inline void WriteFloat(const float &v)
	{
		Write(&v, sizeof(float));
	}

	inline void WriteDouble(const double &v)
	{
		Write(&v, sizeof(double));
	}

	inline void WritePtr(const void *v)
	{
		Write(&v, sizeof(void *));
	}

	inline void WriteString(const char *v)
	{
		const int len = G_StrLen(v);

		Write(v, len);
		WriteChar('\0');
	}

	inline void Read(const int &size, void *data)
	{
		// if we attach to an existing buffer, write pos is 0
		//Assert((positionRead + size) <= position);

		memcpy(data, &buffer[positionRead], size);
		positionRead += size;
	}

	template<class T>
	inline T Read(const int &size)
	{
		// if we attach to an existing buffer, write pos is 0
		//Assert((positionRead + size) <= position);

		T data;
		memcpy(&data, &buffer[positionRead], size);
		positionRead += size;
		return data;
	}

	inline void Peek(const int &size, void *data)
	{
		// if we attach to an existing buffer, write pos is 0
		//Assert((positionRead + size) <= position);

		memcpy(data, &buffer[positionRead], size);
	}

	template<class T>
	inline T Peek(const int &size)
	{
		// if we attach to an existing buffer, write pos is 0
		//Assert((positionRead + size) <= position);

		T data;
		memcpy(&data, &buffer[positionRead], size);
		return data;
	}

	inline void Flush(const int &size)
	{
		positionRead += size;
	}

	inline void ReadBool(bool *v)
	{
		Read(sizeof(bool), v);
	}

	inline bool ReadBool()
	{
		return Read<bool>(sizeof(bool));
	}

	inline void FlushBool()
	{
		Flush(sizeof(bool));
	}

	inline void ReadChar(char *v)
	{
		Read(sizeof(char), v);
	}

	inline char ReadChar()
	{
		return Read<char>(sizeof(char));
	}

	inline void PeekChar(char *v)
	{
		Peek(sizeof(char), v);
	}

	inline char PeekChar()
	{
		return Peek<char>(sizeof(char));
	}

	inline void FlushChar()
	{
		Flush(sizeof(char));
	}

	inline void ReadUChar(unsigned char *v)
	{
		Read(sizeof(unsigned char), v);
	}

	inline unsigned char ReadUChar()
	{
		return Read<unsigned char>(sizeof(unsigned char));
	}

	inline unsigned char ReadUCharSafe(unsigned char defaultValue = 0)
	{
		if (GetReadLeft() < sizeof(unsigned char))
			return defaultValue;

		return Read<unsigned char>(sizeof(unsigned char));
	}

	inline void PeekUChar(unsigned char *v)
	{
		Peek(sizeof(unsigned char), v);
	}

	inline unsigned char PeekUChar()
	{
		return Peek<unsigned char>(sizeof(unsigned char));
	}

	inline void FlushUChar()
	{
		Flush(sizeof(unsigned char));
	}

	inline void ReadShort(short *v)
	{
		Read(sizeof(short), v);
	}

	inline short ReadShort()
	{
		return Read<short>(sizeof(short));
	}

	inline void FlushShort()
	{
		Flush(sizeof(short));
	}

	inline void ReadUShort(unsigned short *v)
	{
		Read(sizeof(unsigned short), v);
	}

	inline unsigned short ReadUShort()
	{
		return Read<unsigned short>(sizeof(unsigned short));
	}

	inline void FlushUShort()
	{
		Flush(sizeof(unsigned short));
	}

	inline void ReadInt(int *v)
	{
		Read(sizeof(int), v);
	}

	inline int ReadInt()
	{
		return Read<int>(sizeof(int));
	}

	inline void FlushInt()
	{
		Flush(sizeof(int));
	}

	inline void ReadUInt(unsigned int *v)
	{
		Read(sizeof(unsigned int), v);
	}

	inline unsigned int ReadUInt()
	{
		return Read<unsigned int>(sizeof(unsigned int));
	}

	inline void FlushUInt()
	{
		Flush(sizeof(unsigned int));
	}

	inline void ReadFloat(float *v)
	{
		Read(sizeof(float), v);
	}

	inline float ReadFloat()
	{
		return Read<float>(sizeof(float));
	}

	inline void FlushFloat()
	{
		Flush(sizeof(float));
	}

	inline void ReadDouble(double *v)
	{
		Read(sizeof(double), v);
	}

	inline double ReadDouble()
	{
		return Read<double>(sizeof(double));
	}

	inline void FlushDouble()
	{
		Flush(sizeof(double));
	}

	template<class T>
	inline void ReadPtr(T *v)
	{
		Read(sizeof(T), v);
	}

	template<class T>
	inline T ReadPtr()
	{
		return Read<T>(sizeof(T));
	}

	inline void FlushPtr()
	{
		Flush(sizeof(void *));
	}

	inline void ReadString(char *v, int sizeInBytes)
	{
		Assert(v != nullptr);
		Assert(sizeInBytes > 0);

		const char *str = (const char *)(buffer + positionRead);
		const int len = G_StrLen(str);

		G_StrNCpy(v, (const char *)(buffer + positionRead), sizeInBytes);
		v[sizeInBytes - 1] = 0;

		positionRead += len + 1;
	}

private:

	inline void GrowOnDemand(const int &addBytes)
	{
		if (external)
		{
			return;
		}

		Assert(growSize > 0);

		if (position + addBytes > size)
		{
			int grow = (position + addBytes) / growSize + 1;
			grow *= growSize;

			grow = MAX(grow, size * 2);
			Grow(grow);
		}

		Assert(size >= (position + addBytes));
	}

	inline void Grow(const int &addBytes)
	{
		Assert(addBytes > 0);
		Assert(!external);
		Assert(buffer == nullptr || size > 0);

		int newSize = size + addBytes;
		Byte *newBuffer = new Byte[newSize];

		if (buffer != nullptr)
		{
			memcpy(newBuffer, buffer, sizeof(Byte) * position);
			delete [] buffer;
		}

		buffer = newBuffer;
		size = newSize;
	}

	int growSize;
	int size;
	int position;
	int positionRead;

	Byte *buffer;
	bool external;
};
#endif	// BYTEBUFFER_H
