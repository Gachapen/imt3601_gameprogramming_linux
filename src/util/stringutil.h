#ifndef STRINGUTIL_H
#define STRINGUTIL_H

//#include <Windows.h>

#include "macros.h"

// platform specific correct path separators
#ifdef PLATFORM_WIN32
#define PATHSEPARATOR '\\'
#define PATHSEPARATOR_STRING "\\"
#define INCORRECT_PATHSEPARATOR '/'
#else
#define PATHSEPARATOR '/'
#define PATHSEPARATOR_STRING "/"
#define INCORRECT_PATHSEPARATOR '\\'
#endif

// shared convention for current working directory
#ifdef PLATFORM_WIN32
#include <direct.h>
#define GetCWD		_getcwd
#else
#include <unistd.h>
#define GetCWD		getcwd
#endif

// content directories, used to magically fix up paths
// i.e. materials always get one directory named materials/ at the beginning of their path
namespace ContentDirectories
{
	enum ContentDirectories_e
	{
		MATERIALS = 0,
		MAPS,
		MODELS,
		PARTICLES,
		SCRIPTS,
		SHADERS,
		RESOURCES,

		COUNT,
	};
}
typedef ContentDirectories::ContentDirectories_e ContentDirectoryId;


// un-safe copy of zero terminated strings
inline void G_StrCpy(char *dest, const char *src)
{
	strcpy(dest, src);
}

// safe copy of zero terminated strings
inline void G_StrNCpy(char *dest, const char *src, int sizeInBytes)
{
	//strncpy_s(dest, sizeInBytes, src, sizeInBytes);

	strncpy(dest, src, sizeInBytes);
}

// advance read position by skipping any slashes
inline const char *G_StrReadSkipSlashes(const char *str)
{
	while (*str
		&& (*str == PATHSEPARATOR
		|| *str == INCORRECT_PATHSEPARATOR))
	{
		str++;
	}

	return str;
}

// search for first occurance of 'search' in 'text'
inline const char *G_StrStr(const char *text, const char *search)
{
	return strstr(text, search);
}

// make sure that all pathseparators are correct and that there are no duplicates
inline void G_StrFixSlashes(char *str)
{
	Assert(str != nullptr);

	char *reader = str;
	char *writer = str;

	bool pathSepLast = false;

	while (*reader)
	{
		bool foundPathSep = false;

		if (*reader == INCORRECT_PATHSEPARATOR)
		{
			*reader = PATHSEPARATOR;
			foundPathSep = true;
		}
		else if (*reader == PATHSEPARATOR)
		{
			foundPathSep = true;
		}

		if (foundPathSep)
		{
			if (pathSepLast)
			{
				reader++;
			}

			pathSepLast = true;
		}
		else
		{
			pathSepLast = false;
		}

		*writer = *reader;

		reader++;
		writer++;
	}

	*writer = 0;
}

// remove extension (after last .)
inline void G_StrStripExtension(char *str)
{
	char *read = str;

	while (*read)
		read++;

	while (read >= str)
	{
		if (*read == '.')
		{
			*read = 0;
			break;
		}

		read--;
	}
}

// replace extension with this (lengths must match!)
inline void G_StrReplaceExtension(char *str, const char *extension)
{
	char *read = str;

	while (*read)
		read++;

	while (read >= str)
	{
		if (*read == '.')
		{
			break;
		}

		read--;
	}

	if (*read != '.')
		return;

	read++;

	const char *ext = extension;

	while (*read)
	{
		*read = *ext;

		read++;
		ext++;
	}
}

// allocate copy of string
inline char *G_StrCreateCopy(const char *str)
{
	if (str == nullptr)
		return nullptr;

	char *string = new char[strlen(str) + 1];
	G_StrCpy(string, str);
	return string;
}

// remove a specific directory from a path
inline void G_StrStripDirectory(char *str, const char *dir)
{
	int lenStr = strlen(str);
	int lenDir = strlen(dir);

	char *read = str;
	char *write = str;

	bool found = false;

	while (*read)
	{
		if (lenDir > lenStr && found)
			break;

		if (STRNICMP(read, dir, lenDir) == 0)
		{
			read += lenDir;

			if (*read == PATHSEPARATOR)
			{
				read++;
			}

			found = true;
		}

		*write = *read;

		read++;
		write++;
		lenStr--;
	}

	*write = 0;
}

// convert char to lower
inline char G_ChrLower(char c)
{
	if (c >= 'A' && c <= 'Z')
		return c | 0x20;
	return c;
}

// convert string to lower
inline void G_StrLower(char *str)
{
	while (*str)
	{
		if (*str >= 'A'
			&& *str <= 'Z')
		{
			*str = *str | 0x20;
		}

		str++;
	}
}

// convert string to upper
inline void G_StrUpper(char *str)
{
	while (*str)
	{
		if (*str >= 'a'
			&& *str <= 'z')
		{
			*str = *str & ~(0x20);
		}

		str++;
	}
}

// string length
inline int G_StrLen(const char *str)
{
	return strlen(str);
}

// formatted printing i.e. %f for floats, %i - int...
inline int G_StrSnPrintf(char *dest, int sizeInBytes, const char *format, ...)
{
	va_list args;
	va_start(args, format);
#ifdef PLATFORM_WIN32
	_vsnprintf_s(dest, sizeInBytes, sizeInBytes, format, args);
#else
	vsnprintf(dest, sizeInBytes, format, args);
#endif
	va_end(args);
	return 0;
}

// fixup content path
inline void G_StrAbsContentPath(ContentDirectoryId type, const char *str, char *dest, int sizeInBytes)
{
	static const char *typeString [] = {
		"materials",
		"maps",
		"models",
		"particles",
		"scripts",
		"shaders",
		"resources",
	};
	static_assert(ARRAYSIZE(typeString) == ContentDirectories::COUNT, "Add directory string");

	char strCopy[MAX_PATH_GAME];
	G_StrNCpy(strCopy, str, sizeof(strCopy));
	G_StrLower(strCopy);

	const char *content = typeString[type];
	char *pos = strstr(strCopy, content);

	if (pos != nullptr)
	{
		G_StrNCpy(dest, strCopy, sizeInBytes);
	}
	else
	{
		G_StrSnPrintf(dest, sizeInBytes, "%s" PATHSEPARATOR_STRING "%s", content, str);
	}
}

// ccat two strings (unsafe)
inline void G_StrCat(char *dest, const char *str)
{
	strcat(dest, str);
}

// ccat two strings (safe)
inline void G_StrNCat(char *dest, int sizeInBytes, const char *str)
{
#ifdef PLATFORM_WIN32
	strcat_s(dest, sizeInBytes, str);
#else
	strncat(dest, str, sizeInBytes);
#endif
}

// add or replace the extension of a filename/path
inline void G_StrAddOrReplaceExtension(const char *str, const char *extension, char *out, int sizeInBytes)
{
	G_StrNCpy(out, str, sizeInBytes);

	char *read = out;

	while (*read && *read != '.')
	{
		read++;
	}

	if (*read == '.')
	{
		read++;
	}
	else
	{
		G_StrNCat(out, sizeInBytes, ".");

		read++;
	}

	if (read - out >= sizeInBytes)
		return;

	*read = 0;

	G_StrNCat(out, sizeInBytes, extension);
}

// are two strings equal? (case-insensitive)
inline bool G_StrEq(const char *str0, const char *str1)
{
	return STRICMP(str0, str1) == 0;
}

// are two strings equal? (case-insensitive)
inline bool G_StrNEq(const char *str0, const char *str1, int sizeInBytes)
{
	return STRNICMP(str0, str1, sizeInBytes) == 0;
}

inline int G_StriCmp(const char *str0, const char *str1)
{
	return STRICMP(str0, str1);
}

inline int G_StrNiCmp(const char *str0, const char *str1, int sizeInBytes)
{
	return STRNICMP(str0, str1, sizeInBytes);
}

inline bool G_ChrIsWhitespace(char c)
{
	return c == ' ' || c == '\t';
}

// compare extension to parameter
inline bool G_StrHasExtension(const char *str, const char *extension)
{
	const char *read = str;

	while (*read)
		read++;

	while (read >= str)
	{
		if (*read == '.')
		{
			break;
		}

		read--;
	}

	if (*read != '.')
		return false;

	read++;

	return G_StrEq(read, extension);
}

// helper class to auto destruct or copy strings in its scope
class StringScope
{
public:
	StringScope(char *str)
	{
		this->string = str;
	}

	StringScope(const char *str)
	{
		this->string = G_StrCreateCopy(str);
	}

	~StringScope()
	{
		delete [] string;
	}

	operator char* const()
	{
		return string;
	}

	operator const char* const()
	{
		return string;
	}

	const char *c_str()
	{
		return string;
	}

	char *str()
	{
		return string;
	}

private:
	char *string;
};

// hashing operators for cstring
//struct cmp_
//{
//	bool operator()(const char* s1, const char* s2) const
//	{
//		return StrEq(s1, s2);
//	}

//	size_t operator()(const char* str) const
//	{
//		unsigned h = 42;
//		while (*str)
//			h = h * 101 + (unsigned char) *str++;
//		return h;
//	}
//};


#endif
