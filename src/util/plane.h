#ifndef PLANE_H
#define PLANE_H

#include "eigen.h"

struct Plane
{
	Plane()
		: normal(vec3::UnitZ())
		, distance(0.0f)
	{
	}

	void Init(const vec3 &normal, float distance)
	{
		this->normal = normal;
		this->distance = distance;
	}

	vec3 normal;
	float distance;
};


inline bool FrustumCullAABB(const Plane *planes, vec3 &mins, vec3 &maxs)
{
	vec3 vmin, vmax;

	for (int i = 0; i < 6; i++)
	{
		if (planes[i].normal.x() > 0)
		{
			vmin.x() = mins.x();
			vmax.x() = maxs.x();
		}
		else
		{
			vmin.x() = maxs.x();
			vmax.x() = mins.x();
		}

		if (planes[i].normal.y() > 0)
		{
			vmin.y() = mins.y();
			vmax.y() = maxs.y();
		}
		else
		{
			vmin.y() = maxs.y();
			vmax.y() = mins.y();
		}

		if (planes[i].normal.z() > 0)
		{
			vmin.z() = mins.z();
			vmax.z() = maxs.z();
		}
		else
		{
			vmin.z() = maxs.z();
			vmax.z() = mins.z();
		}

		if (planes[i].normal.dot(vmin) > planes[i].distance)
		{
			return true;
		}
	}

	return false;
}

#endif