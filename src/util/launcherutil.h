#ifndef LAUNCHERUTIL_H
#define LAUNCHERUTIL_H

// utility variables to create different launcher setups
// NEVER include this in another header file as it defines
// stuff in filescope

#include "platform.h"

#include "util\stringutil.h"

// global libraries we need to precache from the bin/ folder
static const char *const launcherLibraryNames[] =
{
	"SDL2",
	"zlib1",
	"libfreetype-6",
	"libjpeg-9",
	"libpng16-16",
	//"libtiff-5",
	//"libwebp",
	"glew32",
	"SDL2_image",
	"SDL2_ttf",
	"SDL2_Net",
	"appinterface",
	"cvar",
};

static const int launcherLibraryCount = ARRAYSIZE(launcherLibraryNames);

#endif