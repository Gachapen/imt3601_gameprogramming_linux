#ifndef MATHUTIL_H
#define MATHUTIL_H

#include <util/macros.h>
#include <util/eigen.h>

// http://stackoverflow.com/questions/364985/algorithm-for-finding-the-smallest-power-of-two-thats-greater-or-equal-to-a-giv
inline int GetNextPowerOfTwo(int value)
{
	if (value < 0)
	{
		return 0;
	}

	--value;
	value |= value >> 1;
	value |= value >> 2;
	value |= value >> 4;
	value |= value >> 8;
	value |= value >> 16;
	return value + 1;
}

// set/get matrix columns
#if 0
inline void MatrixPosition(const mat4 &matrix, vec3 &origin)
{
	memcpy(origin.data(), matrix.col(3).data(), sizeof(float) * 3);
}

inline void MatrixGetForward(const mat4 &matrix, vec3 &origin)
{
	memcpy(origin.data(), matrix.col(0).data(), sizeof(float) * 3);
}

inline void MatrixGetLeft(const mat4 &matrix, vec3 &origin)
{
	memcpy(origin.data(), matrix.col(1).data(), sizeof(float) * 3);
}

inline void MatrixGetUp(const mat4 &matrix, vec3 &origin)
{
	memcpy(origin.data(), matrix.col(2).data(), sizeof(float) * 3);
}

inline void MatrixSetForward(mat4 &matrix, const vec3 &vec)
{
	memcpy(matrix.data(), vec.data(), sizeof(float) * 3);
}

inline void MatrixSetLeft(mat4 &matrix, const vec3 &vec)
{
	memcpy(matrix.col(1).data(), vec.data(), sizeof(float) * 3);
}

inline void MatrixSetUp(mat4 &matrix, const vec3 &vec)
{
	memcpy(matrix.col(2).data(), vec.data(), sizeof(float) * 3);
}

inline void MatrixSetPosition(mat4 &matrix, const vec3 &vec)
{
	memcpy(matrix.col(3).data(), vec.data(), sizeof(float) * 3);
}
#else
inline void MatrixPosition(const mat4 &matrix, vec3 &origin)
{
	origin.x() = matrix(0, 3);
	origin.y() = matrix(1, 3);
	origin.z() = matrix(2, 3);
}

inline void MatrixGetForward(const mat4 &matrix, vec3 &origin)
{
	origin.x() = matrix(0, 0);
	origin.y() = matrix(0, 1);
	origin.z() = matrix(0, 2);
}

inline void MatrixGetLeft(const mat4 &matrix, vec3 &origin)
{
	origin.x() = matrix(1, 0);
	origin.y() = matrix(1, 1);
	origin.z() = matrix(1, 2);
}

inline void MatrixGetUp(const mat4 &matrix, vec3 &origin)
{
	origin.x() = matrix(2, 0);
	origin.y() = matrix(2, 1);
	origin.z() = matrix(2, 2);
}

inline void MatrixSetForward(mat4 &matrix, const vec3 &vec)
{
	matrix(0, 0) = vec.x();
	matrix(0, 1) = vec.y();
	matrix(0, 2) = vec.z();
}

inline void MatrixSetLeft(mat4 &matrix, const vec3 &vec)
{
	matrix(1, 0) = vec.x();
	matrix(1, 1) = vec.y();
	matrix(1, 2) = vec.z();
}

inline void MatrixSetUp(mat4 &matrix, const vec3 &vec)
{
	matrix(2, 0) = vec.x();
	matrix(2, 1) = vec.y();
	matrix(2, 2) = vec.z();
}

inline void MatrixSetPosition(mat4 &matrix, const vec3 &vec)
{
	matrix(0, 3) = vec.x();
	matrix(1, 3) = vec.y();
	matrix(2, 3) = vec.z();
}
#endif

// set rotation components of a mat4 to identity values
inline void MatrixRotationIdentity(mat4 &matrix)
{
	MatrixSetForward(matrix, vec3(1, 0, 0));
	MatrixSetLeft(matrix, vec3(0, 1, 0));
	MatrixSetUp(matrix, vec3(0, 0, 1));
}

// define rotation of a matrix by euler angles
inline void MatrixSetRotation(const euler &angles, mat4 &m)
{
	vec3 f, l, u;

	AngleVectors(angles, &f, &l, &u);
	MatrixSetForward(m, f);
	MatrixSetLeft(m, l);
	MatrixSetUp(m, u);
}

template< class T >
inline float RemapVal( T val, const T &a, const T &b, const T &c, const T &d)
{
	if ( a == b )
		return SEL( val - b , d , c );
	return c + (d - c) * (val - a) / (b - a);
}

template <class T >
inline float RemapValClamped( T val, const T &a, const T &b, const T &c, const T &d )
{
	if ( a == b )
		return SEL( val - b , d , c );
	float f = ( val - a ) / ( b - a );
	f = CLAMP( f, 0.0f, 1.0f );
	return c + ( d - c ) * f;
}

template< typename T >
inline T sqr(T value)
{
	return value * value;
}

#endif
