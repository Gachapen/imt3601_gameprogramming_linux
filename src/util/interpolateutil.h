#ifndef INTERPOLATEUTIL_H
#define INTERPOLATEUTIL_H

template<typename T>
inline T CatmullRomSpline(const T &a, const T &b, const T &c, const T &d, float t)
{
	// can be triggered due to rounding errors when interval is too large/time too high
	t = CLAMP(t, 0.0f, 1.0f);

	const float c0 = ((-t + 2.0f) * t - 1.0f) * t * 0.5f;
	const float c1 = (((3.0f * t - 5.0f) * t) * t + 2.0f) * 0.5f;
	const float c2 = ((-3.0f * t + 4.0f) * t + 1.0f) * t * 0.5f;
	const float c3 = ((t - 1.0f) * t * t) * 0.5f;

	T v = a * c0;
	v += b * c1;
	v += c * c2;
	v += d * c3;

	return v;
}


template<typename T>
class InterpolatedVar
{
public:
	InterpolatedVar(float interpolationTime = 0.15f)
		: newestTime(-1)
	{
		this->interpolationTime = interpolationTime;
		memset(&interpolatedValue, 0, sizeof(T));
		memset(&currentHead, 0, sizeof(T));
	}

	inline void PushValue(T value, float time)
	{
		if (time < newestTime)
		{
			return;
		}

		if (newestTime == time)
		{
			history.back().value = value;
		}
		else
		{
			newestTime = time;

			HistoryEntry entry;
			entry.value = value;
			entry.time = time;

			history.push_back(entry);

			if (history.size() < 2)
			{
				interpolatedValue = value;
				currentHead = value;
			}
		}
	}

	inline void Interpolate(float currentTime)
	{
		if (history.size() < 2)
		{
			return;
		}

		currentTime -= interpolationTime;

		T *values[4];
		float dt = 1.0f;
		float et = 0.0f;

		bool wasSet = false;
		auto itr = history.end();
		itr--;

		values[3] = &history.back().value;
		values[2] = values[3];

		do
		{
			if (itr->time <= currentTime)
			{
				wasSet = true;

				currentHead = *values[2];

				values[1] = &itr->value;

				dt = (currentTime - itr->time) / (et - itr->time);

				if (--itr != history.begin())
					values[0] = &itr->value;
				else
					values[0] = values[1];

				break;
			}

			values[3] = values[2];
			values[2] = &itr->value;
			et = itr->time;
			itr--;
		}
		while (itr != history.begin());

		if (wasSet)
		{
			interpolatedValue = CatmullRomSpline(*values[0], *values[1], *values[2], *values[3], dt);
			history.erase(history.begin(), itr);
		}
		else
		{
			history.back().time = currentTime;
		}
	}

	inline const T &GetValue()
	{
		return interpolatedValue;
	}

	inline const T &GetHead()
	{
		return currentHead;
	}

private:
	struct HistoryEntry
	{
		T value;
		float time;
	};

	std::vector<HistoryEntry> history;

	float newestTime;
	float interpolationTime;

	T interpolatedValue;
	T currentHead;
};





#endif