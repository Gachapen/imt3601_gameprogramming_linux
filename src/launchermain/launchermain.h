#ifndef LAUNCHERMAIN_H
#define LAUNCHERMAIN_H

// define launcher prototype and export macro
typedef void (*LauncherMainFunc)();

#ifdef LAUNCHERMAIN_EXPORTS
#define LAUNCHERMAIN_API	extern "C" __declspec(dllexport)
#endif

#endif
