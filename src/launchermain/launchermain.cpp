
#include <string>

#include <SDL.h>
#include <SDL_syswm.h>
#include <SDL_image.h>
#include "util\platform.h"
#include "util\macros.h"
#include "util\stringutil.h"

#ifdef PLATFORM_WIN32
#include <Windows.h>
#endif

#define LAUNCHERMAIN_EXPORTS
#include "launchermain.h"

#include "appinterface\iappinterface.h"
#include "engine\iengine.h"


/**
\mainpage Tower Defense - IMT3601

\section intro_sec Documentation for the Tower Defense game

The entry point to the application can be found in the
gamelauncher project. It preloads various libs that need to be eagerly loaded
and calls the function LauncherMain in the launchermain project.
The class Engine will be used in LauncherMain to load further libraries,
query various interfaces and prepare everything as necessary. The game loop will
entered by calling Engine::Run.

When the engine decides that the application should terminate, the game loop will be
exited and Engine::Shutdown will be called.
*/


#ifdef PLATFORM_WIN32
HWND hwnd;
HICON icon;

// OS specific way of setting the application icon of the window
static void InitOS(SDL_Window *window)
{
	HINSTANCE handle = GetModuleHandle(NULL);

	// load icon from resource (same as icon in explorer)
	icon = LoadIcon(handle, MAKEINTRESOURCE(101));

	SDL_SysWMinfo wminfo;
	SDL_VERSION(&wminfo.version);

	// retrieve win32 window handle from SDL window
	if (SDL_GetWindowWMInfo(window, &wminfo) != SDL_TRUE)
	{
		return;
	}

	hwnd = wminfo.info.win.window;

	// changes icon
	SetClassLong(hwnd, GCL_HICON, (LONG)icon);
}

static void ShutdownOS()
{
	// destroy icon if loaded
	if (icon != nullptr)
	{
		DestroyIcon(icon);
	}
}

#else
#error set_app_icon_for_posix
#endif

LAUNCHERMAIN_API void LauncherMain()
{
	// TODO: load res from config?
	const int windowWidth = 640;
	const int windowHeight = 480;

	// startup SDL, we can use it here now because it has been preloaded
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_Window *window = SDL_CreateWindow("Tower defense", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
										  windowWidth, windowHeight,
										  SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

	SDL_SetWindowMinimumSize(window, 640, 480);
	InitOS(window);

	SDL_Surface *cursorTexture = IMG_Load("resources" PATHSEPARATOR_STRING "cursor_alt.png");
	Assert(cursorTexture != nullptr);

	SDL_Cursor *cursor = SDL_CreateColorCursor(cursorTexture, 0, 0);
	Assert(cursor != nullptr);

	SDL_SetCursor(cursor);
	SDL_FreeSurface(cursorTexture);

	// load engine and enter game loop
	GetAppInterface()->LoadSharedLibrary("engine");

	IEngine *engine = (IEngine *)GetAppInterface()->QueryApp(INTERFACE_IENGINE_VERSION);

	if (engine != nullptr
		&& engine->Init(IEngine::INIT_GAME, window, windowWidth, windowHeight))
	{
		engine->Run();

		engine->Shutdown();
	}

	GetAppInterface()->Shutdown();

	SDL_SetCursor(nullptr);
	SDL_FreeCursor(cursor);
	SDL_DestroyWindow(window);

	SDL_Quit();
	ShutdownOS();
}