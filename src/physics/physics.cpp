
#include "pch.h"
#include "physics.h"
#include "physicsdebugdraw.h"


SINGLETON_INSTANCE(Physics);

EXPOSE_APP(INTERFACE_IPHYSICS_VERSION, Physics::GetInstance());

IDebugDraw *debugDraw;

Physics::Physics()
	: world(nullptr)
	, worldDebugDraw(nullptr)
{
}

Physics::~Physics()
{
	Assert(world == nullptr);
	Assert(worldDebugDraw == nullptr);
}

void Physics::Init()
{
	Assert(world == nullptr);
	Assert(worldDebugDraw == nullptr);

	debugDraw = (IDebugDraw *)GetAppInterface()->QueryApp(INTERFACE_IDEBUGDRAW_VERSION);

	Assert(debugDraw != nullptr);

	worldDebugDraw = new PhysicsDebugDraw();

	world = new b2World(b2Vec2(0, 0));
	world->SetDebugDraw(worldDebugDraw);
}

void Physics::Shutdown()
{
	Assert(world != nullptr);

	delete world;
	world = nullptr;

	delete worldDebugDraw;
	worldDebugDraw = nullptr;
}

void Physics::Update(float frametime)
{
	Assert(world != nullptr);

	// default box2d precision
	world->Step(frametime, 8, 3);
}

void Physics::DrawDebug()
{
	Assert(world != nullptr);

	world->DrawDebugData();
}

PhysicsBody Physics::CreateRigidRect(const vec3 &position, const vec3 &size)
{
	b2BodyDef def;
	def.position.Set(position.x() * PHYSICS_SCALE, position.y() * PHYSICS_SCALE);
	def.type = b2_staticBody;
	def.allowSleep = true;

	b2Body *body = world->CreateBody(&def);

	b2PolygonShape box;
	box.SetAsBox(size.x() * PHYSICS_SCALE, size.y() * PHYSICS_SCALE);
	
	b2Fixture *fixture = body->CreateFixture(&box, 0.0f);
	fixture->SetRestitution(0.8f);

	b2Filter filter;
	filter.categoryBits = 0x0002;
	filter.maskBits = 0x0001;
	fixture->SetFilterData(filter);
	return body;
}

PhysicsBody Physics::CreateDynamicCircle(const vec3 &position, float radius)
{
	b2BodyDef def;
	def.position.Set(position.x() * PHYSICS_SCALE, position.y() * PHYSICS_SCALE);
	def.type = b2_dynamicBody;
	def.allowSleep = true;
	def.angle = DEG2RAD(90.0f);
	//def.linearVelocity.Set(0, 50);
	def.fixedRotation = false;
	def.linearDamping = 1.0f;
	def.angularDamping = 0.0f;

	b2Body *body = world->CreateBody(&def);

	b2CircleShape circle;
	circle.m_radius = radius * PHYSICS_SCALE;

	b2Fixture *fixture = body->CreateFixture(&circle, 0.0f);
	fixture->SetRestitution(1.1f);
	fixture->SetFriction(10.0f);
	fixture->SetDensity(100.0f * radius);

	b2Filter filter;
	filter.categoryBits = 0x0001;
	filter.maskBits = 0x0002 | 0x0001;
	fixture->SetFilterData(filter);
	return body;
}

void Physics::SetObjectPosition(PhysicsBody obj, const vec3 &position)
{
	b2Body *body = (b2Body*)obj;

	b2Vec2 newPosition(position.x() * PHYSICS_SCALE,
		position.y() * PHYSICS_SCALE);

	body->SetTransform(newPosition, body->GetAngle());
}

void Physics::SetObjectVelocity(PhysicsBody obj, const vec3 &velocity)
{
	b2Body *body = (b2Body*)obj;

	b2Vec2 newVelocity(velocity.x() * PHYSICS_SCALE,
		velocity.y() * PHYSICS_SCALE);

	body->SetLinearVelocity(newVelocity);
}

void Physics::SetObjectLinearDamping(PhysicsBody obj, float linearDamping)
{
	b2Body *body = (b2Body*)obj;

	body->SetLinearDamping(linearDamping);
}

void Physics::ApplyObjectImpulse(PhysicsBody obj, const vec3 &impulse)
{
	b2Body *body = (b2Body*)obj;

	b2Vec2 impulse2D(impulse.x() * PHYSICS_SCALE, impulse.y() * PHYSICS_SCALE);
	const float linearDampening = body->GetLinearDamping();

	if (linearDampening > 0.0f)
	{
		impulse2D *= body->GetLinearDamping();
	}

	body->ApplyLinearImpulse(impulse2D, body->GetWorldCenter(), true);
}

void Physics::SetObjectCollisionGroup(PhysicsBody obj, int group)
{
	b2Body *body = (b2Body*)obj;

	b2Fixture *fixture = body->GetFixtureList();

	if (fixture != nullptr)
	{
		b2Filter filter = fixture->GetFilterData();

		filter.categoryBits = (1 << group);
		fixture->SetFilterData(filter);
	}
}

void Physics::GetObjectPosition(PhysicsBody obj, vec3 &position)
{
	b2Body *body = (b2Body*)obj;

	const b2Vec2 &vec = body->GetPosition();

	position = vec3(vec.x * PHYSICS_SCALE_INVERSE, vec.y * PHYSICS_SCALE_INVERSE, 0.0f);
}

void Physics::GetObjectVelocity(PhysicsBody obj, vec3 &velocity)
{
	b2Body *body = (b2Body*)obj;

	const b2Vec2 &vec = body->GetLinearVelocity();

	velocity = vec3(vec.x * PHYSICS_SCALE_INVERSE, vec.y * PHYSICS_SCALE_INVERSE, 0.0f);
}

void Physics::DestroyObject(PhysicsBody obj)
{
	Assert(obj != nullptr);

	world->DestroyBody((b2Body*) obj);
}
