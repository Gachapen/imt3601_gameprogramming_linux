#include "pch.h"
#include "physicsdebugdraw.h"

const float debugDrawTime = -1.0f; // PHYSICS_FRAMESPEED

PhysicsDebugDraw::PhysicsDebugDraw()
{
	SetFlags(e_shapeBit);
}

PhysicsDebugDraw::~PhysicsDebugDraw()
{
}

void PhysicsDebugDraw::DrawPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
{
}

void PhysicsDebugDraw::DrawSolidPolygon(const b2Vec2 *vertices, int32 vertexCount, const b2Color &color)
{
	if (vertexCount < 1)
	{
		return;
	}

	static int lastVertexCount = 0;
	static vec3 *vertices3D = nullptr;

	if (lastVertexCount < vertexCount)
	{
		delete [] vertices3D;
		lastVertexCount = vertexCount;

		vertices3D = new vec3[vertexCount];
	}

	for (int i = 0; i < vertexCount; i++)
	{
		vertices3D[i].x() = vertices[i].x * PHYSICS_SCALE_INVERSE;
		vertices3D[i].y() = vertices[i].y * PHYSICS_SCALE_INVERSE;
		vertices3D[i].z() = 1.0f;
	}

	debugDraw->DrawTriangleFan(vertices3D, vertexCount, vec3(color.r, color.g, color.b), 0.5f, debugDrawTime, true);
	debugDraw->DrawLineLoop(vertices3D, vertexCount, vec3(color.r, color.g, color.b), 1.0f, debugDrawTime, true);
}

void PhysicsDebugDraw::DrawCircle(const b2Vec2 &center, float32 radius, const b2Color &color)
{
}

void PhysicsDebugDraw::DrawSolidCircle(const b2Vec2 &center, float32 radius, const b2Vec2 &axis, const b2Color &color)
{
	const int vertexCount = 13;
	static vec3 vertices3D[vertexCount];

	vec3 center3D = vec3(center.x * PHYSICS_SCALE_INVERSE, center.y * PHYSICS_SCALE_INVERSE, 1.0f);
	vec3 forward = vec3(axis.x, axis.y, 0.0f);

	forward.normalize();
	vec3 left = forward.cross(vec3(0, 0, 1.0f));

	forward *= radius  * PHYSICS_SCALE_INVERSE;
	left *= radius  * PHYSICS_SCALE_INVERSE;

	vertices3D[0] = center3D;

	for (int i = 1; i < vertexCount; i++)
	{
		float fraction = (i - 1) / float(vertexCount - 2);

		fraction *= M_PI_F * 2.0f;

		vertices3D[i] = forward * cos(fraction) - left * sin(fraction) + center3D;
		vertices3D[i].z() = 1.0f;
	}

	debugDraw->DrawTriangleFan(vertices3D, vertexCount, vec3(color.r, color.g, color.b), 0.5f, debugDrawTime, true);
	debugDraw->DrawLineLoop(vertices3D + 1, vertexCount - 1, vec3(color.r, color.g, color.b), 1.0f, debugDrawTime, true);
	debugDraw->DrawLine(center3D, center3D + forward, vec3(color.r, color.g, color.b), 1.0f, debugDrawTime, true);
}

void PhysicsDebugDraw::DrawSegment(const b2Vec2 &p1, const b2Vec2 &p2, const b2Color &color)
{
}

void PhysicsDebugDraw::DrawTransform(const b2Transform &xf)
{
}
