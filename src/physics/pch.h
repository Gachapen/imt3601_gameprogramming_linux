#ifndef PCH_H
#define PCH_H

#include "util/platform.h"

#pragma warning( disable: 4996 )
#pragma warning( disable: 4530 )

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//#include <fstream>
//#include <direct.h>
#include <vector>
#include <unordered_map>
#include <stack>
#include <memory>

#include "util/macros.h"
#include "util/util.h"
#include "util/mathutil.h"
#include "util/fileutil.h"
#include "util/singleton.h"
#include "util/referencecounted.h"
#include "util/factory.h"
#include "util/stringutil.h"
#include "util/bytebuffer.h"

#include <shareddefs.h>

#include "appinterface/iappinterface.h"
#include "engine/idebugdraw.h"
#include "materialsystem/irendercontext.h"
#include "materialsystem/meshbuilder.h"
#include "materialsystem/imesh.h"
#include "materialsystem/imaterial.h"
#include "materialsystem/imaterialdict.h"

#include <Box2D/Box2D.h>

extern IDebugDraw *debugDraw;

#endif
