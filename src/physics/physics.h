#ifndef PHYSICS_H
#define PHYSICS_H

#include "physics/iphysics.h"

class b2World;
class PhysicsDebugDraw;

class Physics : public IPhysics
{
	DECLARE_SINGLETON(Physics);
public:

	~Physics();

	virtual void Init();
	virtual void Shutdown();

	virtual void Update(float frametime);
	virtual void DrawDebug();

	virtual PhysicsBody CreateRigidRect(const vec3 &position, const vec3 &size);
	virtual PhysicsBody CreateDynamicCircle(const vec3 &position, float radius);

	virtual void SetObjectPosition(PhysicsBody obj, const vec3 &position);
	virtual void SetObjectVelocity(PhysicsBody obj, const vec3 &velocity);
	virtual void SetObjectLinearDamping(PhysicsBody obj, float linearDamping);
	virtual void ApplyObjectImpulse(PhysicsBody obj, const vec3 &impulse);
	virtual void SetObjectCollisionGroup(PhysicsBody obj, int group);

	virtual void GetObjectPosition(PhysicsBody obj, vec3 &position);
	virtual void GetObjectVelocity(PhysicsBody obj, vec3 &velocity);

	virtual void DestroyObject(PhysicsBody obj);

private:

	b2World *world;
	PhysicsDebugDraw *worldDebugDraw;

};


#endif