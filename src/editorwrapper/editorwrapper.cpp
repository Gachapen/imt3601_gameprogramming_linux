#include "pch.h"
#include "editorwrapper.h"
#include "appinterface/iappinterface.h"
#include "util/launcherutil.h"

#include "engine/iengine.h"
#include "engine/ienginetools.h"
#include <vcclr.h>

using namespace System::Threading;

#define EDITORWRAPPER_MONITOR    "EditorWrapperLock"

std::string MarshalNetToStdString(System::String ^ s)
{
	const char *chars = (const char *)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(s)).ToPointer();
	std::string str = chars;
	System::Runtime::InteropServices::Marshal::FreeHGlobal(System::IntPtr((void *)chars));

	return str;
}

EditorWrapper::EditorWrapper(void)
	: isLockingExternal(false)
{
}

EditorWrapper::~EditorWrapper(void)
{
}

void EditorWrapper::Init(void *handle, int width, int height)
{
	HMODULE launcherLibraries[launcherLibraryCount];

	// Precache all dependencies by hand
	char cwd[MAX_PATH_GAME];

	GetCWD(cwd, sizeof(cwd));
	for (int i = 0; i < launcherLibraryCount; i++)
	{
		char libraryPath[MAX_PATH_GAME];
		G_StrSnPrintf(libraryPath, sizeof(libraryPath),
					  "%s\\bin\\%s", cwd, launcherLibraryNames[i]);

		launcherLibraries[i] = LoadLibrary(libraryPath);

		if (launcherLibraries[i] == nullptr)
		{
			Assert(0);
			break;
		}
	}

	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window *sdlWindow = SDL_CreateWindowFrom(handle);

	Assert(sdlWindow != nullptr);

	// now we're ready to use the custom dll system
	GetAppInterface()->LoadSharedLibrary("engine");

	engine = (IEngine *)GetAppInterface()->QueryApp(INTERFACE_IENGINE_VERSION);

	Assert(engine != nullptr);

	engine->Init(IEngine::INIT_TOOLSMODE, sdlWindow, width, height);

	engineTools = (IEngineTools *)GetAppInterface()->QueryApp(INTERFACE_IENGINETOOLS_VERSION);

	Assert(engineTools != nullptr);

	Initalized();

	// enter game loop
	// we will stay here until we're asked to exit through engineTools
	engine->Run();

	// disconnect engine
	engine->Shutdown();

	// shutdown dll system
	GetAppInterface()->Shutdown();

	// unload all precached libraries
	for (int i = launcherLibraryCount - 1; i >= 0; i--)
	{
		Assert(launcherLibraries[i] != nullptr);

		if (launcherLibraries[i] != nullptr)
		{
			FreeLibrary(launcherLibraries[i]);
		}
	}

	// shutdown SDL completely
	SDL_DestroyWindow(sdlWindow);
	SDL_Quit();

	// signal the thread who asked us to exit the engine
	Monitor::Enter(EDITORWRAPPER_MONITOR);
	Monitor::PulseAll(EDITORWRAPPER_MONITOR);
	Monitor::Exit(EDITORWRAPPER_MONITOR);
}

void EditorWrapper::Shutdown()
{
	// lock early so we don't miss the pulse
	Monitor::Enter(EDITORWRAPPER_MONITOR);

	// ask engine to exit
	engineTools->LockEngine();
	engineTools->RequestEngineQuit();
	engineTools->UnlockEngine();

	// wait for engine thread to exit game loop
	Monitor::Wait(EDITORWRAPPER_MONITOR);
	Monitor::Exit(EDITORWRAPPER_MONITOR);
}

void EditorWrapper::WindowResize(int width, int height)
{
	if ((engine != nullptr)	&&
		(engineTools != nullptr))
	{
		engineTools->LockEngine(true);
		engine->ResizeWindow(width, height);
		engineTools->UnlockEngine();
	}
}

void EditorWrapper::Lock(bool switchContext)
{
	if (engineTools != nullptr)
	{
		isLockingExternal = true;
		engineTools->LockEngine(switchContext);
	}
}

void EditorWrapper::Unlock()
{
	if (engineTools != nullptr)
	{
		engineTools->UnlockEngine();

		isLockingExternal = false;
	}
}

void EditorWrapper::LockOnDemand(bool switchContext)
{
	if (!isLockingExternal)
	{
		engineTools->LockEngine(switchContext);
	}
}

void EditorWrapper::ChangeEditMode(int mode)
{
	if (engineTools != nullptr)
	{
		LockOnDemand(true);
		TileFlagId flag = static_cast<TileFlagId>(mode);
		if (flag == TileFlagId::NONE)
		{
			engineTools->SetPaintHighlight(true, true);
		}
		else
		{
			engineTools->SetPaintHighlight(true, false);
		}
		engineTools->SetFlagHighlight(flag);
		UnlockOnDemand();
	}
}

void EditorWrapper::ChangeTileFlag(int x, int y, bool set)
{
	if (engineTools != nullptr)
	{
		LockOnDemand(true);
		engineTools->SetTileFlags(x, y, set);
		UnlockOnDemand();
	}
}

void EditorWrapper::UnlockOnDemand()
{
	if (!isLockingExternal)
	{
		engineTools->UnlockEngine();
	}
}

void EditorWrapper::LoadMap(System::String ^ filename)
{
	if (System::String::IsNullOrEmpty(filename))
	{
		return;
	}

	if (engineTools != nullptr)
	{
		std::string strFilename = MarshalNetToStdString(filename);

		LockOnDemand(true);
		engineTools->LoadMap(strFilename.c_str());
		UnlockOnDemand();
	}
}

bool EditorWrapper::GetTileAtPos(int mouseX, int mouseY, int % indexX, int % indexY)
{
	if (engineTools != nullptr)
	{
		int x = 0, y = 0;

		LockOnDemand(false);
		bool bHitTile = engineTools->IntersectScreenRayWithWorld(mouseX, mouseY, x, y);
		UnlockOnDemand();

		indexX = x;
		indexY = y;

		return bHitTile;
	}

	return false;
}

void EditorWrapper::ChangeTile(int x, int y, float uvs, float uvt)
{
	if (engineTools != nullptr)
	{
		LockOnDemand(true);
		engineTools->ChangeTileUVs(x, y, uvs, uvt);
		UnlockOnDemand();
	}
}

void EditorWrapper::ReloadAtlas(array<System::Byte> ^ atlas, array<System::Byte> ^ normalAtlas, int width, int height)
{
	if (engineTools != nullptr)
	{
		pin_ptr<unsigned char> base = &atlas[0];
		pin_ptr<unsigned char> baseNormal = &normalAtlas[0];

		LockOnDemand(true);
		engineTools->ReplaceWorldTextureAtlas(base, baseNormal, width, height);
		UnlockOnDemand();
	}
}

void EditorWrapper::MouseEnter()
{
}

void EditorWrapper::MouseLeave()
{
}

void EditorWrapper::MouseDown(System::Windows::Forms::MouseEventArgs ^ args)
{
	if (engineTools != nullptr)
	{
		if (args->Button == System::Windows::Forms::MouseButtons::Left)
		{
			int x, y;

			engineTools->LockEngine();
			bool bHitTile = engineTools->IntersectScreenRayWithWorld(args->X, args->Y, x, y);
			engineTools->UnlockEngine();

			if (bHitTile)
			{
				TileSelected(x, y);
			}
		}
	}
}

void EditorWrapper::MouseUp(System::Windows::Forms::MouseEventArgs ^ args)
{
}

void EditorWrapper::OnKeyUp(System::Windows::Forms::KeyEventArgs ^ args)
{
	SDL_Keycode sdlKey = GetSdlKey(args->KeyCode);

	if (sdlKey != SDLK_UNKNOWN)
	{
		engineTools->LockEngine(true);
		engineTools->OnKeyUp(sdlKey);
		engineTools->UnlockEngine();
	}
}

void EditorWrapper::OnKeyDown(System::Windows::Forms::KeyEventArgs ^ args)
{
	SDL_Keycode sdlKey = GetSdlKey(args->KeyCode);

	if (sdlKey != SDLK_UNKNOWN)
	{
		engineTools->LockEngine(true);
		engineTools->OnKeyDown(sdlKey);
		engineTools->UnlockEngine();
	}
}

SDL_Keycode EditorWrapper::GetSdlKey(System::Windows::Forms::Keys code)
{
	switch (code)
	{
	case System::Windows::Forms::Keys::Down:
		return SDLK_DOWN;

	case System::Windows::Forms::Keys::Up:
		return SDLK_UP;

	case System::Windows::Forms::Keys::Right:
		return SDLK_RIGHT;

	case System::Windows::Forms::Keys::Left:
		return SDLK_LEFT;

	case System::Windows::Forms::Keys::ControlKey:
	case System::Windows::Forms::Keys::ShiftKey:
		return SDLK_LSHIFT;
	}

	return SDLK_UNKNOWN;
}
