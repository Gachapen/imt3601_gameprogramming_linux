#include "pch.h"
#include "gui/rect.h"

using namespace GUI;

Rect::Rect(int x, int y, int w, int h)
{
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
}

Rect::Rect() : x(0), y(0), w(0), h(0)
{
}

bool Rect::Contains(int pointX, int pointY)
{
	if (pointX < x) return false;
	if (pointY < y) return false;
	if (pointX >= x + w) return false;
	if (pointY >= y + h) return false;

	return true;
}

int &Rect::operator[](const int idx)
{
	switch (idx)
	{
	default:
		Assert(0);
	case 0:
		return x;
	case 1:
		return y;
	case 2:
		return w;
	case 3:
		return h;
	}
}
