#include "pch.h"
#include "gui\button.h"

using namespace GUI;

void Button::SetRect(Rect rect)
{
	Control::SetRect(rect);

	label->SetPos(rect.w / 2, (rect.h / 2) - SCALE(12));
}

Button::Button() : Panel(), LOnEnter(this), LOnLeave(this)
{
	label = new Label();
	label->color = Color(1, 1, 1, 1);
	AddChild(label);

	hoverColor = Color(1, 0, 0, 1);

	Enter += &LOnEnter;
	Leave += &LOnLeave;
}

Button::~Button()
{
}

void Button::OnLeave(Control *ctrl)
{
	//color = normalColor;
	borderColor = normalColor;
}

void Button::OnEnter(Control *ctrl)
{
	normalColor = borderColor;
	//color = hoverColor;
	borderColor = hoverColor;
}
