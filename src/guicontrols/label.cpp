#include "pch.h"
#include "gui/label.h"

using namespace GUI;

const int FONT_HEIGHT = 50;	// TODO use correct fontsize height

Label::Label() : color(0, 0, 0, 1.0f), text(nullptr), alignment(Alignment::Center)
{
	Rect rect = Rect(0, 0, 0, FONT_HEIGHT);

	originalPos = rect;
	SetRect(rect);

	SetInputEnabled(false);
	SetFont("default_big");
	SetText(" ");
}

Label::~Label()
{
	delete []text;
}

void Label::SetAlignment(Alignment alignment)
{
	this->alignment = alignment;
}

void Label::SetPos(int x, int y)
{
	originalPos.x = x;
	originalPos.y = y;

	if (alignment == Alignment::Center)
	{
		SetRect(Rect(x - (rect.w / 2), y, rect.w, rect.h));
	}
	else
	{
		SetRect(Rect(x, y, rect.w, rect.h));
	}
}

void Label::SetText(const char *text)
{
	delete []this->text;

	this->text = G_StrCreateCopy(text);

	Resize(renderContext->GetScreenWidth(), renderContext->GetScreenHeight());
}

void Label::Resize(int width, int height)
{
	fontManager->SetDrawTextFont(font);
	rect.w = fontManager->GetTextWidth(text);
	SetPos(originalPos.x, originalPos.y);	//Readjust pos
}

void Label::SetFont(const char *fontName)
{
	font = fontManager->GetFont(fontName);
}

void Label::DrawSelf()
{
	fontManager->SetDrawTextFont(font);

	fontManager->SetDrawTextColor(color.r, color.g, color.b, color.a);
	fontManager->SetDrawTextPos(0, 0);

	fontManager->DrawTextString(text);
}
