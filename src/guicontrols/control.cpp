#include "pch.h"
#include "gui/control.h"
#include <iostream>

using namespace GUI;
using namespace std;

Control::Control(Rect rect)
{
	Init();
	SetRect(rect);
}

Control::Control()
{
	Init();
	SetRect(Rect(0, 0, 0, 0));
}

void Control::Init()
{
	visible = true;
	parent = nullptr;
	inputEnabled = true;
}

Control::~Control()
{
	for (auto child : children)
	{
		delete child;
	}
}

void Control::Resize(int width, int height)
{
	for (auto child : children)
	{
		child->Resize(width, height);
	}
}

Rect Control::GetRect()
{
	return rect;
}

void Control::Update()
{
	if (!visible)
	{
		return;
	}

	UpdateSelf();
	for (auto child : children)
	{
		child->Update();
	}
}

const std::list<Control *> &Control::GetAllChildren()
{
	return children;
}

Control *Control::GetFocusPanel(int x, int y)
{
	if (!IsVisible()
		|| !rect.Contains(x, y)
		|| !IsInputEnabled())
	{
		return nullptr;
	}

	Control *focus = this;

	for (auto child : children)
	{
		if (child)
		{
			Control *childFocus = child->GetFocusPanel(x - rect.x, y - rect.y);

			if (childFocus != nullptr)
			{
				focus = childFocus;
			}
		}
	}

	return focus;
}

bool Control::IsInputEnabled() const
{
	return inputEnabled;
}

Rect Control::GetRecursiveRectPos()
{
	Rect rect = Rect(0, 0, this->rect.w, this->rect.h);
	Control *ctrl = this;

	while (ctrl != nullptr)
	{
		rect.x += ctrl->rect.x;
		rect.y += ctrl->rect.y;
		ctrl = ctrl->parent;
	}

	return rect;
}

void Control::SetRect(Rect rect)
{
	this->rect = rect;
}

void Control::SetVisible(bool visible)
{
	this->visible = visible;
}

void Control::SetInputEnabled(bool enabled)
{
	inputEnabled = enabled;
}

void Control::Draw()
{
	if (!IsVisible())
	{
		return;
	}

	Rect rect = GetRecursiveRectPos();
	graphics->Push2DViewport(rect.x, rect.y, rect.w, rect.h);

	DrawSelf();
	for (auto child : children)
	{
		child->Draw();
	}

	graphics->Pop2DViewport();
}

void Control::AddChild(Control *child)
{
	if (LIST_CONTAINS(children, child))
	{
		return;
	}

	children.push_back(child);
	child->parent = this;
}

void Control::RemoveChild(Control *child)
{
	children.remove(child);
	child->parent = nullptr;
}

void Control::DrawSelf()
{
}

void Control::UpdateSelf()
{
}
