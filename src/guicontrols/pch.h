#ifndef PCH_H
#define PCH_H

#include <string>
#include <SDL.h>

#include "util/stringutil.h"

#include "appinterface/iappinterface.h"
#include "materialsystem/irendercontext.h"
#include "materialsystem\imaterialdict.h"
#include "materialsystem\imaterial.h"

#include "engine/iinput.h"

#include "gui/gui.h"
#include "gui/ifontmanager.h"
#include "gui/igraphics.h"

namespace GUI
{
	extern IRenderContext *renderContext;
	extern IFontManager *fontManager;
	extern IGraphics *graphics;
	extern IInput *input;
}

#endif