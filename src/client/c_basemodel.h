#ifndef C_BASEMODEL_H
#define C_BASEMODEL_H


class C_BaseModel : public ClientEntity
{
	DECLARE_CLASS(C_BaseModel, ClientEntity);
public:

	C_BaseModel();

	virtual void Simulate();

	virtual void Render();

	virtual void SetOrigin(const vec3 &origin);

	virtual IMDXRenderable *GetModel();
	virtual void SetModel(IMDXRenderable *renderable);
	virtual void SetModel(const char *name);

	virtual bool GetRenderBounds(vec3 &min, vec3 &max);

	virtual float GetScale() const { return 1.0f; }

protected:
	~C_BaseModel();

	virtual void OnRelease();

private:
	void ReleaseModel();

	IMDXRenderable *renderable;
};


#endif