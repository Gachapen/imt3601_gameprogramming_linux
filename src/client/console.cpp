#include "pch.h"
#include "console.h"

Console::Console(void)
	: textInputEnabled(false)
	, curAutoCompleteIndex(-1)
{
	input->AddKeyListener(this);
	InitComponents();
	InvokeResize();
	SetVisible(false);
}

Console::~Console(void)
{
	input->RemoveKeyListener(this);
}

void Console::InitComponents()
{
	text = new GUI::Label();
	text->SetAlignment(GUI::Alignment::Left);

	G_StrSnPrintf(curCmd, sizeof(curCmd), "");
	text->SetText(curCmd);

	color = GUI::Color(1, 1, 1, 1);
	borderColor = GUI::Color(0, 0, 0, 1);

	AddChild(text);
}

bool Console::OnKeyDown(SDL_Keycode sdlKey)
{
	if (!visible)
	{
		return false;
	}

	if (sdlKey == SDLK_RETURN)	//Send
	{
		cvar->ExecuteBuffer(curCmd);
		*curCmd = 0;

		ResetAutoComplete();
	}
	else if (sdlKey == SDLK_BACKSPACE)	//del last
	{
		int len = G_StrLen(curCmd);
		if (len > 0)
		{
			curCmd[len - 1] = '\0';
		}

		ResetAutoComplete();
	}
	else if (sdlKey == SDLK_TAB)
	{
		if (curAutoComplete.empty())
		{
			cvar->GetCommandsStartingWith(curCmd, curAutoComplete);

			curAutoCompleteIndex = 0;
		}

		if (!curAutoComplete.empty())
		{
			if (curAutoCompleteIndex >= int(curAutoComplete.size()))
			{
				curAutoCompleteIndex = 0;
			}

			G_StrNCpy(curCmd, curAutoComplete[curAutoCompleteIndex].c_str(), sizeof(curCmd));
			curAutoCompleteIndex++;
		}
	}
	else if (sdlKey == input->GetKeyOfBinding("gui_toggleconsole"))
	{
		cvar->ExecuteBuffer("gui_toggleconsole"); //closes the console
	}

	text->SetText(curCmd);
	return true;
}

bool Console::OnKeyUp(SDL_Keycode sdlKey)
{
	if (!visible)
	{
		return false;
	}

	return true;
}

void Console::OnTextEvent(const char *text)
{
	const int len = G_StrLen(curCmd);

	if (len < ARRAYSIZE(curCmd) - 1)
	{
		G_StrNCat(curCmd, sizeof(curCmd), text);
	}

	this->text->SetText(curCmd);

	ResetAutoComplete();
}

void Console::SetVisible(bool visible)
{
	BaseClass::SetVisible(visible);

	if (visible)
	{
		textInputEnabled = true;
		input->StartTextInput(dynamic_cast<IInputKeyListener*>(this));
	}
	else if (textInputEnabled)
	{
		textInputEnabled = false;
		input->StopTextInput();
	}
}

void Console::SetPositions(int width, int height)
{
	SetRect(GUI::Rect(0, 0, renderContext->GetScreenWidth(), SCALE(25)));
	text->SetPos(SCALE(5), 0);
}

void Console::ResetAutoComplete()
{
	curAutoCompleteIndex = -1;
	curAutoComplete.clear();
}