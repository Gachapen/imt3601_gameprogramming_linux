#ifndef CLIENTENTITY_H
#define CLIENTENTITY_H



class ClientEntityFactory;

class ClientEntity
{
	friend class ClientEntityFactory;

	DECLARE_CLASS_NOBASE(ClientEntity);
	DECLARE_NETWORKTABLE();
public:
	ClientEntity();

	void Release();

	int GetNetworkId() const;

	virtual void OnSpawn();

	virtual void Simulate();

	virtual void Render();

	bool IsClientCreated() const;

	virtual bool IsPlayer() const { return false; }
	virtual bool IsCreep() const { return false; }
	virtual bool IsTower() const { return false; }

	virtual const vec3 &GetOrigin();
	virtual void SetOrigin(const vec3 &origin);

	virtual void SetRenderAngles(const euler &angles);

	virtual const vec3 &GetRenderOrigin();
	virtual const vec3 &GetRenderAngles();

	virtual bool GetRenderBounds(vec3 &min, vec3 &max);

	virtual bool IsTranslucent() const { return false; }
	virtual bool IsVisible() const;
	virtual void SetVisible(bool visible);

	virtual bool ShouldCastShadow() const { return true; }
	virtual bool ShouldReflect() const { return false; }

	virtual int GetHealth() const;
	virtual int GetHealthMax() const;

	virtual ClientEntity *GetOwner() const;
	virtual Uint16 GetOwnerId() const { return ownerId.Get(); }

protected:
	virtual ~ClientEntity();

	void SetNetworkId(int networkId);

	virtual void OnNetworkedSpawn() {};
	virtual void OnRelease();

private:
	int networkId;

	NetworkVariable<vec3> origin;
	InterpolatedVarNetworked<vec3> interpolatedOrigin;

	NetworkVariable<Uint8> health;
	NetworkVariable<Uint8> healthMax;
	NetworkVariable<Uint16> ownerId;

	euler angles;
	bool visible;
};

#endif