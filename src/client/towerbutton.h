#ifndef TOWERBUTTON_H
#define TOWERBUTTON_H

#include "pch.h"
#include "gui/button.h"
#include "towerinfo.h"

class TowerButton : public GUI::Button
{
public:
	TowerButton(TowerInfo *tooltip);
	~TowerButton();

	virtual void DrawSelf();
	virtual void UpdateSelf();
	virtual void Resize(int width, int height);

	void SetTowerCost(int cost);
	void SetTowerScript(ResourceTower towerScript);
	ResourceTower GetTowerScript() { return towerScript; }

	virtual void OnEnter(Control *ctrl);
	virtual void OnLeave(Control *ctrl);

	virtual void OnMouseDown(int mouseButton);
private:
	bool enoughGold;
	int cost;
	ResourceTower towerScript;
	TowerInfo *tooltip;
};
#endif
