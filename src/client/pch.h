#ifndef PCH_H
#define PCH_H


#include "util\platform.h"

#pragma warning( disable: 4996 )
#pragma warning( disable: 4530 )

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//#include <fstream>
//#include <direct.h>
//#include <vector>
//#include <unordered_map>
//#include <stack>
#include <memory>

#include <SDL.h>

#include "util/macros.h"
#include "util/util.h"
#include "util/mathutil.h"
#include "util/fileutil.h"
#include "util/singleton.h"
#include "util/referencecounted.h"
#include "util/factory.h"
#include "util/stringutil.h"
#include "util\bytebuffer.h"
#include "util/interpolateutil.h"
#include "util/collisionutil.h"
#include "util/keyvalues.h"

#include <shareddefs.h>

#include "appinterface\iappinterface.h"
#include "materialsystem\materialsystem.h"
#include "materialsystem\irendercontext.h"
#include "materialsystem\imaterialdict.h"
#include "materialsystem\imaterial.h"
#include "materialsystem\itexturedict.h"
#include "materialsystem\itexture.h"
#include "materialsystem\imdxrenderabledict.h"
#include "materialsystem\ifbodict.h"
#include "materialsystem\imesh.h"
#include "materialsystem\meshbuilder.h"
#include "mdxlib\imdxmodeldict.h"
#include "engine\iglobals.h"
#include "engine\iengineclient.h"
#include "engine\iinput.h"
#include "engine\idebugdraw.h"
#include "gui\controls.h"
#include "engine\iworld.h"
#include "engine\ievents.h"
#include "cvar\icvarinterface.h"

#include "shared\damageinfo.h"
#include "shared\buttons.h"
#include "shared\gamesystem.h"
#include "shared\resource.h"
#include "shared\networktable.h"

#include "cliententity.h"
#include "cliententityfactory.h"

#include "gui/gui.h"
#include "gui/ifontmanager.h"
#include "gui/igraphics.h"

extern IRenderContext *renderContext;
extern IMaterialDict *materialDict;
extern ITextureDict *textureDict;
extern IMDXModelDict *modelDict;
extern IMDXRenderableDict *renderableDict;
extern IFBODict *fboDict;
extern IGlobals *globals;
extern IEngineClient *engine;
extern IInput *input;
extern IDebugDraw *debugDraw;
extern ICvarInterface *cvar;
extern IEvents *events;

namespace GUI
{
	extern IGraphics *graphics;
}
#endif