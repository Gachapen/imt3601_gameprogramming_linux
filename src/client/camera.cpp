#include "pch.h"
#include "camera.h"
#include "clientinput.h"


SINGLETON_INSTANCE(Camera);


Camera::Camera()
	: lastMouseX(-1)
	  , lastMouseY(-1)
	  , zNear(1.0f)
	  , zFar(3000.0f)
	  , fov(90.0f)
{
	origin = vec3::Zero();
	originMove = vec3::Zero();
	originDesired = vec3::Zero();

	angles = euler::Zero();
	viewMatrix = mat4::Identity();
	reflectionViewMatrix = mat4::Identity();
	projectionMatrix = mat4::Identity();

	originDesired.x() += 500;

	angles.x() = -55.0f;
	angles.y() = 180;

	origin = originDesired;

	distanceDesired = 900.0f;
	distance = distanceDesired;
}

Camera::~Camera()
{
}

const mat4 &Camera::GetViewMatrix() const
{
	return viewMatrix;
}

const mat4 &Camera::GetReflectionViewMatrix() const
{
	return reflectionViewMatrix;
}

const mat4 &Camera::GetProjectionMatrix() const
{
	return projectionMatrix;
}

const vec3 &Camera::GetOrigin() const
{
	return origin;
}

const euler &Camera::GetAngles() const
{
	return angles;
}

const float &Camera::GetDistance() const
{
	return distance;
}

const float &Camera::GetNearZ() const
{
	return zNear;
}

const float &Camera::GetFarZ() const
{
	return zFar;
}

const float &Camera::GetFOV() const
{
	return fov;
}

const Plane *Camera::GetFrustum() const
{
	return frustumPlanes;
}

inline vec3 VectorReflect(const vec3 &vector, const vec3 &normal)
{
	float dot = vector.dot(normal);

	return vector - normal * 2 * dot;
}

void Camera::Update(float frametime)
{
	const float cameraSpeed = frametime * (ClientInput::GetInstance()->IsButtonDown(Buttons::SPEED)
										   ? 2500.0f : 1000.0f);
	const float approachSpeed = frametime * 10.0f;

	originDesired.x() += cameraSpeed * ClientInput::GetInstance()->IsButtonDown(Buttons::DOWN);
	originDesired.x() -= cameraSpeed * ClientInput::GetInstance()->IsButtonDown(Buttons::UP);
	originDesired.y() += cameraSpeed * ClientInput::GetInstance()->IsButtonDown(Buttons::RIGHT);
	originDesired.y() -= cameraSpeed * ClientInput::GetInstance()->IsButtonDown(Buttons::LEFT);

	if (!engine->IsInToolsMode() &&
		renderContext->IsFullscreen())
	{
		SimulateMouseCamera();
	}

	originMove.z() = 0.0f;

	vec3 delta = originDesired - originMove;

	if (delta.squaredNorm() > 2.0f)
	{
		originMove += delta * MIN(1.0f, approachSpeed);
	}

	viewMatrix = mat4::Identity();
	reflectionViewMatrix = mat4::Identity();

	distanceDesired += 100 * ClientInput::GetInstance()->IsButtonDown(Buttons::ZOOM_IN);
	distanceDesired -= 100 * ClientInput::GetInstance()->IsButtonDown(Buttons::ZOOM_OUT);

	float deltaDistance = distanceDesired - distance;

	if ((deltaDistance * deltaDistance) > 2.0f)
	{
		distance += deltaDistance * MIN(1.0f, approachSpeed);
	}
	else
	{
		distance = distanceDesired;
	}

	originMove.z() = distance;

	origin = originMove - vec3((900.0f - distance) * 0.3f, 0, 0);

	MatrixSetRotation(angles, viewMatrix);
	viewMatrix = translate(viewMatrix, origin);

	MatrixSetRotation(euler(-angles.x(), angles.y(), angles.z() + 180), reflectionViewMatrix);
	reflectionViewMatrix = translate(reflectionViewMatrix, vec3(origin.x(), origin.y(), -origin.z() - 15.0f));

	fov = 45.0f + (Camera::GetInstance()->GetDistance()) * 0.01f;
	const float ratio = renderContext->GetScreenWidth() / float(renderContext->GetScreenHeight());

	projectionMatrix = perspective(fov, ratio, zNear, zFar);

	UpdateFrustum();
}

void Camera::SimulateMouseCamera()
{
	const int mx = input->GetMousePosX();
	const int my = input->GetMousePosY();

	const int sw = renderContext->GetScreenWidth();
	const int sh = renderContext->GetScreenHeight();

	const float moveRange = 75;
	const float moveRangeLarge = 150;
	const float cameraSpeed = globals->GetFrametime() * 1200.0f;

	float moveVertical = RemapValClamped(float(my), 0.0f, moveRange, -1.0f, 0.0f);

	moveVertical += RemapValClamped(float(my), float(sh - moveRange), float(sh), 0.0f, 1.0f);
	float moveHorizontal = RemapValClamped(float(mx), 0.0f, moveRange, -1.0f, 0.0f);
	moveHorizontal += RemapValClamped(float(mx), float(sw - moveRange), float(sw), 0.0f, 1.0f);

	if (moveVertical != 0.0f)
	{
		if (lastMouseX < 0)
		{
			lastMouseX = mx;
		}
		else
		{
			int xDelta = mx - lastMouseX;
			lastMouseX = mx - int(CLAMP(xDelta, -moveRangeLarge, moveRangeLarge));

			float extraHorizontal = RemapValClamped(float(xDelta), -moveRangeLarge, moveRangeLarge, -1.0f, 1.0f);

			moveHorizontal += extraHorizontal * (1.0f - abs(moveHorizontal));
		}
	}
	else
	{
		lastMouseX = -1;
	}

	if (moveHorizontal != 0.0f)
	{
		if (lastMouseY < 0)
		{
			lastMouseY = my;
		}
		else
		{
			int yDelta = my - lastMouseY;
			lastMouseY = my - int(CLAMP(yDelta, -moveRangeLarge, moveRangeLarge));

			float extraVertical = RemapValClamped(float(yDelta), -moveRangeLarge, moveRangeLarge, -1.0f, 1.0f);
			moveVertical += extraVertical * (1.0f - abs(moveVertical));
		}
	}
	else
	{
		lastMouseY = -1;
	}

	moveHorizontal = Sign(moveHorizontal) * moveHorizontal * moveHorizontal;
	moveVertical = Sign(moveVertical) * moveVertical * moveVertical;

	originDesired.y() += moveHorizontal * cameraSpeed;
	originDesired.x() += moveVertical * cameraSpeed;
}

void Camera::UpdateFrustum()
{
	vec3 fwd, left, up;

	AngleVectors(angles, &fwd, &left, &up);

	const float ratio = renderContext->GetScreenWidth() / float(renderContext->GetScreenHeight());

	float hfar = 2.0f * tan(DEG2RAD(fov) / 2.0f) * zFar;
	float wfar = hfar * ratio;

	vec3 far_leftTop = origin + fwd * zFar + left * wfar * 0.5f + up * hfar * 0.5f;
	vec3 far_rightTop = far_leftTop - left * wfar;
	vec3 far_leftBottom = far_leftTop - up * hfar;
	vec3 far_rightBottom = far_leftBottom - left * wfar;

	// planes facing inwards
	//frustumPlanes[0].Init(fwd, origin.dot(fwd) + zNear);
	//frustumPlanes[1].Init(-fwd, origin.dot(-fwd) - zFar);

	//// left
	//frustumPlanes[2].normal = (origin - far_leftTop).cross(far_leftBottom - far_leftTop);
	//frustumPlanes[2].normal.normalize();
	//frustumPlanes[2].distance = origin.dot(frustumPlanes[2].normal);

	//// right
	//frustumPlanes[3].normal = (origin - far_rightBottom).cross(far_rightTop - far_rightBottom);
	//frustumPlanes[3].normal.normalize();
	//frustumPlanes[3].distance = origin.dot(frustumPlanes[3].normal);

	//// top
	//frustumPlanes[4].normal = (origin - far_rightTop).cross(far_leftTop - far_rightTop);
	//frustumPlanes[4].normal.normalize();
	//frustumPlanes[4].distance = origin.dot(frustumPlanes[4].normal);

	//// bottom
	//frustumPlanes[5].normal = (origin - far_leftBottom).cross(far_rightBottom - far_leftBottom);
	//frustumPlanes[5].normal.normalize();
	//frustumPlanes[5].distance = origin.dot(frustumPlanes[5].normal);

	// planes facing outwards
	frustumPlanes[0].Init(-fwd, origin.dot(-fwd) - zNear);
	frustumPlanes[1].Init(fwd, origin.dot(fwd) + zFar);

	// left
	frustumPlanes[2].normal = (origin - far_leftBottom).cross(far_leftTop - far_leftBottom);
	frustumPlanes[2].normal.normalize();
	frustumPlanes[2].distance = origin.dot(frustumPlanes[2].normal);

	// right
	frustumPlanes[3].normal = (origin - far_rightTop).cross(far_rightBottom - far_rightTop);
	frustumPlanes[3].normal.normalize();
	frustumPlanes[3].distance = origin.dot(frustumPlanes[3].normal);

	// top
	frustumPlanes[4].normal = (origin - far_leftTop).cross(far_rightTop - far_leftTop);
	frustumPlanes[4].normal.normalize();
	frustumPlanes[4].distance = origin.dot(frustumPlanes[4].normal);

	// bottom
	frustumPlanes[5].normal = (origin - far_rightBottom).cross(far_leftBottom - far_rightBottom);
	frustumPlanes[5].normal.normalize();
	frustumPlanes[5].distance = origin.dot(frustumPlanes[5].normal);
}
