
#include "pch.h"
#include "cliententity.h"

IMPLEMENT_NETWORKTABLE(ClientEntity)
	NETWORK_VARIABLE(origin);
	NETWORK_VARIABLE(health);
	NETWORK_VARIABLE(healthMax);
	NETWORK_VARIABLE(ownerId);
END_NETWORKTABLE();


ClientEntity::ClientEntity()
	: networkId(-1)
	, interpolatedOrigin(&origin)
	, angles(euler::Zero())
	, visible(true)
{
	this->networkId = networkId;
}

ClientEntity::~ClientEntity()
{
}

void ClientEntity::SetNetworkId(int networkId)
{
	Assert(networkId >= 0 && networkId < NETWORKED_ENTITY_COUNT);

	this->networkId = networkId;
}

void ClientEntity::Release()
{
	OnRelease();

	ClientEntityFactory::GetInstance()->OnEntityRemoved(this);

	delete this;
}

void ClientEntity::OnRelease()
{
}

int ClientEntity::GetNetworkId() const
{
	return networkId;
}

void ClientEntity::OnSpawn()
{
}

void ClientEntity::Simulate()
{
	interpolatedOrigin.Interpolate(globals->GetTime());
}

void ClientEntity::Render()
{
}

bool ClientEntity::IsClientCreated() const
{
	return networkId < 0;
}

const vec3 &ClientEntity::GetOrigin()
{
	if (IsClientCreated())
		return origin.Get();
	else
		return interpolatedOrigin.GetHead();
}

void ClientEntity::SetOrigin(const vec3 &origin)
{
	this->origin.Set(origin);
}

void ClientEntity::SetRenderAngles(const euler &angles)
{
	this->angles = angles;
}

const vec3 &ClientEntity::GetRenderOrigin()
{
	if (IsClientCreated())
		return origin.Get();
	else
		return interpolatedOrigin.GetValue();
}

const vec3 &ClientEntity::GetRenderAngles()
{
	return angles;
}

bool ClientEntity::GetRenderBounds(vec3 &min, vec3 &max)
{
	return false;
}

int ClientEntity::GetHealth() const
{
	return health.Get();
}

int ClientEntity::GetHealthMax() const
{
	return healthMax.Get();
}

bool ClientEntity::IsVisible() const
{
	return visible;
}

void ClientEntity::SetVisible(bool visible)
{
	this->visible = visible;
}

ClientEntity *ClientEntity::GetOwner() const
{
	const Uint16 index = ownerId.Get();

	if (index < 0xFFFF)
	{
		return ClientEntityFactory::GetInstance()->GetNetworkEntityByIndex(index);
	}
	else
	{
		return nullptr;
	}
}