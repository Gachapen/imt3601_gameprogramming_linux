#ifndef GAMEHUD_H
#define GAMEHUD_H

#include "pch.h"
#include "gui/layoutpanel.h"
#include "towerinfo.h"
#include "towerselectionwindow.h"

class GameHud : public GUI::LayoutPanel
{
public:
	GameHud();
	virtual ~GameHud();

	virtual void DrawSelf();
	virtual void UpdateSelf();
	virtual void OnMouseDown(int mouseButton);

	virtual void InitComponents();
	virtual void SetPositions(int width, int height);

	void OnTowerButtonClick(Control *ctrl);
	LISTENER(GameHud, OnTowerButtonClick, Control *);

	void DrawHealthbars();
private:
	void DeselectTower();

	std::vector<GUI::Button *> towerButtons;
	GUI::Panel *pTowerShop;

	TowerSelectionWindow *towerSelectionWindow;

	GUI::Panel *pInfo;
	GUI::Panel *pGold;
	GUI::Label *lGold;

	int buildSelection;
	int selectedTower;

	int lastGold;

	void TowerSelection();
};
#endif
