
#include "pch.h"
#include "cliententityfactory.h"


SINGLETON_INSTANCE(ClientEntityFactory);

static bool SortNetworkMember(INetworkMember *left, INetworkMember *right)
{
	return left->GetIndex() < right->GetIndex();
}

static struct RegisterEntityFactory
	: public function_register_template<ClientEntityFactory::CreateEntityFunc, RegisterEntityFactory>
{
} firstEntityFunction;


ClientEntityFactory::ClientEntityFactory()
{
	memset(networkEntities, 0, sizeof(networkEntities));
}

bool ClientEntityFactory::Init()
{
	ConvertFactoryListToMap(&firstEntityFunction, functions);

	return true;
}

void ClientEntityFactory::Shutdown()
{
	FreeAllEntities();
}

void ClientEntityFactory::Update(float frametime)
{
	for (auto itr : activeEntities)
	{
		itr->Simulate();
	}
}

void ClientEntityFactory::OnLevelShutdown()
{
	FreeAllEntities();
}

void ClientEntityFactory::RegisterEntityFactory(const char *name, CreateEntityFunc function)
{
	RegisterFactory(&firstEntityFunction, function, name);
}

ClientEntity *ClientEntityFactory::CreateClientEntity(const char *className, int networkId)
{
	auto itr = functions.find(className);

	if (itr == functions.end())
	{
		Assert(0);
		return nullptr;
	}

	const bool isNetworked = networkId >= 0;

	ClientEntity *newEntity = itr->second();

	if (isNetworked)
	{
		newEntity->SetNetworkId(networkId);
	}

	newEntity->RegisterNetworkMembers();

	if (isNetworked)
	{
		networkEntities[networkId] = newEntity;
	}

	activeEntities.push_back(newEntity);

	newEntity->OnSpawn();

	return newEntity;
}

void ClientEntityFactory::OnEntityRemoved(ClientEntity *entity)
{
	const int networkId = entity->GetNetworkId();

	Assert(networkId == -1
		|| networkEntities[networkId] == entity);

	Assert(VECTOR_CONTAINS(activeEntities, entity));
	VECTOR_REMOVE(activeEntities, entity);

	if (networkId >= 0)
	{
		networkEntities[networkId] = nullptr;
	}
}

void ClientEntityFactory::FreeAllEntities()
{
	auto tempEntities = activeEntities;

	for (auto e : tempEntities)
	{
		e->Release();
	}

	memset(networkEntities, 0, sizeof(networkEntities));

	Assert(activeEntities.size() == 0);
}

void ClientEntityFactory::ApplyNetworkSymbolDefinitions(ByteBuffer &buffer)
{
	networkTableDefinitions.clear();

	while (buffer.ReadBool())
	{
		NetworkTableDefinition table;
		buffer.ReadString(table.className, sizeof(table.className));

		int networkMemberCount = buffer.ReadUChar();

		ClientEntity *entity = nullptr;
		auto itr = functions.find(table.className);

		Assert(itr != functions.end());
		if (itr != functions.end())
		{
			entity = itr->second();
			entity->RegisterNetworkMembers();
		}

		for (int receivedVariable = 0; receivedVariable < networkMemberCount; receivedVariable++)
		{
			char networkMemberName[MAX_NETWORKMEMBER_LENGTH];

			buffer.ReadString(networkMemberName, sizeof(networkMemberName));

			if (entity != nullptr)
			{
				bool found = false;

				for (int i = 0; i < entity->networkTable.networkVariablesCount; i++)
				{
					if (G_StrEq(networkMemberName, entity->networkTable.networkVariablesBase[i]->GetName()))
					{
						table.memberLookup.push_back(i);
						table.memberSize.push_back(entity->networkTable.networkVariablesBase[i]->GetSize());
						table.memberIsDynamic.push_back(entity->networkTable.networkVariablesBase[i]->IsSizeDynamic());

						found = true;
						break;
					}
				}

				Assert(found);
			}
		}

		if (entity != nullptr)
		{
			entity->SetGlobalNetworktableIndex(networkTableDefinitions.size());
		}

		delete entity;

		networkTableDefinitions.push_back(table);
	}
}

void ClientEntityFactory::CreateNetworkEntity(int networkId, Uint8 networkTableIndex, ByteBuffer &buffer)
{
	Assert(networkTableIndex >= 0 && networkTableIndex < networkTableDefinitions.size());
	Assert(networkId >= 0 && networkId < NETWORKED_ENTITY_COUNT);

	if (networkEntities[networkId] != nullptr)
	{
		DestroyNetworkEntity(networkId);
	}

	const NetworkTableDefinition &definition = networkTableDefinitions[networkTableIndex];

	ClientEntity *entity = CreateClientEntity(definition.className, networkId);

	if (entity == nullptr)
	{
		Assert(0);
		return;
	}

	networkEntities[networkId] = entity;

	Assert(entity->networkTable.networkVariablesBase != nullptr);

	// sort the network variables in the client now based on what
	// the server declared on connect
	std::vector<INetworkMember*> sortedMembers;

	Assert(entity->networkTable.networkVariablesCount == definition.memberLookup.size());

	for (int i = 0; i < entity->networkTable.networkVariablesCount; i++)
	{
		int clientIndex = definition.memberLookup[i];

		Assert(clientIndex >= 0
			&& clientIndex < int(entity->networkTable.networkVariables.size()));

		sortedMembers.push_back(entity->networkTable.networkVariablesBase[clientIndex]);
	}

	entity->networkTable.networkVariables.clear();
	entity->networkTable.networkVariables = std::move(sortedMembers);
	entity->networkTable.networkVariablesBase = entity->networkTable.networkVariables.data();

	for (int i = 0; i < entity->networkTable.networkVariablesCount; i++)
	{
		entity->networkTable.networkVariablesBase[i]->SetIndex(i);
		entity->networkTable.networkVariablesBase[i]->ApplyChanges(buffer);
	}

	entity->OnNetworkedSpawn();
}

void ClientEntityFactory::DestroyNetworkEntity(int networkId)
{
	Assert(networkId >= 0 && networkId < NETWORKED_ENTITY_COUNT);

	if (networkEntities[networkId] != nullptr)
	{
		Assert(VECTOR_CONTAINS(activeEntities, networkEntities[networkId]));

		networkEntities[networkId]->Release();

		Assert(!VECTOR_CONTAINS(activeEntities, networkEntities[networkId]));

		networkEntities[networkId] = nullptr;
	}

}

void ClientEntityFactory::ApplyWorldStateUpdates(ByteBuffer &buffer)
{
	int networkId;

	int frame = buffer.ReadInt();
	
	while ((networkId = buffer.ReadUShort()) != 0xFFFF)
	{
		int networkTableIndex = buffer.ReadUChar();

		Assert(networkTableIndex >= 0 && networkTableIndex < (int)networkTableDefinitions.size());

		Uint8 memberIndex;

		const NetworkTableDefinition &table = networkTableDefinitions[networkTableIndex];

		if (networkEntities[networkId] == nullptr
			|| networkEntities[networkId]->GetGlobalNetworktableIndex() != networkTableIndex)
		{
			while ((memberIndex = buffer.ReadUChar()) != 0xFF)
			{
				Assert(memberIndex < table.memberSize.size());

				int size = table.memberSize[memberIndex];

				if (table.memberIsDynamic[memberIndex])
				{
					size *= buffer.ReadUChar();
				}

				buffer.Flush(size);
			}
		}
		else
		{
			while ((memberIndex = buffer.ReadUChar()) != 0xFF)
			{
				Assert(memberIndex < table.memberLookup.size());
				Assert(networkEntities[networkId]->networkTable.networkVariablesBase != nullptr);

				networkEntities[networkId]->networkTable.networkVariablesBase[memberIndex]->ApplyChanges(buffer, frame);
			}
		}
	}
}

void ClientEntityFactory::ApplyWorldStateComplete(ByteBuffer &buffer)
{
	auto tempEntities = activeEntities;

	for (auto e : tempEntities)
	{
		if (!e->IsClientCreated())
		{
			e->Release();
		}
	}

	int networkId;

	while ((networkId = buffer.ReadUShort()) != 0xFFFF)
	{
		int networkTableIndex = buffer.ReadUChar();

		CreateNetworkEntity(networkId, networkTableIndex, buffer);
	}
}