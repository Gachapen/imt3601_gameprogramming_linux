#ifndef C_CREEPBASE_H
#define C_CREEPBASE_H


#include "c_basemodel.h"
#include "shared/creepshared.h"

class C_CreepBase : public C_BaseModel
{
	DECLARE_CLASS(C_CreepBase, C_BaseModel);
	DECLARE_NETWORKTABLE_BASE();
public:

	C_CreepBase();
	
	virtual bool IsCreep() const { return true; }

	virtual void OnSpawn();

	virtual void Simulate();

	virtual void Render();

	virtual const vec3 &GetRenderAngles();
	virtual const vec3 &GetRenderOrigin();
	virtual float GetScale() const { return scale.Get(); }

protected:
	~C_CreepBase();

private:
	virtual void OnRelease();

	void UpdateTileChange();

	vec3 lastRenderOrigin;
	vec3 elevatedOrigin;
	float elevationHeight;

	float yaw;
	float yawDesired;
	float speed;

	euler animatedAngles;

	NetworkVariable<Uint8> creepState;
	DECLARE_NETWORKVAR_CALLBACK(CreepStateChanged, Uint8);

	void BloodDecalCallback(const vec3 &origin);
	DECLARE_NETWORKPROCEDURE_1PARAM(vec3, BloodDecal);

	NetworkVariable<float> scale;
	NetworkVariable<float> maxSpeed;
	NetworkVariable<vec3> color;

	int lastTileIndex;
};


#endif