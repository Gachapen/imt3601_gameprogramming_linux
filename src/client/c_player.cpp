
#include "pch.h"
#include "c_player.h"
#include "client.h"
#include "clientgame.h"
#include "view.h"
#include "engine/iworld.h"

REGISTER_CLIENTENTITY_CLASS(player, C_Player);

IMPLEMENT_NETWORKTABLE_BASE(C_Player)
NETWORK_VARIABLE(gold);
//NETWORK_PROCEDURE(BloodDecal);
END_NETWORKTABLE();

C_Player *C_Player::localClientInstance;

C_Player::C_Player()
	//: lastRenderOrigin(vec3::Zero())
	//  , yaw(0.0f)
	//  , yawDesired(0.0f)
	//  , speed(0.0f)
	//  , animatedAngles(euler::Zero())
	//  , CreepStateChanged(this, &C_CreepBase::OnCreepStateChanged)
	//  , elevationHeight(0.0f)
	//  , lastTileIndex(-1)
	//  , BloodDecal(this, &C_CreepBase::BloodDecalCallback)
{
	//creepState.AddCallback(&CreepStateChanged);
}

C_Player::~C_Player()
{
	if (localClientInstance == this)
	{
		localClientInstance = nullptr;
	}
}

C_Player *C_Player::GetLocalPlayer()
{
	return localClientInstance;
}

Uint16 C_Player::GetGold() const
{
	return gold.Get();
}

void C_Player::OnNetworkedSpawn()
{
	BaseClass::OnNetworkedSpawn();

	if (GetNetworkId() == Client::GetInstance()->GetLocalClientIndex())
	{
		localClientInstance = this;
	}
}