#include "pch.h"
#include "statswindow.h"
#include "c_creepbase.h"
#include "c_towerbase.h"

using namespace GUI;

StatsWindow::StatsWindow()
{
	SetInputEnabled(false);

	InitComponents();
	InvokeResize();
	SetVisible(false);
}

StatsWindow::~StatsWindow()
{
}

void StatsWindow::UpdateSelf()
{
	int creepCount = 0;
	int towerCount = 0;
	auto entities = ClientEntityFactory::GetInstance()->GetEntities();

	for (auto e : entities)
	{
		C_CreepBase *creep = dynamic_cast<C_CreepBase *>(e);
		if (creep != nullptr)
		{
			creepCount++;
			continue;
		}

		C_TowerBase *tower = dynamic_cast<C_TowerBase *>(e);
		if (tower != nullptr)
		{
			towerCount++;
			continue;
		}
	}

	char text[255];
	G_StrSnPrintf(text, sizeof(text), "Creep count: %i", creepCount);
	lUnitCount->SetText(text);

	//FPS
	static float accum = 0;
	static int frames = 0;
	accum += graphics->GetFrametime();
	frames++;

	if (accum >= 1)
	{
		float fps = frames / accum;
		char fpsText[255];
		G_StrSnPrintf(fpsText, sizeof(fpsText), "FPS: %.1f", fps);
		lTowerCount->SetText(fpsText);

		accum = 0;
		frames = 0;
	}
}

void StatsWindow::SetPositions(int width, int height)
{
	Rect rect;

	rect.x = 0;
	rect.y = SCALE(30);
	rect.w = width;
	rect.h = height;
	SetRect(rect);

	lTowerCount->SetPos(0, 0);
	lUnitCount->SetPos(0, (int)(height * 0.05f));
}

void StatsWindow::InitComponents()
{
	color = Color(0, 0, 0, 0);

	lTowerCount = new Label();
	lTowerCount->color = Color(1, 0, 0, 1);

	lUnitCount = new Label();
	lUnitCount->color = Color(1, 0, 0, 1);

	AddChild(lTowerCount);
	AddChild(lUnitCount);
}
