#include "pch.h"
#include "towerselectionwindow.h"

using namespace GUI;

TowerSelectionWindow::TowerSelectionWindow(void)
	: LOnTowerUpgradeClick(this)
	, towerId(-1)
{
	InitComponents();
	InvokeResize();
	SetVisible(false);
}

TowerSelectionWindow::~TowerSelectionWindow(void)
{
}

void TowerSelectionWindow::SetTowerScript(ResourceTower towerScript)
{
	this->towerScript = towerScript;
	info->Clear();
	info->SetTowerScript(towerScript);
	info->AddStat(TowerInfoStat::Damage);
	info->AddStat(TowerInfoStat::Range);

	pTowerIcon->SetMaterial(materialDict->FindMaterial(towerScript.icon.c_str()));

	if (upgrades.size() > 0)
	{
		for (auto towerButton : upgrades)
		{
			RemoveChild(towerButton);
			delete towerButton;
		}
	}
	upgrades.clear();

	for (auto upgrade : towerScript.upgrades)
	{
		TowerInfo *tooltip = new TowerInfo();
		tooltip->SetTowerUpgrade(upgrade);

		TowerButton *btn = new TowerButton(tooltip);
		btn->SetTowerScript(Resources::GetInstance()->GetTowers()[upgrade.id]);
		btn->SetTowerCost(upgrade.cost);
		btn->Click += LOnTowerUpgradeClick;

		tooltip->AddStat(TowerInfoStat::UpgradeCost);
		tooltip->AddStat(TowerInfoStat::UpgradeDuration);
		tooltip->AddStat(TowerInfoStat::Damage);
		tooltip->AddStat(TowerInfoStat::Range);

		AddChild(btn);
		upgrades.push_back(btn);
	}

	lUpgrade->SetVisible(upgrades.size() > 0);

	//Sell button
	Label *sellText = new Label();
	sellText->SetText("Sell this tower for 40% of the buy price.");
	sellText->color.Init(0.9f, 0.9f, 0.9f);
	TowerInfo *tooltip = new TowerInfo();
	tooltip->AddCustomLabel(sellText);
	bSell = new TowerButton(tooltip);
	bSell->Click += LOnTowerUpgradeClick;
	bSell->SetMaterial(materialDict->FindMaterial("gui\\icons\\sell_button"));
	AddChild(bSell);
	upgrades.push_back(bSell);


	InvokeResize();
}

void TowerSelectionWindow::OnTowerUpgradeClick(Control *ctrl)
{
	char command[256];

	for (auto towerButton : upgrades)
	{
		if (towerButton == bSell)
		{
			G_StrSnPrintf(command, sizeof(command), "tower_sell %i",
							towerId);

			engine->SendClientCommand(command);

			SetVisible(false);
			return;
		}
		else if (towerButton == ctrl)
		{
			int upgradeId = towerButton->GetTowerScript().id;

			G_StrSnPrintf(command, sizeof(command), "tower_upgrade %i %i",
							towerId, upgradeId);

			engine->SendClientCommand(command);

			SetVisible(false);
			return;
		}
	}
}

void TowerSelectionWindow::InitComponents()
{
	auto towers = Resources::GetInstance()->GetTowers();

	color = Color(0.15f, 0.15f, 0.15f, 0.85f);
	borderColor = Color(0.15f, 0.15f, 0.15f, 1.0f);

	info = new TowerInfo();
	info->SetVisible(true);
	info->borderColor = Color(0, 0, 0, 0);
	info->color = Color(0, 0, 0, 0);
	AddChild(info);

	pTowerIcon = new Panel();
	AddChild(pTowerIcon);

	pTowerIcon = new Panel();
	AddChild(pTowerIcon);

	lUpgrade = new Label();
	lUpgrade->SetText("Upgrades");
	lUpgrade->color = Color(0, 1, 0, 1);
	AddChild(lUpgrade);
}

void TowerSelectionWindow::SetPositions(int width, int height)
{
	Rect rect;

	rect.w = SCALE(280);
	rect.h = SCALE(70);
	rect.x = (int)((width * 0.5f) - (rect.w / 2));
	rect.y = 0;
	SetRect(rect);

	rect.x = SCALE(62);
	rect.y = 0;
	rect.w = SCALE(150);
	rect.h = SCALE(80);
	info->SetRect(rect);

	rect.x = SCALE(5);
	rect.y = SCALE(5);
	rect.w = SCALE(60);
	rect.h = SCALE(60);
	pTowerIcon->SetRect(rect);

	lUpgrade->SetPos(SCALE(230), SCALE(2));

	int xPos = SCALE(190);
	int yPos = SCALE(27);

	for (auto towerButton : upgrades)
	{
		Rect rect = towerButton->GetRect();
		rect.x = xPos;
		rect.y = yPos;
		rect.w = SCALE(40);
		rect.h = SCALE(40);
		towerButton->SetRect(rect);
		xPos += SCALE(42);
	}

	if (xPos > SCALE(280))
	{
		rect = GetRect();
		rect.w = xPos;
		SetRect(rect);
	}
}
