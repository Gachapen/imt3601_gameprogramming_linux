#ifndef C_TOWERBASE_H
#define C_TOWERBASE_H

#include "c_basemodel.h"

class C_TowerBase : public C_BaseModel
{
	DECLARE_CLASS(C_TowerBase, C_BaseModel);
	DECLARE_NETWORKTABLE_BASE();
public:

	C_TowerBase();

	//virtual void Simulate();

	virtual void Render();

	virtual bool IsTower() const { return true; }

	virtual bool GetRenderBounds(vec3 &min, vec3 &max);

	virtual bool ShouldReflect() const { return true; }

	ResourceTower GetTowerScript();

	void SetOutlineVisible(bool visible);

protected:
	virtual void OnNetworkedSpawn();

	virtual void OnRelease();

private:
	~C_TowerBase();

	const ResourceTower *towerScript;

	NetworkVariable<Uint8> towerType;
	DECLARE_NETWORKVAR_CALLBACK(TowerTypeChanged, Uint8);

	bool outlineVisible;
};


#endif