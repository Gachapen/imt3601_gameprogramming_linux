#include "pch.h"
#include "clientbutton.h"
#include "clientinput.h"

SINGLETON_INSTANCE(ClientInput);


static ClientButton inputButtons[] =
{
	ClientButton("camera_up", Buttons::UP),
	ClientButton("camera_down", Buttons::DOWN),
	ClientButton("camera_left", Buttons::LEFT),
	ClientButton("camera_right", Buttons::RIGHT),
	ClientButton("camera_speed", Buttons::SPEED),
	ClientButton("camera_zoom_in", Buttons::ZOOM_IN),
	ClientButton("camera_zoom_out", Buttons::ZOOM_OUT),
};
static const int inputButtonCount = ARRAYSIZE(inputButtons);
static_assert(Buttons::COUNT == inputButtonCount, "Register new buttons in client input!");


ClientInput::ClientInput()
	: buttonsDown(0)
	, buttonsPressed(0)
	, buttonsReleased(0)
{
}

bool ClientInput::Init()
{
	return true;
}

void ClientInput::Shutdown()
{
}

void ClientInput::Update(float frametime)
{
	buttonsPressed = 0;
	buttonsReleased = 0;

	for (int i = 0; i < inputButtonCount; i++)
	{
		ClientButton &button = inputButtons[i];

		button.UpdateState();

		int state = button.GetButtonState();
		int bit = button.GetBit();

		if ((state & ButtonStates::DOWN) != 0)
		{
			buttonsPressed = bit ^ buttonsDown;

			buttonsDown |= bit;
		}
		else if ((state & ButtonStates::RELEASED) != 0)
		{
			buttonsReleased |= bit;

			buttonsDown &= ~bit;
		}
		else
		{
			buttonsDown &= ~bit;
		}
	}
}

int ClientInput::GetButtonsPressed() const
{
	return buttonsPressed;
}

int ClientInput::GetButtonsDown() const
{
	return buttonsDown;
}

int ClientInput::GetButtonsReleased() const
{
	return buttonsReleased;
}

bool ClientInput::WasButtonPressed(ButtonId button) const
{
	return (buttonsPressed & button) != 0;
}

bool ClientInput::IsButtonDown(ButtonId button) const
{
	return (buttonsDown & button) != 0;
}

bool ClientInput::WasButtonReleased(ButtonId button) const
{
	return (buttonsReleased & button) != 0;
}