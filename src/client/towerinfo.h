#ifndef TOWERINFO_H
#define TOWERINFO_H

#include "pch.h"
#include "gui/layoutpanel.h"


enum TowerInfoStat
{
	Damage = 0,
	Range,
	TowerCost,
	UpgradeCost,
	UpgradeDuration
};

class TowerInfo : public GUI::LayoutPanel
{
public:
	TowerInfo();
	~TowerInfo(void);

	void SetTowerScript(ResourceTower tower);
	void SetTowerUpgrade(ResourceTowerUpgrade towerUpgrade);
	void AddStat(TowerInfoStat stat);
	void AddCustomLabel(GUI::Label *label);
	void Clear();

	virtual void InitComponents();
	virtual void SetPositions(int width, int height);
private:
	ResourceTower towerScript;
	ResourceTowerUpgrade towerUpgrade;
	std::vector<GUI::Label*> labels;
};
#endif
