
#include "pch.h"
#include "c_basemodel.h"


C_BaseModel::C_BaseModel()
	: renderable(nullptr)
{
}

C_BaseModel::~C_BaseModel()
{
	Assert(renderable == nullptr);
}

IMDXRenderable *C_BaseModel::GetModel()
{
	return renderable;
}

void C_BaseModel::SetModel(IMDXRenderable *renderable)
{
	ReleaseModel();

	this->renderable = renderable;
}

void C_BaseModel::SetModel(const char *name)
{
	ReleaseModel();

	renderable = renderableDict->CreateMDXRenderableInstance(name);
}

void C_BaseModel::ReleaseModel()
{
	if (renderable != nullptr)
	{
		renderable->Release();
		renderable = nullptr;
	}
}

void C_BaseModel::OnRelease()
{
	ReleaseModel();

	BaseClass::OnRelease();
}

void C_BaseModel::Simulate()
{
	BaseClass::Simulate();

	if (renderable != nullptr)
	{
		renderable->AdvanceFrame(globals->GetFrametime());
	}
}

void C_BaseModel::Render()
{
	if (renderable == nullptr)
	{
		return;
	}

	const float scaleFactor = GetScale();

	mat4 model = translationMatrix4(GetRenderOrigin());
	model = rotate(model, GetRenderAngles());
	model = scale(model, scaleFactor, scaleFactor, scaleFactor);

	renderContext->PushModelMatrix(model);

	renderable->Draw();

	renderContext->PopModelMatrix();
}

void C_BaseModel::SetOrigin(const vec3 &origin)
{
	if (GetModel() != nullptr
		&& origin != BaseClass::GetOrigin())
	{
		GetModel()->InvalidateBoneCache();
	}

	BaseClass::SetOrigin(origin);
}

bool C_BaseModel::GetRenderBounds(vec3 &min, vec3 &max)
{
	if (GetModel() == nullptr)
	{
		return false;
	}

	GetModel()->GetModelExtents(min, max);

	min += GetRenderOrigin();
	max += GetRenderOrigin();
	return true;
}