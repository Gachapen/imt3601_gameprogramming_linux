#ifndef C_PLAYER_H
#define C_PLAYER_H


class C_Player : public ClientEntity
{
	DECLARE_CLASS(C_Player, ClientEntity);
	DECLARE_NETWORKTABLE_BASE();
public:

	C_Player();
	
	virtual bool IsPlayer() const { return true; }

	static C_Player *GetLocalPlayer();

	//virtual void OnSpawn();

	//virtual void Simulate();

	Uint16 GetGold() const;

protected:
	~C_Player();

private:
	virtual void OnNetworkedSpawn();

	static C_Player *localClientInstance;

	//virtual void OnRelease();

	NetworkVariable<Uint16> gold;
	//DECLARE_NETWORKVAR_CALLBACK(CreepStateChanged, Uint8);

	//void BloodDecalCallback(const vec3 &origin);
	//DECLARE_NETWORKPROCEDURE_1PARAM(vec3, BloodDecal);
};


#endif