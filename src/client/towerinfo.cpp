#include "pch.h"
#include "towerinfo.h"

//TODO: extract tooltip base control
TowerInfo::TowerInfo()
{
	InitComponents();
	InvokeResize();
	SetVisible(false);
}

TowerInfo::~TowerInfo(void)
{
}

void TowerInfo::SetTowerUpgrade(ResourceTowerUpgrade upgrade)
{
	this->towerUpgrade = upgrade;
}

void TowerInfo::SetTowerScript(ResourceTower towerScript)
{
	this->towerScript = towerScript;
}

void TowerInfo::AddStat(TowerInfoStat stat)
{
	GUI::Label *label = new GUI::Label();
	label->color.Init(0.9f, 0.9f, 0.9f);

	char text[255];

	switch (stat)
	{
	case Damage:
		G_StrSnPrintf(text, sizeof(text), "Damage: %i", towerScript.damage);
		break;
	case Range:
		G_StrSnPrintf(text, sizeof(text), "Range: %.0f", towerScript.range);
		break;
	case TowerCost:
		G_StrSnPrintf(text, sizeof(text), "Cost: %i", towerScript.cost);
		break;
	case UpgradeCost:
		G_StrSnPrintf(text, sizeof(text), "Cost: %i", towerUpgrade.cost);
		break;
	case UpgradeDuration:
		G_StrSnPrintf(text, sizeof(text), "Upgrade duration: %.0f", towerUpgrade.duration);
		break;
	default:
		Assert(false);
	}

	label->SetText(text);

	AddCustomLabel(label);
}

void TowerInfo::SetPositions(int width, int height)
{
	int xPos = SCALE(12);
	int yPos = SCALE(5);
	int maxWidth = 0;

	for (auto label : labels)
	{
		label->SetPos(xPos, SCALE(yPos));

		int labelWidth = label->GetRect().w;
		if (labelWidth > maxWidth)
		{
			maxWidth = labelWidth;
		}
		yPos += 20;
	}

	rect.h = SCALE(yPos) + SCALE(10);
	rect.w = maxWidth + (xPos * 2);
}

void TowerInfo::Clear()
{
	for (auto label : labels)
	{
		RemoveChild(label);
		delete label;
	}
	labels.clear();
}

void TowerInfo::AddCustomLabel(GUI::Label *label)
{
	label->SetAlignment(GUI::Alignment::Left);

	labels.push_back(label);
	AddChild(label);

	InvokeResize();
}

void TowerInfo::InitComponents()
{
	color = GUI::Color(0.15f, 0.15f, 0.15f, 0.85f);
	borderColor = GUI::Color(0.15f, 0.15f, 0.15f, 1.0f);
}
