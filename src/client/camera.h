#ifndef CAMERA_H
#define CAMERA_H


class PRE_ALIGN_16 Camera : public IGameSystem
{
	DECLARE_SINGLETON(Camera);
public:
	~Camera();

	virtual void Update(float frametime);

	const mat4 &GetViewMatrix() const;
	const mat4 &GetReflectionViewMatrix() const;
	const mat4 &GetProjectionMatrix() const;

	const vec3 &GetOrigin() const;
	const euler &GetAngles() const;

	const float &GetDistance() const;

	const float &GetNearZ() const;
	const float &GetFarZ() const;
	const float &GetFOV() const;

	const Plane *GetFrustum() const;

private:
	void SimulateMouseCamera();
	void UpdateFrustum();

	vec3 origin;
	vec3 originMove;
	vec3 originDesired;

	float distance;
	float distanceDesired;

	euler angles;

	float zNear, zFar;
	float fov;

	mat4 viewMatrix;
	mat4 reflectionViewMatrix;
	mat4 projectionMatrix;

	int lastMouseX;
	int lastMouseY;

	Plane frustumPlanes[6];
} POST_ALIGN_16;

#endif