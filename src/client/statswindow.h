#ifndef STATSWINDOW_H
#define STATSWINDOW_H

#include "pch.h"
#include "gui/layoutpanel.h"

class StatsWindow : public GUI::LayoutPanel
{
public:
	StatsWindow();
	virtual ~StatsWindow();

	virtual void UpdateSelf();

	virtual void InitComponents();
	virtual void SetPositions(int width, int height);

private:
	GUI::Label* lTowerCount;
	GUI::Label* lUnitCount;
};

#endif