#include "pch.h"
#include "towerbutton.h"
#include "c_player.h"

using namespace GUI;

void TowerButton::SetTowerScript(ResourceTower towerScript)
{
	this->towerScript = towerScript;
	SetMaterial(materialDict->FindMaterial(towerScript.icon.c_str()));

	tooltip->SetTowerScript(towerScript);
	this->cost = towerScript.cost;
}

TowerButton::TowerButton(TowerInfo *tooltip) : cost(0)
{
	this->tooltip = tooltip;
	Resize(0, 0);
}

void TowerButton::Resize(int width, int height)
{
	Rect rect = tooltip->GetRect();

	rect.w = SCALE(150);
	rect.h = SCALE(80);
	tooltip->SetRect(rect);
	tooltip->InvokeResize();
}

void TowerButton::OnEnter(Control *ctrl)
{
	Button::OnEnter(ctrl);

	tooltip->SetVisible(true);
}

void TowerButton::OnLeave(Control *ctrl)
{
	Button::OnLeave(ctrl);

	tooltip->SetVisible(false);
}

TowerButton::~TowerButton(void)
{
	delete tooltip;
}

void TowerButton::UpdateSelf()
{
	Button::UpdateSelf();

	Rect rect = tooltip->GetRect();

	rect.x = input->GetMousePosX() - rect.w;
	rect.y = input->GetMousePosY() - rect.h;

	//Tooltip is out of screen, adjust it
	if (rect.y < 0)
	{
		rect.y = input->GetMousePosY();
	}

	tooltip->SetRect(rect);

	C_Player *player = C_Player::GetLocalPlayer();
	if (player != nullptr && cost > player->GetGold()){
		enoughGold = false;
	}else{
		enoughGold = true;
	}
	
}

void TowerButton::SetTowerCost(int cost){
	this->cost = cost;
}

void TowerButton::OnMouseDown(int mouseButton){
	if (enoughGold){
		Panel::OnMouseDown(mouseButton);
	}
}

void TowerButton::DrawSelf()
{
	Button::DrawSelf();

	if (!enoughGold){
		graphics->SetDrawColor(0, 0, 0, 0.7f);
		graphics->DrawFilledRect(0, 0, rect.w, rect.h);
	}

	//Tooltip is not drawn as a child to avoid viewport culling
	tooltip->Draw();
}
