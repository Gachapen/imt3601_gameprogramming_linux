#ifndef MAINMENU_H
#define MAINMENU_H

#include "pch.h"
#include "gui/layoutpanel.h"

class MainMenu : public GUI::LayoutPanel
{
public:
	MainMenu();
	virtual ~MainMenu();
protected:
	GUI::Label* lTitle;
	GUI::Button* bExit;
	GUI::Button* bPlay;

	virtual void InitComponents();
	virtual void SetPositions(int width, int height);

	LISTENER(MainMenu, OnPlayClick, Control *);
	void OnPlayClick(Control* ctrl);
	LISTENER(MainMenu, OnExitClick, Control *);
	void OnExitClick(Control* ctrl);
};
#endif
