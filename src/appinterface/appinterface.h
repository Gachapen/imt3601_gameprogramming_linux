#ifndef APPINTERFACE_H
#define APPINTERFACE_H

#include <unordered_map>
#include <string>

#define IAPPINTERFACE_EXPORTS

#include "appinterface/iappinterface.h"
#include "util/singleton.h"

class AppInterface : public IAppInterface
{
	DECLARE_SINGLETON(AppInterface);

public:
	~AppInterface();

	virtual bool LoadSharedLibrary(const char *libraryName);
	virtual void Shutdown();

	virtual void RegisterApp(const char *identifier, void *target);
	virtual void *QueryApp(const char *identifier);

private:
	std::vector<void*> libraryHandles;
	std::unordered_map<std::string, void*> applications;
};

#endif
