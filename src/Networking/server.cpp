#include <time.h>			// time( )
#include <assert.h>			// assert( )
#include <iostream>

#include "server.h"
#include "util\util.h"
#include "shareddefs.h"

#include "message.h"		// Message
#include "messagetypes.h"	// MTYPE_
#include "connection.h"		// Connection

// Constructor / Desctructor
Server::Server(unsigned short port)
{
	// Copy port parameter
	this->port = port;
	this->started = false;
	this->timeOut = 3;

	// Set variables
	started = 0;

	packetBuffer = new Message();

	// Set up thread data
	rejectConenctions = false;
	allowConnectionless = false;
	thread = nullptr;
}

Server::~Server()
{
	if (started)
	{
		Stop();
	}

	delete packetBuffer;
}

bool Server::Start()
{
	assert(started == false);

	if (!(socket = SDLNet_UDP_Open(port)))
	{
		// TODO: Log error
		return false;
	}

	if (!(sendPacket = SDLNet_AllocPacket(NETWORKING_MAX_PACKET_SIZE)))
	{
		// TODO: Log error
		SDLNet_UDP_Close(socket);
		return false;
	}

	if (!(receivePacket = SDLNet_AllocPacket(NETWORKING_MAX_PACKET_SIZE)))
	{
		// TODO: Log error
		SDLNet_UDP_Close(socket);
		SDLNet_FreePacket(sendPacket);
		return false;
	}

	running = true;
	socket = socket;
	thread = new std::thread(&Server::_WorkerThreadFunction, this);

	started = true;

	return true;
}

void Server::Stop()
{
	if(!started)
		return;

	// Make disconnection notice
	Message *disconnectMessage = new Message();
	disconnectMessage->WriteUint16(0U);
	disconnectMessage->WriteUint8(1U);
	disconnectMessage->WriteUint8(MTYPE_DISCONNECTION);

	// Send it out
	for (Uint32 i = 0; i < clients.size(); i++)
	{
		if (clients[i] != nullptr)
		{
			Networking::SendUdpPacket(socket, clients[i]->ipAddress, clients[i]->sendPacket, disconnectMessage);
		}
	}

	// Discard it
	disconnectMessage->TryKill();

	// Close the socket, and signal the thread it's time to stop
	running = false;
	thread->join();

	delete thread;
	
	SDLNet_UDP_Close(socket);

	// Free up data
	SDLNet_FreePacket(sendPacket);
	SDLNet_FreePacket(receivePacket);

	// Delete all connections' data
	for (Uint32 i = 0; i < clients.size(); i++)
	{
		if (clients[i] != nullptr)
		{
			delete clients[i];
		}
	}

	// Clear the list in case the server restarts to lower the amount of reiterations.
	clients.clear();

	started = false;
}

void Server::Release()
{
	delete this;
}

void Server::SetRejectionMode(bool enable)
{
	rejectConenctions = enable;
}

void Server::SetConnectionlessMode(bool enable)
{
	allowConnectionless = enable;
}

void Server::SetTimeOutThreshold(int milliseconds)
{
	timeOut = milliseconds;
}

float Server::GetRTT(Uint32 connectionID)
{
	if (clients[connectionID] != nullptr)
	{
		return clients[connectionID]->GetRTT();
	}
	else
	{
		return 0.f;
	}
}

void Server::QueueForSending(Uint32 connectionID, Message *message, SyncMode mode)
{
	if(connectionID < 0 || connectionID >= clients.size())
	{
		// TODO: Error logging
		return;
	}

	if (clients[connectionID] != nullptr)
	{
		message->AddLife();
		clients[connectionID]->QueueForSending(message);
	}
	else
	{
		// TODO: Error logging.
	}
}

void Server::QueueForSendingToAll(Message *message, SyncMode mode, int rttFilter)
{
	for (Uint32 i = 0; i < clients.size(); i++)
	{
		if (clients[i] != nullptr)
		{
			clients[i]->QueueForSending(message, mode);
		}
	}
}

void Server::QueueForSending(Uint32 connectionID, Byte *data, size_t size, SyncMode mode)
{
	if(connectionID < 0 || connectionID >= clients.size())
	{
		// TODO: Error logging
		return;
	}

	Message *newMessage = new Message((char *)data, size);

	QueueForSending(connectionID, newMessage, mode);
	newMessage->TryKill();
}

void Server::QueueForSendingToAll(Byte *data, size_t size, SyncMode mode, int rttFilter)
{
	Message *newMessage = new Message((char *)data, size);

	QueueForSendingToAll(newMessage, mode, rttFilter);
	newMessage->TryKill();
}

bool Server::SendQueuedMessages()
{
	for (Uint32 i = 0; i < clients.size(); i++)
	{
		if (clients[i] != nullptr)
		{
			//DBGMSGF("RTT (%i): %f\n", i, clients[i]->GetRTT());
			clients[i]->QueuePingMessage();
			if (!clients[i]->SendMessages())
			{
				// TODO: Error logging
			}
		}
	}

	// All good.
	return true;
}
 
void Server::QueuePingMessage(Uint32 connectionID)
{
	if(clients[connectionID] != nullptr)
	{
		clients[connectionID]->QueuePingMessage();
	}
	else
	{
		// TODO: Log error
	}
}

void Server::QueuePingMessageToAll()
{
	for(size_t i = 0; i < clients.size(); i++)
	{
		if(clients[i] != nullptr)
		{
			clients[i]->QueuePingMessage();
		}
	}
}

Message *Server::PollMessage(Uint32 connectionID)
{
	if(connectionID < 0 || connectionID >= clients.size())
	{
		// TODO: Error logging
		return nullptr;
	}

	if (clients[connectionID] != nullptr)
	{
		return clients[connectionID]->HasNewMessage();
	}
	else
	{
		return nullptr;
	}
}

Byte *Server::HasNewMessage(Uint32 connectionID, Uint32& sizeOutput)
{
	if(connectionID < 0 || connectionID >= clients.size())
	{
		// TODO: Error logging
		return nullptr;
	}

	Message *msg = PollMessage(connectionID);

	if (msg != nullptr)
	{
		packetBuffer->OverwriteData(msg->GetData(), msg->GetSize());
		sizeOutput = msg->GetSize();

		msg->TryKill();

		return (Byte *)packetBuffer->GetData();
	}
	else
	{
		return nullptr;
	}
}

Byte *Server::HasNewMessage(int& clientReferenceOutput, Uint32& sizeOutput, Uint32& addressOutput, Uint16& portOutput)
{
	for(unsigned int i = 0; i < clients.size(); i++)
	{
		if(clients[i] != nullptr)
		{
			Byte* returnValue = HasNewMessage(i, sizeOutput);
			
			if(returnValue == nullptr)
				continue;
			
			clientReferenceOutput = i;
			addressOutput = clients[i]->GetAddress().host;
			portOutput = clients[i]->GetAddress().port;
			
			return returnValue;
		}
	}

	return nullptr;
}

bool Server::HasTimedOut(Uint32 connectionID)
{
	if(connectionID < 0 || connectionID >= clients.size())
	{
		// TODO: Error logging
		return false;
	}

	if(clients[connectionID] == nullptr)
	{
		return true;
	}

	if (time(NULL) - clients[connectionID]->GetLastHeardFrom() > timeOut)
	{
		delete clients[connectionID];
		clients[connectionID] = nullptr;
		return true;
	}
	else
	{
		return false;
	}
}

bool Server::HasDisconnected(Uint32 connectionID, bool includeTimeOut)
{
	if(connectionID < 0 || connectionID >= clients.size())
	{
		// TODO: Error logging
		return false;
	}

	if (includeTimeOut)
	{
		if (HasTimedOut(connectionID))
		{
			return true;
		}
	}

	return (clients[connectionID] == nullptr);
}

bool Server::HasNewConnection(int &connectionIDOutput)
{
	if (newConnections.getCount() > 0)
	{
		connectionIDOutput = newConnections.removeFirstAsValue();
		return true;
	}
	else
	{
		return false;
	}
}

bool Server::HasNewDisconnection(int &connectionIDOutput, bool includeTimeOut)
{
	if (includeTimeOut)
	{
		for (Uint32 i = 0; i < clients.size(); i++)
		{
			if ((clients[i] != nullptr) && HasTimedOut(i))
			{
				contentMutex.lock();
				clients[i] = nullptr;
				newDisconnections.add(new int(i));
				contentMutex.unlock();
				break;
			}
		}
	}

	if (newDisconnections.getCount() > 0)
	{
		contentMutex.lock();
		connectionIDOutput = newDisconnections.removeFirstAsValue();
		contentMutex.unlock();

		return true;
	}
	else
	{
		return false;
	}
}

Message *Server::HasNewConnectionlessMessage(IPaddress &outputAddress)
{
	Message *retVal = nullptr;

	contentMutex.lock();
	if (newConnectionlessMessages.getCount() > 0)
	{
		auto pair = newConnectionlessMessages.removeFirst();

		outputAddress = pair->first;
		retVal = pair->second;

		delete pair;
	}
	contentMutex.unlock();

	return retVal;
}

// Connectionless interactions
bool Server::SendConnectionlessMessage(Uint32 address, Uint16 port, Byte *data, size_t size)
{
	IPaddress addr;

	addr.host = address;
	addr.port = port;

	return Networking::SendUdpPacket(socket, addr, sendPacket, data, size);
}

Byte *Server::HasNewConnectionlessMessage(Uint32 &addr, Uint16 &port, Uint32 &outputSize)
{
	IPaddress temp;

	Message *msg = HasNewConnectionlessMessage(temp);

	if (msg != nullptr)
	{
		// Write to the output variables
		addr = temp.host;
		port = temp.port;
		outputSize = msg->GetSize();

		packetBuffer->OverwriteData(msg->GetData(), msg->GetSize());
		msg->TryKill();

		return (Byte *)packetBuffer->GetData();
	}
	else
	{
		return nullptr;
	}
}

void Server::Drop(Uint32 connectionID)
{
	// Make sure the client still exists. If not, then the point in this is already served.
	if(clients[connectionID] == nullptr)
		return;

	// Make disconnection notice
	Message *disconnectMessage = new Message();
	disconnectMessage->WriteUint16(0U);
	disconnectMessage->WriteUint8(1U);
	disconnectMessage->WriteUint8(MTYPE_DISCONNECTION);

	// Send it.
	Networking::SendUdpPacket(socket, clients[connectionID]->ipAddress, clients[connectionID]->sendPacket, disconnectMessage);

	// Remove the client.
	delete clients[connectionID];
	clients[connectionID] = nullptr;
}

// Private methods
Connection *Server::_GetByIP(const IPaddress ip) const
{
	// Try finding the client
	for (Uint32 i = 0; i < clients.size(); i++)
	{
		if ((clients[i] != nullptr) && clients[i]->HasIP(ip, true))
		{
			return clients[i];
		}
	}	// TODO: Try finding a more efficient way to lookup clients, though it's done by the server thread, which shouldn't affect game performance much.

	// If noone was found, return NULL-pointer
	return nullptr;
}

void Server::_AddMessage(unsigned char type, const IPaddress address, Message *message, unsigned short sequenceNumber)
{
	Connection *connection = _GetByIP(address);

	// If the IP lookup didn't find anything.
	if (connection == nullptr)
	{
		if (allowConnectionless)
		{
			contentMutex.lock();
			newConnectionlessMessages.add(new std::pair<IPaddress, Message *>(address, message));
			contentMutex.unlock();
		}
		else
		{
			message->TryKill();
		}
		return;
	}

	connection->_PassMessage(type, message, sequenceNumber);
}

void Server::_AddConnection(const IPaddress ip)
{
	contentMutex.lock();

	// Try allocating a new packet, or use the shared one.
	UDPpacket* newPacket = SDLNet_AllocPacket(1024);
	if(!newPacket)
	{
		newPacket = sendPacket;
	}

	// Set up a new connection
	Connection *newConnection = new Connection();
	newConnection->_SetData(ip, sendPacket, socket);

	// Add it to the list
	newConnections.add(new int(clients.size()));

	// Put it onto the list.
	clients.push_back(newConnection);

	contentMutex.unlock();
}

void Server::_AddNewDisconnection(IPaddress ip)
{
	contentMutex.lock();

	// Look through connections
	for (Uint32 i = 0; i < clients.size(); i++)
	{
		// Skip empty slots
		if (clients[i] != nullptr)
		{
			// If the client's Ip matches
			if (clients[i]->HasIP(ip))
			{
				// Delete the client and notify the application
				delete clients[i];
				clients[i] = nullptr;
				newDisconnections.add(new int (i));

				// Done
				contentMutex.unlock();
				return;
			}
		}
	}

	contentMutex.unlock();
}

void Server::_AddPingResponse(IPaddress ip, time_t timeStamp, unsigned __int16 identifier)
{
	Connection *connection = _GetByIP(ip);
	if (connection != nullptr)
	{
		connection->_PassPingResponse(timeStamp, identifier);
	}
}

void Server::_WorkerThreadFunction()
{
	// Variables: Thread function content
	char *tempBuffer = new char[NETWORKING_MAX_PACKET_SIZE];																// For reads to not cause thread issues with Message's default buffer
	NewConenction newConnection;
	Message packetWrapper(NETWORKING_MAX_PACKET_SIZE);																	// For using the Message class methods for reading packet data.
	Message response(16);																			// For connection responses
	
	// Make a set
	SDLNet_SocketSet set = SDLNet_AllocSocketSet(1);
	SDLNet_UDP_AddSocket(set, socket);

	// Variables: Pointers to thread data, to make code easier
	UDPpacket *responsePacket = SDLNet_AllocPacket(NETWORKING_MAX_PACKET_SIZE);											// For respones, and to not interfere with regular sending.

	assert(responsePacket != nullptr);

	// Variables: For reading packets
	unsigned __int16 lastAck;
	unsigned __int32 ackBitField;
	unsigned __int8 messageCount;
	unsigned __int8 messageType;
	unsigned __int16 messageLength;
	unsigned __int16 sequenceNumber;

	int test = 0;

	// Main loop
	while (running)
	{
		// Reset bit field, as it's not in all packets
		ackBitField = 0U;

		// Check incomming packets
		
		SDLNet_CheckSockets(set, 20);

		//mutex.lock();
		int recvNumber = SDLNet_UDP_Recv(socket, receivePacket);
		//mutex.unlock();

		// Handle incomming packet if there were any
		if (recvNumber > 0)
		{
			// Take the data
			packetWrapper.OverwriteData((char *)receivePacket->data, receivePacket->len);

			// Read ack data
			lastAck = packetWrapper.ReadUint16(tempBuffer);
			if (lastAck != 0U)
			{
				ackBitField = packetWrapper.ReadUint32(tempBuffer);
			}
			// TODO: Use ack data


			/* FOR DEBUGGING USE ONLY: Display data as bytes
			 * std::cout << "\nServer << ";
			 * char* d = packetWrapper.GetData();
			 * for(size_t i = 0; i < packetWrapper.GetSize(); i++)
			 * {
			 *  std::cout << (int)*(unsigned char*)&d[i] << '-';
			 * }
			 * std::cout << '\n'; */

			// Read messages
			messageCount = packetWrapper.ReadUint8();
			for (Uint32 i = 0; i < messageCount; i++)
			{
				messageType = packetWrapper.ReadUint8();

				switch (messageType)
				{
				case MTYPE_NORMAL:
				case MTYPE_RELIABLE:											// This is the bulk of the communication - might split them up when doing acks
					{
						sequenceNumber = packetWrapper.ReadInt16(tempBuffer);
						messageLength = packetWrapper.ReadUint16(tempBuffer);
						_AddMessage(messageType, receivePacket->address, new Message(packetWrapper.ReadBytes(messageLength, tempBuffer), messageLength), sequenceNumber);

						break;
					}
				case MTYPE_CONNECTION_INIT:
					{
						// Write down data
						newConnection.ip = receivePacket->address;
						newConnection.magicNumber = packetWrapper.ReadInt16(tempBuffer);
						newConnection.accepted = !rejectConenctions;

						// Reset response and construct header
						response.ResetWriteCursor();
						response.WriteUint16(0U);
						response.WriteUint8(1U);

						// Write response
						if (!rejectConenctions)
						{
							response.WriteUint8(MTYPE_CONNECTION_ACCEPT);
						}
						else
						{
							response.WriteUint8(MTYPE_CONNECTION_REJECT);
						}

						// Use connection identifier.
						response.WriteInt16(newConnection.magicNumber);
						Networking::SendUdpPacket(socket, newConnection.ip, responsePacket, &response);

						break;
					}
				case MYTPE_CONNECTION_ACK:
					{
						IPaddress ip = receivePacket->address;
						short magicNumber = packetWrapper.ReadInt16();

						// Check data
						if (newConnection.accepted && (newConnection.ip.host == ip.host) && (newConnection.magicNumber == magicNumber))
						{
							newConnection.accepted = false;
							newConnection.ip.host = 0U;
							newConnection.magicNumber = 0;

							// Signal the server that there's a new connection
							_AddConnection(ip);
						}

						break;
					}
				case MTYPE_PING:											// Immediate response seperate from other communication
					{
						// Reset response and construct header
						response.ResetWriteCursor();
						response.WriteUint16(0U);
						response.WriteUint8(1U);
						response.WriteUint8(MTYPE_PONG);

						// Write the PING identifier
						response.WriteUint16(packetWrapper.ReadUint16());

						// Send it right back
						Networking::SendUdpPacket(socket, receivePacket->address, responsePacket, &response);

						break;
					}
				case MTYPE_PONG:
					{
						_AddPingResponse(receivePacket->address, time(0), packetWrapper.ReadUint16());
						break;
					}
				case MTYPE_DISCONNECTION:
					{
						_AddNewDisconnection(receivePacket->address);
						break;
					}
				}
			}
		}
		else if (recvNumber == -1)
		{
			// TODO: Error logging
		}
	}

	// Free up data.
	SDLNet_FreeSocketSet(set);
	SDLNet_FreePacket(responsePacket);
	delete[] tempBuffer;
	// Messages and buffers will be deleted.
}
