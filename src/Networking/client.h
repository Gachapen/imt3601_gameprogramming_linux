#ifndef CLIENT_H
#define CLIENT_H

#include <thread>
#include "networking.h"
#include "connection.h"
#include "networking\iclient.h"

class Client : public Connection
{
private:
	// Buffer data
	Message* packetBuffer;

	// ConnectionData
	ConnectionStatus status;
	time_t connectionStartTime;
	unsigned short connectionIDentifier;
	UDPpacket *receivePacket;

	// Thread data
	std::thread *thread;
	bool threadIsRunning;

	// Settings
	int timeoutTime;
	void _WorkerThreadFunction();

public:
	// Constructor

	Client();
	~Client();

	// Methods: Connection

	bool Connect(IPaddress address, bool block);
	bool Connect(const char *host, unsigned short portNumber, bool block);
	void Disconnect();
	ConnectionStatus GetConnectionStatus();
	bool IsConnected();

	//// Methods: Connectionless

	// Sets up the socket for sending, but does not connect to a specific server.
	bool SetupConnectionless();
	// Sends a packet to a target. NOTE: It uses a header the Server class understands.
	bool SendConnectionless(IPaddress target, Message* message);
	// Sends a packet to a target. Note: It uses a header the Server class understands. This overload of the function resolves a host or IP and calls the other.
	bool SendConnectionless(const char* targetHost, unsigned short targetPort, Message* message);

	//// Methods: Settings

	// Set the amount of time before the client times out
	void SetTimeoutThreshold(int milliseconds);
	// Increases the buffer for sending packets. Note: This does not up the limit, use the static methods Connection::SetMaxPacketSize(size_t) or Client::SetMaxPacketSize(size_t) for that.
	bool ResizePacketBuffer(size_t newSize = 512U);

	//// Methods: interface specific
	void Release();
	Byte *HasNewMessage(Uint32& sizeOutput);
	void QueueForSending(Byte *data, size_t size, bool reliable = false);
	bool Connect(Uint32 address, Uint16 port, bool block);
	bool SendConnectionless(Uint32 targetAddress, Uint16 targetPort, Byte *data, Uint32 size);
	bool HasTimedOut();
	void Ping();
};
#endif	// CLIENT_H
