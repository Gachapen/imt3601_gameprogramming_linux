#ifndef BIT_PACKING_H
#define BIT_PACKING_H

/*
	Not really pretty code, and it's not likely it'll be used, but here it is if you need it.
	It's a bit more practical for non-boolean bit packings, using the way Simon demonstrated
	in class - bit shift and bitwise &.

	Why it's ugly is that it uses a global variable for the prepared masks.

	Do note that reading from a packet variable should be in an order
	inverse to how it was written to.
*/

// Init-method
void initiliazeBitPacking();

// Global variable (520 byte)
extern unsigned long long* masks;

// Template methods
template <typename CT, typename ST>
inline void packInto(CT& destination, const ST source, size_t bitCount)
{
	// Make a copy of the source's relevant bits
	ST sourceMasked = source & masks[bitCount];

	// Shift the destination and put the source among the least significant bits
	destination <<= bitCount;
	destination |= sourceMasked;
}

template <typename ST, typename CT>
inline ST packOutOf(CT& source, size_t bitCount)
{
	// Get the ret-val
	ST retVal = source & masks[bitCount];

	// Move the bits to the left
	source >>= bitCount;

	// Return
	return retVal;
}

/*
	Quick capacity guide for each bit.
	1	0-1
	2	0-3
	3	0-7
	4	0-15
	5	0-31
	6	0-63
	7	0-127
	8*	0-255
	9	0-511
	10	0-1023
	11	0-2047
	12	0-4095
	13	0-8191
	14	0-16383
	15	0-32767

	for signedness, add a bool (1 bit) to it.

	*Use a "char", unless you just have to fill the last 8 bits of something.
*/
#endif