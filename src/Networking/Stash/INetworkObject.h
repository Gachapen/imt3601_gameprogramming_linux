#ifndef I_NETWORK_OBJECT_H
#define I_NETWORK_OBJECT_H

class Message;

class INetworkObject
{
protected:
	char livesLeft;
	bool markAsDead;
	bool isServer;

public:
	// Constructor
	INetworkObject();

	// Vital creation messages
	virtual void readCreation(Message* message) {}
	virtual void writeCreation(Message* message) {}

	// Non-vital updates (ex: position)
	virtual void readUpdate(Message* message) {}
	virtual void writeUpdate(Message* message) {}

	// Vital-updates (ex: shooting, projectile hitting)
	virtual void readAction(Message* message) {}
	virtual void writeAction(Message* message) {}

	bool isDead();
};

#endif