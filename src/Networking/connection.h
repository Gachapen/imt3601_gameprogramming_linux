#ifndef CONNECTION_H
#define CONNECTION_H

#include <time.h>
#include <mutex>
#include "linkedlist.h"
#include "networking.h"
#include "networking\iclient.h"

class Message;
class Server;

class Connection : public INetworkClient
{
	friend class Server;

	struct PingRequest
	{
		unsigned short	identifier;
		time_t			sent;
		unsigned short	life;
	};

protected:
	// Data: SDLNet
	IPaddress ipAddress;
	UDPpacket *sendPacket;
	UDPsocket sendSocket;
	// Data: Buffers
	Message *packetWrapper;

	// Data: Threading
	std::mutex connectionMutex;
	// Data: Queues
	List < std::pair < unsigned char, Message * >> outQueue;
	List<Message> inQueue;
	List<PingRequest> pings;
	// Data: Sequence numbers
	unsigned short inSeqNumber[COUNT_SYNCMODES];
	unsigned short outSeqNumber[COUNT_SYNCMODES];
	// Data: Time
	float rtt;
	time_t lastHeardFrom;
	unsigned short nextPingId;

	// Static data
	static float rttSmoothingRate;
	static size_t maxPacketSize;

	// Private methods (used by friend-class)
	void _SetData(IPaddress ipAddress, UDPpacket *sendPacket, UDPsocket sendSocket);
	void _PassMessage(unsigned char type, Message *message, unsigned short sequenceNumber);
	void _PassPingResponse(time_t timeStamp, unsigned short number);

public:
	// Constructor
	Connection();
	~Connection();

	// Methods: Message
	void QueueForSending(Message *message, SyncMode syncMode = SYNCMODE_UNRELIABLE_NOORDER);
	bool SendMessages();
	Message *HasNewMessage();

	// Methods: Checks
	bool HasIP(IPaddress address, bool alsoSamePort = true) const;

	// Methods Time functions.
	float GetRTT() const;
	void QueuePingMessage();
	time_t GetLastHeardFrom() const;
	const IPaddress GetAddress();
	bool HasTimedOut();

	// Static methods
	static void SetRttSmoothingRate(float value);
	static void SetMaxPacketSize(size_t value);

	//// Methods: interface specific
	virtual void Release();
	virtual Byte *HasNewMessage(Uint32& sizeOutput);
	virtual void QueueForSending(Byte *data, size_t size, bool reliable = false);
};
#endif
