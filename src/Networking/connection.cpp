#include "Connection.h"

#include <time.h>			// time()

#include "messagetypes.h"	// MTYPE_ macros
#include "message.h"		// Message class
#include "util\util.h"
#include "shareddefs.h"

// Constructor
Connection::Connection()
{
	rtt = 25;	// Start optimistic
	lastHeardFrom = time(NULL);
	sendPacket = nullptr;
	sendSocket = nullptr;
	ipAddress.host = 0U;
	ipAddress.port = 0U;

	packetWrapper = new Message(NETWORKING_MAX_PACKET_SIZE);

	for (Uint32 i = 0; i < COUNT_SYNCMODES; i++)
	{
		inSeqNumber[i] = 0U;
		outSeqNumber[i] = 1U;
	}
}

Connection::~Connection()
{
	delete packetWrapper;
}

// Methods: Message
void Connection::QueueForSending(Message *message, SyncMode syncMode)
{
	message->AddLife();
	if (syncMode == SYNCMODE_UNRELIABLE_NOORDER)
	{
		outQueue.add(new std::pair<unsigned char, Message *>(MTYPE_NORMAL, message));
	}
	else
	{
		outQueue.add(new std::pair<unsigned char, Message *>(MTYPE_RELIABLE, message));
	}
}

bool Connection::SendMessages()
{
	// While there is data to send...
	while (outQueue.getCount() > 0)
	{
		connectionMutex.lock();

		// Reset cursor
		packetWrapper->ResetWriteCursor();

		// TODO: Write acks
		packetWrapper->WriteUint16(0U);
		packetWrapper->WriteUint8(0U);

		unsigned char count = 0U;

		// While it can be packed into a packet
		while (packetWrapper->GetSize() < maxPacketSize && outQueue.getCount() > 0)
		{
			// Increment count
			count++;

			// Fetch message
			auto current = outQueue.removeFirst();
			unsigned char messageType = current->first;
			Message *message = current->second;

			// Write message header: type, sequence number, length
			packetWrapper->WriteUint8(messageType);
			packetWrapper->WriteUint16(outSeqNumber[messageType]++);
			packetWrapper->WriteUint16(message->GetSize());

			// Write message body
			packetWrapper->WriteBytes(message->GetData(), message->GetSize());

			// TODO: Store away messages waiting for ack.

			// Clean up, and try killing the Message, which deletes it if nothing else has a stake in it.
			delete current;
			message->TryKill();
		}

		// Overwrite count
		packetWrapper->OverwritePart((char *)&count, 2U, 1U);

		// If failed, count messages as lost and return.
		if (Networking::SendUdpPacket(sendSocket, ipAddress, sendPacket, packetWrapper))
		{
			connectionMutex.unlock();
			return false;
		}

		connectionMutex.unlock();
	}

	return true;
}

Message *Connection::HasNewMessage()
{
	Message *retVal = nullptr;

	if (inQueue.getCount() > 0)
	{
		connectionMutex.lock();
		retVal = inQueue.removeFirst();
		connectionMutex.unlock();
	}

	return retVal;
}

// Methods: Checks
bool Connection::HasIP(IPaddress address, bool alsoSamePort) const
{
	return(ipAddress.host == address.host && ((alsoSamePort == false) || (ipAddress.port == address.port)));
}

// Time functions.
float Connection::GetRTT() const
{
	return rtt;
}

void Connection::QueuePingMessage()
{
	connectionMutex.lock();

	// Construct message
	Message *pingMessage = new Message(8);
	pingMessage->WriteInt16(nextPingId);

	// Note down the request
	PingRequest *request = new PingRequest();
	request->life = 2000;
	request->identifier = nextPingId;
	request->sent = time(NULL);

	// Add the ping for later reference
	outQueue.add(new std::pair<unsigned char, Message *>(MTYPE_PING, pingMessage));
	pings.add(request);

	// Increment ping count
	nextPingId++;

	connectionMutex.unlock();
}

time_t Connection::GetLastHeardFrom() const
{
	return lastHeardFrom;
}

// Private methods
void Connection::_SetData(IPaddress ipAddress, UDPpacket *sendPacket, UDPsocket sendSocket)
{
	connectionMutex.lock();
	this->ipAddress = ipAddress;
	this->sendPacket = sendPacket;
	this->sendSocket = sendSocket;
	connectionMutex.unlock();
}

void Connection::_PassMessage(unsigned char type, Message *message, unsigned short sequenceNumber)
{
	lastHeardFrom = time(NULL);

	// Allow no order, but try kill duplicates here
	if ((type == MTYPE_NORMAL) && (sequenceNumber == inSeqNumber[SYNCMODE_UNRELIABLE_NOORDER]))
	{
		message->TryKill();
		return;
	}
	else
	{
		inSeqNumber[SYNCMODE_UNRELIABLE_NOORDER] = sequenceNumber;
	}

	// TODO: Check reliable-marked messages to avoid duplicates
	// TODO: Request resends when realizing one's missing

	inQueue.add(message);

	// TODO: Acknowledgement when "type" is MTYPE_RELIABLE
}

void Connection::_PassPingResponse(time_t timeStamp, unsigned short number)
{
	connectionMutex.lock();

	// Iterate through ping list
	for (pings.begin(); !pings.end(); pings.next())
	{
		PingRequest *current = pings.getCurrent();

		// Check if identifier match
		if (current->identifier == number)
		{
			// Add the difference * rttSmoothingRate
			rtt += ((float(timeStamp - current->sent) - rtt) * rttSmoothingRate);
			pings.remove(current);
			delete current;
		}
		else if (current->life < timeStamp - current->sent)
		{
			pings.remove(current);
			delete current;
		}
	}

	lastHeardFrom = timeStamp;
	connectionMutex.unlock();
}

void Connection::Release()
{
	delete this;
}
Byte *Connection::HasNewMessage(Uint32& sizeOutput)
{
	return nullptr;
}
void Connection::QueueForSending(Byte *data, size_t size, bool reliable)
{
	return;
}
bool Connection::HasTimedOut()
{
	return false;
}

const IPaddress Connection::GetAddress()
{
	return ipAddress;
}

// Static variables (default is recommended values)
float Connection::rttSmoothingRate = 0.1f;	//
size_t Connection::maxPacketSize = NETWORKING_MAX_PACKET_SIZE;		// See: http://stackoverflow.com/questions/1098897/what-is-the-largest-safe-udp-packet-size-on-the-internet
