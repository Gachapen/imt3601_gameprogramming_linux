/*
 *  ! The use of Messages themselves aren't inherently thread-safe !
 *
 *  If you are to use Messages in more than one thread, follow these rules.
 *  - Encapsulate function-calls in mutex locks/unlocks
 *  - Use your own buffer, or have a global mutex for Messages
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include <vector>

class Message
{
private:
	char *data;				// Content
	size_t readCursor;		// Reading cursor
	size_t writeCursor;		// Writing cursor
	size_t currentSize;		// Current size
	size_t capacity;		// Capacity
	size_t lives;			// Safe deletion mechanic
	bool dontFreeBuffer;	// Keep the buffer on desctruction	

	// Realloc functions
	void _ReAllocate(size_t newSize);

	// Template functions
	template<typename T> void _TemplateWrite(const T value)
	{
		static const size_t size = sizeof(T);

		WriteBytes((char *)&value, size);
	}

	template<typename T> T _TemplateRead(char *buffer = defaultBuffer)
	{
		static const size_t size = sizeof(T);

		return *(T *)ReadBytes(size, buffer);
	}

public:
	// Constructor/Destructor
	Message(size_t startSize = 2048);
	Message(char *data, size_t size);
	~Message();

	// Cursor Functions
	void SetReadCursor(size_t pos);
	void ResetReadCursor();
	size_t GetReadCursor();
	void ResetWriteCursor();

	// Basic Read/Write
	char *ReadBytes(size_t size, char *buffer = defaultBuffer);
	void WriteBytes(char *bytes, size_t size);

	// Integer Writing
	void WriteInt8(char value);
	void WriteUint8(unsigned char value);
	void WriteInt16(short value);
	void WriteUint16(unsigned short value);
	void WriteInt32(long value);
	void WriteUint32(unsigned long value);
	void WriteInt64(long long value);
	void WriteUint64(unsigned long long value);

	// Integer Reading
	char ReadInt8();
	unsigned char ReadUint8();
	short ReadInt16(char *buffer = defaultBuffer);
	unsigned short ReadUint16(char *buffer = defaultBuffer);
	long ReadInt32(char *buffer = defaultBuffer);
	unsigned long ReadUint32(char *buffer = defaultBuffer);
	long long ReadInt64(char *buffer = defaultBuffer);
	unsigned long long ReadUint64(char *buffer = defaultBuffer);

	// Strings (16-bit prefix)
	char *ReadString(unsigned short *lengthOutput = nullptr, char *buffer = defaultBuffer);
	void WriteString(char *str);

	// Overwriting (for re-use of message)
	void OverwriteData(char *source, size_t size);
	void OverwritePart(char *source, size_t destIndex, size_t size);

	// Export-related functions
	size_t GetSize() const;
	char *CopyTo(char *buffer = defaultBuffer);
	char *GetData() const;

	// Safe deletion mechanism
	void AddLives(size_t amount);
	void AddLife();
	bool TryKill();

	// Static data
	static char defaultBuffer[];
};
#endif
