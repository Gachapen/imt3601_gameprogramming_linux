#include "server.h"
#include "client.h"
#include "message.h"

#include "Networking.h"

Networking::Networking()
{
}

SINGLETON_INSTANCE(Networking);
EXPOSE_APP(INTERFACE_NETWORKING_VERSION, Networking::GetInstance());

// Static members' instances
std::mutex Networking::sendMutex;

// Public methods
bool Networking::Init()
{
	if (SDLNet_Init() < 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void Networking::Shutdown()
{
	SDLNet_Quit();
}

INetworkServer *Networking::CreateServer(Uint16 port)
{
	return new Server(port);
}

INetworkClient *Networking::CreateClient()
{
	return new Client();
}

unsigned short Networking::GetNetworkOrderShort(unsigned short value)
{
	Uint16 newInt;
	_SDLNet_Write16(value, &newInt);

	return newInt;
}

unsigned int Networking::GetNetworkOrderUInt(unsigned int value)
{
	Uint32 newInt;
	_SDLNet_Write32(value, &newInt);

	return newInt;
}

// Static methods
bool Networking::SendUdpPacket(UDPsocket socket, IPaddress destination, UDPpacket *packet, Message *message)
{
	if (message->GetSize() > (Uint32)packet->maxlen)
	{
		Assert(0);
		return false;
	}

	// Thread-safely copy and send.
	//sendMutex.lock();
	memcpy(packet->data, message->GetData(), message->GetSize());
	packet->address = destination;
	packet->len = message->GetSize();
	int sendResult = SDLNet_UDP_Send(socket, -1, packet);
	//sendMutex.unlock();

	return(sendResult != 0);
}

bool Networking::SendUdpPacket(UDPsocket socket, IPaddress destination, UDPpacket *packet, Byte *data, Uint32 size)
{
	if (size > (Uint32)packet->maxlen)
	{
		Assert(0);
		return false;
	}

	// Thread-safely copy and send.
	//sendMutex.lock();
	memcpy(packet->data, data, size);
	packet->address = destination;
	packet->len = size;
	int sendResult = SDLNet_UDP_Send(socket, -1, packet);
	//sendMutex.unlock();

	return(sendResult != 0);
}