/*
 *
 *  This is old code I'm reusing, hence it not being in the same style.
 *
 */

#ifndef LIST_H
#define LIST_H

template<typename T> class List
{
	struct Node								// The node
	{
		T		*reference;					// Content
		Node	*next;						// Next

		Node(T *reference)					// Constructor
		{
			this->reference = reference;	//
			next = nullptr;					// Clear values
		}
	};

private:
	Node *first;					// Head node
	Node **tail;					// Where to connect next new node
	size_t count;					// Item count
	Node *iter;						// Iterator value
	Node *iterPrev;					// Last visited node
	bool skipNext;					// Iterator state set when 'iter' removed

public:
	List()						// Constructor
	{
		first = nullptr;		//
		tail = &first;			// Clear values
		count = 0;				//
	}

	void add(T *reference)				// Add new item
	{
		*tail = new Node(reference);	// Add new node at the end.
		tail = &((*tail)->next);		// Set tail to its next
		count++;						// Increment count
	}

	bool remove(T *reference)		// Remove item
	{
		Node *current = first;		// Current node
		Node *prev = nullptr;		// Previous node

		if (iter == current)		// If iterator's there already
		{
			current = iter;			// Put current there
			prev = iterPrev;		// Note it previous
		}

		while (current != nullptr)					// While still in list
		{
			if (current->reference == reference)	// If found reference
			{
				if (prev != nullptr)				// If its not first
				{
					prev->next = current->next;		// Connect previous to next
					if (prev->next == nullptr)		// If current was last
					{
						tail = &(prev->next);		// Move tail back.
					}
				}
				else								// If it happens to be first
				{
					first = current->next;			// Connect head to it
					if (first == nullptr)			// If this was only node
					{
						tail = &first;				// Move tail to head
					}
				}

				if (iter == current)					// If the iterator is on current
				{
					iter = current->next;				// Move it one forward
					skipNext = true;					// Tell it to not do so on Next()
				}

				count--;							// Excrement count
				delete current;						// Delete current node
				return true;						// Return success
			}

			prev = current;
			current = current->next;
		}

		return false;
	}

	T *removeFirst()
	{
		T *ref = first->reference;

		if (tail == &(first->next))
		{
			tail = &first;
		}

		Node *firstWas = first;

		first = first->next;
		delete firstWas;
		count--;

		return ref;
	}

	T removeFirstAsValue()
	{
		T *ref = first->reference;

		if (tail == &(first->next))
		{
			tail = &first;
		}

		Node *firstWas = first;
		T value = *firstWas->reference;
		first = first->next;

		delete firstWas->reference;
		delete firstWas;
		count--;

		return value;
	}

	size_t getCount()					// Get count
	{
		return count;					// Return count value
	}

	bool contains(T *reference)
	{
		Node *current = first;

		while (current != nullptr)
		{
			if (current->reference == reference)
			{
				return true;
			}

			current = current->next;
		}

		return false;
	}

	void clear()
	{
		count = 0;
		iter = nullptr;
		Node *current = first;
		Node *next;
		if (current != nullptr)
		{
			next = current->next;
			delete current;
			current = next;
		}

		first = nullptr;
		tail = &first;
	}

	void begin()					// Begin iteration
	{
		iter = first;				// Put iter at the front
		iterPrev = nullptr;			// Clear values
		skipNext = false;			//
	}

	bool end()						// Iteration done?
	{
		return(iter == nullptr);	// Return if iter's out of the list
	}

	void next()						// Go to next
	{
		if (iter != nullptr)		// If iter's still in list
		{
			if (!skipNext)			// If not instructed to not move forward
			{
				iterPrev = iter;	// Set prev-value to current
				iter = iter->next;	// Move iterator forward
			}
			else					// If instructed to skip
			{
				skipNext = false;	// Untoggle the skipNext state
			}
		}
	}

	T *getCurrent()					// Get current
	{
		return (iter != nullptr) ? iter->reference : nullptr;
	}

	T *getPrevious()				// Get previous
	{
		return (iterPrev != nullptr) ? iterPrev->reference : nullptr;
	}

	T *peekNext()					// Peek at the next
	{
		return (iter->next != nullptr) ? iter->next->reference : nullptr;
	}
};
#endif
