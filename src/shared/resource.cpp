
#include "pch.h"
#include "resource.h"
#include "util/xmlutil.h"

SINGLETON_INSTANCE(Resources);

Resources::Resources()
{
}

bool Resources::Init()
{
	CXMLDocument doc;

	if (!LoadXMLFile("scripts/tower_manifest.xml", doc))
	{
		return false;
	}

	rapidxml::xml_node<> *root = doc.GetDocumentRoot();

	for (rapidxml::xml_node<> *towerNode = root->first_node("tower", 0U, false);
		towerNode != nullptr;
		towerNode = towerNode->next_sibling("tower", 0U, false))
	{
		ResourceTower tower;
		// require
		if (!GetXMLAttrInt(towerNode, "id", tower.id))
			continue;

		if (!GetXMLNodeString(towerNode, "name", tower.name))
			continue;

		if (!GetXMLNodeString(towerNode, "icon", tower.icon))
			continue;

		if (!GetXMLNodeString(towerNode, "model", tower.model))
			continue;

		if (!GetXMLNodeString(towerNode, "animation", tower.animation))
			continue;

		if (!GetXMLNodeInt(towerNode, "cost", tower.cost))
			continue;

		if (!GetXMLNodeFloat(towerNode, "firedelay", tower.fireDelay))
			continue;

		if (!GetXMLNodeFloat(towerNode, "range", tower.range))
			continue;

		if (!GetXMLNodeInt(towerNode, "damage", tower.damage))
			continue;

		if (!GetXMLNodeFloat(towerNode, "rotation", tower.rotation))
			tower.rotation = 0.0f;

		rapidxml::xml_node<> *upgradeParent = towerNode->first_node("upgrades", 0U, false);

		if (upgradeParent != nullptr)
		{
			for (rapidxml::xml_node<> *upgradeNode = upgradeParent->first_node("upgrade", 0U, false);
				upgradeNode != nullptr;
				upgradeNode = upgradeNode->next_sibling("upgrade", 0U, false))
			{
				ResourceTowerUpgrade upgrade;

				if (!GetXMLNodeInt(upgradeNode, "id", upgrade.id))
					continue;

				if (!GetXMLNodeInt(upgradeNode, "cost", upgrade.cost))
					continue;

				if (!GetXMLNodeFloat(upgradeNode, "duration", upgrade.duration))
					continue;

				tower.upgrades.push_back(upgrade);
			}
		}

		towers.push_back(tower);
	}

	return true;
}

const std::vector<ResourceTower> &Resources::GetTowers() const
{
	return towers;
}