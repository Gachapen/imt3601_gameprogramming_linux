
#include "pch.h"
#include "pathfinder.h"


PathFinder::PathFinder()
	: pathFindingNodes(nullptr)
	, pathFindingVisited(nullptr)
	, tileCount(0)
	, tileWidth(0)
	, tileHeight(0)
{
}

PathFinder::~PathFinder()
{
	delete [] pathFindingNodes;
	delete [] pathFindingVisited;
}

void PathFinder::Init(IWorld *world)
{
	Assert(world != nullptr);

	pairs.clear();
	tileCount = world->GetTileCount();
	tileWidth = world->GetWidth();
	tileHeight = world->GetHeight();

	delete [] pathFindingNodes;
	delete [] pathFindingVisited;

	pathFindingNodes = new AStarNode[tileCount];
	pathFindingVisited = new bool[tileCount];

	memset(pathFindingVisited, 0, sizeof(bool) * tileCount);

	const int width = world->GetWidth();
	const int height = world->GetHeight();

	for (int i = 0; i < tileCount; i++)
	{
		const WorldTile &tile = world->GetTile(i);

		AStarNode &node = pathFindingNodes[i];
		node.flags = 0;
		node.costDeathMap = 0.0f;

		if ((tile.flags & TileFlags::WALKABLE) == 0)
		{
			node.flags |= PATHFIND_SOLID;
		}

		node.index = i;
		node.x = tile.x;
		node.y = tile.y;

		if ((i % width) != 0)
			node.links[0] = &pathFindingNodes[i - 1];
		else
			node.links[0] = nullptr;

		if ((i % width) != width - 1)
			node.links[2] = &pathFindingNodes[i + 1];
		else
			node.links[2] = nullptr;

		if ((i / width) > 0)
			node.links[1] = &pathFindingNodes[i - width];
		else
			node.links[1] = nullptr;

		if ((i / width) < height - 1)
			node.links[3] = &pathFindingNodes[i + width];
		else
			node.links[3] = nullptr;

		node.creepCount = 0;
	}

	std::vector<int> startTiles;
	std::vector<int> endTiles;

	// find all start and end tiles
	for (int i = 0; i < world->GetTileCount(); i++)
	{
		const WorldTile &tile = world->GetTile(i);

		if ((tile.flags & TileFlags::SPAWN) != 0)
		{
			startTiles.push_back(i);
		}
		else if ((tile.flags & TileFlags::END) != 0)
		{
			endTiles.push_back(i);
		}
	}

	// for each start tile, find a fitting end tile and store as pair
	for (auto startTile : startTiles)
	{
		for (auto endTile : endTiles)
		{
			const bool hasPath = ConstructPath(startTile, endTile) != nullptr;

			if (hasPath)
			{
				PathPair pair;
				pair.start = startTile;
				pair.end = endTile;

				pairs.push_back(pair);
			}
		}
	}
}

void PathFinder::ModifyNodeCreepCount(int tileIndex, int delta)
{
	Assert(tileIndex >= 0 && tileIndex < tileCount);
	Assert(delta != 0);
	Assert(pathFindingNodes[tileIndex].creepCount > 0
		|| delta > 0);

	pathFindingNodes[tileIndex].creepCount += delta;
}

int PathFinder::GetNodeCreepCount(int tileIndex) const
{
	Assert(tileIndex >= 0 && tileIndex < tileCount);

	return pathFindingNodes[tileIndex].creepCount;
}

int PathFinder::GetNodeCount() const
{
	return tileCount;
}

void PathFinder::ModifyNodeDeathCost(int tileIndex, int radius, float delta)
{
	Assert(tileIndex >= 0 && tileIndex < tileCount);
	Assert(radius >= 0);

	int tileX = tileIndex % tileWidth;
	int tileY = tileIndex / tileWidth;

	for (int x = tileX - radius; x <= tileX + radius; x++)
	{
		if (x < 0 || x >= tileWidth)
		{
			continue;
		}

		for (int y = tileY - radius; y <= tileY + radius; y++)
		{
			if (y < 0 || y >= tileHeight)
			{
				continue;
			}

			const int currentIndex = x + y * tileWidth;
			float weight = vec2(x - tileX, y - tileY).norm() / sqr(radius);

			weight = 1.0f - weight;
			weight *= weight;

			Assert(currentIndex >= 0 && currentIndex < tileCount);

			pathFindingNodes[currentIndex].costDeathMap += delta * weight;

			pathFindingNodes[currentIndex].costDeathMap = MAX(0.0f, pathFindingNodes[currentIndex].costDeathMap);
		}
	}
}

float PathFinder::GetNodeDeathCost(int tileIndex)
{
	Assert(tileIndex >= 0 && tileIndex < tileCount);

	return pathFindingNodes[tileIndex].costDeathMap;
}

void PathFinder::DecayDeathCost(float delta)
{
	for (int i = 0; i < tileCount; i++)
	{
		pathFindingNodes[i].costDeathMap -= delta;

		pathFindingNodes[i].costDeathMap = MAX(0.0f, pathFindingNodes[i].costDeathMap);
	}
}

void PathFinder::SetTileBlocked(int tileIndex, bool isBlocked)
{
	Assert(tileIndex >= 0 && tileIndex < tileCount);

	if (isBlocked)
	{
		pathFindingNodes[tileIndex].flags |= PATHFIND_BLOCKED;
	}
	else
	{
		pathFindingNodes[tileIndex].flags &= ~PATHFIND_BLOCKED;
	}
}

bool PathFinder::IsTileBlocked(int tileIndex) const
{
	Assert(tileIndex >= 0 && tileIndex < tileCount);

	return (pathFindingNodes[tileIndex].flags & PATHFIND_BLOCKED) != 0;
}

bool PathFinder::IsAnyPairBlocked()
{
	for (const auto &p : pairs)
	{
		if (ConstructPath(p.start, p.end) == nullptr)
		{
			return true;
		}
	}

	return false;
}

const PathFinder::AStarNode *PathFinder::ConstructPath(int tileStart, int tileGoal)
{
	if (tileStart < 0 || tileStart >= tileCount)
	{
		Assert(0);
		return nullptr;
	}

	if (tileGoal < 0 || tileGoal >= tileCount)
	{
		Assert(0);
		return nullptr;
	}

	if (pathFindingVisited == nullptr)
	{
		Assert(0);
		return nullptr;
	}

	struct AStarNodeLess
	{
		bool operator()(const AStarNode *left, const AStarNode *right) const
		{
			return (left->costReach + left->costHeuristic + left->costDeathMap)
				> (right->costReach + right->costHeuristic + right->costDeathMap);
		}

		static float DistanceCost(const AStarNode *a, const AStarNode *b)
		{
			return float(abs(b->y - a->y) + abs(b->x - a->x));
		}
	};

	memset(pathFindingVisited, 0, sizeof(bool) * tileCount);
	std::vector<AStarNode*> queue;

	AStarNode *startNode = &pathFindingNodes[tileStart];
	AStarNode *goalNode = &pathFindingNodes[tileGoal];

	if (startNode->flags != 0
		|| goalNode->flags != 0)
	{
		return false;
	}

	pathFindingVisited[tileStart] = true;
	startNode->costHeuristic = 0.0f;
	startNode->costReach = 0.0f;
	startNode->previous = nullptr;

	queue.push_back(startNode);
	std::make_heap(queue.begin(), queue.end(), AStarNodeLess());

	while (!queue.empty())
	{
		AStarNode *tile = queue.front();
		std::pop_heap(queue.begin(), queue.end(), AStarNodeLess());
		queue.pop_back();

		if (tile->index == tileGoal)
			return tile;

		pathFindingVisited[tile->index] = true;

		for (int i = 0; i < 4; i++)
		{
			AStarNode *nextTile = tile->links[i];

			if (nextTile != nullptr
				&& nextTile->flags == 0)
			{
				nextTile->costHeuristic = AStarNodeLess::DistanceCost(nextTile, goalNode);
				float newCost = tile->costReach + 1 + nextTile->costHeuristic;

				if (pathFindingVisited[nextTile->index]
					//&& newCost > nextTile->costReach + nextTile->costHeuristic)
					&& newCost >= nextTile->costReach + nextTile->costHeuristic)
					continue;

				Assert(nextTile != startNode);
				nextTile->costReach = tile->costReach + 1;
				nextTile->previous = tile;

				if (pathFindingVisited[nextTile->index])
				{
					std::make_heap(queue.begin(), queue.end(), AStarNodeLess());
				}
				else
				{
					queue.push_back(nextTile);
					std::push_heap(queue.begin(), queue.end(), AStarNodeLess());

					pathFindingVisited[nextTile->index] = true;
				}
			}
		}
	}

	return nullptr;
}
