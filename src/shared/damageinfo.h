#ifndef DAMAGEINFO_H
#define DAMAGEINFO_H


struct DamageInfo
{
	DamageInfo()
		: origin(vec3::Zero())
		, damage(0)
		, inflictorNetworkId(-1)
	{
	}

	vec3 origin;
	int damage;
	int inflictorNetworkId;
};


#endif