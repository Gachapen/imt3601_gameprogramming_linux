#ifndef NETWORKTABLE_H
#define NETWORKTABLE_H

#include <unordered_set>

#include "util/platform.h"
#include "util/bytebuffer.h"
#include "util/callbackutil.h"
#include "util/interpolateutil.h"



class INetworkMember
{
public:
	virtual ~INetworkMember() {}

	virtual bool IsReliable() const = 0;
	virtual bool HasChanged() const = 0;

	virtual Uint8 GetIndex() const = 0;
	virtual void SetIndex(Uint8 index) = 0;

	virtual const char *GetName() const = 0;
	virtual void SetName(const char *nameLiteral) = 0;

	virtual int GetSize() const = 0;
	virtual bool IsSizeDynamic() const = 0;

	virtual void GetChanges(ByteBuffer &buffer) = 0;
	virtual void ApplyChanges(ByteBuffer &buffer, int frame = -1) = 0;
};


class NetworkMemberBase : public INetworkMember
{
public:
	NetworkMemberBase()
		: hasChanged(false)
		  , index(0)
		  , name("")
	{
	}

	virtual bool IsReliable() const { return false; }
	virtual bool HasChanged() const { return hasChanged; }

	virtual Uint8 GetIndex() const { return index; }
	virtual void SetIndex(Uint8 index) { this->index = index; }

	virtual const char *GetName() const { return name; }
	virtual void SetName(const char *nameLiteral) { name = nameLiteral; }

protected:
	inline void SetHasChanged(bool hasChanged) { this->hasChanged = hasChanged; }

private:
	bool hasChanged;

	Uint8 index;
	const char *name;
};


template<typename T>
class NetworkVariable : public NetworkMemberBase
{
public:
	NetworkVariable()
		  : lastFrameUpdated(-1)
	{
		memset(&value, 0, sizeof(T));
	}

	virtual bool IsReliable() const { return false; }

	virtual int GetSize() const { return sizeof(T); }
	virtual bool IsSizeDynamic() const { return false; }

	virtual void GetChanges(ByteBuffer &buffer)
	{
		buffer.Write(&value, sizeof(T));

		SetHasChanged(false);
	}

	virtual void ApplyChanges(ByteBuffer &buffer, int frame)
	{
		if (frame >= 0)
		{
			if (frame <= lastFrameUpdated)
			{
				return;
			}
			else
			{
				lastFrameUpdated = frame;
			}
		}

		buffer.Read(sizeof(T), &value);

		for (auto itr : callbacks)
		{
			itr->Run(&value);
		}
	}

	inline const T &Get()
	{
		return value;
	}

	inline const T &Get() const
	{
		return value;
	}

	inline void Set(const T &newValue)
	{
		if (newValue != value)
		{
			value = newValue;
			SetHasChanged(true);
		}
	}

	inline void AddCallback(ICallback *callback)
	{
		callbacks.insert(callback);
	}

private:
	T value;

	int lastFrameUpdated;
	std::unordered_set<ICallback *> callbacks;
};


class NetworkProcedureBase : public NetworkMemberBase
{
public:
	virtual bool IsReliable() const { return false; }
	virtual bool IsSizeDynamic() const { return true; }
};


template<typename C, typename P1>
class NetworkProcedureParams1 : public NetworkProcedureBase
{
public:
	typedef const P1& P1Const;
	typedef void (C::*func)(P1Const p1);

#ifdef SERVER_DLL
	NetworkProcedureParams1() : obj(nullptr), function(nullptr) {}
#else
	NetworkProcedureParams1(C *obj, func function)
	{
		Assert(obj != nullptr);
		Assert(function != nullptr);

		this->obj = obj;
		this->function = function;
	}
#endif

	virtual int GetSize() const { return sizeof(P1); }

	virtual void GetChanges(ByteBuffer &buffer)
	{
		buffer.WriteUChar(Uint8(paramData.size()));

		for (const auto &d : paramData)
		{
			buffer.Write(&d, sizeof(P1));
		}

		paramData.clear();

		SetHasChanged(false);
	}

	virtual void ApplyChanges(ByteBuffer &buffer, int frame)
	{
#ifdef SERVER_DLL
		Assert(0);
#else
		Uint8 count = buffer.ReadUChar();

		for (int i = 0; i < count; i++)
		{
			P1 p1;
			buffer.Read(sizeof(P1), &p1);

			(obj->*function)(p1);
		}
#endif
	}

	inline void operator()(P1Const p1)
	{
		if (paramData.size() > 255)
		{
			return;
		}

		paramData.push_back(p1);

		SetHasChanged(true);
	}

private:
	C *obj;
	func function;

	std::vector<P1> paramData;
};

#define DECLARE_NETWORKPROCEDURE_1PARAM(Param1Type, name) \
	NetworkProcedureParams1<ThisClass, Param1Type> name

#define DECLARE_NETWORKTABLE()								  \
public:														  \
	class NetworkTable										  \
	{														  \
public:														  \
		inline void AddNetworkMember(INetworkMember * member) \
		{													  \
			networkVariables.push_back(member);				  \
			networkVariablesBase = networkVariables.data();	  \
			networkVariablesCount = networkVariables.size();  \
		}													  \
		std::vector<INetworkMember *> networkVariables;		  \
		INetworkMember **networkVariablesBase;				  \
		int networkVariablesCount;							  \
		int currentIndex;									  \
	};														  \
	NetworkTable networkTable;								  \
	virtual void RegisterNetworkMembers();					  \
	virtual void SetGlobalNetworktableIndex(int index)		  \
	{														  \
		globalTableIndex = index;							  \
	}														  \
	virtual int GetGlobalNetworktableIndex()				  \
	{														  \
		return globalTableIndex;							  \
	}														  \
	virtual const char *GetNetworkClassName();				  \
	virtual void SetNetworkClassName(const char *name);		  \
private:													  \
	static const char *networkClassName;					  \
	static int globalTableIndex


#define DECLARE_NETWORKTABLE_BASE()					   \
public:												   \
	virtual void RegisterNetworkMembers();			   \
	virtual void SetGlobalNetworktableIndex(int index) \
	{												   \
		globalTableIndex = index;					   \
	}												   \
	virtual int GetGlobalNetworktableIndex()		   \
	{												   \
		return globalTableIndex;					   \
	}												   \
	virtual const char *GetNetworkClassName();		   \
	virtual void SetNetworkClassName(const char *name);		  \
private:											   \
	static const char *networkClassName;			   \
	static int globalTableIndex

#define IMPLEMENT_NETWORKTABLE(className)			   \
	const char *className::networkClassName = "";	   \
	int className::globalTableIndex = -1;			   \
	const char *className::GetNetworkClassName()				  \
	{ \
		return networkClassName; \
	} \
	void className::SetNetworkClassName(const char *name)		  \
	{ \
		networkClassName = name; \
	} \
	void className::RegisterNetworkMembers()		   \
	{												   \
		Assert(networkTable.networkVariables.empty()); \
		networkTable.networkVariablesBase = nullptr;   \
		networkTable.networkVariablesCount = 0;		   \
		networkTable.currentIndex = 0;


#define IMPLEMENT_NETWORKTABLE_BASE(className) \
	const char *className::networkClassName = "";	   \
	int className::globalTableIndex = -1;			   \
	const char *className::GetNetworkClassName()				  \
	{ \
		return networkClassName; \
	} \
	void className::SetNetworkClassName(const char *name)		  \
	{ \
		networkClassName = name; \
	} \
	void className::RegisterNetworkMembers()   \
	{										   \
		BaseClass::RegisterNetworkMembers();

#define END_NETWORKTABLE() \
	}

#define NETWORK_VARIABLE(variable)					\
	variable.SetIndex(networkTable.currentIndex++);	\
	variable.SetName(# variable);					\
	networkTable.AddNetworkMember(&variable)

#define NETWORK_PROCEDURE(procedure) NETWORK_VARIABLE(procedure)

template<typename T>
class InterpolatedVarNetworked : public InterpolatedVar<T>, public ICallback
{
public:
	InterpolatedVarNetworked(NetworkVariable<T> *var)
	{
		var->AddCallback(this);
	}

	virtual void Run(void *p)
	{
		PushValue(*((T *)p), globals->GetTime());
	}
};

#define DECLARE_NETWORKVAR_CALLBACK(name, type) \
	void On ## name(type *value); \
	DECLARE_CALLBACK(name, type);
#endif
