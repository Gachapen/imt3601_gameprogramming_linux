#ifndef CREEPSHARED_H
#define CREEPSHARED_H


namespace CreepStates
{
	enum CreepState_e
	{
		IDLE = 0,
		WALKING,
		COUNT,
		DEAD,
	};
}
typedef CreepStates::CreepState_e CreepStateId;


#endif