#ifndef BUTTONS_H
#define BUTTONS_H


namespace ButtonStates
{
	enum ButtonState_e
	{
		PRESSED = (1 << 0),		/// pressed this frame
		DOWN = (1 << 1),		/// is down
		RELEASED = (1 << 2),	/// released this frame
	};
}
typedef ButtonStates::ButtonState_e ButtonStateId;


namespace Buttons
{
	enum Button_e
	{
		UP = (1 << 0),
		DOWN = (1 << 1),
		LEFT = (1 << 2),
		RIGHT = (1 << 3),
		SPEED = (1 << 4),
		ZOOM_IN = (1 << 5),
		ZOOM_OUT = (1 << 6),

		/// needs to be updated manually!
		COUNT = 7,
	};
};
typedef Buttons::Button_e ButtonId;


#endif