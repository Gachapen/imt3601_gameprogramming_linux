
#include "pch.h"
#include "vbodict.h"

SINGLETON_INSTANCE(VBODict);

VBODict::VBODict()
	: nextKey(0)
{
}

void VBODict::Shutdown()
{
	for (auto vbo : vboMap)
	{
		FreeVBO(vbo.second);
	}

	vboMap.clear();
}

VBO_KEY VBODict::CreateVBO(const VertexAttribBinding &attributes, const int sizeData, void *data,
						bool isDynamic)
{
	Assert(sizeData > 0);
	Assert(data != nullptr);

	VBOData vbo;
	vbo.attributes = attributes;

	const GLenum dataType = isDynamic ? GL_STREAM_DRAW : GL_STATIC_DRAW;

	glGenBuffers(1, &vbo.arrayBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vbo.arrayBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeData, data, dataType);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	vboMap[nextKey] = std::move(vbo);

	return nextKey++;
}

void VBODict::FreeVBO(VBO_KEY vbo)
{
	auto itr = vboMap.find(vbo);

	Assert(itr != vboMap.end());

	FreeVBO(itr->second);
	vboMap.erase(itr);
}

void VBODict::FreeVBO(VBOData &vbo)
{
	if (vbo.arrayBuffer != GL_INVALID_INDEX)
		glDeleteBuffers(1, &vbo.arrayBuffer);
}

void VBODict::ModifyVBO(VBO_KEY vbo, const int sizeData, void *data,
						bool isDynamic)
{
	auto itr = vboMap.find(vbo);

	Assert(itr != vboMap.end());

	const GLenum dataType = isDynamic ? GL_STREAM_DRAW : GL_STATIC_DRAW;

	if (sizeData > 0)
	{
		Assert(data != nullptr);
		glBindBuffer(GL_ARRAY_BUFFER, itr->second.arrayBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeData, data, dataType);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

void VBODict::BindVBO(VBO_KEY vbo, int destinationVertexFormat) const
{
	auto itr = vboMap.find(vbo);

	Assert(itr != vboMap.end());
	Assert(itr->second.arrayBuffer != GL_INVALID_INDEX);

	glBindBuffer(GL_ARRAY_BUFFER, itr->second.arrayBuffer);
	itr->second.attributes.Apply(destinationVertexFormat);

	itr->second.attributes.EnableAttributes();
}

void VBODict::UnbindVBO(VBO_KEY vbo)
{
	auto itr = vboMap.find(vbo);

	Assert(itr != vboMap.end());
	Assert(itr->second.arrayBuffer != GL_INVALID_INDEX);

	itr->second.attributes.DisableAttributes();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

float *VBODict::BindMap(VBO_KEY vbo, int offset, int range)
{
	auto itr = vboMap.find(vbo);

	Assert(itr != vboMap.end());
	Assert(itr->second.arrayBuffer != GL_INVALID_INDEX);

	glBindBuffer(GL_ARRAY_BUFFER, itr->second.arrayBuffer);

	if (offset >= 0)
	{
		// GL_MAP_INVALIDATE_BUFFER_BIT										// new buffer/orphaning
		// GL_MAP_INVALIDATE_RANGE_BIT										// same as above but allows memory to be uninitialized
		// GL_MAP_UNSYNCHRONIZED_BIT										// requires manual synchronization = best?
		//glBufferData(GL_ARRAY_BUFFER, range, nullptr, GL_STREAM_DRAW);	// new buffer/orphaning

		return (float *)glMapBufferRange(GL_ARRAY_BUFFER, offset, range, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
	}
	else
	{
		return (float *)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
	}
}

void VBODict::BindUnmap(VBO_KEY vbo)
{
	auto itr = vboMap.find(vbo);

	Assert(itr != vboMap.end());
	Assert(itr->second.arrayBuffer != GL_INVALID_INDEX);

	glBindBuffer(GL_ARRAY_BUFFER, itr->second.arrayBuffer);
	glUnmapBuffer(GL_ARRAY_BUFFER);
}