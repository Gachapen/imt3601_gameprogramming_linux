#include "pch.h"
#include "fbodict.h"
#include "renderutil.h"
#include "texture.h"
#include "texturedict.h"
#include "rendercontext.h"
#include "appinterface\iappinterface.h"


SINGLETON_INSTANCE(FBODict);

EXPOSE_APP(INTERFACE_IFBODICT_VERSION, FBODict::GetInstance());

FBODict::FBODict()
{
}

void FBODict::Init()
{
}

void FBODict::Shutdown()
{
	for (auto i : renderTargetList)
	{
		i.texture->DecrementReferenceCount();
	}

	for (auto i : renderBufferList)
	{
		glDeleteRenderbuffers(1, &i.id);
	}

	for (auto i : fboList)
	{
		glDeleteFramebuffers(1, &i.id);
	}

	renderTargetList.clear();
	renderBufferList.clear();
	fboList.clear();
}

void FBODict::OnResize(int w, int h)
{
	for (auto i : renderBufferList)
	{
		if (!RequiresResize(i.resizeMode))
			continue;

		int targetWidth = w;
		int targetHeight = h;

		const TextureFormats::GlFormat_s &info = TextureFormats::GetTextureFormatInfo(i.format);
		GetAdjustedSize(i.resizeMode, targetWidth, targetHeight);

		glBindRenderbuffer(GL_RENDERBUFFER, i.id);
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, i.multisampleCount, info.driverFormat, targetWidth, targetHeight);
		glBindRenderbuffer(GL_RENDERBUFFER, 0);
	}

	for (auto i : renderTargetList)
	{
		if (!RequiresResize(i.resizeMode))
			continue;

		int targetWidth = w;
		int targetHeight = h;

		GetAdjustedSize(i.resizeMode, targetWidth, targetHeight);

		const TextureFormats::GlFormat_s &info = TextureFormats::GetTextureFormatInfo(i.texture->GetFormat());

		glBindTexture(GL_TEXTURE_2D, i.texture->GetTextureId());
		glTexImage2D(GL_TEXTURE_2D, 0, info.bytesPerPixel, targetWidth, targetHeight,
					 0, info.driverFormat, info.componentType, NULL);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

FBO_HANDLE FBODict::CreateFBO()
{
	FBO fbo;

	fbo.bufferCount = 0;
	memset(fbo.buffers, 0, sizeof(fbo.buffers));

	glGenFramebuffers(1, &fbo.id);

	fboList.push_back(fbo);
	return fboList.size() - 1;
}

void FBODict::PushFBO(FBO_HANDLE handle)
{
	if (handle == FBO_HANDLE_INVALID)
	{
		fboStack.push(nullptr);
		return;
	}

	Assert(handle >= 0 && handle < fboList.size());

	const FBO &fbo = fboList[handle];
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, fbo.id);
	glDrawBuffers(fbo.bufferCount, (const GLenum*)fbo.buffers);

	fboStack.push(&fboList[handle]);
}

void FBODict::PopFBO()
{
	fboStack.pop();

	if (!fboStack.empty())
	{
		const FBO *fbo = fboStack.top();

		if (fbo != nullptr)
		{
			glBindFramebuffer(GL_FRAMEBUFFER_EXT, fbo->id);
			glDrawBuffers(fbo->bufferCount, (const GLenum*)fbo->buffers);
		}
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);
	}
}

RB_HANDLE FBODict::CreateRenderBuffer(TextureFormatId format, FramebufferResizeModeId resizeMode, int multisampleCount /*= 0*/, int width /*= -1*/, int height /*= -1*/)
{
	if (width < 0)
	{
		width = RenderContext::GetInstance()->GetScreenWidth();
	}

	if (height < 0)
	{
		height = RenderContext::GetInstance()->GetScreenHeight();
	}

	RenderBuffer buffer;
	buffer.format = format;
	buffer.multisampleCount = multisampleCount;
	buffer.resizeMode = resizeMode;

	const TextureFormats::GlFormat_s &info = TextureFormats::GetTextureFormatInfo(format);
	GetAdjustedSize(resizeMode, width, height);

	glGenRenderbuffers(1, &buffer.id);
	glBindRenderbuffer(GL_RENDERBUFFER, buffer.id);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, multisampleCount, info.driverFormat, width, height);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	renderBufferList.push_back(buffer);
	return renderBufferList.size() - 1;
}

void FBODict::BindRenderBufferColor(RB_HANDLE handle, int index /*= 0*/)
{
	Assert(handle >= 0 && handle < renderBufferList.size());
	Assert(index >= 0);
	Assert(fboStack.size() > 0);
	Assert(index < MAX_MRT_BUFFERS);

	const RenderBuffer &buffer = renderBufferList[handle];

	FBO *fbo = fboStack.top();
	fbo->bufferCount = MAX(index + 1, fbo->bufferCount);
	fbo->buffers[index] = GL_COLOR_ATTACHMENT0 + index;

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + index, GL_RENDERBUFFER, buffer.id);
}

void FBODict::BindRenderBufferDepth(RB_HANDLE handle)
{
	Assert(handle >= 0 && handle < renderBufferList.size());
	Assert(fboStack.size() > 0);

	const RenderBuffer &buffer = renderBufferList[handle];

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, buffer.id);
}

RT_HANDLE FBODict::CreateRenderTarget(const char *name, TextureFormatId format, FramebufferResizeModeId resizeMode, int width /*= -1*/, int height /*= -1*/, int flags)
{
	if (width < 0)
	{
		width = RenderContext::GetInstance()->GetScreenWidth();
	}

	if (height < 0)
	{
		height = RenderContext::GetInstance()->GetScreenHeight();
	}

	RenderTarget target;
	target.resizeMode = resizeMode;

	GetAdjustedSize(resizeMode, width, height);

	Texture *texture = new Texture();
	texture->Init(name, format, TextureTypes::TEXTURE_2D, TextureGroups::RENDERTARGETS, flags, width, height, nullptr);

	target.texture = texture;

	texture->IncrementReferenceCount();
	TextureDict::GetInstance()->InsertTexture(name, texture);
	renderTargetList.push_back(target);
	return renderTargetList.size() - 1;
}

void FBODict::BindRenderTargetColor(RT_HANDLE handle, int index /*= 0*/)
{
	Assert(handle >= 0 && handle < renderTargetList.size());
	Assert(fboStack.size() > 0);
	Assert(index < MAX_MRT_BUFFERS);

	const RenderTarget &target = renderTargetList[handle];

	FBO *fbo = fboStack.top();
	fbo->bufferCount = MAX(index + 1, fbo->bufferCount);
	fbo->buffers[index] = GL_COLOR_ATTACHMENT0 + index;

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + index, GL_TEXTURE_2D, target.texture->GetTextureId(), 0);
}

void FBODict::BindRenderTargetDepth(RT_HANDLE handle)
{
	Assert(handle >= 0 && handle < renderTargetList.size());
	Assert(fboStack.size() > 0);

	const RenderTarget &target = renderTargetList[handle];

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, target.texture->GetTextureId(), 0);
}

ITexture *FBODict::GetRenderTargetTexture(RT_HANDLE handle)
{
	Assert(handle >= 0 && handle < renderTargetList.size());

	return renderTargetList[handle].texture;
}

void FBODict::CopyFBOtoFBO(FBO_HANDLE src, FBO_HANDLE dest, FramebufferResizeModeId mode)
{
	RenderContext *context = RenderContext::GetInstance();
	int w = context->GetScreenWidth();
	int h = context->GetScreenHeight();

	GetAdjustedSize(mode, w, h);

	PushFBO(FBO_HANDLE_INVALID);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, fboList[src].id);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fboList[dest].id);

	glBlitFramebuffer(0, 0, w, h, 0, 0, w, h,
		GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

	PopFBO();
}

void FBODict::ReadFBOColor(void *pixels, int x, int y, int w, int h, int attachmentIndex)
{
	if (!fboStack.empty())
	{
		glReadBuffer((GLenum)(GL_COLOR_ATTACHMENT0_EXT + attachmentIndex));
	}

	glReadPixels(x, y, w, h, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid*)pixels);
}

void FBODict::GetAdjustedSize(FramebufferResizeModeId mode, int &w, int &h)
{
	switch (mode)
	{
	default:
		Assert(0);

	case FramebufferResizeModes::FULL_FRAMEBUFFER:
		// do nothing
		break;
	case FramebufferResizeModes::QUARTER_FRAMEBUFFER:
		w /= 4;
		h /= 4;
		break;
	case FramebufferResizeModes::OFFSCREEN:
		// do nothing
		break;
	}
}

bool FBODict::RequiresResize(FramebufferResizeModeId mode)
{
	switch (mode)
	{
	default:
		Assert(0);

	case FramebufferResizeModes::FULL_FRAMEBUFFER:
	case FramebufferResizeModes::QUARTER_FRAMEBUFFER:
		return true;
	case FramebufferResizeModes::OFFSCREEN:
		return false;
	}
}
