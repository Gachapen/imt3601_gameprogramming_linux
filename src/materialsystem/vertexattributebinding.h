#ifndef VERTEXATTRIBUTEBINDING_H
#define VERTEXATTRIBUTEBINDING_H

#include "materialsystem\materialsystem.h"

#include <vector>
#include <GL/GLU.h>

/// Writes default vertex input attributes.
/// Requires shaders to use a specific convention for input names!
class VertexAttribBinding
{
public:
	VertexAttribBinding();
	VertexAttribBinding(int vertexFormat);

	/// Add a new binding of specified size
	void AddBinding(const int size);

	void Apply() const;
	void Apply(int destinationFormat) const;

	void EnableAttributes() const;
	void DisableAttributes() const;

	int GetVertexFormatSize() const { return vertexSize; }

private:
	void Init();

	int vertexSize;
	int vertexFormat;
	int attributeCount;

	struct Binding
	{
		int size;
		GLvoid *offset;
	};

	std::vector<Binding> bindings;
};

#endif