#include "pch.h"
#include "mdxrenderablegeometry.h"
#include "mesh.h"
#include "materialsystem\meshbuilder.h"
#include "mdxlib\imdxmodel.h"
#include "mdxlib\mdxinterpolators.h"
#include "material.h"
#include "materialdict.h"

Mesh *CreateMeshFromGeoset(const MDXModelData *model, int geosetIndex)
{
	Assert(geosetIndex >= 0 && geosetIndex < int(model->pGeosets->Geosets.size()));

	MDXGeosetChunk::MDXGeoset *pGeoset = model->pGeosets->Geosets[geosetIndex];

	Mesh *mesh = new Mesh();
	mesh->Init(FaceTypes::TRIANGLES,
		VertexFormat::POSITION_3F | VertexFormat::BONEINDICES_3F | VertexFormat::TEXTURE_COORD_2F
		| VertexFormat::NORMAL_3F,
		pGeoset->Faces.size());

	MeshBuilder builder;
	builder.Attach(mesh);

	MDXDynamicChunk::MDXGeosetHelper *pGeosetHelper = model->pDynamic->GeosetHelpers[geosetIndex];

	for (unsigned int i = 0; i < pGeoset->Faces.size(); i++)
	{
		for (int f = 0; f < 3; f++)
		{
			int index = pGeoset->Faces[i].Index[f];

			vec3 pos = pGeoset->VertexPositions[index].Position;
			vec3 normal = pGeoset->VertexNormals[index].Normal;
			vec2 uv = pGeoset->VertexTexturePositions[index].TexturePosition;

			int matrixGroup = pGeoset->VertexGroups[index].MatrixGroup;

			builder.Position3p(pos.data());
			builder.Normal3p(normal.data());
			builder.Texcoord2p(uv.data());
			builder.Boneindices3p(pGeosetHelper->NodeBoneIndices[matrixGroup].data());
			builder.AdvanceVertex();
		}
	}

	builder.Detach();

	return mesh;
}


MDXRenderableGeometry::MDXRenderableGeometry()
{
}

MDXRenderableGeometry::~MDXRenderableGeometry()
{
}

void MDXRenderableGeometry::Init(IMDXModel *model)
{
	const MDXModelData *modelData = model->GetModelData();

	for (unsigned int i = 0; i < modelData->pGeosets->Geosets.size(); i++)
	{
		MDXGeosetChunk::MDXGeoset *geoset = modelData->pGeosets->Geosets[i];
		Mesh *mesh = CreateMeshFromGeoset(modelData, i);

		meshes.push_back(mesh);
		materialIds.push_back(geoset->MaterialId);
	}

	std::vector<int> layerMaterialCount;
	layerMaterialCount.resize(modelData->pTextures->Textures.size());

	for (unsigned int i = 0; i < modelData->pGeosets->Geosets.size(); i++)
	{
		int materialId = materialIds[i];

		MDXMaterialChunk::MDXMaterial *material = modelData->pMaterials->Materials[materialId];

		for (unsigned int l = 0; l < material->pLayer->Layers.size(); l++)
		{
			MDXLayerChunk::MDXLayer *layer = material->pLayer->Layers[l];
			const int textureId = layer->TextureId;

			const char *path = modelData->pTextures->Textures[textureId].FileName;

			if (path == nullptr
				|| *path == 0)
				continue;

			// allow layers to have specialized materials (for coloring, translucency)
			char materialBase[MAX_PATH_GAME];
			G_StrNCpy(materialBase, path, sizeof(materialBase));
			G_StrStripExtension(materialBase);

			char materialOverride[MAX_PATH_GAME];
			G_StrSnPrintf(materialOverride, sizeof(materialOverride), "materials%c%s_%i.xml",
				PATHSEPARATOR, materialBase, layerMaterialCount[textureId]);

			layerMaterialCount[textureId]++;

			if (FileExists(materialOverride))
				path = materialOverride;

			IMaterial *material = MaterialDict::GetInstance()->FindMaterial(path);
			material->IncrementReferenceCount();
			materials.push_back(material);

			if (material->IsErrorMaterial())
				continue;

			Layer entry;
			entry.material = material;
			entry.mesh = meshes[i];
			entry.geosetAlphaAnimation = modelData->pGeosetAnimations->GeosetAnimations[i]->pGeosetAlpha;
			layers.push_back(entry);
		}
	}

	layerBase = layers.data();
}

void MDXRenderableGeometry::Release()
{
	Assert(!IsReferenced());

	for (auto m : meshes)
		m->Release();

	for (auto m : materials)
		m->DecrementReferenceCount();

	delete this;
}

void MDXRenderableGeometry::Draw(int animationtime, int sequence)
{
	RenderContext *context = RenderContext::GetInstance();
	IMaterial *materialOverride = context->GetMaterialOverride();

	for (unsigned int i = 0; i < layers.size(); i++)
	{
		Layer &layer = layerBase[i];

		if (layer.geosetAlphaAnimation != nullptr)
		{
			float alpha = MDXAnimateByTrack(*layer.geosetAlphaAnimation, animationtime, sequence, 1.0f);

			if (alpha < 0.5f)
				continue;
		}

		context->BindMesh(layer.mesh);

		if (materialOverride == nullptr)
		{
			layer.material->Draw();
		}
		else
		{
			materialOverride->Draw();
		}
	}
}