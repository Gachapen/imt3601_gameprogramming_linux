#ifndef VBODICT_H
#define VBODICT_H

#include "vertexattributebinding.h"
#include <unordered_map>


class VBODict
{
	DECLARE_SINGLETON(VBODict);

public:
	void Shutdown();

	VBO_KEY CreateVBO(const VertexAttribBinding &attributes, const int sizeData, void *data, bool isDynamic = false);
	void FreeVBO(VBO_KEY vbo);

	void ModifyVBO(VBO_KEY vbo, const int sizeData = 0, void *data = nullptr, bool isDynamic = false);
	void BindVBO(VBO_KEY vbo, int destinationVertexFormat) const;
	void UnbindVBO(VBO_KEY vbo);

	float *BindMap(VBO_KEY vbo, int offset, int range);
	void BindUnmap(VBO_KEY vbo);

private:
	struct VBOData
	{
		GLuint arrayBuffer;
		VertexAttribBinding attributes;
		int vertexFormat;
	};

	VBO_KEY nextKey;
	std::unordered_map<VBO_KEY, VBOData> vboMap;

	void FreeVBO(VBOData &vbo);
};

#endif