#include "pch.h"
#include "mdxrenderable.h"
#include "mdxrenderabledict.h"
#include "appinterface\iappinterface.h"
#include "mdxlib\imdxmodeldict.h"
#include "mdxrenderablegeometry.h"


SINGLETON_INSTANCE(MDXRenderableDict);

EXPOSE_APP(INTERFACE_IMDXRENDERABLEDICT_VERSION, MDXRenderableDict::GetInstance());


MDXRenderableDict::MDXRenderableDict()
	: modelDict(nullptr)
{
}

MDXRenderableDict::~MDXRenderableDict()
{
	Assert(geometryCache.empty());
}

void MDXRenderableDict::Init()
{
	modelDict = (IMDXModelDict*)GetAppInterface()->QueryApp(INTERFACE_IMDXMODELDICT_VERSION);

	Assert(modelDict != nullptr);
}

void MDXRenderableDict::Shutdown()
{
	for (auto g : geometryCache)
		g.second->Release();
	geometryCache.clear();
}

MDXRenderableGeometry *MDXRenderableDict::FindMDXRenderableGeometry(IMDXModel *modelData)
{
	auto itr = geometryCache.find(modelData);

	if (itr != geometryCache.end())
		return itr->second;

	MDXRenderableGeometry *geometry = new MDXRenderableGeometry();
	geometry->Init(modelData);

	geometryCache[modelData] = geometry;
	return geometry;
}

IMDXRenderable *MDXRenderableDict::CreateMDXRenderableInstance(const char *path)
{
	if (modelDict == nullptr)
		return nullptr;

	IMDXModel *model = modelDict->FindModel(path);

	MDXRenderableGeometry *geometry = FindMDXRenderableGeometry(model);
	MDXRenderable *renderable = new MDXRenderable();

	renderable->Init(model, geometry);
	return renderable;
}