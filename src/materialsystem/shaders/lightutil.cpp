#include "pch.h"
#include "lightutil.h"
#include "baseshader.h"
#include "rendercontext.h"


void ApplyFrustumReconstructionData(BaseShader *shader, RenderContext *context)
{
	mat4 viewInv = context->GetOGLViewProjectionMatrix();
	const mat4 &view = context->GetViewMatrix();

	vec3 origin;
	MatrixPosition(view, origin);
	origin = rotate(origin, view.inverse().eval());

	MatrixSetPosition(viewInv, vec3::Zero());
	viewInv = viewInv.inverse().eval();

#if COMBINE_ORTHOGRAPHIC
	const float depthDist = 2.0f;
#else
	const float depthDist = 1.0f;
#endif

	vec4 rayFwd(0, 0, depthDist, 1.0f);
	vec4 rayFwdTop(0, 1.0f, depthDist, 1.0f);
	vec4 rayFwdLeft(1.0f, 0, depthDist, 1.0f);

	rayFwd = viewInv * rayFwd;
	rayFwdTop = viewInv * rayFwdTop;
	rayFwdLeft = viewInv * rayFwdLeft;

	const float zFar = context->GetZFar();
	const float zNear = context->GetZNear();
	const float zRange = context->GetZRange();
	//const float zUnmap = (zNear + zFar) / zRange;

#if COMBINE_ORTHOGRAPHIC == 0
	rayFwd *= zFar;
	rayFwdTop *= zFar;
	rayFwdLeft *= zFar;
#endif

	rayFwdTop = rayFwdTop - rayFwd;
	rayFwdLeft = rayFwdLeft - rayFwd;

	shader->SetUniform3fv(rayFwd.data());
	shader->SetUniform3fv(rayFwdTop.data());
	shader->SetUniform3fv(rayFwdLeft.data());
	shader->SetUniform3fv(origin.data());
}