#include "pch.h"
#include "baseshader.h"
#include "lightutil.h"


BEGIN_SHADER(light)
BEGIN_SHADER_VARS
	SHADER_VAR(NORMALTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	SHADER_VAR(DEPTHTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	LoadTexture(vars, NORMALTEXTURE);
	LoadTexture(vars, DEPTHTEXTURE);
}

SHADER_INIT_PASSES()
{
	AddPass(VertexFormat::POSITION_2F
			, "light.vert", "light.frag");

	PushUniformVariable("normalSampler");
	PushUniformVariable("depthSampler");

	PushUniformVariable("frustum_fwd");
	PushUniformVariable("frustum_top");
	PushUniformVariable("frustum_right");
	PushUniformVariable("view_origin");

	PushUniformVariable("light_count");
	PushUniformVariable("light_data");
}

SHADER_DRAW_PASSES()
{
	BeginPass();

	BindTexture(vars, NORMALTEXTURE, 0);
	BindTexture(vars, DEPTHTEXTURE, 1);

	ApplyFrustumReconstructionData(this, context);

	// light test data
	mat3x4 data[3];
	data[0].col(0) = vec3(512 + 128 * -sin(context->GetShaderTime() * 1.5f),
		128 + 128 * -cos(context->GetShaderTime() * 1.5f), 200);
	data[0].col(1) = vec3(0.05f, 0.6f, 1) * 5;
	data[0].col(3) = vec3(350, 0, 0);

	data[1].col(1) = vec3(1, 0.3, 0.05) * 4.5f;
	data[1].col(3) = vec3(900, 0, 0);

	data[1].col(0) = vec3(768 + 384 * cos(context->GetShaderTime()),
		768 + 384 * sin(context->GetShaderTime()), 150);

	SetUniform1i(2);
	SetUniformMatrix4x3fv(data[0].data(), 2);

	// one small light
	//data[2].col(0) = vec3(20, 0, 20);
	//data[2].col(1) = vec3(1, 0.1, 0.3) * 3;
	//data[2].col(3) = vec3(50, 0, 0);

	//SetUniform1i(1);
	//SetUniformMatrix4x3fv(data[2].data(), 1);

	SetDepthTestEnabled(false);
	SetDepthWriteEnabled(false);
	SetBlendMode(BlendModes::ADDITIVE);

	DrawPass();

	SetDepthTestEnabled(true);
	SetDepthWriteEnabled(true);
	SetBlendMode(BlendModes::NONE);
}
END_SHADER();

