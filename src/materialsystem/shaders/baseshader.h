#ifndef BASESHADER_H
#define BASESHADER_H

#include "util/macros.h"
#include "materialsystem\materialsystem.h"
#include "materialvar.h"
#include "rendercontext.h"
#include "shaderfactory.h"
#include "shadershared.h"
#include "shaderprogramdict.h"

class CDrawPass;
class Mesh;
class ShaderCombo;

class IShaderVar
{
public:
	virtual ~IShaderVar() {}

	virtual MaterialVarTypeId GetType() const = 0;
	virtual const char *GetName() const = 0;
	virtual const char *GetDefaultValue() const = 0;
};

class MaterialContext
{
public:
	//MaterialContext();
	//~MaterialContext();

	std::vector<CDrawPass *> passes;
};

// If you change this, update the static CShaderVar in the shader creation macro!
// not in a namespace so they can be used more nicely in shader descriptors
enum DefaultShaderVars_e
{
	NOCULL = 0,
	IGNOREZ,
	MODEL,
	ALPHATESTING,
	ALPHATESTREF,
	COLOR,
	VERTEXCOLOR,
	VERTEXALPHA,
	TRANSLUCENT,

	DEFAULTSHADERVAR_COUNT,
};

class BaseShader
{
public:
	virtual ~BaseShader();

	void SetupVars();

	virtual int GetParamCount() const = 0;
	virtual IShaderVar *GetParam(int index) const = 0;

	void Init(MaterialContext *materialData, MaterialVar **vars);
	void Draw(MaterialContext *materialData, MaterialVar **vars);
	void Shutdown(MaterialContext *materialData, MaterialVar **vars);

	void AddPass(int vertexFormat, const char *vertexShaderName, const char *fragmentShaderName);
	void AddPass(int vertexFormat, ShaderCombo &&vertexCombo, ShaderCombo &&fragmentCombo);
	void BeginPass();

	void SkipPass();
	void DrawPass();

	void PushUniformVariable(const char *name);

	void SkipUniform();

	void SetUniform1i(int value);
	void SetUniform1iv(const int *value, int count = 1);
	void SetUniform1f(float value);
	void SetUniform1fv(const float *value, int count = 1);
	void SetUniform2i(int value0, int value1);
	void SetUniform2iv(const int *value, int count = 1);
	void SetUniform2f(float value0, float value1);
	void SetUniform2fv(const float *value, int count = 1);
	void SetUniform3i(int value0, int value1, int value2);
	void SetUniform3iv(const int *value, int count = 1);
	void SetUniform3f(float value0, float value1, float value2);
	void SetUniform3fv(const float *value, int count = 1);
	void SetUniform4i(int value0, int value1, int value2, int value3);
	void SetUniform4iv(const int *value, int count = 1);
	void SetUniform4f(float value0, float value1, float value2, float value3);
	void SetUniform4fv(const float *value, int count = 1);
	void SetUniformMatrix4x4fv(const float *matrix, int count = 1);
	void SetUniformMatrix4x3fv(const float *matrix, int count = 1);
	void SetUniformMatrix3x4fv(const float *matrix, int count = 1);
	void SetBones();

	void SetPolygonMode(PolygonFaceModeId mode, PolygonFillModeId fill = PolygonFillModes::FILL);

	void SetDepthWriteEnabled(bool enabled);
	void SetDepthTestEnabled(bool enabled);

	void SetBlendMode(BlendModeId mode);

	// load textures
	void LoadTexture(MaterialVar **vars, int materialParam, int flags = 0);
	void LoadCubemap(MaterialVar **vars, int materialParam, int flags = 0);

	// bind textures
	void BindTexture(MaterialVar **vars, int materialParam, int samplerIndex);

protected:
	BaseShader();

	virtual void LinkStandardVars() = 0;

	virtual void OnInitParameters(MaterialVar **vars, RenderContext *context) = 0;
	virtual void OnInitPasses(MaterialVar **vars, RenderContext *context) = 0;
	virtual void OnDrawPasses(MaterialVar **vars, RenderContext *context) = 0;

private:
	void LoadTexture(MaterialVar **vars, int materialParam, TextureTypeId type, int flags);

	MaterialContext *activeMaterialData;
	CDrawPass **currentPass;
	const Mesh *activeMesh;
};

#define BEGIN_SHADER(shaderName)																				\
	namespace shaderName ## shader																				\
	{																											\
	static const char *factoryShaderName = # shaderName;														\
	class CShaderVar;																							\
	static std::vector<CShaderVar *> shaderVars;																\
	class CShaderVar : public IShaderVar																		\
	{																											\
public:																											\
		CShaderVar(const char *name, MaterialVarTypeId type, const char *defaultValue, int indexOverwrite = -1)	\
		{																										\
			this->name = new char[strlen(name) + 1];															\
			G_StrCpy(this->name, name);																			\
																												\
			this->defaultValue = new char[strlen(defaultValue) + 1];											\
			G_StrCpy(this->defaultValue, defaultValue);															\
																												\
			this->type = type;																					\
																												\
			if (indexOverwrite >= 0)																			\
			{																									\
				index = indexOverwrite;																			\
			}																									\
			else																								\
			{																									\
				index = (int)shaderVars.size() + DEFAULTSHADERVAR_COUNT;										\
				shaderVars.push_back(this);																		\
			}																									\
		}																										\
																												\
		operator int() const																					\
		{																										\
			return index;																						\
		}																										\
																												\
		virtual MaterialVarTypeId GetType() const																\
		{																										\
			return type;																						\
		}																										\
																												\
		virtual const char *GetName() const																		\
		{																										\
			return name;																						\
		}																										\
																												\
		virtual const char *GetDefaultValue() const																\
		{																										\
			return defaultValue;																				\
		}																										\
																												\
private:																										\
		int index;																								\
		char *name;																								\
		char *defaultValue;																						\
		MaterialVarTypeId type;																					\
	};

#define BEGIN_SHADER_VARS

#define SHADER_VAR(varName, type, defaultvalue)	\
	static CShaderVar varName(# varName, type, defaultvalue)

#define END_SHADER_VARS																			   \
	class ShaderImpl : public BaseShader														   \
	{																							   \
public:																							   \
																								   \
		virtual int GetParamCount() const														   \
		{																						   \
			return (int)shaderVars.size();														   \
		}																						   \
																								   \
		virtual IShaderVar *GetParam(int index) const											   \
		{																						   \
			return shaderVars[index];															   \
		}																						   \
		virtual void LinkStandardVars()															   \
		{																						   \
			static CShaderVar var_nocull("nocull", MaterialVarTypes::INT, "0", 0);				   \
			static CShaderVar var_ignorez("ignorez", MaterialVarTypes::INT, "0", 0);			   \
			static CShaderVar var_model("model", MaterialVarTypes::INT, "0", 1);				   \
			static CShaderVar var_alphatested("alphatesting", MaterialVarTypes::INT, "0", 2);	   \
			static CShaderVar var_alphatestref("alphatestref", MaterialVarTypes::FLOAT, "0.5", 3); \
			static CShaderVar var_color("color", MaterialVarTypes::VEC3, "[1.0 1.0 1.0]", 4);	   \
			static CShaderVar var_vertexcolor("vertexcolor", MaterialVarTypes::INT, "0", 5);	   \
			static CShaderVar var_vertexalpha("vertexalpha", MaterialVarTypes::INT, "0", 6);	   \
			static CShaderVar var_translucent("translucent", MaterialVarTypes::INT, "0", 7);	   \
			shaderVars.insert(shaderVars.begin(), &var_translucent);							   \
			shaderVars.insert(shaderVars.begin(), &var_vertexalpha);							   \
			shaderVars.insert(shaderVars.begin(), &var_vertexcolor);							   \
			shaderVars.insert(shaderVars.begin(), &var_color);									   \
			shaderVars.insert(shaderVars.begin(), &var_alphatestref);							   \
			shaderVars.insert(shaderVars.begin(), &var_alphatested);							   \
			shaderVars.insert(shaderVars.begin(), &var_model);									   \
			shaderVars.insert(shaderVars.begin(), &var_ignorez);								   \
			shaderVars.insert(shaderVars.begin(), &var_nocull);									   \
		}

#define SHADER_INIT_PARAMS() \
protected:					 \
	virtual void OnInitParameters(MaterialVar * *vars, RenderContext * context)

#define SHADER_INIT_PASSES() \
protected:					 \
	virtual void OnInitPasses(MaterialVar * *vars, RenderContext * context)

#define SHADER_DRAW_PASSES() \
protected:					 \
	virtual void OnDrawPasses(MaterialVar * *vars, RenderContext * context)

#define END_SHADER()																 \
	};																				 \
	static ShaderImpl shaderInstance;												 \
	static BaseShader *createShader()												 \
	{																				 \
		return &shaderInstance;														 \
	}																				 \
	static class LinkFactory														 \
	{																				 \
public:																				 \
		LinkFactory()																 \
		{																			 \
			ShaderFactory::RegisterShaderFactory(factoryShaderName, &createShader); \
		}																			 \
	}																				 \
	gs_linkFactory;																	 \
	}


#define IS_VAR_TEXTURE_VALID(x) \
	(vars[x]->GetType() == MaterialVarTypes::TEXTURE \
		&& vars[x]->GetTexture() != nullptr)

#endif
