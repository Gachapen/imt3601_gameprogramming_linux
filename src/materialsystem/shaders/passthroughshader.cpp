#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(passthrough)
BEGIN_SHADER_VARS
	SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	LoadTexture(vars, ALBEDOTEXTURE);
}

SHADER_INIT_PASSES()
{
	AddPass(VertexFormat::POSITION_2F
	        //| VertexFormat::TEXTURE_COORD_2F
			, "passthrough.vert", "passthrough.frag");

	PushUniformVariable("albedoSampler");
}

SHADER_DRAW_PASSES()
{
	BeginPass();

	BindTexture(vars, ALBEDOTEXTURE, 0);

	SetDepthTestEnabled(false);
	SetDepthWriteEnabled(false);

	DrawPass();

	SetDepthTestEnabled(true);
	SetDepthWriteEnabled(true);
}
END_SHADER();
