
#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(ground)
	BEGIN_SHADER_VARS
		SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(NORMALTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	END_SHADER_VARS

	SHADER_INIT_PARAMS()
	{
		LoadTexture(vars, ALBEDOTEXTURE);
		LoadTexture(vars, NORMALTEXTURE);
	}

	SHADER_INIT_PASSES()
	{
		AddPass(VertexFormat::POSITION_3F
			| VertexFormat::TEXTURE_COORD_2F
			| VertexFormat::NORMAL_3F
			| VertexFormat::BINORMAL_3F
			| VertexFormat::TANGENT_3F,
			"ground.vert", "ground.frag");

		PushUniformVariable("albedoSampler");
		PushUniformVariable("normalSampler");

		PushUniformVariable("MVP");
		PushUniformVariable("M");
		PushUniformVariable("znear");
		PushUniformVariable("zrange");
		PushUniformVariable("color");

		AddPass(VertexFormat::POSITION_3F,
			"ground_selection.vert", "ground_selection.frag");

		PushUniformVariable("MVP");
		PushUniformVariable("znear");
		PushUniformVariable("zrange");
	}

	SHADER_DRAW_PASSES()
	{
		switch (context->GetRenderStage())
		{
		case RenderStages::SHADOW:
			SkipPass();
			SkipPass();
			break;
		case RenderStages::NORMAL:
			{
				BeginPass();

				BindTexture(vars, ALBEDOTEXTURE, 0);
				BindTexture(vars, NORMALTEXTURE, 1);

				SetUniformMatrix4x4fv(context->GetOGLModelViewProjectionMatrix().data());
				SetUniformMatrix4x4fv(context->GetModelMatrix().data());
				SetUniform1f(context->GetZNear());
				SetUniform1f(context->GetZRange());
				SetUniform3fv(vars[COLOR]->GetVec3().data());

				DrawPass();

				SkipPass();
			}
			break;
		case RenderStages::SELECTION:
			{
				SkipPass();

				BeginPass();

				SetUniformMatrix4x4fv(context->GetOGLModelViewProjectionMatrix().data());
				SetUniform1f(context->GetZNear());
				SetUniform1f(context->GetZRange());

				DrawPass();
			}
			break;
		}
	}
END_SHADER();