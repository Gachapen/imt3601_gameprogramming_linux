#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(gaussblur7)
BEGIN_SHADER_VARS
	SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	SHADER_VAR(HORIZONTAL, MaterialVarTypes::INT, "0");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	LoadTexture(vars, ALBEDOTEXTURE);
}

SHADER_INIT_PASSES()
{
	AddPass(VertexFormat::POSITION_2F
		, "passthrough.vert",
		vars[HORIZONTAL]->GetBool() ? "gaussblur7hbloom.frag" : "gaussblur7v.frag");

	PushUniformVariable("albedoSampler");
	PushUniformVariable("texelSize");
}

SHADER_DRAW_PASSES()
{
	BeginPass();

	BindTexture(vars, ALBEDOTEXTURE, 0);

	vec2 texelSize(8.0f / float(context->GetScreenWidth()),
		8.0f / float(context->GetScreenHeight()));

	SetUniform2fv(texelSize.data());

	SetDepthTestEnabled(false);
	SetDepthWriteEnabled(false);

	DrawPass();

	SetDepthTestEnabled(true);
	SetDepthWriteEnabled(true);
}
END_SHADER();

BEGIN_SHADER(gaussblur13)
BEGIN_SHADER_VARS
	SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	SHADER_VAR(HORIZONTAL, MaterialVarTypes::INT, "0");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	LoadTexture(vars, ALBEDOTEXTURE);
}

SHADER_INIT_PASSES()
{
	AddPass(VertexFormat::POSITION_2F
		, "passthrough.vert",
		vars[HORIZONTAL]->GetBool() ? "gaussblur13hbloom.frag" : "gaussblur13v.frag");

	PushUniformVariable("albedoSampler");
	PushUniformVariable("texelSize");
}

SHADER_DRAW_PASSES()
{
	BeginPass();

	BindTexture(vars, ALBEDOTEXTURE, 0);

	vec2 texelSize(8.0f / float(context->GetScreenWidth()),
		8.0f / float(context->GetScreenHeight()));

	SetUniform2fv(texelSize.data());

	SetDepthTestEnabled(false);
	SetDepthWriteEnabled(false);

	DrawPass();

	SetDepthTestEnabled(true);
	SetDepthWriteEnabled(true);
}
END_SHADER();
