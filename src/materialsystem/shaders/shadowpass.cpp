
#include "pch.h"
#include "baseshader.h"
#include "shadowpass.h"



void InitShadowPass(RenderContext *context, BaseShader *shader, MaterialVar **vars, const ShadowPassData &data)
{
	ShaderCombo vertexCombo("shadowpass.vert");
	ShaderCombo fragmentCombo("shadowpass.frag");

	int format = VertexFormat::POSITION_3F;

	if (data.useSkinning)
	{
		format |= VertexFormat::BONEINDICES_3F;
	}

	if (data.useAlphatesting)
	{
		vertexCombo.SetCombo("ALPHATESTING", 1);
		fragmentCombo.SetCombo("ALPHATESTING", 1);

		format |= VertexFormat::TEXTURE_COORD_2F;
	}

	shader->AddPass(format, std::move(vertexCombo), std::move(fragmentCombo));

	shader->PushUniformVariable("VP");
	shader->PushUniformVariable("bones");

	if (data.useAlphatesting)
	{
		shader->PushUniformVariable("albedoSampler");
		shader->PushUniformVariable("alphaTestRef");
	}
}

void DrawShadowPass(RenderContext *context, BaseShader *shader, MaterialVar **vars, const ShadowPassData &data)
{
	shader->BeginPass();

	shader->SetUniformMatrix4x4fv(context->GetOGLViewProjectionMatrix().data());
	shader->SetBones();

	if (data.useAlphatesting)
	{
		shader->BindTexture(vars, data.albedoTexture, 0);
		shader->SetUniform1f(vars[data.alphaTestRef]->GetFloat());
	}

	if (data.useNocull)
		shader->SetPolygonMode(PolygonFaceModes::FRONT_AND_BACK);

	shader->DrawPass();

	if (data.useNocull)
		shader->SetPolygonMode(PolygonFaceModes::FRONT);
}