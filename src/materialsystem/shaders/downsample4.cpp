#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(downsample4)
BEGIN_SHADER_VARS
	SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	LoadTexture(vars, ALBEDOTEXTURE);
}

SHADER_INIT_PASSES()
{
	AddPass(VertexFormat::POSITION_2F
			, "passthrough.vert", "downsample4.frag");

	PushUniformVariable("albedoSampler");
	PushUniformVariable("texelSizeHalf");
	PushUniformVariable("texelSize3Half");
}

SHADER_DRAW_PASSES()
{
	BeginPass();

	BindTexture(vars, ALBEDOTEXTURE, 0);

	vec2 texelSizeHalf(2.0f / float(context->GetScreenWidth()),
		2.0f / float(context->GetScreenHeight()));
	vec2 texelSize3Half = texelSizeHalf * 3.0f;

	SetUniform2fv(texelSizeHalf.data());
	SetUniform2fv(texelSize3Half.data());

	SetDepthTestEnabled(false);
	SetDepthWriteEnabled(false);

	DrawPass();

	SetDepthTestEnabled(true);
	SetDepthWriteEnabled(true);
}
END_SHADER();
