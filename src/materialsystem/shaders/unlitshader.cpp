#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(unlit)
BEGIN_SHADER_VARS
	SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "");
	SHADER_VAR(SKINNING, MaterialVarTypes::INT, "0");
	SHADER_VAR(WIREFRAME, MaterialVarTypes::INT, "0");
	SHADER_VAR(DIFFUSEMODULATION, MaterialVarTypes::INT, "0");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	if (vars[ALBEDOTEXTURE]->IsDefined())
	{
		LoadTexture(vars, ALBEDOTEXTURE);
	}
}

SHADER_INIT_PASSES()
{
	const bool isTextured = IS_VAR_TEXTURE_VALID(ALBEDOTEXTURE);
	const bool isUsingDiffuseModulation = vars[DIFFUSEMODULATION]->GetBool();

	ShaderCombo vertexCombo("unlit.vert");
	ShaderCombo fragmentCombo("unlit.frag");
	int format = VertexFormat::POSITION_3F;

	if (vars[SKINNING]->GetBool())
	{
		format |= VertexFormat::BONEINDICES_3F;
		vertexCombo.SetCombo("SKINNING", 1);
	}
	else if (vars[MODEL]->GetBool())
	{
		vertexCombo.SetCombo("MODEL", 1);
	}

	if (vars[VERTEXALPHA]->GetBool())
	{
		format |= VertexFormat::COLOR_4F;
		vertexCombo.SetCombo("VERTEXALPHA", 1);
		fragmentCombo.SetCombo("VERTEXALPHA", 1);
	}
	else if (vars[VERTEXCOLOR]->GetBool())
	{
		format |= VertexFormat::COLOR_3F;
		vertexCombo.SetCombo("VERTEXCOLOR", 1);
		fragmentCombo.SetCombo("VERTEXCOLOR", 1);
	}

	if (isTextured)
	{
		format |= VertexFormat::TEXTURE_COORD_2F;
		vertexCombo.SetCombo("TEXTURED", 1);
		fragmentCombo.SetCombo("TEXTURED", 1);
	}

	if (isUsingDiffuseModulation)
	{
		fragmentCombo.SetCombo("DIFFUSEMODULATION", 1);
	}

	AddPass(format, std::move(vertexCombo), std::move(fragmentCombo));

	if (vars[SKINNING]->GetBool())
	{
		PushUniformVariable("VP");
		PushUniformVariable("bones");
	}
	else if (vars[MODEL]->GetBool())
	{
		PushUniformVariable("MVP");
	}

	PushUniformVariable("znear");
	PushUniformVariable("zrange");

	if (isTextured)
	{
		PushUniformVariable("albedoSampler");
	}

	if (isUsingDiffuseModulation)
	{
		PushUniformVariable("diffusemodulation");
	}
}

SHADER_DRAW_PASSES()
{
	const bool ignoreZ = vars[IGNOREZ]->GetBool();
	const bool nocull = vars[NOCULL]->GetBool();
	const bool isWireframe = vars[WIREFRAME]->GetBool();
	const bool isTranslucent = vars[TRANSLUCENT]->GetBool();
	const bool isTextured = IS_VAR_TEXTURE_VALID(ALBEDOTEXTURE);
	const bool isUsingDiffuseModulation = vars[DIFFUSEMODULATION]->GetBool();

	BeginPass();

	if (vars[SKINNING]->GetBool())
	{
		SetUniformMatrix4x4fv(context->GetOGLViewProjectionMatrix().data());
		SetBones();
	}
	else if (vars[MODEL]->GetBool())
	{
		SetUniformMatrix4x4fv(context->GetOGLModelViewProjectionMatrix().data());
	}

	SetUniform1f(context->GetZNear());
	SetUniform1f(context->GetZRange());

	if (isTextured)
	{
		BindTexture(vars, ALBEDOTEXTURE, 0);
	}

	if (isUsingDiffuseModulation)
	{
		vec4 diffuseModulation;
		context->GetRenderParamVec4(RenderParams::VEC4_DIFFUSE_MODULATION, diffuseModulation);
		SetUniform4fv(diffuseModulation.data());
	}

	PolygonFillModeId fillMode = isWireframe ? PolygonFillModes::WIREFRAME : PolygonFillModes::FILL;

	SetPolygonMode(nocull ? PolygonFaceModes::FRONT_AND_BACK : PolygonFaceModes::FRONT, fillMode);

	if (ignoreZ)
	{
		SetDepthTestEnabled(false);
		SetDepthWriteEnabled(false);
	}
	else if (isTranslucent)
	{
		SetDepthWriteEnabled(false);
	}

	if (isTranslucent)
	{
		SetBlendMode(BlendModes::SRC_ALPHA);
	}

	DrawPass();

	if (isTranslucent)
	{
		SetBlendMode(BlendModes::NONE);
	}

	SetDepthTestEnabled(true);
	SetDepthWriteEnabled(true);

	SetPolygonMode(PolygonFaceModes::FRONT);
}
END_SHADER();
