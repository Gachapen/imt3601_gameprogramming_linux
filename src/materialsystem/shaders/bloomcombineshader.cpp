#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(bloomcombine)
BEGIN_SHADER_VARS
	SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	SHADER_VAR(BLOOMTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	LoadTexture(vars, ALBEDOTEXTURE);
	LoadTexture(vars, BLOOMTEXTURE);
}

SHADER_INIT_PASSES()
{
	AddPass(VertexFormat::POSITION_2F
		, "passthrough.vert", "bloomcombine.frag");

	PushUniformVariable("albedoSampler");
	PushUniformVariable("bloomSampler");
	PushUniformVariable("texelSize");
}

SHADER_DRAW_PASSES()
{
	BeginPass();

	BindTexture(vars, ALBEDOTEXTURE, 0);
	BindTexture(vars, BLOOMTEXTURE, 1);

	vec2 texelSize(4.0f / float(context->GetScreenWidth()),
		4.0f / float(context->GetScreenHeight()));
	SetUniform2fv(texelSize.data());

	SetDepthTestEnabled(false);
	SetDepthWriteEnabled(false);

	DrawPass();

	SetDepthTestEnabled(true);
	SetDepthWriteEnabled(true);
}
END_SHADER();
