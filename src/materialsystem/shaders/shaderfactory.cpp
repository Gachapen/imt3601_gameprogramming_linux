
#include "pch.h"
#include "shaderfactory.h"
#include "baseshader.h"

SINGLETON_INSTANCE(ShaderFactory);

static struct function_register_shader_s
	: public function_register_template<ShaderFactory::ShaderFactoryFunc, function_register_shader_s>
{
} firstFactory_shader;

ShaderFactory::ShaderFactory()
{
}

void ShaderFactory::Init()
{
	ConvertFactoryListToMap(&firstFactory_shader, functions);

	for (auto s : functions)
		s.second()->SetupVars();
}

void ShaderFactory::RegisterShaderFactory(const char *name, ShaderFactoryFunc function)
{
	RegisterFactory(&firstFactory_shader, function, name);
}

BaseShader *ShaderFactory::CreateShader(const char *name)
{
	Assert(functions.find(name) != functions.end());

	return functions[name]();
}