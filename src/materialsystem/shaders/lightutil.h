#ifndef LIGHTUTIL_H
#define LIGHTUTIL_H

class BaseShader;
class RenderContext;

void ApplyFrustumReconstructionData(BaseShader *shader, RenderContext *context);

#endif