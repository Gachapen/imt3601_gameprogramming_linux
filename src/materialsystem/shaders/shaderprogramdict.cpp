#include "pch.h"
#include "shaderprogramdict.h"
#include "materialsystem\materialsystem.h"

using namespace std;

SINGLETON_INSTANCE(ShaderProgramDict);


#ifdef _DEBUG
string GetShaderLog(GLuint obj)
{
	int infologLength = 0;

	int charsWritten = 0;
	char *infoLog;

	glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &infologLength);

	if (infologLength > 1)
	{
		infoLog = new char[infologLength + 1];
		glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);

		string log = infoLog;

		delete [] infoLog;

		return log;
	}

	return "<no errors>";
}
#endif

string ResolveIncludes(const string &path, const string &src)
{
	string line;
	string merged;
	string includeCommand = "#include";

	stringstream stream = stringstream(src);

	while (stream.good())
	{
		std::getline(stream, line);

		if (line.substr(0, includeCommand.length()) == includeCommand)
		{
			int s = line.find_first_of('\"');
			int e = line.find_first_of('\"', s + 1);

			if (e > s)
			{
				line = line.substr(s + 1, e - s - 1);
				line = path + line;
				string included = LoadFile(line);

				if (!included.empty())
				{
					included = ResolveIncludes(path, included);

					merged += included;
					merged += '\n';
				}
			}
		}
		else
		{
			merged += line + '\n';
		}
	}

	return merged;
}

void ApplyComboDefines(const ShaderCombo &combo, string &src)
{
	for (auto itr = combo.begin(); itr != combo.end(); itr++)
	{
		char defineString[128];

		G_StrSnPrintf(defineString, sizeof(defineString),
			"#define %s %i\n", itr->first.c_str(), itr->second);

		src = defineString + src;
	}
}

void ResolveDefaultCombo(ShaderCombo &combo, const string &src)
{
	string line;
	string includeCommand = "//COMBO:";

	stringstream stream = stringstream(src);

	while (stream.good())
	{
		std::getline(stream, line);

		if (line.substr(0, includeCommand.length()) == includeCommand)
		{
			const char *base = line.c_str() + includeCommand.length();
			const char *read = base;

			while (*read == ' '
				|| *read == '\t')
				read++;

			const char *nameStart = read;

			while (*read && *read != ' ' && *read != '\t')
				read++;

			string name(nameStart, read);

			if (!combo.HasCombo(name.c_str()))
				combo.SetCombo(name.c_str(), 0);
		}
	}
}

static GLuint CompileShader(GLenum type, const ShaderCombo &combo)
{
	GLuint shader = 0;
	string source = LoadFile(combo.GetShaderName());

	Assert(source.length() > 0);

	if (source.length() > 0)
	{
		char directory[MAX_PATH_GAME];
		G_StrNCpy(directory, combo.GetShaderName(), sizeof(directory));
		G_StrFixSlashes(directory);

		string strDirectory = directory;
		int lastSeparator = strDirectory.find_last_of(PATHSEPARATOR);

		if (lastSeparator > 0)
		{
			strDirectory = strDirectory.substr(0, lastSeparator + 1);
		}
		else
		{
			strDirectory.clear();
		}

		source = ResolveIncludes(strDirectory, source);
		ApplyComboDefines(combo, source);

		source = SHADER_VERSION_STRING + source;

		char const *pSource = source.c_str();

		shader = glCreateShader(type);

		glShaderSource(shader, 1, &pSource, NULL);
		glCompileShader(shader);
	}

#ifdef _DEBUG
	//if (glGetError() != 0)
	{
		string log = GetShaderLog(shader);
		DBGMSGF("Compiling shader (%s): %s\n", combo.GetShaderName(), log.c_str());
	}
#endif

	return shader;
}

void BuildDefaultCombo(const char *path, ShaderCombo &combo)
{
	string source = LoadFile(path);

	Assert(source.length() > 0);

	if (source.length() > 0)
	{
		char directory[MAX_PATH_GAME];
		G_StrNCpy(directory, path, sizeof(directory));
		G_StrFixSlashes(directory);

		string strDirectory = directory;
		int lastSeparator = strDirectory.find_last_of(PATHSEPARATOR);

		if (lastSeparator > 0)
		{
			strDirectory = strDirectory.substr(0, lastSeparator + 1);
		}
		else
		{
			strDirectory.clear();
		}

		source = ResolveIncludes(strDirectory, source);
		ResolveDefaultCombo(combo, source);
	}
}


ShaderCombo::ShaderCombo()
{
	*shaderName = 0;
}

ShaderCombo::ShaderCombo(const char *s)
{
	Assert(*s != 0);

	G_StrAbsContentPath(ContentDirectories::SHADERS, s, shaderName, sizeof(shaderName));
}

ShaderCombo::ShaderCombo(const ShaderCombo &&other)
{
	memcpy(shaderName, other.shaderName, sizeof(shaderName));
	combos = std::move(other.combos);
}

void ShaderCombo::SetShaderName(const char *name)
{
	Assert(*name != 0);

	G_StrAbsContentPath(ContentDirectories::SHADERS, name, shaderName, sizeof(shaderName));
}

const char *ShaderCombo::GetShaderName() const
{
	return shaderName;
}

void ShaderCombo::SetCombo(const char *name, int value)
{
	char nameUpper[32];
	G_StrNCpy(nameUpper, name, sizeof(nameUpper));
	G_StrUpper(nameUpper);

	combos[nameUpper] = value;
}

bool ShaderCombo::HasCombo(const char *name) const
{
	return combos.find(name) != combos.end();
}

void ShaderCombo::CatHashString(char *out, int sizeInBytes) const
{
	Assert(sizeInBytes > 0);
	Assert(*shaderName);

	G_StrNCat(out, sizeInBytes, shaderName);

	for (auto itr = combos.begin();
		itr != combos.end();
		itr++)
	{
		G_StrNCat(out, sizeInBytes, itr->first.c_str());

		char value[32];
		G_StrSnPrintf(value, sizeof(value), "%i", itr->second);
		value[sizeof(value) - 1] = 0;

		G_StrNCat(out, sizeInBytes, value);
	}

	out[sizeInBytes - 1] = 0;
}

std::map<std::string, int>::const_iterator ShaderCombo::begin() const
{
	return combos.cbegin();
}

std::map<std::string, int>::const_iterator ShaderCombo::end() const
{
	return combos.cend();
}



ShaderProgramDict::ShaderProgramDict()
{
}

ShaderProgramDict::~ShaderProgramDict()
{
	Assert(shaderPrograms.size() == 0);
	Assert(shaders.size() == 0);
}

void ShaderProgramDict::Shutdown()
{
	for (auto s : shaders)
		glDeleteShader(s.second);
	shaders.clear();

	for (auto s : shaderPrograms)
		glDeleteProgram(s.second);
	shaderPrograms.clear();

	shaderDefaults.clear();
}

GLuint ShaderProgramDict::FindShaderProgram(int vertexFormat, ShaderCombo &&comboVertex, ShaderCombo &&comboFragment)
{
	char hashVertex[512];
	char hashFragment[512];
	char hashComplete[1024];

	*hashVertex = 0;
	*hashFragment = 0;
	*hashComplete = 0;

	ApplyMissingDefaults(comboVertex.GetShaderName(), comboVertex);
	ApplyMissingDefaults(comboFragment.GetShaderName(), comboFragment);

	comboVertex.CatHashString(hashVertex, sizeof(hashVertex));
	comboFragment.CatHashString(hashFragment, sizeof(hashFragment));

	G_StrNCat(hashComplete, sizeof(hashComplete), hashVertex);
	G_StrNCat(hashComplete, sizeof(hashComplete), hashFragment);

	auto itr = shaderPrograms.find(hashComplete);

	if (itr != shaderPrograms.end())
		return itr->second;

	GLuint vertexShader = FindShader(GL_VERTEX_SHADER, hashVertex, comboVertex);
	GLuint fragmentShader = FindShader(GL_FRAGMENT_SHADER, hashFragment, comboFragment);

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	int bindDestination = 0;
	for (int i = 0; i < VertexFormat::NUM_BITS; i++)
	{
		const int element = VertexFormat::GetVertexElementFlag(i);

		if (VertexFormat::HasElement(vertexFormat, element))
		{
			const char *elementName = VertexFormat::GetVertexElementName(element);

			glBindAttribLocation(shaderProgram, bindDestination, elementName);
			bindDestination++;
		}
	}

	glLinkProgram(shaderProgram);

	shaderPrograms[hashComplete] = shaderProgram;
	return shaderProgram;
}

GLuint ShaderProgramDict::FindShader(GLenum type, const char *hash, const ShaderCombo &combo)
{
	auto itr = shaders.find(hash);

	if (itr != shaders.end())
		return itr->second;

	GLuint shader = CompileShader(type, combo);

	Assert(shader != GL_INVALID_INDEX);

	shaders[hash] = shader;
	return shader;
}

void ShaderProgramDict::GetShaderDefault(const char *path, ShaderCombo &comboOut)
{
	auto itr = shaderDefaults.find(path);

	if (itr != shaderDefaults.end())
	{
		comboOut = itr->second;
		return;
	}

	BuildDefaultCombo(path, comboOut);
	shaderDefaults[path] = comboOut;
}

void ShaderProgramDict::ApplyMissingDefaults(const char *path, ShaderCombo &combo)
{
	ShaderCombo defaults;
	GetShaderDefault(path, defaults);

	for (auto itr = defaults.begin(); itr != defaults.end(); itr++ )
	{
		const char *name = itr->first.c_str();
		if (!combo.HasCombo(name))
			combo.SetCombo(name, itr->second);
	}
}