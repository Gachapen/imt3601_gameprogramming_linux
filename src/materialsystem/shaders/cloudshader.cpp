
#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(clouds)
	BEGIN_SHADER_VARS
		SHADER_VAR(NOISETEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(NORMALTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(NOISETEXTURE2, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(NORMALTEXTURE2, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(REFLECTION, MaterialVarTypes::INT, "0");
	END_SHADER_VARS

	SHADER_INIT_PARAMS()
	{
		LoadTexture(vars, NOISETEXTURE);
		LoadTexture(vars, NORMALTEXTURE);
		LoadTexture(vars, NOISETEXTURE2);
		LoadTexture(vars, NORMALTEXTURE2);
	}

	SHADER_INIT_PASSES()
	{
		ShaderCombo vertexCombo("clouds.vert");
		ShaderCombo fragmentCombo("clouds.frag");

		if (vars[REFLECTION]->GetBool())
		{
			fragmentCombo.SetCombo("REFLECTION", 1);
		}

		AddPass(VertexFormat::POSITION_3F,
			std::move(vertexCombo), std::move(fragmentCombo));

		PushUniformVariable("noiseSampler");
		PushUniformVariable("noise2Sampler");
		PushUniformVariable("normalSampler");
		PushUniformVariable("normal2Sampler");

		PushUniformVariable("MVP");
		PushUniformVariable("time");
		PushUniformVariable("view_origin");
	}

	SHADER_DRAW_PASSES()
	{
		BeginPass();

		BindTexture(vars, NOISETEXTURE, 0);
		BindTexture(vars, NOISETEXTURE2, 1);
		BindTexture(vars, NORMALTEXTURE, 2);
		BindTexture(vars, NORMALTEXTURE2, 3);

		SetUniformMatrix4x4fv(context->GetOGLModelViewProjectionMatrix().data());
		SetUniform1f(context->GetShaderTime());

		const mat4 &view = context->GetViewMatrix();
		vec3 origin;
		MatrixPosition(view, origin);
		origin = rotate(origin, view.inverse().eval());

		SetUniform3fv(origin.data());

		SetBlendMode(BlendModes::SRC_ALPHA);
		SetPolygonMode(PolygonFaceModes::FRONT_AND_BACK);
		SetDepthTestEnabled(false);
		SetDepthWriteEnabled(false);

		DrawPass();

		SetBlendMode(BlendModes::NONE);
		SetPolygonMode(PolygonFaceModes::FRONT);
		SetDepthTestEnabled(true);
		SetDepthWriteEnabled(true);
	}
END_SHADER();