#include "pch.h"
#include "baseshader.h"
#include "lightutil.h"


BEGIN_SHADER(lightcombine)
BEGIN_SHADER_VARS
	SHADER_VAR(ALBEDOTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	SHADER_VAR(LIGHTTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");

	SHADER_VAR(NORMALTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	SHADER_VAR(DEPTHTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	SHADER_VAR(SHADOWTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
END_SHADER_VARS

SHADER_INIT_PARAMS()
{
	LoadTexture(vars, ALBEDOTEXTURE);
	LoadTexture(vars, LIGHTTEXTURE);

	LoadTexture(vars, NORMALTEXTURE);
	LoadTexture(vars, DEPTHTEXTURE);
	LoadTexture(vars, SHADOWTEXTURE);
}

SHADER_INIT_PASSES()
{
	AddPass(VertexFormat::POSITION_2F
			, "light.vert", "lightcombine.frag");

	PushUniformVariable("albedoSampler");
	PushUniformVariable("lightSampler");

	PushUniformVariable("normalSampler");
	PushUniformVariable("depthSampler");
	PushUniformVariable("shadowSampler");

	PushUniformVariable("frustum_fwd");
	PushUniformVariable("frustum_top");
	PushUniformVariable("frustum_right");
	PushUniformVariable("view_origin");

	PushUniformVariable("shadowVP");
	PushUniformVariable("shadow_map_size");
	PushUniformVariable("directional_light_dir");
	PushUniformVariable("directional_light_color");
	PushUniformVariable("directional_light_ambient");

	PushUniformVariable("fogColor");
	PushUniformVariable("fogParams");
}

SHADER_DRAW_PASSES()
{
	BeginPass();

	BindTexture(vars, ALBEDOTEXTURE, 0);
	BindTexture(vars, LIGHTTEXTURE, 1);

	BindTexture(vars, NORMALTEXTURE, 2);
	BindTexture(vars, DEPTHTEXTURE, 3);
	BindTexture(vars, SHADOWTEXTURE, 4);

	ApplyFrustumReconstructionData(this, context);

	mat4 shadowVP;
	vec3 globalLightDir;
	vec3 globalLightColor;
	vec3 globalLightAmbient;

	context->GetRenderParamMat4(RenderParams::MAT4_SHADOW_VIEWPROJECTION, shadowVP);
	context->GetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_DIR, globalLightDir);
	context->GetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_COLOR, globalLightColor);
	context->GetRenderParamVec3(RenderParams::VEC3_GLOBALLIGHT_AMBIENT, globalLightAmbient);

	SetUniformMatrix4x4fv(shadowVP.data());
	SetUniform1f(context->GetRenderParamFloat(RenderParams::FLOAT_SHADOWMAP_SIZE));
	SetUniform3fv(globalLightDir.data());
	SetUniform3fv(globalLightColor.data());
	SetUniform3fv(globalLightAmbient.data());

	vec3 fogColor;
	vec3 fogParams(context->GetRenderParamFloat(RenderParams::FLOAT_FOG_AMOUNT ),
		context->GetRenderParamFloat(RenderParams::FLOAT_FOG_START),
		context->GetRenderParamFloat(RenderParams::FLOAT_FOG_RANGE));
	context->GetRenderParamVec3(RenderParams::VEC3_FOG_COLOR, fogColor);
	SetUniform3fv(fogColor.data());
	SetUniform3fv(fogParams.data());

	SetDepthTestEnabled(false);
	SetDepthWriteEnabled(false);

	DrawPass();

	SetDepthTestEnabled(true);
	SetDepthWriteEnabled(true);
}
END_SHADER();
