
#include "pch.h"
#include "baseshader.h"


BEGIN_SHADER(water)
	BEGIN_SHADER_VARS
		SHADER_VAR(NORMALTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(NORMALTEXTURE2, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(DEPTHTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(REFRACTTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
		SHADER_VAR(REFLECTTEXTURE, MaterialVarTypes::TEXTURE, "error.tga");
	END_SHADER_VARS

	SHADER_INIT_PARAMS()
	{
		LoadTexture(vars, NORMALTEXTURE);
		LoadTexture(vars, NORMALTEXTURE2);
		LoadTexture(vars, DEPTHTEXTURE);
		LoadTexture(vars, REFRACTTEXTURE);
		LoadTexture(vars, REFLECTTEXTURE);
		//LoadCubemap(vars, CUBEMAPTEXTURE);
	}

	SHADER_INIT_PASSES()
	{
		ShaderCombo vertexCombo("water.vert");
		ShaderCombo fragmentCombo("water.frag");

		AddPass(VertexFormat::POSITION_3F
			| VertexFormat::NORMAL_3F
			| VertexFormat::BINORMAL_3F
			| VertexFormat::TANGENT_3F
			| VertexFormat::TEXTURE_COORD_2F,
			std::move(vertexCombo), std::move(fragmentCombo));

		PushUniformVariable("normalSampler");
		PushUniformVariable("normal2Sampler");
		PushUniformVariable("depthSampler");
		PushUniformVariable("refractSampler");
		PushUniformVariable("reflectSampler");
		//PushUniformVariable("cubemapSampler");

		PushUniformVariable("MVP");
		PushUniformVariable("M");
		PushUniformVariable("znear");
		PushUniformVariable("zrange");
		PushUniformVariable("time");
		PushUniformVariable("view_origin");
	}

	SHADER_DRAW_PASSES()
	{
		BeginPass();

		BindTexture(vars, NORMALTEXTURE, 0);
		BindTexture(vars, NORMALTEXTURE2, 4);
		BindTexture(vars, DEPTHTEXTURE, 1);
		BindTexture(vars, REFRACTTEXTURE, 2);
		BindTexture(vars, REFLECTTEXTURE, 3);
		//BindTexture(vars, CUBEMAPTEXTURE, 1);

		SetUniformMatrix4x4fv(context->GetOGLModelViewProjectionMatrix().data());
		SetUniformMatrix4x4fv(context->GetModelMatrix().data());
		SetUniform1f(context->GetZNear());
		SetUniform1f(context->GetZRange());
		SetUniform1f(context->GetShaderTime());

		const mat4 &view = context->GetViewMatrix();
		vec3 origin;
		MatrixPosition(view, origin);
		origin = rotate(origin, view.inverse().eval());

		SetUniform3fv(origin.data());

		SetBlendMode(BlendModes::SRC_ALPHA);

		DrawPass();

		SetBlendMode(BlendModes::NONE);
	}
END_SHADER();