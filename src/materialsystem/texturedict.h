#ifndef TEXTUREDICT_H
#define TEXTUREDICT_H

#include "materialsystem\materialsystem.h"
#include "materialsystem\itexturedict.h"

class Texture;

class TextureDict : public ITextureDict
{
	DECLARE_SINGLETON(TextureDict);
public:

	void Shutdown();

	// insert procedural textures like rendertargets
	void InsertTexture(const char *path, Texture *texture);

	// ITextureDict
	virtual bool ReleaseTextureIfUnreferenced(ITexture *texture);
	virtual void ReleaseUnreferencedTextures(TextureGroupId group = TextureGroups::NONE);

	// load textures from disk
	ITexture *FindOrLoadTexture(const char *path,
		TextureGroupId group = TextureGroups::OTHER,
		TextureTypeId type = TextureTypes::TEXTURE_2D,
		TextureFormatId format = TextureFormats::RGBA_8,
		int flags = 0);

	// find a previously loaded texture by name
	ITexture *FindTexture(const char *path);

	ITexture *CreateTexture(const char *path,
		void *data, int width, int height, int flags,
		TextureFormatId format = TextureFormats::RGBA_8,
		TextureGroupId group = TextureGroups::OTHER);

private:

	void LoadTextureMetaData(const char *textureName, int &flags);

	Texture *errorTexture;

	std::unordered_map<std::string, Texture*> textures;
};



#endif