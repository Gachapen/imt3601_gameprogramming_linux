
#include "pch.h"
#include "renderutil.h"


namespace TextureFormats
{
	static GlFormat_s formats [] = {
		// bpp, driverformat, componenttype
		{ 4, GL_RGBA, GL_UNSIGNED_BYTE },
		{ 3, GL_RGB, GL_UNSIGNED_BYTE },
		{ 4, GL_BGRA, GL_UNSIGNED_BYTE },
		{ 3, GL_BGR, GL_UNSIGNED_BYTE },
		{ GL_RGBA16F, GL_RGBA, GL_FLOAT },		//RGBA16F
		{ GL_RGB16F, GL_RGB, GL_FLOAT },		//RGB16F
		{ GL_RGBA32F, GL_RGBA, GL_FLOAT },		//RGBA32F
		{ GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT, GL_FLOAT },
		{ GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_FLOAT },
	};
	static_assert(ARRAYSIZE(formats) == TextureFormats::COUNT, "Add texture info");

	const GlFormat_s &GetTextureFormatInfo(TextureFormatId textureFormat)
	{
		Assert(textureFormat >= 0 && textureFormat < TextureFormats::COUNT);

		return formats[textureFormat];
	}
}