#ifndef MATERIALSYSTEMUTIL_H
#define MATERIALSYSTEMUTIL_H

#include "util/stringutil.h"
#include "materialsystem/materialsystem.h"

// return texture group of material path
inline TextureGroupId G_StrGetTextureGroup(const char *filepath)
{
	static const char *materialPaths[] = {
		"", // other
		"debug",
		"models",
		"postprocessing",
		"effects",
		"", // rendertargets
		"cubemaps",
		"", // fonts
		"maps",
		"gui",
	};
	static_assert(TextureGroups::COUNT == ARRAYSIZE(materialPaths), "Add material/texture path!");

	char pathLowerCase[MAX_PATH_GAME];

	// to lower case so we can do strstr
	G_StrNCpy(pathLowerCase, filepath, sizeof(pathLowerCase));
	G_StrLower(pathLowerCase);

	const char *reader = pathLowerCase;

	reader = G_StrReadSkipSlashes(reader);

	// if it starts with materials, skip it
	if (G_StrStr(reader, "materials") == reader)
	{
		reader += 9;
		reader = G_StrReadSkipSlashes(reader);
	}

	for (int i = 0; i < ARRAYSIZE(materialPaths); i++)
	{
		if (!*materialPaths[i])
		{
			continue;
		}

		if (G_StrStr(reader, materialPaths[i]) == reader)
		{
			return (TextureGroupId)i;
		}
	}

	return TextureGroupId::OTHER;
}

#endif