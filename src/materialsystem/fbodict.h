#ifndef FBODICT_H
#define FBODICT_H

#include "materialsystem\materialsystem.h"
#include "materialsystem\ifbodict.h"

class Texture;

class FBODict : public IFBODict
{
	DECLARE_SINGLETON(FBODict);
public:

	// create standard fbos/renderbuffers/rendertargets
	void Init();

	// destroy all fbos/rb/rt
	void Shutdown();

	// resize all objects that use automatic resizing
	void OnResize(int w, int h);

	// IFBODict
	virtual FBO_HANDLE CreateFBO();
	virtual void PushFBO(FBO_HANDLE handle);
	virtual void PopFBO();

	virtual RB_HANDLE CreateRenderBuffer(TextureFormatId format, FramebufferResizeModeId resizeMode, int multisampleCount = 0, int width = -1, int height = -1);
	virtual void BindRenderBufferColor(RB_HANDLE handle, int index = 0);
	virtual void BindRenderBufferDepth(RB_HANDLE handle);

	virtual RT_HANDLE CreateRenderTarget(const char *name, TextureFormatId format, FramebufferResizeModeId resizeMode, int width = -1, int height = -1,
		int flags = TextureFlags::CLAMP_S | TextureFlags::CLAMP_T | TextureFlags::NO_MIPMAP);
	virtual void BindRenderTargetColor(RT_HANDLE handle, int index = 0);
	virtual void BindRenderTargetDepth(RT_HANDLE handle);
	virtual ITexture *GetRenderTargetTexture(RT_HANDLE handle);

	virtual void CopyFBOtoFBO(FBO_HANDLE src, FBO_HANDLE dest, FramebufferResizeModeId mode);

	virtual void ReadFBOColor(void *pixels, int x, int y, int w, int h, int attachmentIndex);

private:

	void GetAdjustedSize(FramebufferResizeModeId mode, int &w, int &h);
	bool RequiresResize(FramebufferResizeModeId mode);

	struct FBO
	{
		GLuint id;
		int bufferCount;
		int buffers[MAX_MRT_BUFFERS];
	};

	struct RenderTarget
	{
		FramebufferResizeModeId resizeMode;
		Texture *texture;
	};

	struct RenderBuffer
	{
		GLuint id;
		int multisampleCount;
		FramebufferResizeModeId resizeMode;
		TextureFormatId format;
	};

	std::vector<FBO> fboList;
	std::vector<RenderBuffer> renderBufferList;
	std::vector<RenderTarget> renderTargetList;

	std::stack<FBO*> fboStack;

};



#endif