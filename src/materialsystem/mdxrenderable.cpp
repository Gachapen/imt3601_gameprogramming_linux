#include "pch.h"
#include "mdxrenderable.h"
#include "mdxrenderablegeometry.h"
#include "mdxlib\imdxmodel.h"
#include "rendercontext.h"
#include "materialsystem\meshbuilder.h"
#include "materialsystem\imaterial.h"
#include "materialdict.h"


MDXRenderable::MDXRenderable()
	: modelData(nullptr)
	, geometry(nullptr)
	, boneCount(0)
	  , sequence(0)
	  , cycle(0)
	  , absoluteCycle(0)
	  , isSequenceLooping(true)
	  , sequenceRate(1.0f)
	  , playbackRate(1.0f)
	  , isSkeletonDirty(true)
	  , wasSequenceSet(false)
	  , skeletonMaterial(nullptr)
{
}

MDXRenderable::~MDXRenderable()
{
	Assert(modelData == nullptr);
	Assert(geometry == nullptr);
}

void MDXRenderable::Init(IMDXModel *modelData, MDXRenderableGeometry *geometry)
{
	Assert(this->modelData == nullptr);
	Assert(modelData != nullptr);
	Assert(geometry != nullptr);

	this->modelData = modelData;
	modelData->IncrementReferenceCount();

	this->geometry = geometry;
	geometry->IncrementReferenceCount();

	InitBones();
}

void MDXRenderable::Release()
{
	Assert(modelData != nullptr);
	Assert(geometry != nullptr);

	modelData->DecrementReferenceCount();
	modelData = nullptr;

	geometry->DecrementReferenceCount();
	geometry = nullptr;

	if (skeletonMaterial != nullptr)
	{
		skeletonMaterial->DecrementReferenceCount();
		skeletonMaterial = nullptr;
	}

	delete this;
}

void MDXRenderable::Draw()
{
	RenderContext *context = RenderContext::GetInstance();

	SetupBonesOnDemand();

	//context->SetBoneCount(boneCount);

	//memcpy(context->GetBoneTransformsForModify(), bones, sizeof(mat4) * boneCount);

	context->SetBonePointer(bones, sizeof(mat4) * boneCount);

	geometry->Draw(absoluteCycle, sequence);

	context->SetBoneCount(0);
}

void MDXRenderable::DrawSkeleton()
{
	if (modelData == nullptr)
	{
		return;
	}

	int visibleBoneCount = modelData->GetModelData()->pHelpers->Helpers.size() - 1;

	if (visibleBoneCount < 1)
	{
		return;
	}

	if (skeletonMaterial == nullptr)
	{
		skeletonMaterial = MaterialDict::GetInstance()->FindMaterial("debug/flatnoz");
		skeletonMaterial->IncrementReferenceCount();
	}

	RenderContext *context = RenderContext::GetInstance();
	context->PushLineWidth(3.0f);

	IMesh *mesh = context->GetDynamicMesh(FaceTypes::LINES, VertexFormat::POSITION_3F | VertexFormat::COLOR_4F, visibleBoneCount * 2);

	MeshBuilder builder;
	builder.AttachModify(mesh);

	for (int i = 1; i <= visibleBoneCount; i++)
	{
		int boneIndex = modelData->GetModelData()->pHelpers->Helpers[i]->ObjectId;

		Uint32 parent = modelData->GetModelData()->pDynamic->Nodes[boneIndex]->ParentId;

		vec3 origin, parentOrigin;
		mat4 tmp;

		tmp = translate(mat4::Identity(), modelData->GetModelData()->pPivots->Pivots[boneIndex]);

		MatrixPosition(bones[boneIndex] * tmp, origin);

		if (parent >= 0 && parent < 0xFFFFFFFF)
		{
			tmp = translate(mat4::Identity(), modelData->GetModelData()->pPivots->Pivots[parent]);
			MatrixPosition(bones[parent] * tmp, parentOrigin);
		}
		else
		{
			parentOrigin = origin;
		}

		vec3 color;
		color[0] = 0.5f + (i % 9) / 8.0f * 0.5f;
		color[1] = 0.5f + (i % 5) / 4.0f * 0.5f;
		color[2] = 0.5f + (i % 7) / 6.0f * 0.5f;

		builder.Position3v(origin);
		builder.Color4f(XYZ(color), 1);
		builder.AdvanceVertex();
		builder.Position3v(parentOrigin);
		builder.Color4f(XYZ(color), 1);
		builder.AdvanceVertex();
	}

	builder.Detach();

	context->BindMesh(mesh);
	skeletonMaterial->Draw();

	context->PopLineWidth();
}

void MDXRenderable::InvalidateBoneCache()
{
	MakeSkeletonDirty();
}

void MDXRenderable::SetSequence(const char *name)
{
	SetSequence(modelData->FindSequence(name));
}

void MDXRenderable::SetSequence(int index)
{
	Assert(index >= 0 && index < (int)modelData->GetModelData()->pSequences->Sequences.size());

	if (index == sequence)
		return;

	if (wasSequenceSet)
	{
		SequenceTransitionLayer transition;
		transition.weight = 1.0f;
		transition.cycle = cycle;
		transition.isLooping = isSequenceLooping;
		transition.sequenceRate = sequenceRate;
		transition.sequence = sequence;
		transitions.push_back(std::move(transition));

		if (transitions.size() > MODEL_MAX_TRANSITION_LAYERS)
		{
			transitions.erase(transitions.begin());
		}
	}

	MakeSkeletonDirty();

	sequence = index;
	sequenceRate = modelData->GetSequencePlaybackRate(index);
	cycle = 0.0f;

	wasSequenceSet = true;
}

int MDXRenderable::GetSequence() const
{
	return sequence;
}

const char *MDXRenderable::GetSequenceName() const
{
	return modelData->GetSequenceName(sequence);
}

void MDXRenderable::SetCycle(float cycle)
{
	if (cycle != this->cycle)
		MakeSkeletonDirty();

	this->cycle = cycle;
}

float MDXRenderable::GetCycle() const
{
	return cycle;
}

void MDXRenderable::SetPlaybackRate(float rate)
{
	playbackRate = rate;
}

float MDXRenderable::GetPlaybackRate(float rate) const
{
	return playbackRate;
}

void MDXRenderable::AdvanceFrame(float frametime)
{
	if (frametime == 0.0f)
		return;

	if (playbackRate == 0.0f
		|| sequenceRate == 0.0f)
		return;

	if (!isSequenceLooping && cycle >= 1.0f)
		return;

	cycle += sequenceRate * playbackRate * frametime;

	MakeSkeletonDirty();

	if (cycle >= 1.0f)
	{
		if (isSequenceLooping)
			cycle = fmodf(cycle, 1.0f);
		else
			cycle = 1.0f;
	}

	for (auto itr = transitions.begin(); itr != transitions.end();)
	{
		SequenceTransitionLayer &s = *itr;

		s.weight -= frametime * 3.0f;

		if (s.weight <= 0.0f)
		{
			itr = transitions.erase(itr);
		}
		else
		{
			s.cycle += s.sequenceRate * frametime;

			if (s.cycle >= 1.0f)
			{
				if (s.isLooping)
					s.cycle = fmodf(s.cycle, 1.0f);
				else
					s.cycle = 1.0f;
			}

			itr++;
		}
	}
}

void MDXRenderable::GetModelExtents(vec3 &min, vec3 &max)
{
	if (sequence >= 0)
	{
		const MDXSequenceChunk::MDXSequence &seqData = modelData->GetModelData()->pSequences->Sequences[sequence];
		min = seqData.MinimumExtent;
		max = seqData.MaximumExtent;
	}
	else
	{
		const MDXModelChunk *mdlData = modelData->GetModelData()->pModel;
		min = mdlData->MinimumExtent;
		max = mdlData->MaximumExtent;
	}
}

void MDXRenderable::InitBones()
{
	boneCount = modelData->GetBoneCount();

	modelData->BuildBindPose(bindPose);
	modelData->BuildInversionPose(inversionPose);
}

void MDXRenderable::SetupBonesOnDemand()
{
	if (!IsSkeletonDirty())
		return;

	SetupBones();

	isSkeletonDirty = false;
}

void MDXRenderable::SetupBones()
{
	const mat4 &viewMatrix = RenderContext::GetInstance()->GetViewMatrix();
	const mat4 &modelMatrix = RenderContext::GetInstance()->GetModelMatrix();

	int animationtime = modelData->RemapSequenceCycle(sequence, cycle);

	absoluteCycle = animationtime;

	modelData->BuildAnimatedPose(modelMatrix, bindPose, animationtime, sequence, bones);

	for (const auto &s : transitions)
	{
		modelData->AccumulateAnimatedPose(modelMatrix, bindPose,
			modelData->RemapSequenceCycle(s.sequence, s.cycle),
			s.sequence, bones, s.weight);
	}

	modelData->FinalizeAnimatedPose(viewMatrix, inversionPose, bones);
}

void MDXRenderable::MakeSkeletonDirty()
{
	isSkeletonDirty = true;
}

bool MDXRenderable::IsSkeletonDirty() const
{
	return isSkeletonDirty;
}