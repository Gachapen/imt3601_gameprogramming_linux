
#include "pch.h"
#include "materialsystem\materialsystem.h"
#include "mesh.h"
#include "vbodict.h"
#include "material.h"
#include "rendercontext.h"

#include "baseshader.h"

Mesh::Mesh()
	: meshData(nullptr)
	, vbo(VBO_KEY_INVALID)
	, arrayBufferSize(0)
	, vertexCount(0)
	, vertexFormat(0)
	, isDynamic(false)
	, initialVertexCount(0)
	, ringbufferSize(0)
	, ringbufferOffset(0)
	, vertexFormatSize(0)
	, renderOffset(0)
{

}

Mesh::~Mesh()
{
	delete [] meshData;

	if (vbo != VBO_KEY_INVALID)
	{
		VBODict::GetInstance()->FreeVBO(vbo);
	}
}

void Mesh::Init(FaceTypeId type, int vertexFormat, int faceCount, bool isDynamic)
{
	Assert(meshData == nullptr);
	Assert(vbo == VBO_KEY_INVALID);

	faceType = type;
	this->isDynamic = isDynamic;

	this->vertexFormat = vertexFormat;
	vertexBinding = VertexAttribBinding(vertexFormat);

	vertexCount = CalcVertexCount(faceCount);
	int vertexBufferSize = vertexCount;

	if (isDynamic)
	{
		// fixed ring buffer size
		// TODO: tweak/fix if it's too small
		ringbufferSize = (1 << 19);
		vertexBufferSize = ringbufferSize;

		Assert(ringbufferSize > DYNAMIC_MESH_MAX_VERTICES);
	}

	initialVertexCount = vertexBufferSize;
	vertexFormatSize = vertexBinding.GetVertexFormatSize();

	arrayBufferSize = vertexFormatSize * vertexBufferSize;
	arrayBufferSize /= sizeof(float);

	meshData = new float[arrayBufferSize];
}

void Mesh::Release()
{
	delete this;
}

void Mesh::FlushCache()
{
	delete [] meshData;
	meshData = nullptr;
}

void Mesh::Upload()
{
	Assert(arrayBufferSize > 0);

	if (vbo == VBO_KEY_INVALID)
		vbo = VBODict::GetInstance()->CreateVBO(vertexBinding, sizeof(float) * arrayBufferSize, meshData, isDynamic);
	else
		VBODict::GetInstance()->ModifyVBO(vbo, sizeof(float) * arrayBufferSize, meshData, isDynamic);
}

float *Mesh::BindMap(int offset, int range)
{
	Assert(offset < 0
		|| offset <= vertexCount);

	Assert(range < 0
		|| offset >= 0
		&& range + offset <= vertexCount);

	Assert(!isDynamic || offset < 0);
	Assert(!isDynamic || range < 0);

	// dynamic meshes always bind ranges
	if (isDynamic
		&& offset < 0)
	{
		offset = 0;
	}

	// bind till the end
	if (offset >= 0
		&& range < 0)
	{
		range = vertexCount - offset;
	}

	// handle VBO as ringbuffer
	if (ringbufferSize > 0)
	{
		Assert(isDynamic);

		// tweak ringbufferSize!!!
		Assert(range < ringbufferSize);

		if (range > ringbufferSize)
		{
			DBGMSGF("Dynamic mesh overflow!!! (max: %i)\n", ringbufferSize);
		}

		if (ringbufferOffset + range < ringbufferSize)
		{
			offset = ringbufferOffset;

			ringbufferOffset += range;
		}
		else
		{
			offset = 0;

			ringbufferOffset = range;
		}

		// remember offset for rendering
		renderOffset = offset;
	}

	return VBODict::GetInstance()->BindMap(vbo, offset * vertexFormatSize, range * vertexFormatSize);
}

void Mesh::BindUnmap()
{
	VBODict::GetInstance()->BindUnmap(vbo);
}

void Mesh::Bind(int destinationVertexFormat) const
{
	Assert(vbo != VBO_KEY_INVALID);

	VBODict::GetInstance()->BindVBO(vbo, destinationVertexFormat);
}

void Mesh::Unbind() const
{
	Assert(vbo != VBO_KEY_INVALID);

	VBODict::GetInstance()->UnbindVBO(vbo);
}

void Mesh::Draw() const
{
	int mode = 0;

	switch (faceType)
	{
	case FaceTypes::LINES:
		mode = GL_LINES;
		break;
	case FaceTypes::LINE_LOOP:
		mode = GL_LINE_LOOP;
		break;
	case FaceTypes::TRIANGLES:
		mode = GL_TRIANGLES;
		break;
	case FaceTypes::TRIANGLE_STRIP:
		mode = GL_TRIANGLE_STRIP;
		break;
	case FaceTypes::TRIANGLE_FAN:
		mode = GL_TRIANGLE_FAN;
		break;
	case FaceTypes::QUADS:
		mode = GL_QUADS;
		break;
	default:
		Assert(0);
	}

	glDrawArrays(mode, renderOffset, vertexCount);
}

void Mesh::SetFaceCount(int newFaceCount)
{
	Assert(newFaceCount <= initialVertexCount);
	Assert(newFaceCount > 0);

	vertexCount = CalcVertexCount(newFaceCount);
}

int Mesh::CalcVertexCount(int faceCount)
{
	int count = 0;

	if (faceType == FaceTypes::TRIANGLE_STRIP)
	{
		count = 2;
		count += faceCount;
	}
	else
	{
		int faceSize = FaceTypes::GetFaceTypeElementSize(faceType);
		count = faceSize * faceCount;
	}

	return count;
}