#ifndef MATERIALDICT_H
#define MATERIALDICT_H

#include "util\singleton.h"
#include "materialsystem\imaterialdict.h"

class Material;

class MaterialDict : public IMaterialDict
{
	DECLARE_SINGLETON(MaterialDict);
public:
	~MaterialDict();

	void Shutdown();

	// IMaterialDict
	virtual IMaterial *FindMaterial(const char *path);

	virtual bool ReleaseMaterialIfUnreferenced(IMaterial *material);
	virtual void ReleaseUnreferencedMaterials();
	virtual void ReloadAllMaterials();

private:

	std::unordered_map<std::string, Material*> materials;
};



#endif