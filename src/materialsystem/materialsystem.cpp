
#include "pch.h"
#include "materialsystem\materialsystem.h"

static const char *const vertexElementNames[] =
{
	"position",
	"position",
	"texcoord",
	"color",
	"color",
	"normal",
	"tangent",
	"binormal",
	"boneindices",
};

static_assert(ARRAYSIZE(vertexElementNames) == VertexFormat::NUM_BITS, "Update array above!");

namespace VertexFormat
{
	const char *const GetVertexElementName(int element)
	{
		return vertexElementNames[GetVertexElementIndex(element)];
	}
}