
#include "pch.h"
#include "materialsystem\materialsystem.h"
#include "vertexattributebinding.h"

VertexAttribBinding::VertexAttribBinding()
{
	Init();
}

VertexAttribBinding::VertexAttribBinding(int vertexFormat)
{
	Init();

	this->vertexFormat = vertexFormat;

	for (int i = 0; i < VertexFormat::NUM_BITS; i++)
	{
		const int flag = 1 << i;

		if ((flag & vertexFormat) != 0)
		{
			const int size = VertexFormat::GetVertexElementSize(flag) / sizeof(float);

			AddBinding(size);
		}
	}
}

void VertexAttribBinding::Init()
{
	vertexSize = 0;
	vertexFormat = 0;
	attributeCount = 0;
}

void VertexAttribBinding::AddBinding(const int size)
{
	Binding binding;
	binding.size = size;
	binding.offset = (GLvoid*)vertexSize;

	vertexSize += sizeof(float)  * size;

	bindings.push_back(binding);

	attributeCount++;
}

void VertexAttribBinding::Apply() const
{
	const Binding *pBinding = bindings.data();

	for (int i = 0; i < attributeCount; i++)
	{
		glVertexAttribPointer(i, pBinding->size, GL_FLOAT, GL_FALSE, vertexSize, pBinding->offset);
		pBinding++;
	}
}

void VertexAttribBinding::Apply(int destinationFormat) const
{
	Assert(vertexFormat > 0);

	// format matches mesh, do naive binding
	if (vertexFormat == destinationFormat)
	{
		Apply();
		return;
	}

	const Binding *pBinding = bindings.data();
	int writeIndex = 0;

	for (int i = 0; i < VertexFormat::NUM_BITS; i++)
	{
		const int element = VertexFormat::GetVertexElementFlag(i);

		if (VertexFormat::HasElement(destinationFormat, element))
		{
			Assert(writeIndex < attributeCount); // binding pointer out of range

			glVertexAttribPointer(writeIndex, pBinding->size, GL_FLOAT, GL_FALSE, vertexSize, pBinding->offset);

			pBinding++;
			writeIndex++;

			Assert(VertexFormat::HasElement(vertexFormat, element)); // vertex format too thin
		}
		else if (VertexFormat::HasElement(vertexFormat, element))
		{
			// stuff binding artificially
			pBinding++;
		}
	}
}

void VertexAttribBinding::EnableAttributes() const
{
	for (int i = 0; i < attributeCount; i++)
		glEnableVertexAttribArray(i);
}

void VertexAttribBinding::DisableAttributes() const
{
	for (int i = 0; i < attributeCount; i++)
		glDisableVertexAttribArray(i);
}