
#include "pch.h"
#include "materialvar.h"


MaterialVar::MaterialVar(const char *name)
	: value_string(nullptr)
	, type(MaterialVarTypes::INVALID)
	, isDefined(false)
{
	this->name = G_StrCreateCopy(name);
}

MaterialVar::~MaterialVar()
{
	delete [] name;

	if (type == MaterialVarTypes::STRING)
		delete [] value_string;
}

void MaterialVar::Init(const char *rawValue, MaterialVarTypeId defaultType)
{
	Assert(type == MaterialVarTypes::INVALID);
	Assert(rawValue != nullptr);

	isDefined = true;

	bool hasDot = false;
	bool hasVectorBacket = false;
	bool hasText = false;

	const char *read = rawValue;

	while (*read)
	{
		if (*read == '[')
			hasVectorBacket = true;
		else if (*read == '.')
			hasDot = true;
		else if ((*read < '0'
			|| *read > '9')
			&& *read != '['
			&& *read != ']'
			&& *read != '.'
			&& *read != ' '
			&& *read != '\t')
			hasText = true;

		read++;
	}

	if (hasText)
	{
		type = MaterialVarTypes::STRING;

		value_string = G_StrCreateCopy(rawValue);
	}
	else if (hasVectorBacket)
	{
		float values[4];
		int count = 0;

		read = rawValue;
		while (*read != '[')
			read++;
		read++;

		for (int i = 0; i < 4 && *read; i++)
		{
			// traverse white space
			while (*read == ' '
				|| *read == '\t')
				read++;

			if (*read == ']')
				break;

			// first non-whitespace to float
			values[count] = (float)atof(read);

			// forward till whitespace
			while (*read != ' '
				&& *read != '\t'
				&& *read)
				read++;

			count++;
		}

		Assert(count >= 1);

		switch (count)
		{
		default:
			Assert(0);
		case 1:
			type = MaterialVarTypes::FLOAT;
			value_float = values[0];
			break;
		case 2:
			type = MaterialVarTypes::VEC2;
			value_vec2 = vec2(values[0], values[1]);
			break;
		case 3:
			type = MaterialVarTypes::VEC3;
			value_vec3 = vec3(values[0], values[1], values[2]);
			break;
		case 4:
			type = MaterialVarTypes::VEC4;
			value_vec4 = vec4(values[0], values[1], values[2], values[3]);
			break;
		}
	}
	else if (hasDot)
	{
		type = MaterialVarTypes::FLOAT;
		value_float = (float)atof(rawValue);
	}
	else
	{
		type = defaultType;

		switch (type)
		{
		case MaterialVarTypes::INT:
			value_int = atoi(rawValue);
			break;
		case MaterialVarTypes::TEXTURE:
			value_texture = nullptr;
			break;
		default:
			Assert(0);
			break;
		}

		isDefined = *rawValue != 0;
	}
}

MaterialVarTypeId MaterialVar::GetType() const
{
	return type;
}

const char *MaterialVar::GetName() const
{
	return name;
}

bool MaterialVar::IsDefined() const
{
	return isDefined;
}

int MaterialVar::GetInt() const
{
	Assert(type == MaterialVarTypes::INT);
	return value_int;
}

bool MaterialVar::GetBool() const
{
	Assert(type == MaterialVarTypes::INT);
	return value_int != 0;
}

float MaterialVar::GetFloat() const
{
	Assert(type == MaterialVarTypes::FLOAT);
	return value_float;
}

vec2 MaterialVar::GetVec2() const
{
	Assert(type == MaterialVarTypes::VEC2);
	return value_vec2;
}

vec3 MaterialVar::GetVec3() const
{
	Assert(type == MaterialVarTypes::VEC3);
	return value_vec3;
}

vec4 MaterialVar::GetVec4() const
{
	Assert(type == MaterialVarTypes::VEC4);
	return value_vec4;
}

const char *MaterialVar::GetString() const
{
	Assert(type == MaterialVarTypes::STRING);
	return value_string;
}

ITexture *MaterialVar::GetTexture() const
{
	Assert(type == MaterialVarTypes::TEXTURE);
	return value_texture;
}

void MaterialVar::SetInt(int value)
{
	Assert(type == MaterialVarTypes::INT);
	value_int = value;
}

void MaterialVar::SetBool(bool value)
{
	Assert(type == MaterialVarTypes::INT);
	value_int = value ? 1 : 0;
}

void MaterialVar::SetFloat(float value)
{
	Assert(type == MaterialVarTypes::FLOAT);
	value_float = value;
}

void MaterialVar::SetVec2(const vec2 &value)
{
	Assert(type == MaterialVarTypes::VEC2);
	value_vec2 = value;
}

void MaterialVar::SetVec3(const vec3 &value)
{
	Assert(type == MaterialVarTypes::VEC3);
	value_vec3 = value;
}

void MaterialVar::SetVec4(const vec4 &value)
{
	Assert(type == MaterialVarTypes::VEC4);
	value_vec4 = value;
}

void MaterialVar::SetString(const char *value)
{
	Assert(type == MaterialVarTypes::STRING);

	delete [] value_string;
	value_string = new char[strlen(value) + 1];
	G_StrCpy(value_string, value);
}

void MaterialVar::SetTexture(ITexture *value)
{
	Assert(type == MaterialVarTypes::STRING
		|| type == MaterialVarTypes::TEXTURE);

	if (type == MaterialVarTypes::STRING)
	{
		delete [] value_string;
	}
	else if (value_texture != nullptr)
	{
		Assert(type == MaterialVarTypes::TEXTURE);

		value_texture->DecrementReferenceCount();
	}

	value_texture = value;
	type = MaterialVarTypes::TEXTURE;

	if (value_texture != nullptr)
	{
		value_texture->IncrementReferenceCount();
	}
}
