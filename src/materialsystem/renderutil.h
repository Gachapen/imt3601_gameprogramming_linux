#ifndef RENDERUTIL_H
#define RENDERUTIL_H

#include "materialsystem\materialsystem.h"

namespace TextureFormats
{
	struct GlFormat_s
	{
		GLint bytesPerPixel;
		GLuint driverFormat;
		GLenum componentType;
	};

	extern const GlFormat_s &GetTextureFormatInfo(TextureFormatId textureFormat);
}


#endif