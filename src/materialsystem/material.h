#ifndef MATERIAL_H
#define MATERIAL_H

#include "materialsystem\materialsystem.h"
#include "shaders/baseshader.h"
#include "materialsystem\imaterial.h"

class IMaterialVar;
class MaterialVar;

class Material : public IMaterial, public CReferenceCounted
{
	~Material();
public:
	Material();

	bool LoadFromFile(const char *name);
	bool LoadFromString(const char *name, const char *text);
	void Release();
	void ReleaseResources(bool deleteTextures = false);

	static void FlushMaterialVarCache();

	// IMaterial
	FORWARD_REFERENCE_COUNTED_DEFINE();

	virtual const char *GetName() const;
	virtual bool IsErrorMaterial() const;

	virtual void Draw();

	virtual IMaterialVar *FindMaterialVar(const char *name);
	virtual IMaterialVar *FindMaterialVarFast(MATERIAL_VAR_HANDLE &handle, const char *name);

private:
	void InitShader();
	void LoadErrorMaterial();

	char *name;
	bool isErrorMaterial;

	BaseShader *shaderInstance;
	MaterialContext *shaderContext;

	std::vector<MaterialVar*> materialVars;
	MaterialVar **materialVarsBase;

	static MATERIAL_VAR_HANDLE lastInvalidVarHandle;
	static std::vector<IMaterialVar*> materialVarCache;
};

#endif