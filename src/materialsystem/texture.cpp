
#include "pch.h"
#include "materialsystem\materialsystem.h"
#include "renderutil.h"
#include "texture.h"


Texture::Texture()
	: name(nullptr)
	, width(0)
	, height(0)
	, type(TextureTypes::INVALID)
	, format(TextureFormats::INVALID)
	, flags(0)
	, textureId(GL_INVALID_INDEX)
	, glTextureType(GL_INVALID_INDEX)
{
}

Texture::~Texture()
{
	delete [] name;

	Assert(textureId == GL_INVALID_INDEX);
}

void Texture::Init(const char *name,
	TextureFormatId format, TextureTypeId type, TextureGroupId group,
	int flags, int width, int height, void *data)
{
	Assert(textureId == GL_INVALID_INDEX);

	delete [] this->name;
	this->name = new char[strlen(name) + 1];
	G_StrCpy(this->name, name);
	G_StrFixSlashes(this->name);
	G_StrStripExtension(this->name);

	this->type = type;
	this->flags = flags;
	this->group = group;
	this->width = width;
	this->height = height;
	this->format = format;

	const bool bMipmap = (flags & TextureFlags::NO_MIPMAP) == 0;
	const bool bFiltering = (flags & TextureFlags::NO_FILTERING) == 0;

	const bool bClampS = (flags & TextureFlags::CLAMP_S) != 0;
	const bool bClampT = (flags & TextureFlags::CLAMP_T) != 0;
	const bool bClampR = bMipmap && (flags & TextureFlags::CLAMP_R) != 0;

	switch (type)
	{
	default:
		Assert(0);
	case TextureTypes::TEXTURE_2D:
		glTextureType = GL_TEXTURE_2D;
		break;
	case TextureTypes::TEXTURE_CUBEMAP:
		glTextureType = GL_TEXTURE_CUBE_MAP;
		break;
	}

	// alloc texture
	glGenTextures(1, &textureId);
	glBindTexture(glTextureType, textureId);

	// set texture data
	const TextureFormats::GlFormat_s &formatInfo = GetTextureFormatInfo(format);

	glTexImage2D(glTextureType, 0, formatInfo.bytesPerPixel, width, height, 0,
		formatInfo.driverFormat, formatInfo.componentType, data);

	// set bilinear filtering
	GLuint filterType = bFiltering ? GL_LINEAR : GL_NEAREST;
	GLuint minFilter = (bMipmap && bFiltering) ? GL_LINEAR_MIPMAP_LINEAR : filterType;

	glTexParameteri(glTextureType, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(glTextureType, GL_TEXTURE_MAG_FILTER, filterType);

	// lookup clamping
	if (bClampS)
	{
		glTexParameteri(glTextureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	}

	if (bClampT)
	{
		glTexParameteri(glTextureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	if (bClampR)
	{
		glTexParameteri(glTextureType, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}

	// generate mipmap
	if (bMipmap && data != nullptr)
	{
		GenerateMipmaps(glTextureType);
	}

	if (bFiltering)
	{
		glTexParameterf(glTextureType, GL_TEXTURE_MAX_ANISOTROPY_EXT, 8.0f);
	}

	glBindTexture(glTextureType, GL_INVALID_INDEX);
}

void Texture::SetContent(TextureFormatId format, int width, int height, void *data)
{
	Assert(glTextureType != GL_INVALID_INDEX);

	this->format = format;

	const TextureFormats::GlFormat_s &formatInfo = GetTextureFormatInfo(format);

	glBindTexture(glTextureType, textureId);
	glTexImage2D(glTextureType, 0, formatInfo.bytesPerPixel, width, height, 0,
		formatInfo.driverFormat, formatInfo.componentType, data);

	const bool bMipmap = (flags & TextureFlags::NO_MIPMAP) == 0;
	if (bMipmap && data != nullptr)
	{
		GenerateMipmaps(glTextureType);
	}

	glBindTexture(glTextureType, 0);
}

void Texture::SetCubemapFace(int index, void *data)
{
	Assert(textureId != GL_INVALID_INDEX);
	Assert(type == TextureTypes::TEXTURE_CUBEMAP);

	const TextureFormats::GlFormat_s &formatInfo = GetTextureFormatInfo(format);

	glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);
	glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + index, 0, formatInfo.bytesPerPixel, width, height, 0,
		formatInfo.driverFormat, formatInfo.componentType, data);
}

void Texture::FinalizeCubemap()
{
	Assert(type == TextureTypes::TEXTURE_CUBEMAP);

	if ((flags & TextureFlags::NO_MIPMAP) == 0)
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);
		GenerateMipmaps(GL_TEXTURE_CUBE_MAP);
	}

	glBindTexture(textureId, GL_INVALID_INDEX);
}

void Texture::Release()
{
	Assert(textureId != GL_INVALID_INDEX);

	glDeleteTextures(1, &textureId);
	textureId = GL_INVALID_INDEX;

	Assert(CReferenceCounted::IsReferenceCountValid() && !CReferenceCounted::IsReferenced());

	if (!CReferenceCounted::IsReferenced())
		delete this;
}

void Texture::Bind()
{
	if (type == TextureTypes::TEXTURE_CUBEMAP)
	{
		glEnable(GL_TEXTURE_CUBE_MAP);
		glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);
		glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);
		glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP);
		glEnable(GL_TEXTURE_GEN_S);
		glEnable(GL_TEXTURE_GEN_T);
		glEnable(GL_TEXTURE_GEN_R);

		glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, textureId);
	}
}

void Texture::GenerateMipmaps(GLuint target)
{
	glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
	glGenerateMipmap(target);
}

GLuint Texture::GetTextureId() const
{
	Assert(textureId != GL_INVALID_INDEX);
	return textureId;
}

const bool Texture::IsReferenced() const
{
	Assert(textureId != GL_INVALID_INDEX);
	return CReferenceCounted::IsReferenced();
}

const char *Texture::GetTextureName() const
{
	Assert(textureId != GL_INVALID_INDEX);
	return name;
}

int Texture::GetWidth() const
{
	Assert(textureId != GL_INVALID_INDEX);
	return width;
}

int Texture::GetHeight() const
{
	Assert(textureId != GL_INVALID_INDEX);
	return height;
}

TextureFormatId Texture::GetFormat() const
{
	Assert(textureId != GL_INVALID_INDEX);
	return format;
}

TextureTypeId Texture::GetType() const
{
	Assert(textureId != GL_INVALID_INDEX);
	return type;
}

TextureGroupId Texture::GetGroup() const
{
	Assert(textureId != GL_INVALID_INDEX);
	return group;
}

void Texture::IncrementReferenceCount()
{
	Assert(textureId != GL_INVALID_INDEX);
	CReferenceCounted::IncrementReferenceCount();
}

void Texture::DecrementReferenceCount()
{
	Assert(textureId != GL_INVALID_INDEX);
	CReferenceCounted::DecrementReferenceCount();
}