
#include "pch.h"
#include "materialsystem\materialsystem.h"
#include "material.h"
#include "materialdict.h"
#include "texturedict.h"
#include "shaders\shaderprogramdict.h"
#include "appinterface\iappinterface.h"

SINGLETON_INSTANCE(MaterialDict);

EXPOSE_APP(INTERFACE_IMATERIALDICT_VERSION, MaterialDict::GetInstance());


CON_COMMAND(r_reloadallmaterials)
{
	MaterialDict::GetInstance()->ReloadAllMaterials();
}

MaterialDict::MaterialDict()
{
}

MaterialDict::~MaterialDict()
{
	Assert(materials.empty());
}

void MaterialDict::Shutdown()
{
	for (auto m : materials)
	{
		m.second->Release();
	}

	materials.clear();
}

IMaterial *MaterialDict::FindMaterial(const char *path)
{
	StringScope pathFixed(path);
	G_StrStripExtension(pathFixed);
	G_StrFixSlashes(pathFixed);

	auto itr = materials.find(pathFixed.c_str());

	if (itr != materials.end())
	{
		return itr->second;
	}

	Material *newMaterial = new Material();
	newMaterial->LoadFromFile(pathFixed);

	materials[pathFixed.c_str()] = newMaterial;

	return newMaterial;
}

bool MaterialDict::ReleaseMaterialIfUnreferenced(IMaterial *material)
{
	Assert(material != nullptr);

	Material *concreteMaterial = assert_cast<Material*>(material);

	if (concreteMaterial->IsReferenced())
	{
		return false;
	}

	auto itr = materials.find(material->GetName());

	if (itr == materials.end())
	{
		Assert(0);
		return false;
	}

	materials.erase(itr);

	concreteMaterial->Release();
	return true;
}

void MaterialDict::ReleaseUnreferencedMaterials()
{
	for (auto itr = materials.begin(); itr != materials.end();)
	{
		if (!itr->second->IsReferenced())
		{
			itr->second->Release();
			itr = materials.erase(itr);
		}
		else
		{
			itr++;
		}
	}
}

void MaterialDict::ReloadAllMaterials()
{
	for (auto m : materials)
	{
		m.second->ReleaseResources();
	}

	ShaderProgramDict::GetInstance()->Shutdown();
	TextureDict::GetInstance()->ReleaseUnreferencedTextures();
	Material::FlushMaterialVarCache();

	for (auto m : materials)
	{
		char name[MAX_PATH_GAME];
		G_StrCpy(name, m.second->GetName());
		m.second->LoadFromFile(name);
	}
}