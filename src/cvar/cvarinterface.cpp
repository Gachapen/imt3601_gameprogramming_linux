
#include "pch.h"

#include <algorithm>

#define ICVARINTERFACE_EXPORTS
#include "cvarinterface.h"

SINGLETON_INSTANCE(CvarInterface);

EXPOSE_APP(INTERFACE_ICVAR_VERSION, CvarInterface::GetInstance());

using namespace std;

static struct CommandEntry
{
	string name;
	CvarExecutionTypeId executionType;
	ICallback *command;

	CommandEntry *next;
} firstCommandEntry;

CommandEntry *lastCommandEntry = &firstCommandEntry;

void RegisterCommand(const char *name, CvarExecutionTypeId executionType, ICallback *command)
{
	CommandEntry *entry = new CommandEntry;
	entry->command = command;
	entry->name = name;
	entry->next = nullptr;
	entry->executionType = executionType;

	lastCommandEntry->next = entry;
	lastCommandEntry = entry;
}

inline bool ChrIsDelimiter(char c)
{
	return c == ';' || c == '\n';
}

inline bool ChrIsQuote(char c)
{
	return c == '"';
}

class CommandArgs : public ICommandArgs
{
public:
	~CommandArgs();

	virtual int GetArgCount();
	virtual const char *GetArg(int index);

	void AddArg(const char *arg);

private:
	std::vector<char *> args;
};

CommandArgs::~CommandArgs()
{
	for (auto a : args)
	{
		delete [] a;
	}
}

int CommandArgs::GetArgCount()
{
	return args.size();
}

const char *CommandArgs::GetArg(int index)
{
	Assert(index >= 0 && index < int(args.size()));

	return args[index];
}

void CommandArgs::AddArg(const char *arg)
{
	Assert(arg && *arg);

	args.push_back( G_StrCreateCopy(arg) );
}

CvarInterface::CvarInterface()
{
}

void CvarInterface::Init()
{
	CommandEntry *current = firstCommandEntry.next;

	while (current != nullptr)
	{
		CommandEntry *deleteMe = current;

		Assert(current->executionType >= 0 && current->executionType < CvarExecutionTypes::COUNT);
		Assert(commands[current->executionType].find(current->name) == commands[current->executionType].end());

		commands[current->executionType][current->name] = current->command;

		current = current->next;
		delete deleteMe;
	}

	firstCommandEntry.next = nullptr;
	lastCommandEntry = &firstCommandEntry;
}

enum TokenParse_e
{
	TOKEN_DELIMITER = 0,
	TOKEN_SPACE,
	TOKEN_QUOTE,

	TOKEN_INVALID,
};

TokenParse_e CmdTokenize(const char *start, const char **end = nullptr,
						 char *out = nullptr, int sizeInBytes = -1)
{
	Assert(start);

	if (!*start)
	{
		return TOKEN_INVALID;
	}

	TokenParse_e parse = TOKEN_INVALID;

	while (*start
		&& G_ChrIsWhitespace(*start))
	{
		start++;
	}

	const char *reader = start;

	for (;;)
	{
		if (*reader == 0
			|| ChrIsDelimiter(*reader))
			parse = TOKEN_DELIMITER;
		else if (ChrIsQuote(*reader))
			parse = TOKEN_QUOTE;
		else if (G_ChrIsWhitespace(*reader))
			parse = TOKEN_SPACE;

		if (parse != TOKEN_INVALID)
		{
			if (out != nullptr
				&& sizeInBytes > 0)
			{
				if (reader <= start)
				{
					*out = 0;
				}
				else
				{
					int len = MIN((reader - start + 1), sizeInBytes);
					G_StrNCpy(out, start, len);
					out[len - 1] = 0;
				}
			}

			if (end != nullptr)
			{
				*end = reader;

				// advance unless end of string
				if (**end)
				{
					(*end)++;
				}
			}

			return parse;
		}

		reader++;
	}
};

void CvarInterface::ExecuteBuffer(const char *buffer, CvarExecutionTypeId executionType)
{
	const char *start = buffer;

	// nothing to execute
	if (!*start)
	{
		return;
	}

	bool enabledExecutionModes[CvarExecutionTypes::COUNT] = { false };

	for (int i = 0; i < CvarExecutionTypes::COUNT; i++)
	{
		enabledExecutionModes[i] = i == executionType
			|| executionType == CvarExecutionTypes::ALL;
	}

	char tmpCommand[256];

	TokenParse_e parse = CmdTokenize(start, &start, tmpCommand, sizeof(tmpCommand));

	while (parse != TOKEN_INVALID)
	{
		for (int i = 0; i < CvarExecutionTypes::COUNT; i++)
		{
			if (!enabledExecutionModes[i])
			{
				continue;
			}

			auto itr = commands[i].find(tmpCommand);

			if (itr != commands[i].end())
			{
				CommandArgs args;

				if (parse == TOKEN_SPACE
					|| parse == TOKEN_QUOTE)
				{
					start = ParseCommandArgs(start, args);
				}

				itr->second->Run(&args);

				break;
			}
		}

		parse = CmdTokenize(start, &start, tmpCommand, sizeof(tmpCommand));
	}
}

void CvarInterface::GetCommandsStartingWith(const char *text, std::vector<std::string> &commandsOut)
{
	for (int i = 0; i < CvarExecutionTypes::COUNT; i++)
	{
		for (auto itr : commands[i])
		{
			if (itr.first.find(text) == 0)
			{
				commandsOut.push_back(itr.first);
			}
		}
	}

	std::sort(commandsOut.begin(), commandsOut.end());
}

const char *CvarInterface::ParseCommandArgs(const char *start, CommandArgs &args)
{
	char arg[256];

	TokenParse_e parse = CmdTokenize(start, &start, arg, sizeof(arg));

	while (parse != TOKEN_INVALID)
	{
		if (parse == TOKEN_SPACE)
		{
			args.AddArg(arg);
		}
		else if (parse == TOKEN_DELIMITER)
		{
			args.AddArg(arg);
			break;
		}

		parse = CmdTokenize(start, &start, arg, sizeof(arg));
	}

	return start;
}
