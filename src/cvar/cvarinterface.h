#ifndef CVAR_H
#define CVAR_H


#include "cvar/icvarinterface.h"

class CommandArgs;

class CvarInterface : public ICvarInterface
{
	DECLARE_SINGLETON(CvarInterface);

public:
	virtual void Init();

	virtual void ExecuteBuffer(const char *buffer, CvarExecutionTypeId executionType);

	virtual void GetCommandsStartingWith(const char *text, std::vector<std::string> &commandsOut);

private:
	const char *ParseCommandArgs(const char *start, CommandArgs &args);

	std::unordered_map<std::string, ICallback*> commands[CvarExecutionTypes::COUNT];
};

#endif