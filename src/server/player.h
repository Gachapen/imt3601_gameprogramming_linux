#ifndef PLAYER_H
#define PLAYER_H


class Player : public ServerEntity
{
	~Player();

	DECLARE_CLASS(Player, ServerEntity);
	DECLARE_NETWORKTABLE_BASE();
public:
	Player();

	Uint16 GetGold() const;
	void SetGold(Uint16 gold);
	void AddGold(Uint16 amount);
	void RemoveGold(Uint16 amount);

protected:

	//virtual void OnSpawn();
	//virtual void OnRelease();

private:
	//void OnEvent(KeyValues *data);

	//DECLARE_CALLBACK(callbackEvent, KeyValues);

	NetworkVariable<Uint16> gold;

	//DECLARE_NETWORKPROCEDURE_1PARAM(vec3, BloodDecal);
};

#endif