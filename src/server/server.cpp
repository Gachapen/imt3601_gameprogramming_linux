
#include "pch.h"
#include "server.h"

SINGLETON_INSTANCE(Server);

EXPOSE_APP(INTERFACE_ISERVER_VERSION, Server::GetInstance());

IEngineServer *engine;
IGlobals *globals;
IPhysics *physics;
IDebugDraw *debugDraw;
IEvents *events;

Server::Server()
	: clientCommandHost(-1)
{
}

Server::~Server()
{
}

int Server::GetClientCommandHost() const
{
	return clientCommandHost;
}

bool Server::Init()
{
	if ((engine = (IEngineServer *)GetAppInterface()->QueryApp(INTERFACE_IENGINESERVER_VERSION)) == nullptr)
	{
		return false;
	}

	if ((globals = (IGlobals *)GetAppInterface()->QueryApp(INTERFACE_IGLOBALS_SERVER_VERSION)) == nullptr)
	{
		return false;
	}

	if ((physics = (IPhysics *)GetAppInterface()->QueryApp(INTERFACE_IPHYSICS_VERSION)) == nullptr)
	{
		return false;
	}

	if ((debugDraw = (IDebugDraw *)GetAppInterface()->QueryApp(INTERFACE_IDEBUGDRAW_VERSION)) == nullptr)
	{
		return false;
	}

	if ((events = (IEvents *)GetAppInterface()->QueryApp(INTERFACE_IEVENTS_VERSION)) == nullptr)
	{
		return false;
	}

	return GameSystemManager::InitAllGameSystems();
}

void Server::Shutdown()
{
	GameSystemManager::ShutdownAllGameSystems();
}

void Server::Update()
{
	GameSystemManager::UpdateAllGameSystems(globals->GetFrametime());
}

void Server::OnLevelInit()
{
	GameSystemManager::LevelInitAllGameSystems();
}

void Server::OnLevelShutdown()
{
	GameSystemManager::LevelShutdownAllGameSystems();
}

void Server::WriteNetworkSymbolDefinitions(ByteBuffer &buffer)
{
	ServerEntityFactory::GetInstance()->WriteNetworkSymbolDefinitions(buffer);
}

bool Server::WriteEntityConstruction(ByteBuffer &buffer)
{
	return ServerEntityFactory::GetInstance()->WriteEntityConstruction(buffer);
}

bool Server::WriteEntityWorldStateUpdate(ByteBuffer &buffer)
{
	return ServerEntityFactory::GetInstance()->WriteEntityWorldStateUpdate(buffer);
}

void Server::WriteEntityWorldStateComplete(ByteBuffer &buffer)
{
	ServerEntityFactory::GetInstance()->WriteEntityWorldStateComplete(buffer);
}

void Server::SetClientCommandHost(int clientIndex)
{
	clientCommandHost = clientIndex;
}

void Server::OnClientConnected(int clientIndex)
{
	ServerEntity *playerEntity = ServerEntityFactory::GetInstance()->CreateServerEntity("player", clientIndex);

	Assert(playerEntity != nullptr);
}

void Server::OnClientDisconnected(int clientIndex)
{
	auto &entities = ServerEntityFactory::GetInstance()->GetEntities();

	for (auto e : entities)
	{
		if (e->GetOwnerId() == clientIndex)
		{
			e->Remove();
		}
	}

	ServerEntityFactory::GetInstance()->ReleaseScheduledEntities();

	ServerEntity *playerEntity = ServerEntityFactory::GetInstance()->GetNetworkEntityByIndex(clientIndex);

	if (playerEntity != nullptr)
	{
		playerEntity->Release();
	}
}