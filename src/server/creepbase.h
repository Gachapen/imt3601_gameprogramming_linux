#ifndef CREEPBASE_H
#define CREEPBASE_H

#include "shared/creepshared.h"

class CreepBase : public ServerEntity
{
	~CreepBase();

	DECLARE_CLASS(CreepBase, ServerEntity);
	DECLARE_NETWORKTABLE_BASE();
public:
	CreepBase();

	virtual void SetOrigin(const vec3 &origin);
	virtual void SetVelocity(const vec3 &velocity);

	virtual void Simulate();

	virtual float GetRemainingDistance() const;

	virtual void SetScale(float scale);
	virtual void SetSpeed(float speed);
	virtual void SetColor(const vec3 &color);
	virtual void SetGold(int gold) { this->gold = gold; }

protected:

	virtual void OnSpawn();
	virtual void OnRelease();

	virtual void OnTakeDamage(const DamageInfo &info);
	virtual void OnKilled(const DamageInfo &info);

	virtual void UpdatePhysicsState();
	virtual void UpdatePath();
	virtual void UpdateVelocity();
	virtual void UpdateTileChange();

	virtual PhysicsBody GetPhysicsObject();

private:
	void OnEvent(KeyValues *data);

	DECLARE_CALLBACK(callbackEvent, KeyValues);

	PhysicsBody physicsObject;

	std::vector<vec3> pathPoints;
	int lastTileIndex;

	NetworkVariable<Uint8> creepState;
	float delayedRemovalTime;

	DECLARE_NETWORKPROCEDURE_1PARAM(vec3, BloodDecal);

	NetworkVariable<float> scale;
	NetworkVariable<float> maxSpeed;
	NetworkVariable<vec3> color;

	float pathDistanceMinor; // dist to next point
	float pathDistanceMajor; // dist between remaining points

	int gold;
};

#endif