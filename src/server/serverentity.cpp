#include "pch.h"
#include "serverentity.h"


IMPLEMENT_NETWORKTABLE(ServerEntity)
	NETWORK_VARIABLE(health);
	NETWORK_VARIABLE(healthMax);
	NETWORK_VARIABLE(origin);
	NETWORK_VARIABLE(ownerId);
END_NETWORKTABLE();


ServerEntity::ServerEntity()
	:  angles(euler::Zero())
	  , velocity(vec3::Zero())
	  , networkId(-1)
{
	this->networkId = networkId;
	ownerId.Set(0xFFFF);
}

ServerEntity::~ServerEntity()
{
}

void ServerEntity::Release()
{
	OnRelease();

	ServerEntityFactory::GetInstance()->OnEntityReleased(networkId);

	delete this;
}

void ServerEntity::Remove()
{
	ServerEntityFactory::GetInstance()->OnEntityRemoved(this);
}

void ServerEntity::SetNetworkId(int networkId)
{
	Assert(networkId >= 0 && networkId < NETWORKED_ENTITY_COUNT);

	this->networkId = networkId;
}

int ServerEntity::GetNetworkId() const
{
	return networkId;
}

void ServerEntity::DispatchInit()
{
	OnInit();
}

void ServerEntity::DispatchSpawn()
{
	OnSpawn();
}

const vec3 &ServerEntity::GetOrigin()
{
	return origin.Get();
}

const euler &ServerEntity::GetAngles()
{
	return angles;
}

const vec3 &ServerEntity::GetVelocity()
{
	return velocity;
}

void ServerEntity::SetOrigin(const vec3 &origin)
{
	this->origin.Set(origin);
}

void ServerEntity::SetAngles(const euler &angles)
{
	this->angles = angles;
}

void ServerEntity::SetVelocity(const vec3 &velocity)
{
	this->velocity = velocity;
}

int ServerEntity::GetHealth() const
{
	return health.Get();
}

int ServerEntity::GetHealthMax() const
{
	return healthMax.Get();
}

void ServerEntity::SetHealth(int health)
{
	this->health.Set(health);
}

void ServerEntity::SetHealthMax(int healthMax)
{
	this->healthMax.Set(healthMax);
}

void ServerEntity::ApplyDamage(const DamageInfo &info)
{
	if (info.damage <= 0)
	{
		return;
	}

	if (healthMax.Get() <= 0)
	{
		return;
	}

	int newHealth = health.Get() - info.damage;
	newHealth = MAX(0, newHealth);

	OnTakeDamage(info);

	if (health.Get() > 0
		&& newHealth == 0)
	{
		OnKilled(info);
	}

	health.Set(newHealth);
}

void ServerEntity::Simulate()
{
}

void ServerEntity::OnInit()
{
}

void ServerEntity::OnSpawn()
{
}

void ServerEntity::OnRelease()
{
}

void ServerEntity::OnTakeDamage(const DamageInfo &info)
{
}

void ServerEntity::OnKilled(const DamageInfo &info)
{
	Remove();
}

ServerEntity *ServerEntity::GetOwner() const
{
	const Uint16 id = ownerId.Get();

	if (id != 0xFFFF)
	{
		return ServerEntityFactory::GetInstance()->GetNetworkEntityByIndex(id);
	}
	else
	{
		return nullptr;
	}
}

void ServerEntity::SetOwner(ServerEntity *owner)
{
	if (owner != nullptr)
	{
		ownerId.Set(owner->GetNetworkId());
	}
	else
	{
		ownerId.Set(0xFFFF);
	}
}