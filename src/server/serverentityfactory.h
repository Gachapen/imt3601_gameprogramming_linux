#ifndef SERVERENTITYFACTORY_H
#define SERVERENTITYFACTORY_H

#define REGISTER_SERVERENTITY_CLASS(scriptName, className)											   \
	static ServerEntity * __createInstance ## scriptName()											   \
	{																								   \
		return new className();																		   \
	}																								   \
	static class __entityRegister ## scriptName														   \
	{																								   \
public:																								   \
		__entityRegister ## scriptName()															   \
		{																							   \
			ServerEntityFactory::RegisterEntityFactory(# scriptName, &__createInstance ## scriptName); \
		}																							   \
	}																								   \
	__entityRegister ## scriptName ## Instance;

class ServerEntityFactory : public IGameSystem
{
	DECLARE_SINGLETON(ServerEntityFactory);
public:

	typedef ServerEntity *(*CreateEntityFunc)();

	virtual GameSystemPriorityId GetPriority() { return GameSystemPriorites::SERVER_ENTITY_FACTORY; }

	virtual bool Init();
	virtual void Shutdown();

	virtual void Update(float frametime);

	virtual void OnLevelShutdown();

	static void RegisterEntityFactory(const char *name, CreateEntityFunc function);

	ServerEntity *CreateServerEntity(const char *name, int networkIdOverride = -1);
	void OnEntityRemoved(ServerEntity *entity);
	void OnEntityReleased(int networkId);

	void ReleaseScheduledEntities();

	inline std::vector<ServerEntity *> &GetEntities()
	{
		return activeEntities;
	}

	inline ServerEntity *GetNetworkEntityByIndex(int index)
	{
		if (index < 0 || index >= NETWORKED_ENTITY_COUNT)
		{
			return nullptr;
		}

		return networkEntities[index];
	}

	bool WriteEntityConstruction(ByteBuffer &buffer);
	bool WriteEntityWorldStateUpdate(ByteBuffer &buffer);
	void WriteNetworkSymbolDefinitions(ByteBuffer &buffer);
	void WriteEntityWorldStateComplete(ByteBuffer &buffer);

private:
	void ResetFreeNetworkIds();
	int GetFreeNetworkId();
	void FreeAllEntities();

	std::unordered_map<std::string, CreateEntityFunc> functions;

	ServerEntity *networkEntities[NETWORKED_ENTITY_COUNT];
	std::vector<ServerEntity *> activeEntities;
	std::vector<int> freeNetworkIndices;

	std::unordered_set<ServerEntity *> scheduledForDeletion;

	struct EntityConstructionEvent
	{
		int		networkId;
		bool	removed;
		Uint8	networktableIndex;
		//char	className[64];
	};
	std::vector<EntityConstructionEvent> constructionEvents;

	struct NetworkTableDefinition
	{
		std::string					className;
		std::vector<std::string>	memberNames;
	};
	std::vector<NetworkTableDefinition> networkTableDefinitions;
};
#endif
