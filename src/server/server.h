#ifndef SERVER_H
#define SERVER_H

#include "server/iserver.h"

class Server : public IServer
{
	DECLARE_SINGLETON(Server);

	static unsigned int maxBufferSize;

public:

	~Server();

	int GetClientCommandHost() const;

	// IServer
	virtual bool Init();
	virtual void Shutdown();

	virtual void Update();

	virtual void OnLevelInit();
	virtual void OnLevelShutdown();

	virtual void WriteNetworkSymbolDefinitions(ByteBuffer &buffer);
	virtual bool WriteEntityConstruction(ByteBuffer &buffer);
	virtual bool WriteEntityWorldStateUpdate(ByteBuffer &buffer);
	virtual void WriteEntityWorldStateComplete(ByteBuffer &buffer);

	virtual void SetClientCommandHost(int clientIndex);

	virtual void OnClientConnected(int clientIndex);
	virtual void OnClientDisconnected(int clientIndex);

private:
	int clientCommandHost;
};


#endif