#ifndef SERVERENTITY_H
#define SERVERENTITY_H

class ServerEntityFactory;

#define INVALID_ENTITY_HANDLE 0xFFFFFFFF


class ServerEntity
{
	friend class ServerEntityFactory;

	DECLARE_CLASS_NOBASE(ServerEntity);
	DECLARE_NETWORKTABLE();
public:

	ServerEntity();

	// Deletes the object, not save to call during simulate
	void Release();

	// Deferred deletion, should be used instead of deleting directly
	void Remove();

	int GetNetworkId() const;

	void DispatchSpawn();

	virtual const vec3 &GetOrigin();
	virtual const euler &GetAngles();
	virtual const vec3 &GetVelocity();

	virtual void SetOrigin(const vec3 &origin);
	virtual void SetAngles(const euler &angles);
	virtual void SetVelocity(const vec3 &velocity);

	virtual int GetHealth() const;
	virtual int GetHealthMax() const;

	virtual void SetHealth(int health);
	virtual void SetHealthMax(int healthMax);

	virtual void ApplyDamage(const DamageInfo &info);

	virtual void Simulate();

	virtual ServerEntity *GetOwner() const;
	virtual Uint16 GetOwnerId() const { return ownerId.Get(); }
	virtual void SetOwner(ServerEntity *owner);

protected:
	virtual ~ServerEntity();

	virtual void OnInit();
	virtual void OnSpawn();
	virtual void OnRelease();

	virtual void OnTakeDamage(const DamageInfo &info);
	virtual void OnKilled(const DamageInfo &info);

private:
	void SetNetworkId(int networkId);

	void DispatchInit();

	int networkId;

	NetworkVariable<vec3> origin;
	NetworkVariable<Uint8> health;
	NetworkVariable<Uint8> healthMax;
	euler angles;
	vec3 velocity;
	NetworkVariable<Uint16> ownerId;
};
#endif
