
#include "pch.h"
#include "serverentityfactory.h"


SINGLETON_INSTANCE(ServerEntityFactory);


static struct RegisterEntityFactory
	: public function_register_template<ServerEntityFactory::CreateEntityFunc, RegisterEntityFactory>
{
} firstEntityFunction;


ServerEntityFactory::ServerEntityFactory()
{
	memset(networkEntities, 0, sizeof(networkEntities));
}

bool ServerEntityFactory::Init()
{
	ConvertFactoryListToMap(&firstEntityFunction, functions);

	int tableIndex = 0;

	for (auto itr : functions)
	{
		NetworkTableDefinition table;

		table.className = itr.first.c_str();

		ServerEntity *entity = itr.second();
		entity->RegisterNetworkMembers();

		entity->SetGlobalNetworktableIndex(tableIndex);
		tableIndex++;

		for (int i = 0; i < entity->networkTable.networkVariablesCount; i++)
		{
			table.memberNames.push_back(entity->networkTable.networkVariablesBase[i]->GetName());
		}

		delete entity;

		networkTableDefinitions.push_back(table);
	}

	ResetFreeNetworkIds();
	return true;
}

void ServerEntityFactory::Shutdown()
{
	FreeAllEntities();
}

void ServerEntityFactory::Update(float frametime)
{
	for (auto itr : activeEntities)
	{
		itr->Simulate();
	}

	ReleaseScheduledEntities();
}

void ServerEntityFactory::OnLevelShutdown()
{
	FreeAllEntities();
}

void ServerEntityFactory::RegisterEntityFactory(const char *name, CreateEntityFunc function)
{
	RegisterFactory(&firstEntityFunction, function, name);
}

ServerEntity *ServerEntityFactory::CreateServerEntity(const char *name, int networkIdOverride)
{
	auto itr = functions.find(name);

	if (itr == functions.end())
	{
		Assert(0);
		return nullptr;
	}

	int networkId = -1;

	if (networkIdOverride >= 0)
	{
		if (networkIdOverride >= NETWORKED_ENTITY_COUNT
			|| networkEntities[networkIdOverride] != nullptr)
		{
			Assert(0);
			return nullptr;
		}

		networkId = networkIdOverride;
	}
	else
	{
		networkId = GetFreeNetworkId();
	}

	if (networkId < 0)
	{
		Assert(0);
		return nullptr;
	}

	ServerEntity *newEntity = itr->second();
	newEntity->SetNetworkId(networkId);
	networkEntities[networkId] = newEntity;
	activeEntities.push_back(newEntity);

	newEntity->RegisterNetworkMembers();
	newEntity->DispatchInit();

	// abort pending destruction of previous entity
	for (auto itr = constructionEvents.begin();
		itr != constructionEvents.end();
		itr++)
	{
		if (itr->networkId == networkId)
		{
			constructionEvents.erase(itr);
			break;
		}
	}

	EntityConstructionEvent construction;
	construction.removed = false;
	construction.networkId = networkId;
	construction.networktableIndex = newEntity->GetGlobalNetworktableIndex();
	//G_StrNCpy(construction.className, name, sizeof(construction.className));
	constructionEvents.push_back(construction);

	return newEntity;
}

void ServerEntityFactory::OnEntityRemoved(ServerEntity *entity)
{
	scheduledForDeletion.insert(entity);
}

void ServerEntityFactory::OnEntityReleased(int networkId)
{
	if (networkId < 0)
	{
		return;
	}

	Assert(networkId >= 0 && networkId < NETWORKED_ENTITY_COUNT);
	Assert(networkEntities[networkId] != nullptr);

	Assert(VECTOR_CONTAINS(activeEntities, networkEntities[networkId]));
	VECTOR_REMOVE(activeEntities, networkEntities[networkId]);

	networkEntities[networkId] = nullptr;

	// If it's not an ID reserved to a player, push it back onto the stack
	if(networkId >= MAX_PLAYERS)
	{
		freeNetworkIndices.push_back(networkId);
	}

	// abort pending construction
	for (auto itr = constructionEvents.begin();
		itr != constructionEvents.end();
		itr++)
	{
		if (itr->networkId == networkId)
		{
			constructionEvents.erase(itr);
			break;
		}
	}

	EntityConstructionEvent construction;
	construction.removed = true;
	construction.networkId = networkId;
	construction.networktableIndex = 0xFF;
	//*construction.className = 0;
	constructionEvents.push_back(construction);
}

void ServerEntityFactory::ReleaseScheduledEntities()
{
	if (!scheduledForDeletion.empty())
	{
		for (auto itr : scheduledForDeletion)
		{
			Assert(VECTOR_CONTAINS(activeEntities, itr));

			itr->Release();

			Assert(!VECTOR_CONTAINS(activeEntities, itr));
		}

		scheduledForDeletion.clear();
	}
}

void ServerEntityFactory::ResetFreeNetworkIds()
{
	freeNetworkIndices.clear();
	freeNetworkIndices.reserve(NETWORKED_ENTITY_COUNT);

	for (int i = NETWORKED_ENTITY_COUNT - 1; i >= MAX_PLAYERS; i--)
	{
		freeNetworkIndices.push_back(i);
	}
}

int ServerEntityFactory::GetFreeNetworkId()
{
	if (freeNetworkIndices.empty())
	{
		return -1;
	}

	int freeId = freeNetworkIndices.back();
	freeNetworkIndices.pop_back();

	Assert(networkEntities[freeId] == nullptr);
	return freeId;
}

void ServerEntityFactory::FreeAllEntities()
{
	auto tempEntities = activeEntities;

	for (auto e : tempEntities)
	{
		e->Release();
	}

	Assert(activeEntities.empty());

	memset(networkEntities, 0, sizeof(networkEntities));

	ResetFreeNetworkIds();
}

bool ServerEntityFactory::WriteEntityConstruction(ByteBuffer &buffer)
{
	if (constructionEvents.empty())
	{
		return false;
	}

	buffer.WriteUShort(constructionEvents.size());

	for (const auto &e : constructionEvents)
	{
		buffer.WriteUShort(e.networkId);
		buffer.WriteBool(!e.removed);

		if (!e.removed)
		{
			//Assert(G_StrLen(e.className) > 0);
			Assert(e.networktableIndex != 0xFF);

			//buffer.WriteString(e.className);
			buffer.WriteUChar(e.networktableIndex);

			Assert(networkEntities[e.networkId] != nullptr);

			// initial world state
			ServerEntity *entity = networkEntities[e.networkId];
			for (int i = 0; i < entity->networkTable.networkVariablesCount; i++)
			{
				Assert(entity->networkTable.networkVariablesBase != nullptr);

				entity->networkTable.networkVariablesBase[i]->GetChanges(buffer);
			}
		}
	}

	constructionEvents.clear();
	return true;
}

bool ServerEntityFactory::WriteEntityWorldStateUpdate(ByteBuffer &buffer)
{
	bool wroteAnything = false;

	// only send every third entity each packet
	const int recordGate = globals->GetFrameCount() % 3;

	for (auto &e : activeEntities)
	{
		Assert(e->GetNetworkId() >= 0);

		if (e->GetNetworkId() % 3 != recordGate)
		{
			continue;
		}

		bool recordEntity = false;

		for (int i = 0; i < e->networkTable.networkVariablesCount; i++)
		{
			INetworkMember *member = e->networkTable.networkVariablesBase[i];

			if (member->HasChanged())
			{
				if (!wroteAnything)
				{
					wroteAnything = true;

					buffer.WriteInt(globals->GetFrameCount());
				}

				if (!recordEntity)
				{
					buffer.WriteUShort(e->GetNetworkId());
					buffer.WriteUChar(e->GetGlobalNetworktableIndex());

					recordEntity = true;
				}

				buffer.WriteUChar(member->GetIndex());
				member->GetChanges(buffer);
			}
		}

		if (recordEntity)
		{
			buffer.WriteUChar(0xFF);
		}
	}

	if (wroteAnything)
	{
		buffer.WriteUShort(0xFFFF);
	}

	return wroteAnything;
}

void ServerEntityFactory::WriteNetworkSymbolDefinitions(ByteBuffer &buffer)
{
	int tableIndex = 0;

	for (auto itr : networkTableDefinitions)
	{
		buffer.WriteBool(true);

		buffer.WriteString(itr.className.c_str());

		Assert(itr.memberNames.size() <= 255);

		buffer.WriteUChar(itr.memberNames.size());

		for (auto m : itr.memberNames)
		{
			buffer.WriteString(m.c_str());
		}
	}

	buffer.WriteBool(false);
}

void ServerEntityFactory::WriteEntityWorldStateComplete(ByteBuffer &buffer)
{
	for (auto &e : activeEntities)
	{
		Assert(e->GetNetworkId() >= 0);

		buffer.WriteUShort(e->GetNetworkId());
		buffer.WriteUChar(e->GetGlobalNetworktableIndex());

		for (int i = 0; i < e->networkTable.networkVariablesCount; i++)
		{
			INetworkMember *member = e->networkTable.networkVariablesBase[i];

			member->GetChanges(buffer);
		}
	}

	buffer.WriteUShort(0xFFFF);
}