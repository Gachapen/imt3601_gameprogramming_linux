
#include "pch.h"
#include "serverentityfactory.h"
#include "towerbase.h"
#include "creepbase.h"
#include "game.h"

REGISTER_SERVERENTITY_CLASS(tower, TowerBase);

IMPLEMENT_NETWORKTABLE_BASE(TowerBase)
	NETWORK_VARIABLE(towerType);
END_NETWORKTABLE();

TowerBase::TowerBase()
	: physicsObject(nullptr)
	, tilePosition(0)
	, towerScript(nullptr)
	, fireDelay(0.0f)
{
}

TowerBase::~TowerBase()
{
	Assert(physicsObject == nullptr);
}

void TowerBase::SetTileIndex(int index)
{
	tilePosition = index;
}

int TowerBase::GetTileIndex() const
{
	return tilePosition;
}

void TowerBase::SetTowerType(int type)
{
	towerType.Set(type);

	Assert(VECTOR_IS_INDEX_VALID(Resources::GetInstance()->GetTowers(), type));

	towerScript = &Resources::GetInstance()->GetTowers()[type];
	fireDelay = towerScript->fireDelay;
}

void TowerBase::OnSpawn()
{
	BaseClass::OnSpawn();

	IWorld *world = engine->GetWorld();

	if (world != nullptr)
	{
		int tileIndex = world->GetTileIndexAtPosition(GetOrigin());

		if (tileIndex >= 0)
		{
			const WorldTile &tile = world->GetTile(tileIndex);

			if ((tile.flags & TileFlags::WALKABLE) != 0)
			{
				physicsObject = physics->CreateRigidRect(GetOrigin(), vec3(WORLD_TILE_SCALE * 0.5f, WORLD_TILE_SCALE * 0.5f, 0.0f));
			}
		}
	}

	Assert(towerScript != nullptr);
}

void TowerBase::OnRelease()
{
	if (physicsObject != nullptr)
	{
		physics->DestroyObject(physicsObject);
		physicsObject = nullptr;
	}

	Game::GetInstance()->UnlistTower(this);

	BaseClass::OnRelease();
}

void TowerBase::SetOrigin(const vec3 &origin)
{
	BaseClass::SetOrigin(origin);
}

void TowerBase::Simulate()
{
	BaseClass::Simulate();

	UpdateFire();
}

PhysicsBody TowerBase::GetPhysicsObject()
{
	return physicsObject;
}

void TowerBase::UpdateFire()
{
	fireDelay -= globals->GetFrametime();

	if (fireDelay > 0.0f)
	{
		return;
	}

	CreepBase *creep = GetCreepTarget();

	if (creep == nullptr)
	{
		return;
	}

	fireDelay = towerScript->fireDelay;

	DamageInfo info;
	info.damage = towerScript->damage;
	info.origin = GetOrigin() + vec3(0, 0, 128.0f);
	info.inflictorNetworkId = GetOwnerId();
	creep->ApplyDamage(info);
}

CreepBase *TowerBase::GetCreepTarget()
{
	const float maxDistanceSquared = towerScript->range * towerScript->range;

	float bestRemainingDist = FLT_MAX;
	CreepBase *bestCreep = nullptr;

	for (auto entity : ServerEntityFactory::GetInstance()->GetEntities())
	{
		CreepBase *creep = dynamic_cast<CreepBase *>(entity);

		if (creep == nullptr)
		{
			continue;
		}

		if (creep->GetHealth() <= 0)
		{
			continue;
		}

		const float distanceSquared = (creep->GetOrigin() - GetOrigin()).squaredNorm();

		if (distanceSquared > maxDistanceSquared)
		{
			continue;
		}

		float remainingDist = creep->GetRemainingDistance();

		if (remainingDist >= bestRemainingDist)
		{
			continue;
		}

		bestRemainingDist = remainingDist;
		bestCreep = creep;
	}

	return bestCreep;
}