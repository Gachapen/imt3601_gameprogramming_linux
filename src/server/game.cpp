
#include "pch.h"
#include "game.h"
#include "server.h"
#include "towerbase.h"
#include "player.h"
#include "creepbase.h"

SINGLETON_INSTANCE(Game);

static int creepCount = 0;

CON_COMMAND(dbg_toggledeathmap)
{
	Game::GetInstance()->ToggleDrawDeathMap();
}


Game::Game()
	: CommandCreateTowerCallback(this, &Game::CommandCreateTower, "tower_create", CvarExecutionTypes::CLIENTCOMMAND)
	, CommandSellTowerCallback(this, &Game::CommandSellTower, "tower_sell", CvarExecutionTypes::CLIENTCOMMAND)
	, CommandUpgradeTowerCallback(this, &Game::CommandUpgradeTower, "tower_upgrade", CvarExecutionTypes::CLIENTCOMMAND)
	, drawDeathMap(false)
	, callbackEvent(this, &Game::OnEvent)
	, deathDecayTime(0.0f)
	, currentWaveDelay(0.0f)
{
}

Game::~Game()
{
}

bool Game::Init()
{
	events->AddListener("creep_died", &callbackEvent);

	return true;
}

void Game::Update(float frametime)
{
	if (engine->IsInToolsMode())
	{
		return;
	}

	// TODO: remove test code
	IWorld *world = engine->GetWorld();

	if (world == nullptr)
	{
		Assert(0);
		return;
	}

	// TODO remove test waves
	if (waveTestData.empty()
		&& !startTiles.empty())
	{
		auto PushCreeps = [&](vec3 color, int count, float duration, int health,
			float scale, float speed, int gold, float delay, bool incrementDelay) {
			WaveTestData data;
			data.color = color;
			data.count = count;
			data.duration = duration;
			data.health = health;
			data.scale = scale;
			data.speed = speed;
			data.starttime = currentWaveDelay + delay;
			data.gold = gold;

			if (incrementDelay)
				currentWaveDelay = data.starttime + duration;

			waveTestData.push_back(data);
		};

		const float delay = 8.0f;

		PushCreeps(vec3(1.0f, 0.2f, 0.1f), 10, 20.0f, 5, 0.8f, 400.0f, 10, delay, true);
		PushCreeps(vec3(1.0f, 0.45f, 0.1f), 15, 20.0f, 20, 1.0f, 450.0f, 10, delay, true);
		PushCreeps(vec3(0.0f, 0.2f, 0.8f), 20, 10.0f, 15, 0.8f, 650.0f, 5, delay, true);
		PushCreeps(vec3(1.0f, 0.0f, 0.05f), 10, 8.0f, 60, 1.3f, 250.0f, 25, delay, true);
		PushCreeps(vec3(0.2f, 0.8f, 0.3f), 15, 10.0f, 40, 1.15f, 350.0f, 15, delay, true);
	}

	for (auto itr = waveTestData.begin();
		itr != waveTestData.end(); )
	{
		float time = globals->GetTime();

		if (time < itr->starttime)
		{
			itr++;
			continue;
		}

		if (itr->spawnTimer > 0.0f)
		{
			itr->spawnTimer -= globals->GetFrametime();
			itr++;
			continue;
		}

		if (itr->spawnedCount >= itr->count)
		{
			itr = waveTestData.erase(itr);
			continue;
		}

		itr->spawnTimer = itr->duration / itr->count;

		const WorldTile *tile = startTiles[randRange(0, startTiles.size() - 1)];

		CreepBase *creep = assert_cast<CreepBase*>(ServerEntityFactory::GetInstance()->CreateServerEntity("creep"));

		vec3 tilePosition(vec3::Zero());
		tilePosition.x() = tile->y * WORLD_TILE_SCALE;
		tilePosition.y() = tile->x * WORLD_TILE_SCALE;

		tilePosition.x() += randf() * WORLD_TILE_SCALE;
		tilePosition.y() += randf() * WORLD_TILE_SCALE;

		creep->SetScale(itr->scale);
		creep->SetColor(itr->color);
		creep->SetSpeed(itr->speed);
		creep->SetHealthMax(itr->health);
		creep->SetGold(itr->gold);

		creep->SetOrigin(tilePosition);
		creep->DispatchSpawn();

		itr->spawnedCount++;
	}

	if (drawDeathMap)
	{
		DrawDeathMap();
	}

	if (deathDecayTime > 0.0f)
	{
		deathDecayTime -= frametime;
	}
	else
	{
		deathDecayTime += 0.5f;

		pathFinder.DecayDeathCost(0.7f);
	}
}

void Game::OnLevelInit()
{
	IWorld *world = engine->GetWorld();

	Assert(world);

	for (int i = 0; i < world->GetTileCount(); i++)
	{
		const WorldTile &tile = world->GetTile(i);

		if ((tile.flags & TileFlags::SPAWN) != 0)
		{
			startTiles.push_back(&tile);
		}
		else if ((tile.flags & TileFlags::END) != 0)
		{
			endTiles.push_back(&tile);
		}
	}

	pathFinder.Init(world);

	currentWaveDelay = globals->GetTime();
	waveTestData.clear();
}

void Game::OnLevelShutdown()
{
	towers.clear();

	startTiles.clear();
	endTiles.clear();
}

bool Game::ConstructPath(int tileStart, int tileGoal, std::vector<vec3> &points)
{
	const PathFinder::AStarNode *node = pathFinder.ConstructPath(tileStart, tileGoal);

	if (node != nullptr)
	{
		IWorld *world = engine->GetWorld();

		const vec3 offset = vec3(WORLD_TILE_SCALE * 0.5f, WORLD_TILE_SCALE * 0.5f, 0.0f);
		points.push_back(world->GetTilePosition(node->index) + offset);

		const PathFinder::AStarNode *lastCorner = node;

		node = node->previous;

		while (node)
		{
			PathFinder::AStarNode *previous = node->previous;

			if (previous == nullptr
				|| previous->x != lastCorner->x
				&& previous->y != lastCorner->y)
			{
				lastCorner = node;

				points.push_back(world->GetTilePosition(node->index) + offset);
			}

			node = previous;
		}

		std::reverse(points.begin(), points.end());
		return true;
	}

	return false;
}

void Game::UnlistTower(TowerBase *tower)
{
	// Make sure the pathFinding tile it holds is unblocked
	pathFinder.SetTileBlocked(tower->GetTileIndex(), false);

	// Remove it from the vector.
	if (VECTOR_CONTAINS(towers, tower))
	{
		VECTOR_REMOVE(towers, tower);
	}
}

void Game::CommandCreateTower(ICommandArgs *args)
{
	IWorld *world = engine->GetWorld();

	if (world == nullptr)
	{
		return;
	}

	if (args->GetArgCount() < 3)
	{
		Assert(0);
		return;
	}

	int clientIndex = Server::GetInstance()->GetClientCommandHost();

	Assert(clientIndex >= 0 && clientIndex < MAX_PLAYERS);

	Player *playerEntity = dynamic_cast<Player*>(ServerEntityFactory::GetInstance()->GetNetworkEntityByIndex(clientIndex));

	if (playerEntity == nullptr)
	{
		Assert(0);
		return;
	}

	int tileX = atoi(args->GetArg(0));
	int tileY = atoi(args->GetArg(1));

	if (tileX < 0 || tileX >= world->GetWidth()
		|| tileY < 0 || tileY >= world->GetHeight())
	{
		return;
	}

	int tileIndex = tileY * world->GetWidth() + tileX;

	// abort if there are any creeps on that tile
	if (pathFinder.GetNodeCreepCount(tileIndex) > 0)
	{
		return;
	}

	// abort if there's already a tower
	for (auto itr : towers)
	{
		if (itr->GetTileIndex() == tileIndex)
		{
			return;
		}
	}

	// abort if we would be fully blocking this tile
	Assert(!pathFinder.IsTileBlocked(tileIndex));
	pathFinder.SetTileBlocked(tileIndex, true);

	if (pathFinder.IsAnyPairBlocked())
	{
		pathFinder.SetTileBlocked(tileIndex, false);
		return;
	}

	int towerType = atoi(args->GetArg(2));

	if (!VECTOR_IS_INDEX_VALID(Resources::GetInstance()->GetTowers(), towerType))
	{
		return;
	}

	const ResourceTower &towerInfo = Resources::GetInstance()->GetTowers()[towerType];

	if (playerEntity->GetGold() < towerInfo.cost)
	{
		return;
	}

	DBGMSGF("Client at game index %i builds a tower at %i %i of type %i\n",
		clientIndex, tileX, tileY, towerType);

	TowerBase *tower = assert_cast<TowerBase*>(ServerEntityFactory::GetInstance()->CreateServerEntity("tower"));

	towers.push_back(tower);

	vec3 origin(WORLD_TILE_SCALE * tileY + WORLD_TILE_SCALE * 0.5f,
		WORLD_TILE_SCALE * tileX + WORLD_TILE_SCALE * 0.5f, 0.0f);
	origin.z() = world->GetElevationHeight(origin, true);

	tower->SetTowerType(towerType);
	tower->SetTileIndex(tileIndex);
	tower->SetOrigin(origin);
	tower->SetOwner(playerEntity);
	tower->DispatchSpawn();


	KeyValues *towerEvent = new KeyValues("tower_created");
	towerEvent->SetInt("x", tileX);
	towerEvent->SetInt("y", tileY);

	events->FireEvent(towerEvent);

	playerEntity->RemoveGold(towerInfo.cost);
}

void Game::CommandSellTower(ICommandArgs *args)
{
	IWorld *world = engine->GetWorld();

	if (world == nullptr)
	{
		return;
	}

	if (args->GetArgCount() < 1)
	{
		Assert(0);
		return;
	}

	int clientIndex = Server::GetInstance()->GetClientCommandHost();
	const int towerIndex = atoi(args->GetArg(0));

	Assert(clientIndex >= 0 && clientIndex < MAX_PLAYERS);

	Player *player = dynamic_cast<Player*>(ServerEntityFactory::GetInstance()->GetNetworkEntityByIndex(clientIndex));
	TowerBase *tower = dynamic_cast<TowerBase*>(ServerEntityFactory::GetInstance()->GetNetworkEntityByIndex(towerIndex));

	if (player == nullptr
		|| tower == nullptr)
	{
		Assert(0);
		return;
	}

	if (tower->GetOwnerId() != clientIndex)
	{
		Assert(0);
		return;
	}

	player->AddGold(Uint16(tower->GetTowerScript()->cost * 0.4f));
	tower->Remove();
}

void Game::CommandUpgradeTower(ICommandArgs *args)
{
	IWorld *world = engine->GetWorld();

	if (world == nullptr)
	{
		return;
	}

	if (args->GetArgCount() < 2)
	{
		Assert(0);
		return;
	}

	int clientIndex = Server::GetInstance()->GetClientCommandHost();
	const int towerIndex = atoi(args->GetArg(0));
	const int upgradeId = atoi(args->GetArg(1));

	Assert(clientIndex >= 0 && clientIndex < MAX_PLAYERS);

	Player *player = dynamic_cast<Player*>(ServerEntityFactory::GetInstance()->GetNetworkEntityByIndex(clientIndex));
	TowerBase *tower = dynamic_cast<TowerBase*>(ServerEntityFactory::GetInstance()->GetNetworkEntityByIndex(towerIndex));

	if (player == nullptr
		|| tower == nullptr)
	{
		Assert(0);
		return;
	}

	for (auto &u : tower->GetTowerScript()->upgrades)
	{
		if (u.id == upgradeId)
		{
			if (u.cost <= player->GetGold())
			{
				tower->SetTowerType(upgradeId);

				player->RemoveGold(u.cost);
			}

			break;
		}
	}
}

void Game::ModifyNodeCreepCount(int tileIndex, int delta)
{
	pathFinder.ModifyNodeCreepCount(tileIndex, delta);
}

void Game::OnEvent(KeyValues *data)
{
	const char *name = data->GetName();

	if (G_StrEq("creep_died", name))
	{
		int tileIndex = data->GetInt("tileindex");
		int attackerId = data->GetInt("playerkillerid");

		Player *attacker = dynamic_cast<Player*>(ServerEntityFactory::GetInstance()->GetNetworkEntityByIndex(attackerId));

		if (tileIndex >= 0
			&& tileIndex < pathFinder.GetNodeCount())
		{
			pathFinder.ModifyNodeDeathCost(tileIndex, 3, 3.5f);
		}

		if (attacker != nullptr)
		{
			attacker->AddGold(data->GetInt("gold"));
		}
	}
}

void Game::ToggleDrawDeathMap()
{
	drawDeathMap = !drawDeathMap;
}

void Game::DrawDeathMap()
{
	IWorld *world = engine->GetWorld();

	if (world == nullptr)
	{
		return;
	}

	for (int i = 0; i < pathFinder.GetNodeCount(); i++)
	{
		float deathCost = pathFinder.GetNodeDeathCost(i);

		if (deathCost > 0.0f)
		{
			vec4 heights;
			world->GetElevationHeights(i, heights, true);

			vec3 vertices[4];

			vertices[0] = world->GetTilePosition(i);
			vertices[0].x() += WORLD_TILE_SCALE;

			vertices[1] = vertices[0];
			vertices[1].y() += WORLD_TILE_SCALE;

			vertices[2] = vertices[1];
			vertices[2].x() -= WORLD_TILE_SCALE;

			vertices[3] = vertices[2];
			vertices[3].y() -= WORLD_TILE_SCALE;

			vertices[0].z() = heights[1] + 5.0f;
			vertices[1].z() = heights[2] + 5.0f;
			vertices[2].z() = heights[3] + 5.0f;
			vertices[3].z() = heights[0] + 5.0f;

			debugDraw->DrawTriangleFan(vertices, 4, vec3(1, 0, 0), deathCost * 0.1f, SIMULATION_FRAMESPEED);
		}
	}
}