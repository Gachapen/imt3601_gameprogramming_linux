#include "pch.h"
#include "graphics.h"

SINGLETON_INSTANCE(Graphics);

EXPOSE_APP(INTERFACE_IGRAPHICS_VERSION, Graphics::GetInstance());


Graphics::Graphics()
	: currentColor(vec4::Ones())
	  , flatMaterial(nullptr)
	  , viewportDepth(0)
	  , currentLineWidth(1.0f)
{
}

Graphics::~Graphics()
{
}

void Graphics::Init()
{
	Assert(flatMaterial == nullptr);

	flatMaterial = materialDict->FindMaterial("gui/flat");
	flatMaterial->IncrementReferenceCount();
}

void Graphics::Shutdown()
{
	Assert(flatMaterial != nullptr);

	flatMaterial->DecrementReferenceCount();
	flatMaterial = nullptr;
}

int Graphics::GetValueScaled(int value)
{
	return VALUE_SCALED(value, renderContext->GetScreenHeight());
}

int Graphics::GetValueNormalized(int value)
{
	return VALUE_NORMALIZED(value, renderContext->GetScreenHeight());
}

float Graphics::GetFrametime()
{
	return globals->GetFrametime();
}

float Graphics::GetTime()
{
	return globals->GetTime();
}

void Graphics::Push2DViewport(int x, int y, int w, int h)
{
	// the ogl viewport originates at the lower left corner
	y = -y + renderContext->GetScreenHeight() - h;

	viewportDepth++;
	renderContext->PushViewport(x, y, w, h);

	mat4 screenMatrix = ScreenMatrix(w, h);

	renderContext->SetWorldCoordinatesEnabled(false);
	renderContext->PushViewMatrix(screenMatrix);
}

void Graphics::Pop2DViewport()
{
	renderContext->PopViewport();
	viewportDepth--;

	Assert(viewportDepth >= 0);

	if (viewportDepth == 0)
	{
		renderContext->SetWorldCoordinatesEnabled(true);
	}

	renderContext->PopViewMatrix();
}

void Graphics::SetDrawColor(float r, float g, float b, float a)
{
	currentColor = vec4(r, g, b, a);
}

void Graphics::SetDrawLineWidth(float width)
{
	currentLineWidth = width;
}

void Graphics::DrawFilledRect(int x0, int y0, int x1, int y1)
{
	IMesh *mesh = renderContext->GetDynamicMesh(FaceTypes::TRIANGLES,
												VertexFormat::POSITION_3F | VertexFormat::COLOR_4F, 2);

	MeshBuilder builder;

	builder.AttachModify(mesh);

	builder.Position3f((float)x0, (float)y0, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();
	builder.Position3f((float)x0, (float)y1, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();
	builder.Position3f((float)x1, (float)y0, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();

	builder.Position3f((float)x1, (float)y0, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();
	builder.Position3f((float)x0, (float)y1, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();
	builder.Position3f((float)x1, (float)y1, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();

	builder.Detach();

	renderContext->BindMesh(mesh);
	flatMaterial->Draw();
}

void Graphics::DrawTexturedRect(int x0, int y0, int x1, int y1, IMaterial *mat)
{
	IMesh *mesh = renderContext->GetDynamicMesh(FaceTypes::TRIANGLES,
												VertexFormat::POSITION_3F | VertexFormat::TEXTURE_COORD_2F, 2);

	MeshBuilder builder;

	builder.AttachModify(mesh);

	builder.Position3f((float)x0, (float)y0, 0.0f);
	builder.Texcoord2f(0, 0);
	builder.AdvanceVertex();
	builder.Position3f((float)x0, (float)y1, 0.0f);
	builder.Texcoord2f(0, 1);
	builder.AdvanceVertex();
	builder.Position3f((float)x1, (float)y0, 0.0f);
	builder.Texcoord2f(1, 0);
	builder.AdvanceVertex();

	builder.Position3f((float)x1, (float)y0, 0.0f);
	builder.Texcoord2f(1, 0);
	builder.AdvanceVertex();
	builder.Position3f((float)x0, (float)y1, 0.0f);
	builder.Texcoord2f(0, 1);
	builder.AdvanceVertex();
	builder.Position3f((float)x1, (float)y1, 0.0f);
	builder.Texcoord2f(1, 1);
	builder.AdvanceVertex();

	builder.Detach();

	renderContext->BindMesh(mesh);
	
	mat->Draw();
}

void Graphics::DrawRect(int x0, int y0, int x1, int y1)
{
	IMesh *mesh = renderContext->GetDynamicMesh(FaceTypes::LINE_LOOP,
												VertexFormat::POSITION_3F | VertexFormat::COLOR_4F, 4);

	MeshBuilder builder;

	builder.AttachModify(mesh);

	builder.Position3f((float)x0 + 1, (float)y0, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();

	builder.Position3f((float)x1, (float)y0, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();

	builder.Position3f((float)x1, (float)y1 - 1, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();

	builder.Position3f((float)x0, (float)y1 - 1, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();

	builder.Detach();

	renderContext->PushLineWidth(currentLineWidth);

	renderContext->BindMesh(mesh);
	flatMaterial->Draw();

	renderContext->PopLineWidth();
}

void Graphics::DrawLine(int x0, int y0, int x1, int y1)
{
	IMesh *mesh = renderContext->GetDynamicMesh(FaceTypes::LINES,
												VertexFormat::POSITION_3F | VertexFormat::COLOR_4F, 1);

	MeshBuilder builder;

	builder.AttachModify(mesh);

	builder.Position3f((float)x0, (float)y0, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();
	builder.Position3f((float)x1, (float)y1, 0.0f);
	builder.Color4v(currentColor);
	builder.AdvanceVertex();

	builder.Detach();

	renderContext->PushLineWidth(currentLineWidth);

	renderContext->BindMesh(mesh);
	flatMaterial->Draw();

	renderContext->PopLineWidth();
}
