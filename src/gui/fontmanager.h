#ifndef FONTMANAGER_H
#define FONTMANAGER_H

#include "gui/ifontmanager.h"

#define FIRST_PRINTABLE_CHARACTER 32
#define CHARACTER_MAP_LINE_COUNT 10
#define FONT_ENTRY_INVALID 0xFFFFFFFF

class PRE_ALIGN_16 FontManager : public IFontManager
{
	DECLARE_SINGLETON(FontManager);

public:
	~FontManager();

	void Init();
	void Shutdown();

	void OnResize();

	void LoadFontScript(const char *filename);
	GUI::FONT_KEY LoadFont(const char *filename, int tall);

	// IFontManager
	virtual GUI::FONT_KEY GetFont(const char *name);

	virtual void SetDrawTextFont(GUI::FONT_KEY fontKey);
	virtual void SetDrawTextColor(float r, float g, float b, float a);
	virtual void SetDrawTextPos(int x, int y);
	virtual void SetDrawTextScale(float scaleWidth, float scaleHeight);

	virtual int GetTextWidth(const char *text);

	virtual void DrawTextString(const char *text);

private:

	void ReleaseAll();
	void ReleaseFontMaps();
	void CreateFontMaps();

	void BlitFontMapLine(void *dest, void *src,
		SDL_Rect &destDimensions, SDL_Rect &srcDimensions, int yPosition);

	struct CharacterPosition
	{
		unsigned short x, w, line;
	};

	struct LinePosition
	{
		unsigned short y, h;
	};

	// loaded font maps
	struct FontEntry
	{
		char *name;
		int tall;

		ITexture *texture;

		CharacterPosition characters[255 - FIRST_PRINTABLE_CHARACTER + 1];
		LinePosition lines[CHARACTER_MAP_LINE_COUNT];
	};

	// loaded font scripts
	struct ScriptEntry
	{
		// index into font maps
		Uint32 fontEntry;

		char *name;
		char *filename;
		bool relative;
		int tall;
	};

	std::vector<FontEntry> fonts;
	std::vector<ScriptEntry> fontScripts;

	IMaterial *fontMaterial;

	GUI::FONT_KEY currentFont;
	int currentPositionX;
	int currentPositionY;
	vec4 currentColor;
	vec2 currentScale;
} POST_ALIGN_16;

#endif