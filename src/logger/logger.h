#ifndef LOGGER_H
#define LOGGER_H

#include "logger/iLogger.h"
#include "util/Singleton.h"

class Logger : public ILogger
{
private:
	DECLARE_SINGLETON(Logger);

protected:
	~Logger() {}

public:
	virtual void Log(const char *message, LogType type); // __LINE__ __FILE__
	virtual void Log(const char *message, LogType type, const char *file, int line);

};

#endif
