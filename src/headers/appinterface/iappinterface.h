#ifndef IAPPINTERFACE_H
#define IAPPINTERFACE_H


#ifdef PLATFORM_WIN32

#ifdef IAPPINTERFACE_EXPORTS
#define APPINTERFACE_API	__declspec(dllexport)
#else
#define APPINTERFACE_API	__declspec(dllimport)
#endif

#else

#define APPINTERFACE_API

#endif


/// Publicates a pointer under a specific identifier to the app system
#define EXPOSE_APP(identifier, handle)							\
	static class _exposeInterface ## identifier					\
	{															\
public:															\
		_exposeInterface ## identifier()						\
		{														\
			GetAppInterface()->RegisterApp(identifier, handle);	\
		}														\
	}															\
	_exposeInterface ## identifier ## _instance;

/// Interface to manage apps (e.g. singletons for specific systems)
/// across the whole application
class IAppInterface
{
protected:
	virtual ~IAppInterface() {}

public:
	/// Globally load a shared library
	virtual bool LoadSharedLibrary(const char *libraryName) = 0;
	/// Unload all libraries and remove all apps
	virtual void Shutdown() = 0;

	/// Publicate an app under a specific identifier
	virtual void RegisterApp(const char *identifier, void *target) = 0;
	/// Search for an app based on an identifier
	virtual void *QueryApp(const char *identifier) = 0;
};

#ifndef IAPPINTERFACE_EXPORTS
/// Access singleton of app interface
APPINTERFACE_API IAppInterface *GetAppInterface();
#endif
#endif
