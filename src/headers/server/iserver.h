#ifndef ISERVER_H
#define ISERVER_H

/// Exposed from server to engine
class IServer
{
protected:
	virtual ~IServer() {}

public:
	virtual bool Init() = 0;
	virtual void Shutdown() = 0;

	virtual void Update() = 0;

	virtual void OnLevelInit() = 0;
	virtual void OnLevelShutdown() = 0;

	virtual void WriteNetworkSymbolDefinitions(ByteBuffer &buffer) = 0;
	virtual bool WriteEntityConstruction(ByteBuffer &buffer) = 0;
	virtual bool WriteEntityWorldStateUpdate(ByteBuffer &buffer) = 0;
	virtual void WriteEntityWorldStateComplete(ByteBuffer &buffer) = 0;

	virtual void SetClientCommandHost(int clientIndex) = 0;

	virtual void OnClientConnected(int clientIndex) = 0;
	virtual void OnClientDisconnected(int clientIndex) = 0;
};

#define INTERFACE_ISERVER_VERSION "iserver001"

#endif