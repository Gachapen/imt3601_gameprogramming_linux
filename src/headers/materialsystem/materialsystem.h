#ifndef RENDER_H
#define RENDER_H

#include "util/macros.h"
#include "shareddefs.h"

/// Handle for VBOs
typedef Uint32 VBO_KEY;
#define VBO_KEY_INVALID 0xFFFFFFFF

/// Handle for FBOs
typedef Uint32 FBO_HANDLE;
#define FBO_HANDLE_INVALID 0xFFFFFFFF

/// Handle for render buffers
typedef Uint32 RB_HANDLE;
#define RB_HANDLE_INVALID 0xFFFFFFFF

/// Handle for render targets/textures
typedef Uint32 RT_HANDLE;
#define RT_HANDLE_INVALID 0xFFFFFFFF

/// Handle for material variables
typedef Uint32 MATERIAL_VAR_HANDLE;
#define MATERIAL_VAR_HANDLE_DEFAULT 0x00000000
#define MATERIAL_VAR_HANDLE_INVALID 0xFFFFFFFF

/// Max allowed color buffer count per FBO
#define MAX_MRT_BUFFERS 4

/// GLSL version used during shader compilation
#define SHADER_VERSION_STRING "#version 330\n"

/// Vertex format information
namespace VertexFormat
{
	/// Pre sorted vertex data
	enum VertexFormatIndex_e
	{
		INDEX_POSITION_2F = 0,
		INDEX_POSITION_3F,

		INDEX_TEXTURE_COORD_2F,

		INDEX_COLOR_3F,
		INDEX_COLOR_4F,
		INDEX_NORMAL_3F,
		INDEX_TANGENT_3F,
		INDEX_BINORMAL_3F,

		INDEX_BONEINDICES_3F,
	};

	/// Format bit flags
	enum VertexFormat_e
	{
		POSITION_2F = (1 << INDEX_POSITION_2F),
		POSITION_3F = (1 << INDEX_POSITION_3F),

		TEXTURE_COORD_2F = (1 << INDEX_TEXTURE_COORD_2F),

		COLOR_3F = (1 << INDEX_COLOR_3F),
		COLOR_4F = (1 << INDEX_COLOR_4F),
		NORMAL_3F = (1 << INDEX_NORMAL_3F),
		TANGENT_3F = (1 << INDEX_TANGENT_3F),
		BINORMAL_3F = (1 << INDEX_BINORMAL_3F),

		BONEINDICES_3F = (1 << INDEX_BONEINDICES_3F),

		NUM_BITS = 9,
	};

	/// Get vertex input name for component flag
	extern const char* const GetVertexElementName(int element);

	/// Get flag from component index
	inline int GetVertexElementFlag(int index)
	{
		Assert(index >= 0);
		Assert(index < NUM_BITS);

		return (1 << index);
	}

	/// Get index from component flag
	inline int GetVertexElementIndex(int element)
	{
		switch (element)
		{
		case POSITION_2F:
			return INDEX_POSITION_2F;
		case POSITION_3F:
			return INDEX_POSITION_3F;
		case TEXTURE_COORD_2F:
			return INDEX_TEXTURE_COORD_2F;
		case COLOR_3F:
			return INDEX_COLOR_3F;
		case COLOR_4F:
			return INDEX_COLOR_4F;
		case NORMAL_3F:
			return INDEX_NORMAL_3F;
		case TANGENT_3F:
			return INDEX_TANGENT_3F;
		case BINORMAL_3F:
			return INDEX_BINORMAL_3F;
		case BONEINDICES_3F:
			return INDEX_BONEINDICES_3F;
		default:
			Assert(0);
			return 0;
		}
	}

	/// Check if format contains component flag
	inline int HasElement(int format, int element)
	{
		Assert(element < (1 << NUM_BITS));

		return (format & element) != 0;
	}

	/// Get float count for single component flag
	inline int GetVertexElementStride(int element)
	{
		switch (element)
		{
		case POSITION_2F:
			return 2;
		case POSITION_3F:
			return 3;
		case TEXTURE_COORD_2F:
			return 2;
		case COLOR_3F:
			return 3;
		case COLOR_4F:
			return 4;
		case NORMAL_3F:
			return 3;
		case BINORMAL_3F:
			return 3;
		case TANGENT_3F:
			return 3;
		case BONEINDICES_3F:
			return 3;
		default:
			Assert(0);
			return 0;
		}
	}

	/// Get byte size for single component flag
	inline int GetVertexElementSize(int element)
	{
		return sizeof(float) * GetVertexElementStride(element);
	}

	/// Get float count for full vertex format
	inline int GetVertexFormatStride(int format)
	{
		int size = 0;

		for (int i = 0; i < VertexFormat::NUM_BITS; i++)
		{
			const int flag = VertexFormat::GetVertexElementFlag(i);

			if (VertexFormat::HasElement(format, flag))
			{
				size += GetVertexElementStride(flag);
			}
		}

		return size;
	}

	/// Get byte size for full vertex format
	inline int GetVertexFormatSize(int format)
	{
		return GetVertexFormatStride(format) * sizeof(float);
	}
}
typedef VertexFormat::VertexFormat_e VertexFormatId;

/// Supported face types for meshes
namespace FaceTypes
{
	enum FaceTypes_e
	{
		LINES = 0,
		//LINE_STRIP,
		LINE_LOOP,
		TRIANGLES,
		TRIANGLE_STRIP,
		TRIANGLE_FAN,
		QUADS,
		//QUAD_STRIP,

		COUNT,
	};

	/// Get face type vertex count per element
	/// for linearily incrementing types
	inline int GetFaceTypeElementSize(FaceTypes_e type)
	{
		switch (type)
		{
		case LINES:
			return 2;
		case LINE_LOOP:
			return 1;
		case TRIANGLES:
			return 3;
		case TRIANGLE_FAN:
			return 1;
		case QUADS:
			return 4;
		default:
			Assert(0);
			return 0;
		}
	}
}
typedef FaceTypes::FaceTypes_e FaceTypeId;

/// Texture groups assigned via filepath or by hand
namespace TextureGroups
{
	enum TextureGroups_e
	{
		OTHER,
		DEBUGTEXTURE,
		MODELS,
		POSTPROCESSING,
		EFFECTS,
		RENDERTARGETS,
		CUBEMAPS,
		FONTS,
		MAPS,
		GUI,

		COUNT,
		NONE = COUNT,
	};
}
typedef TextureGroups::TextureGroups_e TextureGroupId;

/// Texture types defined when loaded through shader
namespace TextureTypes
{
	enum TextureTypes_e
	{
		TEXTURE_2D = 0,
		//TEXTURE_3D,
		TEXTURE_CUBEMAP,

		INVALID,
	};
}
typedef TextureTypes::TextureTypes_e TextureTypeId;

/// Supported texture formats
/// for color and depth buffers
namespace TextureFormats
{
	enum TextureFormats_e
	{
		// integral
		RGBA_8 = 0,
		RGB_8,
		BGRA_8,
		BGR_8,

		// floating point
		RGBA_16F,
		RGB_16F,
		RGBA_32F,

		// depth formats
		DEPTH_16F,
		DEPTH_24F,

		COUNT,
		INVALID = COUNT,
	};

	/// Fixup format for variable byte count, check RGBA/BGRA and convert based on 3 or 4 bytes
	inline TextureFormats_e GetCorrectedFormat(int bytesPerPixel, TextureFormats_e format)
	{
		if (bytesPerPixel == 3 && format == TextureFormats::RGBA_8)
			return TextureFormats::RGB_8;
		else if (bytesPerPixel == 4 && format == TextureFormats::RGB_8)
			return TextureFormats::RGBA_8;
		else if (bytesPerPixel == 3 && format == TextureFormats::BGRA_8)
			return TextureFormats::BGR_8;
		else if (bytesPerPixel == 4 && format == TextureFormats::BGR_8)
			return TextureFormats::BGRA_8;

		return format;
	}
}
typedef TextureFormats::TextureFormats_e TextureFormatId;

/// Texture flags that modify e.g. texture lookup behavior
namespace TextureFlags
{
	enum TextureFlags_e
	{
		NO_MIPMAP = (1 << 0),
		NO_FILTERING = (1 << 1),
		CLAMP_S = (1 << 2),
		CLAMP_T = (1 << 3),
		CLAMP_R = (1 << 4),
	};

	inline bool HasFlag(int flags, TextureFlags_e test)
	{
		return (flags & test) != 0;
	}
}
typedef TextureFlags::TextureFlags_e TextureFlagId;

/// Types for material variables
namespace MaterialVarTypes
{
	enum MaterialVarTypes_e
	{
		INT = 0,
		FLOAT,
		VEC2,
		VEC3,
		VEC4,
		STRING,
		TEXTURE,

		COUNT,
		INVALID = COUNT,
	};

	/// Check if two types can be converted implicitely
	inline bool AreTypesMatching(MaterialVarTypes_e testType, MaterialVarTypes_e realType)
	{
		if (testType == realType)
			return true;

		if (testType == STRING && realType == TEXTURE)
			return true;

		return false;
	}
}
typedef MaterialVarTypes::MaterialVarTypes_e MaterialVarTypeId;

/// Face render modes
namespace PolygonFaceModes
{
	enum PolygonFaceModes_e
	{
		FRONT = 0,
		BACK,
		FRONT_AND_BACK,
	};
}
typedef PolygonFaceModes::PolygonFaceModes_e PolygonFaceModeId;

/// Fill render modes
namespace PolygonFillModes
{
	enum PolygonFillModes_e
	{
		FILL = 0,
		WIREFRAME,
	};
};
typedef PolygonFillModes::PolygonFillModes_e PolygonFillModeId;

/// Automated modes for frame buffer resizing
namespace FramebufferResizeModes
{
	enum FramebufferResizeModes_e
	{
		FULL_FRAMEBUFFER = 0,
		//HALF_FRAMEBUFFER,
		QUARTER_FRAMEBUFFER,
		OFFSCREEN,
	};
}
typedef FramebufferResizeModes::FramebufferResizeModes_e FramebufferResizeModeId;

/// Blend modes for alpha blended shaders
namespace BlendModes
{
	enum BlendModes_e
	{
		NONE = 0,
		SRC_ALPHA,
		ADDITIVE,
	};
}
typedef BlendModes::BlendModes_e BlendModeId;

/// Render stages to e.g. switch active pass in shader
namespace RenderStages
{
	enum RenderStages_e
	{
		NORMAL = 0,
		SHADOW,
		SELECTION,
	};
}
typedef RenderStages::RenderStages_e RenderStageId;

/// Various game specific render parameters usable in shaders
namespace RenderParams
{
	enum RenderParams_Float
	{
		FLOAT_SHADOWMAP_SIZE = 0,

		FLOAT_FOG_AMOUNT,
		FLOAT_FOG_START,
		FLOAT_FOG_RANGE,

		FLOAT_OUTLINE_WIDTH,

		FLOAT_COUNT,
	};

	enum RenderParams_Int
	{
		INT_ENTINDEX = 0,

		INT_COUNT = 10,
	};

	enum RenderParams_Vec3
	{
		VEC3_GLOBALLIGHT_DIR = 0,
		VEC3_GLOBALLIGHT_COLOR,
		VEC3_GLOBALLIGHT_AMBIENT,

		VEC3_FOG_COLOR,

		VEC3_MODEL_TINT_COLOR,

		VEC3_COUNT,
	};

	enum RenderParams_Vec4
	{
		VEC4_DIFFUSE_MODULATION = 0,

		VEC4_COUNT,
	};

	enum RenderParams_Mat4
	{
		MAT4_SHADOW_VIEWPROJECTION = 0,

		MAT4_COUNT,
	};
}
typedef RenderParams::RenderParams_Float RenderParam_Float;
typedef RenderParams::RenderParams_Int RenderParam_Int;
typedef RenderParams::RenderParams_Vec3 RenderParam_Vec3;
typedef RenderParams::RenderParams_Vec4 RenderParam_Vec4;
typedef RenderParams::RenderParams_Mat4 RenderParam_Mat4;

#endif