#ifndef IMESH_H
#define IMESH_H

/// Represents geometry that will be send to OpenGL
/// or allows you to modify it
class IMesh
{
protected:
	virtual ~IMesh() {}

public:
	virtual void Release() = 0;
	/// Destroys non-opengl cache, can only be directly modified afterwards
	virtual void FlushCache() = 0;

	/// Uploads cache to an OpenGL VBO
	virtual void Upload() = 0;
	/// Access local cache, only valid before FlushCache has been called
	virtual float *GetData() = 0;

	/// Access OpenGL cache
	virtual float *BindMap(int offset = -1, int range = -1) = 0;
	/// Call after OpenGL cache has been modified
	virtual void BindUnmap() = 0;

	virtual int GetVertexFormat() const = 0;
	virtual int GetVertexCount() const = 0;

	virtual FaceTypeId GetFaceType() const = 0;
};


#endif