#ifndef IRENDERCONTEXT_H
#define IRENDERCONTEXT_H

#include "materialsystem.h"
#include "util/eigen.h"

class IMesh;
class IMaterial;

/// Wraps most OpenGL calls into an abstract concept
class IRenderContext
{
protected:
	virtual ~IRenderContext() {}

public:
	virtual void Init(void *windowHandle, int windowWidth, int windowHeight) = 0;
	virtual void Resize(int windowWidth, int windowHeight) = 0;
	virtual void Shutdown() = 0;
	
	virtual bool IsFullscreen() = 0;
	virtual void SetFullscreen(bool isFullscreen) = 0;

	/// Call with true to assign context to calling thread.
	/// Call with false to release context.
	/// @warning Context MUST BE released before assigning it elsewhere or opengl will explode
	virtual void MakeCurrent(bool assignContext) = 0;

	/// Check if there were any OpenGL errors thus far. Should only be called
	/// once per frame or for debugging purposes
	virtual bool CheckForErrors() = 0;

	/// Set current render stage so shaders can react to it (e.g. shadow mapping mode)
	virtual void SetRenderStage(RenderStageId stage) = 0;
	virtual RenderStageId GetRenderStage() const = 0;

	virtual void SetRenderParamFloat(RenderParam_Float param, float value) = 0;
	virtual void SetRenderParamInt(RenderParam_Int param, int value) = 0;
	virtual void SetRenderParamVec3(RenderParam_Vec3 param, const vec3 &value) = 0;
	virtual void SetRenderParamVec4(RenderParam_Vec4 param, const vec4 &value) = 0;
	virtual void SetRenderParamMat4(RenderParam_Mat4 param, const mat4 &value) = 0;

	virtual float GetRenderParamFloat(RenderParam_Float param) const = 0;
	virtual int GetRenderParamInt(RenderParam_Int param) const = 0;
	virtual void GetRenderParamVec3(RenderParam_Vec3 param, vec3 &value) const = 0;
	virtual void GetRenderParamVec4(RenderParam_Vec4 param, vec4 &value) const = 0;
	virtual void GetRenderParamMat4(RenderParam_Mat4 param, mat4 &value) const = 0;

	/// Set current expected bone count
	virtual void SetBoneCount(int boneCount) = 0;
	/// Access bone indices (disabled)
	virtual int *GetBoneIndicesForModify() = 0;
	/// Set current bone indices (disabled)
	virtual void SetBoneIndices(int *boneIndices) = 0;
	/// Set pointer to bone array (mat4)
	virtual void SetBonePointer(mat4 *bones, int count) = 0;
	/// Access cached bone array for modification (deprecated)
	virtual mat4 *GetBoneTransformsForModify() = 0;

	virtual void PushModelMatrix(const mat4 &matrix) = 0;
	virtual void PushViewMatrix(const mat4 &matrix) = 0;
	virtual void PushProjectionMatrix(const mat4 &matrix) = 0;

	virtual void PopModelMatrix() = 0;
	virtual void PopViewMatrix() = 0;
	virtual void PopProjectionMatrix() = 0;

	virtual void LoadModelIdentity() = 0;
	virtual void LoadViewIdentity() = 0;
	virtual void LoadProjectionIdentity() = 0;

	/// Enable or disable world coordinate system to OpenGL conversion
	virtual void SetWorldCoordinatesEnabled(bool enabled) = 0;
	/// Update combined matrices if source data is dirty
	virtual void UpdateMatrices() = 0;

	/// Remember current matrices as main view matrices (e.g. for use in GUI)
	virtual void SetMainMatrices() = 0;

	/// Set depth range used in projection (shaders can use these to remap depth)
	virtual void SetDepthRange(float zNear, float zFar) = 0;

	virtual float GetZNear() const = 0;
	virtual float GetZFar() const = 0;
	/// Get current depth range (far - near)
	virtual float GetZRange() const = 0;

	virtual void PushLineWidth(float width) = 0;
	virtual void PopLineWidth() = 0;

	// currently pushed matrices which are used during rendering
	virtual const mat4 &GetModelViewProjectionMatrix() const = 0;
	virtual const mat4 &GetViewProjectionMatrix() const = 0;
	virtual const mat4 &GetModelMatrix() const = 0;
	virtual const mat4 &GetViewMatrix() const = 0;
	virtual const mat4 &GetProjectionMatrix() const = 0;
	
	// same as above but include translation into OGL space
	virtual const mat4 &GetOGLViewProjectionMatrix() const = 0;
	virtual const mat4 &GetOGLModelViewProjectionMatrix() const = 0;

	// the last world view matrix that the client publicated
	virtual const mat4 &GetMainViewProjectionMatrix() const = 0;

	virtual int GetScreenWidth() const = 0;
	virtual int GetScreenHeight() const = 0;

	virtual void PushViewport(int x = -1, int y = -1, int w = -1, int h = -1) = 0;
	virtual void SetViewPort(int x = -1, int y = -1, int w = -1, int h = -1) = 0;
	virtual void PopViewport() = 0;

	virtual void SetClearColor(float r, float g, float b, float a = 1.0f) = 0;
	virtual void ClearBuffers(bool color, bool depth) = 0;

	virtual void SetDepthTestEnabled(bool enabled) = 0;
	virtual void SetClipPlaneEnabled(int index, bool enabled) = 0;

	virtual void SwapBuffers() = 0;

	/// Create a static mesh. Should not be modified each frame. Should not be created/released each frame.
	virtual IMesh *CreateStaticMesh(FaceTypeId type, int vertexFormat, int faceCount) = 0;
	/// Use a dynamic mesh. Should be build each frame
	/// @warning DO NOT CACHE IT
	/// @warning DO NOT RELEASE IT
	virtual IMesh *GetDynamicMesh(FaceTypeId type, int vertexFormat, int faceCount) = 0;

	/// Bind mesh for rendering
	virtual void BindMesh(const IMesh *mesh) = 0;
	virtual const IMesh *GetActiveMesh() const = 0;

	/// Override material of e.g. models
	virtual void SetMaterialOverride(IMaterial *material) = 0;

	virtual void DrawFullscreenQuad(IMaterial *material) = 0;

	virtual void SetShaderTime(float time) = 0;
	virtual float GetShaderTime() const = 0;

	virtual void TakeScreenshot(const char *file) = 0;
};

#define INTERFACE_IRENDERCONTEXT_VERSION "irendercontext001"

#endif
