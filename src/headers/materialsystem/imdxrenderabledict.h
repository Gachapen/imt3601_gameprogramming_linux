#ifndef IMDXRENDERABLEDICT_H
#define IMDXRENDERABLEDICT_H

#include "imdxrenderable.h"

/// Manages shared model resources and allows the creation of a new model instance
class IMDXRenderableDict
{
protected:
	virtual ~IMDXRenderableDict() {}

public:
	/// Create a new instance for a model
	virtual IMDXRenderable *CreateMDXRenderableInstance(const char *path) = 0;

};

#define INTERFACE_IMDXRENDERABLEDICT_VERSION "imdxrenderable001"


#endif