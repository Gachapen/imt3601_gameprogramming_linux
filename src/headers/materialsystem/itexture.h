#ifndef ITEXTURE_H
#define ITEXTURE_H

#include "materialsystem\materialsystem.h"

/// A single texture
class ITexture
{
protected:
	virtual ~ITexture() {}

public:
	virtual const char *GetTextureName() const = 0;

	virtual int GetWidth() const = 0;
	virtual int GetHeight() const = 0;

	virtual TextureFormatId GetFormat() const = 0;
	virtual TextureTypeId GetType() const = 0;
	virtual TextureGroupId GetGroup() const = 0;

	// Reference counting
	virtual void IncrementReferenceCount() = 0;
	virtual void DecrementReferenceCount() = 0;

	/// Overwrite the texture contents at runtime
	virtual void SetContent(TextureFormatId format, int width, int height, void *data) = 0;
};


#endif