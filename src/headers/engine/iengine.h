#ifndef IENGINE_H
#define IENGINE_H


/// Interface used by launchers/editor to run the engine
class IEngine
{
protected:
	virtual ~IEngine() {}

public:

	enum InitializationMode
	{
		/// normal mode, run autonomously, use opengl/show gui
		INIT_GAME = 0,

		// dedicated server, don't load client/matsys lib, console output only
		//INIT_SERVER,

		/// load client and server, use opengl, no gui, let thirdparty direct engine
		/// expose IEngineTools for editor
		INIT_TOOLSMODE,

		// count of init modes
		INIT_COUNT,
		INIT_INVALID = INIT_COUNT,
	};

	/// Starts the engine. Loads all libraries and queries interfaces
	virtual bool Init(InitializationMode mode, void *windowHandle, int width, int height) = 0;

	/// Terminates the engine. Frees all libraries and shuts down any loaded modules
	virtual void Shutdown() = 0;

	/// Enters the main loop. Begin polling input, updating the worldstate, network comm, rendering
	virtual void Run() = 0;

	virtual void ResizeWindow(int width, int height) = 0;
};

#define INTERFACE_IENGINE_VERSION "iengine001"


#endif