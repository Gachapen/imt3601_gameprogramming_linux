#ifndef IEVENTS_H
#define IEVENTS_H

#include "util/keyvalues.h"
#include "util/callbackutil.h"

class IEvents
{
protected:
	virtual ~IEvents() {}

public:
	virtual void AddListener(const char *eventName, ICallbackConcrete<KeyValues> *listener) = 0;
	virtual void RemoveListener(ICallbackConcrete<KeyValues> *listener) = 0;

	/// Takes ownership!
	virtual void FireEvent(KeyValues *eventData) = 0;

};

#define INTERFACE_IEVENTS_VERSION "ievents001"

#endif