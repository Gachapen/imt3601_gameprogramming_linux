#ifndef IGLOBALS_H
#define IGLOBALS_H


class IGlobals
{
protected:
	virtual ~IGlobals() {}

public:
	virtual float GetFrametime() = 0;
	virtual int GetFrameCount() = 0;

	virtual float GetTime() = 0;
	virtual double GetTimeHighPrecision() = 0;

};

#define INTERFACE_IGLOBALS_CLIENT_VERSION "iglobalsclient001"
#define INTERFACE_IGLOBALS_SERVER_VERSION "iglobalsserver001"
#define INTERFACE_IGLOBALS_GUI_VERSION "iglobalsgui001"

#endif