#ifndef IINPUT_H
#define IINPUT_H

class IInputKeyListener
{
protected:
	virtual ~IInputKeyListener() {}

public:
	virtual bool OnKeyDown(SDL_Keycode sdlKey) = 0;
	virtual bool OnKeyUp(SDL_Keycode sdlKey) = 0;
	virtual void OnTextEvent(const char *text) = 0;
};

class IInputMouseListener
{
protected:
	virtual ~IInputMouseListener() {}

public:
	virtual void OnMouseDown(int mouseButton) = 0;
	virtual void OnMouseUp(int mouseButton) = 0;
};

/// Generic input handler. Translates input into commands which can be processed
/// from anywhere in the application
class IInput
{
protected:
	virtual ~IInput() {}

public:
	virtual int GetMousePosX() = 0;
	virtual int GetMousePosY() = 0;

	/// Called when a key was pressed. Called multiple times when key is held down
	virtual void KeyDown(SDL_Keycode sdlKey) = 0;
	virtual void KeyUp(SDL_Keycode sdlKey) = 0;
	virtual void OnTextInput(const char *text) = 0;

	virtual void MouseDown(int mouseButton) = 0;
	virtual void MouseUp(int mouseButton) = 0;
	virtual void MouseWheel(int delta) = 0;

	virtual void StartTextInput(IInputKeyListener *listener) = 0;
	virtual void StopTextInput() = 0;

	virtual void AddKeyListener(IInputKeyListener *listener) = 0;
	virtual void RemoveKeyListener(IInputKeyListener *listener) = 0;

	virtual void AddMouseListener(IInputMouseListener *listener) = 0;
	virtual void RemoveMouseListener(IInputMouseListener *listener) = 0;

	virtual const char *GetBinding(SDL_Keycode sdlKey) = 0;

	/// Reverse lookup (slow)
	virtual SDL_Keycode GetKeyOfBinding(const char *command) = 0;
};

#define INTERFACE_IINPUT_VERSION "iinput001"

#endif