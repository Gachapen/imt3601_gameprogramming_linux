#ifndef IENGINETOOLS_H
#define IENGINETOOLS_H

/// Exposed from engine to editor if engine was initialized in tools mode
class IEngineTools
{
protected:
	virtual ~IEngineTools() {}

public:

	virtual void RequestEngineQuit() = 0;

	virtual void LoadMap(const char *filename) = 0;

	/// Ray cast mouse against world plane
	virtual bool IntersectScreenRayWithWorld(int screenx, int screeny, int &tileXOut, int &tileYOut) = 0;

	virtual void ChangeTileUVs(int tilex, int tiley, float uvs, float uvt) = 0;
	virtual void ReplaceWorldTextureAtlas(void *rgbData, void *rgbDataNormal, int width, int height) = 0;

	/// Locks the engine, no recusive locking allowed
	/// @param switchContext Acquire opengl context to savely execute opengl calls from calling thread
	virtual void LockEngine(bool switchContext = false) = 0;
	virtual void UnlockEngine() = 0;

	/// Show/hide paint highlight, specify if it's centered on the tile or the corner
	virtual void SetPaintHighlight(bool visible, bool isCorner) = 0;
	virtual void SetFlagHighlight(TileFlagId flag) = 0;
	virtual void SetTileFlags(int x, int y, bool set) = 0;

	virtual void OnKeyDown(SDL_Keycode sdlKey) = 0;
	virtual void OnKeyUp(SDL_Keycode sdlKey) = 0;
};

#define INTERFACE_IENGINETOOLS_VERSION "ienginetools001"

#endif