#ifndef COLOR_H
#define COLOR_H

#include "gui.h"

namespace GUI
{
struct Color
{
public:
	Color(float r, float g, float b, float a);
	Color();

	void Init(float r, float g, float b, float a = 1.0f)
	{
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	float r, g, b, a;
};

}

#endif