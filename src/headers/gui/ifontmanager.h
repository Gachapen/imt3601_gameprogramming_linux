#ifndef IFONTMANAGER_H
#define IFONTMANAGER_H

#include "gui/gui.h"

/// GUI system for text rendering
class IFontManager
{
protected:
	virtual ~IFontManager() {}

public:
	/// Look ups a font from the font script
	virtual GUI::FONT_KEY GetFont(const char *name) = 0;

	/// Set current font for rendering
	virtual void SetDrawTextFont(GUI::FONT_KEY fontKey) = 0;
	/// Set current text color
	virtual void SetDrawTextColor(float r, float g, float b, float a) = 0;
	/// Set current text start position
	virtual void SetDrawTextPos(int x, int y) = 0;
	/// Set the current text scale, 1 is default and will render fonts pixel-perfect
	/// > 1 will stretch the texture, < 1 compress it
	virtual void SetDrawTextScale(float scaleWidth, float scaleHeight) = 0;

	/// Calculate text width in pixels with current font
	virtual int GetTextWidth(const char *text) = 0;

	/// Draw text with current settings
	virtual void DrawTextString(const char *text) = 0;

};

#define INTERFACE_IFONTMANAGER_VERSION "ifontmanager001"


#endif