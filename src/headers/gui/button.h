#ifndef BUTTON_H
#define BUTTON_H

#include "gui.h"
#include "panel.h"
#include "label.h"

namespace GUI
{
class Button : public Panel
{
public:
	Button();
	virtual ~Button();

	virtual void SetRect(Rect rect);

	LISTENER(Button, OnEnter, Control *);
	virtual void OnEnter(Control *ctrl);

	LISTENER(Button, OnLeave, Control *);
	virtual void OnLeave(Control *ctrl);

	Label *label;
	Color hoverColor;

private:
	Color normalColor;
};
}
#endif
