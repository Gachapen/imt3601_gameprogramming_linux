#ifndef IGUI_H
#define IGUI_H


#include "ifontmanager.h"

/// Manages GUI startup and shutdown
class IGUI
{
protected:
	virtual ~IGUI() {}

public:
	virtual void Init() = 0;
	virtual void Shutdown() = 0;

	/// Called when window size changes to resize procedural textures
	virtual void Resize(int width, int height) = 0;

};

#define INTERFACE_IGUI_VERSION "igui001"


#endif