#ifndef IMDXMODELDICT_H
#define IMDXMODELDICT_H


#include "imdxmodel.h"

/// Manages MDX loading
class IMDXModelDict
{
protected:
	virtual ~IMDXModelDict() {}

public:
	/// Shutdown all models
	virtual void Shutdown() = 0;

	/// Releases all unreferenced models
	virtual void ShutdownUnreferencedModels() = 0;

	/// Find a model by filepath or load it if non-existent
	virtual IMDXModel *FindModel(const char *path) = 0;
};

#define INTERFACE_IMDXMODELDICT_VERSION    "imdxmodeldict001"
#endif
