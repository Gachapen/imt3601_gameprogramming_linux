#ifndef IMDXMODEL_H
#define IMDXMODEL_H

struct MDXModelData;

/// Model data loaded from the WC3 MDX format
class IMDXModel
{
protected:
	virtual ~IMDXModel() {}

public:
	// reference counting
	virtual void IncrementReferenceCount() = 0;
	virtual void DecrementReferenceCount() = 0;

	/// Access the model data directly
	virtual const MDXModelData *GetModelData() const = 0;
	virtual const char *GetModelPath() = 0;

	virtual int FindSequence(const char *name) const = 0;
	virtual const char *GetSequenceName(int sequence) const = 0;
	virtual float GetSequencePlaybackRate(int sequence) const = 0;
	
	/// Get animation interval start of a sequence
	virtual int GetSequenceIntervalStart(int sequence) const = 0;
	/// Get animation interval end of a sequence
	virtual int GetSequenceIntervalEnd(int sequence) const = 0;
	/// Get animation interval position for a sequence and a normalized cycle
	virtual int RemapSequenceCycle(int sequence, float cycle) const = 0;

	/// Build the relative bone xforms used for binding the mesh (the default pose)
	virtual void BuildBindPose(mat4 *out) = 0;
	/// Build the inversive transforms for bones. Used to nullify default transforms. Used to figure
	/// out the actual deformations necessary in the shader, because the default transforms are
	/// applied to the mesh already
	virtual void BuildInversionPose(mat4 *out) = 0;
	/// Build an absolute animated pose
	virtual void BuildAnimatedPose(const mat4 &modelMatrix,
		const mat4 *bindPose, int frame, int sequence, mat4 *worldTransforms) = 0;
	/// Add a full skeleton animation on top of the current one. Scaled by normalized weight
	virtual void AccumulateAnimatedPose(const mat4 &modelMatrix, const mat4 *bindPose,
		int frame, int sequence, mat4 *worldTransforms, float weight) = 0;
	/// Finalize the animated pose. Builds worldspace transforms from relative ones
	/// and extracts deformation relative to default pose
	virtual void FinalizeAnimatedPose(const mat4 &viewMatrix, const mat4 *inversionPose, mat4 *worldTransforms) = 0;

	virtual int GetBoneCount() = 0;
};

#endif