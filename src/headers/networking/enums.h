#ifndef NETWORKING_ENUMS_H
#define NETWORKING_ENUMS_H

enum SyncMode
{
	SYNCMODE_UNRELIABLE_NOORDER,
	SYNCMODE_RELIABLE_NOORDER,
	//SYNCMODE_RELIABLE_ORDERED,  (Not needed for our game. If you need order, use a request-response model)
	COUNT_SYNCMODES			// To avoid it being mistaken for a syncmode
};

enum ConnectionStatus : int
{
	CSTATUS_CONNECTED,				// !< Client is connected.
	CSTATUS_REJECTED,				// !< Server rejected connection.
	CSTATUS_TIMEOUT,				// !< A timeout happened. If during connection: regular timeout, if while connected, server wasn't heard from within the timeout interval.
	CSTATUS_NOT_CONNECTED,			// !< No connection has been attempted, or the client disconencted manually
	CSTATUS_OPEN_SOCKET_FAILED,		// !< SDLNet returned an error while trying to open a socket
	CSTATUS_COULDNT_RESOLVE_HOST,	// !< Coudln't resolve host
	CSTATUS_PACKET_ALLOC_FAILED,	// !< Couldn't allocate memory for packet buffer
	CSTATUS_DROPPED,				// !< Server sent an MTYPE_DISCCONECT message, possibly caused by shutting down
	CSTATUS_PENDING,				// !< The connection wasn't blocked,
	CSTATUS_CONNECTIONLESS,			// !< Host application opted to not use connection Client::Connect(
	CSTATUS_NOT_CLIENT_CLASS		// !< Called in Connection
};
#endif
