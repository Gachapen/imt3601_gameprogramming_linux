#ifndef INETSERVER_H
#define INETSERVER_H

#include "util\macros.h"
#include "util\platform.h"
#include "networking\enums.h"

class INetworkServer
{
public:
	virtual bool Start() = 0;	// Opens socket, allocates buffers, and starts listening for packets
	virtual void Stop() = 0;	// Notifies the clients, frees up memory and closes the socket.
	// NOTE: Make sure you Stop() it first.
	virtual void Release() = 0;	// Deletes the server, should be called after Stop() = 0;

	// Settings

	// If enabled, it will not allow any new connections.
	virtual void SetRejectionMode(bool enable) = 0;
	virtual void SetConnectionlessMode(bool enable) = 0;
	virtual void SetTimeOutThreshold(int milliseconds) = 0;

	// Send methods
	virtual void QueueForSending(Uint32 connectionID, Byte *data, size_t size, SyncMode mode = SYNCMODE_UNRELIABLE_NOORDER) = 0;
	virtual void QueueForSendingToAll(Byte *data, size_t size, SyncMode mode = SYNCMODE_UNRELIABLE_NOORDER, int rttFilter = 20000) = 0;
	virtual bool SendQueuedMessages() = 0;

	// Ping methods
	virtual void QueuePingMessage(Uint32 connectionID) = 0;
	virtual void QueuePingMessageToAll() = 0;

	// Interactions with individual clients (RECOMMENDED)
	virtual float GetRTT(Uint32 connectionID) = 0;
	virtual bool HasTimedOut(Uint32 connectionID) = 0;
	virtual bool HasDisconnected(Uint32 connectionID, bool includeTimeOut = false) = 0;
	// WARNING: Copy this to a new buffer right away, as the bytes pointed to will be reused for the next call to this function or the connectionless equialent.
	virtual Byte *HasNewMessage(Uint32 connectionID, Uint32& sizeOutput) = 0;
	// WARNING: Copy this to a new buffer right away, as the bytes pointed to will be reused for the next call to this function or the connectionless equialent.
	virtual Byte *HasNewMessage(int& clientReferenceOutput, Uint32& sizeOutput, Uint32& addressOutput, Uint16& portOutput) = 0;
	virtual void Drop(Uint32 connectionID) = 0;

	virtual bool HasNewConnection(int &connectionIDOutput) = 0;
	virtual bool HasNewDisconnection(int &connectionIDOutput, bool includeTimeOut = true) = 0;

	// Connectionless interactions
	// NOTE: Address and port should be Network Byte Order. (If 127.0.0.1:12345 is address=0x7f000001 and port=0x3930, you're doing it right)
	virtual bool SendConnectionlessMessage(Uint32 address, Uint16 port, Byte *data, size_t size) = 0;
	// WARNING: Copy this to a new buffer right away, as the bytes pointed to will be reused for the next call to this function or the connection-dependent equalent.
	virtual Byte *HasNewConnectionlessMessage(Uint32 &outputAddress, Uint16 &outputPort, Uint32 &outputSize) = 0;
};

#define INTERFACE_NETSERVER_VERSION    "inetworkingserver001"
#endif	//ISERVER_H
