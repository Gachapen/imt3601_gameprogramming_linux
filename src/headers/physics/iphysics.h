#ifndef IPHYSICS_H
#define IPHYSICS_H

typedef void* PhysicsBody;

/// Interface to abstract Box2D away
class IPhysics
{
protected:
	virtual ~IPhysics() {}

public:

	virtual void Init() = 0;
	virtual void Shutdown() = 0;

	virtual void Update(float frametime) = 0;
	virtual void DrawDebug() = 0;

	virtual PhysicsBody CreateRigidRect(const vec3 &position, const vec3 &size) = 0;
	virtual PhysicsBody CreateDynamicCircle(const vec3 &position, float radius) = 0;

	virtual void SetObjectPosition(PhysicsBody obj, const vec3 &position) = 0;
	virtual void SetObjectVelocity(PhysicsBody obj, const vec3 &velocity) = 0;
	virtual void SetObjectLinearDamping(PhysicsBody obj, float linearDamping) = 0;
	virtual void ApplyObjectImpulse(PhysicsBody obj, const vec3 &impulse) = 0;
	virtual void SetObjectCollisionGroup(PhysicsBody obj, int group) = 0;

	virtual void GetObjectPosition(PhysicsBody obj, vec3 &position) = 0;
	virtual void GetObjectVelocity(PhysicsBody obj, vec3 &velocity) = 0;

	virtual void DestroyObject(PhysicsBody obj) = 0;
};

#define INTERFACE_IPHYSICS_VERSION "iphysics001"

#endif