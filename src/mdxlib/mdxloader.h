#ifndef MDXLOADER_H
#define MDXLOADER_H


#include "mdxlib\mdxformat.h"

extern MDXModelData *LoadMDX(const char *filename);


#endif