#ifndef MDXMODELDICT_H
#define MDXMODELDICT_H

#include "util\singleton.h"
#include "mdxlib\imdxmodeldict.h"

class MDXModel;

class MDXModelDict : public IMDXModelDict
{
	DECLARE_SINGLETON(MDXModelDict);

public:
	~MDXModelDict();

	//IMDXModelDict
	virtual void Shutdown();

	virtual void ShutdownUnreferencedModels();

	virtual IMDXModel *FindModel(const char *path);

	//virtual IMDXModel *CreateModelInstance(const char *path);

private:
	std::unordered_map<std::string, MDXModel *> modelCache;
};
#endif
