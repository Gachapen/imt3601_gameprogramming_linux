#ifndef MDXMODEL_H
#define MDXMODEL_H

#include "mdxmodeldict.h"
#include "shareddefs.h"

struct MDXModelData;

class MDXModel : public IMDXModel, public CReferenceCounted
{
	~MDXModel();
public:
	MDXModel();

	void Release();

	void Init(const char *path, MDXModelData *modelData);

	//IMDXModel
	FORWARD_REFERENCE_COUNTED_DEFINE();

	virtual const MDXModelData *GetModelData() const;
	virtual const char *GetModelPath();

	virtual int FindSequence(const char *name) const;
	virtual const char *GetSequenceName(int sequence) const;
	virtual float GetSequencePlaybackRate(int sequence) const;
	virtual int GetSequenceIntervalEnd(int sequence) const;
	virtual int GetSequenceIntervalStart(int sequence) const;
	virtual int RemapSequenceCycle(int sequence, float cycle) const;

	virtual void BuildBindPose(mat4 *out);
	virtual void BuildInversionPose(mat4 *out);
	virtual void BuildAnimatedPose(const mat4 &modelMatrix, const mat4 *bindPose,
		int frame, int sequence, mat4 *worldTransforms);
	virtual void AccumulateAnimatedPose(const mat4 &modelMatrix, const mat4 *bindPose,
		int frame, int sequence, mat4 *worldTransforms, float weight);
	virtual void FinalizeAnimatedPose(const mat4 &viewMatrix, const mat4 *inversionPose, mat4 *worldTransforms);

	virtual int GetBoneCount();

private:

	char *modelPath;
	const MDXModelData *modelData;
	MDXNode *const *nodeBase;
	int nodeCount;
};
#endif
