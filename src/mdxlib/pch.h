#ifndef PCH_H
#define PCH_H

#include "platform.h"

#pragma warning( disable: 4996 )
#pragma warning( disable: 4530 )

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <fstream>
#include <direct.h>
#include <vector>
#include <unordered_map>
#include <stack>

#include "util/macros.h"
#include "util/util.h"
#include "util/mathutil.h"
#include "util/fileutil.h"
#include "util/singleton.h"
#include "util/referencecounted.h"
#include "util/factory.h"
#include "util/stringutil.h"

#include <util\eigen.h>

#include <SDL.h>
//#include <SDL_image.h>
//#include <SDL_ttf.h>

#endif