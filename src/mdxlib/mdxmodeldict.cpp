#include "pch.h"
#include "mdxmodeldict.h"
#include "mdxloader.h"
#include "mdxmodel.h"
#include "appinterface\iappinterface.h"


SINGLETON_INSTANCE(MDXModelDict);

EXPOSE_APP(INTERFACE_IMDXMODELDICT_VERSION, MDXModelDict::GetInstance());


MDXModelDict::MDXModelDict()
{
}

MDXModelDict::~MDXModelDict()
{
	Assert(modelCache.empty());
}

void MDXModelDict::Shutdown()
{
	for (auto m : modelCache)
	{
		Assert(!m.second->IsReferenced());
		m.second->Release();
	}
	modelCache.clear();
}

void MDXModelDict::ShutdownUnreferencedModels()
{
	for (auto itr = modelCache.begin(); itr != modelCache.end(); )
	{
		if (!itr->second->IsReferenced())
		{
			itr->second->Release();
			itr = modelCache.erase(itr);
		}
		else
		{
			itr++;
		}
	}
}

IMDXModel *MDXModelDict::FindModel(const char *path)
{
	char fixedPath[MAX_PATH_GAME];

	G_StrNCpy(fixedPath, path, sizeof(fixedPath));
	G_StrFixSlashes(fixedPath);

	auto itr = modelCache.find(fixedPath);

	if (itr != modelCache.end())
	{
		return itr->second;
	}

	MDXModelData *modelData = LoadMDX(fixedPath);

	Assert(modelData);

	if (modelData == nullptr)
	{
		return nullptr;
	}

	MDXModel *model = new MDXModel();
	model->Init(fixedPath, modelData);

	modelCache[fixedPath] = model;
	return model;
}
