#include "pch.h"
#include "mdxlib\mdxformat.h"
#include "mdxlib\mdxinterpolators.h"
#include "mdxmodel.h"



// constructs relative transformations between nodes
inline void BuildRelativeNode(MDXNode *const *nodes, const vec3 *pivots, int index, mat4 *nodesOut)
{
	Assert(index < MODEL_MAX_BONES);

	const MDXNode *node = nodes[index];

	vec3 origin = pivots[index];

	mat4 &out = nodesOut[index];
	out = mat4::Identity();

	if ((node->ParentId != MDX_INVALID_INDEX) &&
		((node->Flags & MDXNode::FL_DONT_INHERIT_TRANSLATION) == 0))
	{
		origin -= pivots[node->ParentId];
	}

	out = translate(out, origin);
}

// builds initial relative transforms
inline void BuildAnimatedNode(const mat4 &modelMatrix, MDXNode *const *nodes, int index,
					Uint32 animationTime, int sequence, Uint64 &nodesBuilt, mat4 *worldOut)
{
	Assert(index < MODEL_MAX_BONES);

	Uint64 flag = (1ull << index);

	// already built
	if ((nodesBuilt & flag) != 0)
	{
		return;
	}

	nodesBuilt |= flag;

	const MDXNode *node = nodes[index];

	mat4 &out = worldOut[index];

	const bool isBillboard = (node->Flags & MDXNode::FL_BILLBOARDED) != 0;
	const bool hasParent = node->ParentId != MDX_INVALID_INDEX;

	if (!isBillboard)
	{
		if (!hasParent)
		{
			out = modelMatrix * out;
		}

		if (node->pScaling != nullptr)
		{
			vec3 s = MDXAnimateByTrack<vec3>(*node->pScaling, animationTime, sequence, vec3::Ones());
			out = scale(out, s);
		}

		if (node->pTranslation != nullptr)
		{
			vec3 t = MDXAnimateByTrack<vec3>(*node->pTranslation, animationTime, sequence, vec3::Zero());
			out = translate(out, t);
		}

		if (node->pRotation != nullptr)
		{
			quat rotationQuat = MDXAnimateByTrack<quat>(*node->pRotation, animationTime, sequence, quat::Identity());
			out = rotate(out, rotationQuat);
		}
	}
}

// accumulates relative transforms to blend animations
inline void AccumulateAnimatedNode(const mat4 &modelMatrix, const mat4 &modelInvMatrix, const mat4 *relativeNodes, MDXNode *const *nodes, int index,
						 Uint32 animationTime, int sequence, Uint64 &nodesBuilt, mat4 *worldOut, float weight)
{
	Assert(index < MODEL_MAX_BONES);

	Uint64 flag = (1ull << index);

	// already built
	if ((nodesBuilt & flag) != 0)
	{
		return;
	}

	nodesBuilt |= flag;

	const MDXNode *node = nodes[index];

	mat4 &out = worldOut[index];
	mat4 normalSetup = relativeNodes[index];

	const bool isBillboard = (node->Flags & MDXNode::FL_BILLBOARDED) != 0;
	const bool hasParent = node->ParentId != MDX_INVALID_INDEX;

	if (isBillboard)
	{
		return;
	}

	if (node->pScaling != nullptr)
	{
		vec3 s = MDXAnimateByTrack<vec3>(*node->pScaling, animationTime, sequence, vec3::Ones());
		normalSetup = scale(normalSetup, s);
	}

	if (node->pTranslation != nullptr)
	{
		vec3 t = MDXAnimateByTrack<vec3>(*node->pTranslation, animationTime, sequence, vec3::Zero());
		normalSetup = translate(normalSetup, t);
	}

	if (node->pRotation != nullptr)
	{
		quat rotationQuat = MDXAnimateByTrack<quat>(*node->pRotation, animationTime, sequence, quat::Identity());
		normalSetup = rotate(normalSetup, rotationQuat);
	}

	// need to lerp in model space, transform back from current pos
	if (!hasParent)
	{
		out = modelInvMatrix * out;
	}

	vec3 p0, p1;
	MatrixPosition(out, p0);
	MatrixPosition(normalSetup, p1);

	quat q0, q1;
	q0 = quat(rotationMatrix(out));
	q1 = quat(rotationMatrix(normalSetup));

	q0 = q0.slerp(weight, q1);
	p0 = Lerp(weight, p0, p1);

	normalSetup = mat4::Identity();
	normalSetup = translate(normalSetup, p0);
	normalSetup = rotate(normalSetup, q0);

	// now move us back into world space
	if (!hasParent)
	{
		normalSetup = modelMatrix * normalSetup;
	}

	if (!hasParent ||
		((node->Flags &
		  (MDXNode::FL_DONT_INHERIT_ROTATION
		   | MDXNode::FL_DONT_INHERIT_TRANSLATION
		   | MDXNode::FL_DONT_INHERIT_SCALING)) == 0))
	{
		out = normalSetup;
	}
}

// builds world nodes and translates them for deformation
inline void SolveParentNodes(MDXNode *const *nodes, const mat4 &invViewRotation, const vec3 &viewOrigin,
					  int index, Uint64 &nodesBuilt, mat4 *worldOut)
{
	Assert(index < MODEL_MAX_BONES);

	Uint64 flag = (1ull << index);

	// already built
	if ((nodesBuilt & flag) != 0)
	{
		return;
	}

	nodesBuilt |= flag;

	const MDXNode *node = nodes[index];

	const bool isBillboard = (node->Flags & MDXNode::FL_BILLBOARDED) != 0;

	mat4 &out = worldOut[index];

	if ( node->ParentId != MDX_INVALID_INDEX
		&& (node->Flags &
		  (MDXNode::FL_DONT_INHERIT_ROTATION
		   | MDXNode::FL_DONT_INHERIT_TRANSLATION
		   | MDXNode::FL_DONT_INHERIT_SCALING)) == 0)
	{
		const int parent = node->ParentId;

		SolveParentNodes(nodes, invViewRotation, viewOrigin, parent, nodesBuilt, worldOut);

		out = worldOut[parent] * out;
	}

	if (isBillboard)
	{
		//MatrixRotationIdentity(out);
		//out *= invViewRotation;

		vec3 pos;
		MatrixPosition(out, pos);

		vec3 fwd = viewOrigin - pos;
		fwd.normalize();

		vec3 up(0,0,1);
		up = rotate(up, invViewRotation);

		vec3 right =  up .cross(fwd);
		up =  fwd.cross(right);

		mat4 scale = mat4::Zero();
		scale(0,0) = out.col(0).norm();
		scale(1,1) = out.col(1).norm();
		scale(2,2) = out.col(2).norm();
		scale(3,3) = 1;

		MatrixSetForward(out, fwd);
		MatrixSetLeft(out, right);
		MatrixSetUp(out, up);

		out *= scale;
	}
}

MDXModel::MDXModel()
	: modelPath(nullptr)
	  , modelData(nullptr)
	  , nodeCount(0)
	  , nodeBase(nullptr)
{
}

MDXModel::~MDXModel()
{
	delete [] modelPath;
	delete modelData;
}

void MDXModel::Init(const char *path, MDXModelData *modelData)
{
	Assert(this->modelData == nullptr);

	delete [] modelPath;
	modelPath = G_StrCreateCopy(path);

	this->modelData = modelData;

	nodeBase = modelData->pDynamic->Nodes.data();
	nodeCount = (int)modelData->pDynamic->Nodes.size();
}

void MDXModel::Release()
{
	delete this;
}

const MDXModelData *MDXModel::GetModelData() const
{
	return modelData;
}

const char *MDXModel::GetModelPath()
{
	return modelPath;
}

int MDXModel::FindSequence(const char *name) const
{
	for (unsigned int i = 0; i < modelData->pSequences->Sequences.size(); i++)
	{
		if (G_StrEq(modelData->pSequences->Sequences[i].Name, name))
		{
			return i;
		}
	}

	return -1;
}

const char *MDXModel::GetSequenceName(int sequence) const
{
#if DEBUG
	if ((sequence < 0) || (sequence >= (int)modelData->pSequences->Sequences.size()))
	{
		Assert(0);
		return "";
	}
#endif

	return modelData->pSequences->Sequences[sequence].Name;
}

float MDXModel::GetSequencePlaybackRate(int sequence) const
{
#if DEBUG
	if ((sequence < 0) || (sequence >= (int)modelData->pSequences->Sequences.size()))
	{
		Assert(0);
		return 1.0f;
	}
#endif

	const int intervalMin = modelData->pSequences->Sequences[sequence].IntervalStart;
	const int intervalMax = modelData->pSequences->Sequences[sequence].IntervalEnd;

	return 960.0f / float(intervalMax - intervalMin);
}

int MDXModel::GetSequenceIntervalEnd(int sequence) const
{
#if DEBUG
	if ((sequence < 0) || (sequence >= (int)modelData->pSequences->Sequences.size()))
	{
		Assert(0);
		return 0;
	}
#endif

	return modelData->pSequences->Sequences[sequence].IntervalEnd;
}

int MDXModel::GetSequenceIntervalStart(int sequence) const
{
#if DEBUG
	if ((sequence < 0) || (sequence >= (int)modelData->pSequences->Sequences.size()))
	{
		Assert(0);
		return 0;
	}
#endif

	return modelData->pSequences->Sequences[sequence].IntervalStart;
}

int MDXModel::RemapSequenceCycle(int sequence, float cycle) const
{
#if DEBUG
	if ((sequence < 0) || (sequence >= (int)modelData->pSequences->Sequences.size()))
	{
		Assert(0);
		return 0;
	}
#endif

	int intervalMin = modelData->pSequences->Sequences[sequence].IntervalStart;
	int intervalMax = modelData->pSequences->Sequences[sequence].IntervalEnd;

	return Lerp(cycle, intervalMin, intervalMax);
}

void MDXModel::BuildBindPose(mat4 *out)
{
	MDXNode *const *nodes = modelData->pDynamic->Nodes.data();
	const vec3 *pivots = modelData->pPivots->Pivots.data();
	const int size = modelData->pDynamic->Nodes.size();

	for (int i = 0; i < size; i++)
	{
		BuildRelativeNode(nodes, pivots, i, out);
	}
}

void MDXModel::BuildInversionPose(mat4 *out)
{
	MDXNode *const *nodes = modelData->pDynamic->Nodes.data();
	const vec3 *pivots = modelData->pPivots->Pivots.data();
	const int size = modelData->pDynamic->Nodes.size();

	for (int i = 0; i < size; i++)
	{
		out[i] = translationMatrix4(pivots[i]);
		out[i] = out[i].inverse().eval();
	}
}

void MDXModel::BuildAnimatedPose(const mat4 &modelMatrix, const mat4 *bindPose,
								 int frame, int sequence, mat4 *worldTransforms)
{
	Uint64 flags = 0;

	Assert(MODEL_MAX_BONES <= 64);

	memcpy(worldTransforms, bindPose, sizeof(mat4) * MODEL_MAX_BONES);

	for (int i = 0; i < nodeCount; i++)
	{
		BuildAnimatedNode(modelMatrix,
					   nodeBase, i,
					   frame, sequence, flags,
					   worldTransforms);
	}
}

void MDXModel::AccumulateAnimatedPose(const mat4 &modelMatrix, const mat4 *bindPose,
									  int frame, int sequence, mat4 *worldTransforms, float weight)
{
	Uint64 flags = 0;

	Assert(MODEL_MAX_BONES <= 64);

	mat4 modelInverse = modelMatrix.inverse().eval();

	for (int i = 0; i < nodeCount; i++)
	{
		AccumulateAnimatedNode(modelMatrix, modelInverse,
							bindPose, nodeBase, i,
							frame, sequence, flags,
							worldTransforms, weight);
	}
}

void MDXModel::FinalizeAnimatedPose(const mat4 &viewMatrix, const mat4 *inversionPose, mat4 *worldTransforms)
{
	Uint64 flags = 0;

	mat4 invViewRotation = viewMatrix;
	invViewRotation.col(3) (0) = 0;
	invViewRotation.col(3) (1) = 0;
	invViewRotation.col(3) (2) = 0;
	invViewRotation = invViewRotation.transpose().eval();

	vec3 viewPos;
	MatrixPosition(viewMatrix, viewPos);
	viewPos = rotate(viewPos, invViewRotation);

	for (int i = 0; i < nodeCount; i++)
	{
		SolveParentNodes(nodeBase, invViewRotation, viewPos, i, flags, worldTransforms);
	}

	for (int i = 0; i < nodeCount; i++)
	{
		worldTransforms[i] *= inversionPose[i];
	}
}

int MDXModel::GetBoneCount()
{
	return nodeCount;
}
