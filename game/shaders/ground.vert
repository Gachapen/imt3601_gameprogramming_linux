
#include "common.h"

uniform mat4 MVP;
uniform mat4 M;
uniform float znear;

in vec3 position;
in vec2 texcoord;
in vec3 normal;
in vec3 tangent;
in vec3 binormal;


//out vec3 vertex_position;
out vec3 vertex_normal;
out vec3 vertex_tangent;
out vec3 vertex_binormal;
out vec2 vertex_uv;
out float vertex_depth;

void main()
{
	gl_Position = MVP * vec4(position, 1.0);
	
	vertex_depth = gl_Position.z - znear;

	vertex_uv = texcoord;

	//vertex_position = worldSpacePosition.xyz;
	
	vertex_normal = rotate3x3(M, normal);
	vertex_tangent = rotate3x3(M, tangent);
	vertex_binormal = rotate3x3(M, binormal);
}
