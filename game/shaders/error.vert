

uniform mat4 VP;
uniform mat4 bones[64];

in vec3 position;
in vec2 texcoord;
in vec3 normal;
in vec3 boneindices;

out vec2 vertex_uv;

void main()
{
	vec4 position4 = vec4(position, 1);

	vec4 worldSpacePosition =  bones[int(boneindices.x)] * position4 * 0.333333f
					+ bones[int(boneindices.y)] * position4 * 0.333333f
					+ bones[int(boneindices.z)] * position4 * 0.333333f;
	
	
	gl_Position = VP * worldSpacePosition;
	
	vertex_uv = texcoord;
}
