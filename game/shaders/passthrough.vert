
in vec2 position;

out vec2 vertex_texcoord;



void main()
{
	gl_Position = vec4( position, 0, 1 );
	
	vertex_texcoord = position * 0.5 + 0.5;
}

