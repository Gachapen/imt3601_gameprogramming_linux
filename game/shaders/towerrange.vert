
#include "common.h"

uniform mat4 MVP;
uniform float znear;

in vec3 position;
in vec2 texcoord;

out vec2 vertex_uv;
out float vertex_depth;

void main()
{
	gl_Position = MVP * vec4(position, 1.0);

	vertex_uv = texcoord;
	
	vertex_depth = gl_Position.z - znear;
}
