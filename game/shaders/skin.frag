
//COMBO: ALPHATESTING
//COMBO: COLORMASK

#include "common.h"

uniform sampler2D albedoSampler;
uniform samplerCube cubemapSampler;

uniform vec3 viewOrigin;
uniform vec3 viewUp;
uniform float zrange;

in vec3 vertex_normal;
in vec2 vertex_uv;
in float vertex_depth;

out vec3 fragment_color[2];

#if ALPHATESTING == 1
uniform float alphaTestRef;
#endif

#if COLORMASK == 1
uniform vec3 colorMask;
#endif

void main()
{
	vec4 albedo = texture2D(albedoSampler, vertex_uv);
	vec3 normal = vertex_normal;

	normal *= (gl_FrontFacing?1:-1);

#if ALPHATESTING == 1
	if (albedo.a < alphaTestRef)
	{
		discard;
	}
#endif

#if COLORMASK == 1
	albedo.rgb = mix(colorMask, albedo.rgb, albedo.a);
#endif
	
	fragment_color[0] = albedo.rgb;
	
	fragment_color[1] = normal * 0.5 + 0.5;

	// need to normalize depth for depth buffer lookup because it's clamped otherwise?? :(
#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = vertex_depth / zrange;
#endif
}


