
#include "../../src/headers/shadershared.h"

vec3 rotate3x3(mat4 matrix, vec3 ray)
{
// possibly faster than cast and matrix mul, need to check?
	return vec3(dot(vec3(matrix[0][0], matrix[1][0], matrix[2][0]), ray),
			dot(vec3(matrix[0][1], matrix[1][1], matrix[2][1]), ray),
			dot(vec3(matrix[0][2], matrix[1][2], matrix[2][2]), ray));

	//return mat3(matrix) * ray;
}

float BlinnPhong( in vec3 world2Eye, in vec3 world2Light, in vec3 normal, in float power )
{
	vec3 halfVec = normalize( world2Eye + world2Light );
	
	float phong = dot(normal, halfVec);
	phong = clamp( phong, 0.0, 1.0 );
	phong = pow( phong, power );
	
	return phong;
}

const float gauss7[7] = float[]
(
	0.023977,
	0.097843,
	0.227491,
	0.301377,
	0.227491,
	0.097843,
	0.023977
);

const float gauss13[13] = float[]
(
	0.006299,
	0.017298,
	0.039533,
	0.075189,
	0.119007,
	0.156756,
	0.171834,
	0.156756,
	0.119007,
	0.075189,
	0.039533,
	0.017298,
	0.006299
);