
//COMBO: ALPHATESTING

#if ALPHATESTING == 1
uniform sampler2D albedoSampler;
uniform float alphaTestRef;

in vec2 vertex_uv;
#endif


void main()
{
#if ALPHATESTING == 1
	if (texture2D(albedoSampler, vertex_uv).a < alphaTestRef)
	{
		discard;
	}
#endif

	// write depth
}


