
#include "common.h"

uniform sampler2D albedoSampler;
uniform vec2 texelSize;

in vec2 vertex_texcoord;

out vec3 fragment_color;


const vec2 offsets[7] = vec2[]
(
	vec2(0,-3),
	vec2(0,-2),
	vec2(0,-1),
	vec2(0,0),
	vec2(0,1),
	vec2(0,2),
	vec2(0,3)
);


void main()
{

				
		vec3 albedo = vec3(0);
				
	for (int i = 0; i < 7; i++)
	{
		albedo += texture2D(albedoSampler, vertex_texcoord + texelSize * offsets7[i]).rgb * gauss7[i];
	}
	
	fragment_color = albedo;
}