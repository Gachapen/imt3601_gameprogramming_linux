
#include "common.h"

uniform sampler2D albedoSampler;
uniform sampler2D lightSampler;

uniform sampler2D normalSampler;
uniform sampler2D depthSampler;
uniform sampler2D shadowSampler;

uniform vec3 frustum_fwd;
uniform vec3 frustum_top;
uniform vec3 frustum_right;
uniform vec3 view_origin;

uniform mat4 shadowVP;
uniform vec3 directional_light_dir;
uniform vec3 directional_light_color;
uniform vec3 directional_light_ambient;
uniform float shadow_map_size;

uniform vec3 fogParams; // amount, start, range
uniform vec3 fogColor;

float shadowResolution = shadow_map_size;
vec2 texelSize = vec2(1.0/shadowResolution, 1.0/shadowResolution);
vec2 texelSizeDouble = vec2(2.0/shadowResolution, 2.0/shadowResolution);
vec2 texelSizeTriple = vec2(3.0/shadowResolution, 3.0/shadowResolution);

in vec2 vertex_texcoord;
in vec2 vertex_position;

out vec3 fragment_color;

const float gauss3x3[9] = float[]
(
	0.038747, 0.119348, 0.038747,
	0.119348, 0.367619, 0.119348,
	0.038747, 0.119348, 0.038747
);

float DoSoftwarePCF2x2(vec2 texCoord, float zpos)
{
	vec2 fraction = fract(texCoord * shadowResolution);
	float acneFix = 0.001;
	
	float shadowAmount00 = texture2D(shadowSampler, texCoord).r;
	float shadowAmount01 = texture2D(shadowSampler, texCoord + vec2(0, texelSize.y)).r;
	float shadowAmount10 = texture2D(shadowSampler, texCoord + vec2(texelSize.x, 0)).r;
	float shadowAmount11 = texture2D(shadowSampler, texCoord + texelSize).r;
	
	zpos -= acneFix;
	shadowAmount00 = step(zpos,shadowAmount00);
	shadowAmount01 = step(zpos,shadowAmount01);
	shadowAmount10 = step(zpos,shadowAmount10);
	shadowAmount11 = step(zpos,shadowAmount11);
	
	shadowAmount00 = mix(shadowAmount00, shadowAmount10, fraction.x);
	shadowAmount01 = mix(shadowAmount01, shadowAmount11, fraction.x);
	shadowAmount00  = mix(shadowAmount00, shadowAmount01, fraction.y);
	
	return shadowAmount00;
}

float DoSoftwarePCF3x3(vec2 texCoord, float zpos)
{
	vec2 fraction = fract(texCoord * shadowResolution);
	float acneFix = 0.001;
	zpos -= acneFix;
	
	mat4 samples = mat4( 	step(zpos,		texture2D(shadowSampler, texCoord).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSize.x, 0)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeDouble.x, 0)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeTriple.x, 0)).r	),
							
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(0, texelSize.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSize.x, texelSize.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeDouble.x, texelSize.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeTriple.x, texelSize.y)).r	),
							
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(0, texelSizeDouble.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSize.x, texelSizeDouble.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeDouble.x, texelSizeDouble.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeTriple.x, texelSizeDouble.y)).r	),
							
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(0, texelSizeTriple.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSize.x, texelSizeTriple.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeDouble.x, texelSizeTriple.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeTriple.x, texelSizeTriple.y)).r	)
						);
	
	float shadowAmount = mix( mix(samples[0][0], samples[0][1], fraction.x), mix(samples[1][0], samples[1][1], fraction.x), fraction.y)
						+ mix( mix(samples[0][1], samples[0][2], fraction.x), mix(samples[1][1], samples[1][2], fraction.x), fraction.y)
						+ mix( mix(samples[0][2], samples[0][3], fraction.x), mix(samples[1][2], samples[1][3], fraction.x), fraction.y)
	
						+ mix( mix(samples[1][0], samples[1][1], fraction.x), mix(samples[2][0], samples[2][1], fraction.x), fraction.y)
						+ mix( mix(samples[1][1], samples[1][2], fraction.x), mix(samples[2][1], samples[2][2], fraction.x), fraction.y)
						+ mix( mix(samples[1][2], samples[1][3], fraction.x), mix(samples[2][2], samples[2][3], fraction.x), fraction.y)
						
						+ mix( mix(samples[2][0], samples[2][1], fraction.x), mix(samples[3][0], samples[3][1], fraction.x), fraction.y)
						+ mix( mix(samples[2][1], samples[2][2], fraction.x), mix(samples[3][1], samples[3][2], fraction.x), fraction.y)
						+ mix( mix(samples[2][2], samples[2][3], fraction.x), mix(samples[3][2], samples[3][3], fraction.x), fraction.y);
	
	return shadowAmount * (1 / 9.0);
}

float DoSoftwarePCF3x3Gauss(vec2 texCoord, float zpos)
{
	vec2 fraction = fract(texCoord * shadowResolution);
	float acneFix = 0.002;
	zpos -= acneFix;
	
	mat4 samples = mat4( 	step(zpos,		texture2D(shadowSampler, texCoord).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSize.x, 0)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeDouble.x, 0)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeTriple.x, 0)).r	),
							
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(0, texelSize.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSize.x, texelSize.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeDouble.x, texelSize.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeTriple.x, texelSize.y)).r	),
							
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(0, texelSizeDouble.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSize.x, texelSizeDouble.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeDouble.x, texelSizeDouble.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeTriple.x, texelSizeDouble.y)).r	),
							
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(0, texelSizeTriple.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSize.x, texelSizeTriple.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeDouble.x, texelSizeTriple.y)).r	),
							step(zpos,		texture2D(shadowSampler, texCoord + vec2(texelSizeTriple.x, texelSizeTriple.y)).r	)
						);
	
	
	float shadowAmount = mix( mix(samples[0][0], samples[0][1], fraction.x), mix(samples[1][0], samples[1][1], fraction.x), fraction.y) * gauss3x3[0]
						+ mix( mix(samples[0][1], samples[0][2], fraction.x), mix(samples[1][1], samples[1][2], fraction.x), fraction.y) * gauss3x3[1]
						+ mix( mix(samples[0][2], samples[0][3], fraction.x), mix(samples[1][2], samples[1][3], fraction.x), fraction.y) * gauss3x3[2]
	
						+ mix( mix(samples[1][0], samples[1][1], fraction.x), mix(samples[2][0], samples[2][1], fraction.x), fraction.y) * gauss3x3[3]
						+ mix( mix(samples[1][1], samples[1][2], fraction.x), mix(samples[2][1], samples[2][2], fraction.x), fraction.y) * gauss3x3[4]
						+ mix( mix(samples[1][2], samples[1][3], fraction.x), mix(samples[2][2], samples[2][3], fraction.x), fraction.y) * gauss3x3[5]
						
						+ mix( mix(samples[2][0], samples[2][1], fraction.x), mix(samples[3][0], samples[3][1], fraction.x), fraction.y) * gauss3x3[6]
						+ mix( mix(samples[2][1], samples[2][2], fraction.x), mix(samples[3][1], samples[3][2], fraction.x), fraction.y) * gauss3x3[7]
						+ mix( mix(samples[2][2], samples[2][3], fraction.x), mix(samples[3][2], samples[3][3], fraction.x), fraction.y) * gauss3x3[8];
	
	return shadowAmount;
}

void main()
{
	vec3 albedo = texture2D(albedoSampler, vertex_texcoord).rgb;
	vec3 lighting = texture2D(lightSampler, vertex_texcoord).rgb;
	
	float depth = texture2D(depthSampler, vertex_texcoord).r;

	if ( depth < 0.99 )
	{
		vec3 normal = texture2D(normalSampler, vertex_texcoord).rgb * 2 - 1;
		float dotLight = dot(normal, -directional_light_dir);
		
		float offsetAmount = (1.0 - abs(dotLight)) * 5;
	
#if COMBINE_ORTHOGRAPHIC == 0
		vec3 worldRay = frustum_fwd + frustum_top * vertex_position.y + frustum_right * vertex_position.x;
		vec3 worldPosition = view_origin + worldRay * depth;
#else
		vec3 worldRay = frustum_top * vertex_position.y + frustum_right * vertex_position.x;
		vec3 worldPosition = view_origin + worldRay + frustum_fwd * depth;
#endif

		worldPosition += offsetAmount * normal;

		vec4 shadowPos = shadowVP * vec4(worldPosition, 1.0);
		vec3 boundsVec = floor(shadowPos.xyz);
		float bounds = step(0.0001,boundsVec.x*boundsVec.x+boundsVec.y*boundsVec.y+boundsVec.z*boundsVec.z);
		
		float shadowAmount = DoSoftwarePCF3x3Gauss(shadowPos.xy, shadowPos.z);
		//shadowAmount = step(shadowPos.z - 0.001, shadowAmount);
		shadowAmount = mix(shadowAmount, 1.0, bounds);
		
		float lightAmt = clamp(dotLight, 0, 1);
		vec3 lightColor = mix(directional_light_ambient, directional_light_color, shadowAmount * lightAmt);
		//shadowAmount = shadowAmount * 0.85 + 0.15;
		
		
		vec3 world2Eye = view_origin - worldPosition;
		world2Eye = normalize(world2Eye);
		float phong = BlinnPhong(world2Eye, -directional_light_dir, normal, 25.0);
		phong *= step( 0.01, lightAmt );
		
		lightColor = lightColor + directional_light_color * phong * shadowAmount;
		
		lighting += lightColor;
		
		float fogAmount = clamp((length(view_origin - worldPosition) - fogParams.y) / fogParams.z, 0.0, 1.0) * fogParams.x;
		
		albedo = mix(albedo * lighting, fogColor, fogAmount * fogAmount);
		
		//albedo = (normal * 0.5 + 0.5);
	}
	else
	{
		albedo = fogColor;
	}

	fragment_color = albedo;
}