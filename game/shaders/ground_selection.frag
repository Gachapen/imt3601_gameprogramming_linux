
#include "common.h"

uniform float zrange;

in float vertex_depth;

out vec3 fragment_color;


void main()
{
	fragment_color = vec3(0, 0, 1);

	// need to normalize depth for depth buffer lookup because it's clamped otherwise?? :(
#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = vertex_depth / zrange;
#endif
}


