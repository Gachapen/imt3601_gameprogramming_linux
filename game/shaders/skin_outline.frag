
#include "common.h"

uniform float zrange;
uniform float time;

in float vertex_depth;

out vec3 fragment_color;

void main()
{
	float timeAnim = sin(time * 20) * 0.5 + 0.5;
	fragment_color = vec3(timeAnim, timeAnim, 0);

	// need to normalize depth for depth buffer lookup because it's clamped otherwise?? :(
#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = vertex_depth / zrange;
#endif
}


