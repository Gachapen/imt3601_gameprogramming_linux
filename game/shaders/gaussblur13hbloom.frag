
#include "common.h"

uniform sampler2D albedoSampler;
uniform vec2 texelSize;

in vec2 vertex_texcoord;

out vec3 fragment_color;


const vec2 offsets13[13] = vec2[]
(
	vec2(-6,0),
	vec2(-5,0),
	vec2(-4,0),
	vec2(-3,0),
	vec2(-2,0),
	vec2(-1,0),
	vec2(0,0),
	vec2(1,0),
	vec2(2,0),
	vec2(3,0),
	vec2(4,0),
	vec2(5,0),
	vec2(6,0)
);

void main()
{

	vec3 albedo = vec3(0);
	
	for (int i = 0; i < 13; i++)
	{
		albedo += texture2D(albedoSampler, vertex_texcoord + texelSize * offsets13[i]).rgb * gauss13[i];
	}
	

	
	//albedo -= 0.2;
	albedo *= 1.2;
	albedo = clamp(albedo,0,1);
	
	float desaturated = dot(vec3(0.59,0.3,0.11), albedo);

	albedo = clamp(albedo,0,1);
	albedo = mix(albedo, vec3(desaturated, desaturated, desaturated), desaturated);
	albedo = pow(albedo, vec3(2.5));
	
	fragment_color = albedo;
}