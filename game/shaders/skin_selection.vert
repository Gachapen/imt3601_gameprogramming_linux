
#include "common.h"

uniform mat4 VP;
uniform mat4 bones[64];
uniform float znear;

in vec3 position;
in vec2 texcoord;
in vec3 boneindices;


out float vertex_depth;

void main()
{
	vec4 position4 = vec4(position, 1.0);

	vec4 worldSpacePosition =  bones[int(boneindices.x)] * position4 * 0.333333
					+ bones[int(boneindices.y)] * position4 * 0.333333
					+ bones[int(boneindices.z)] * position4 * 0.333333;

	gl_Position = VP * worldSpacePosition;
	
	vertex_depth = gl_Position.z - znear;
					
	gl_ClipDistance[0] = dot(worldSpacePosition, vec4(0, 0, 1 ,-15));
}
