
#include "common.h"

uniform mat4 MVP;
uniform float znear;

in vec3 position;
in vec2 texcoord;
in vec3 normal;


out vec3 vertex_normal;
out vec2 vertex_uv;
out float vertex_depth;

void main()
{
	gl_Position = MVP * vec4(position, 1.0);
	
	vertex_depth = gl_Position.z - znear;

	vertex_uv = texcoord;
	
	vertex_normal = rotate3x3(MVP, normal);
					
	gl_ClipDistance[0] = dot(vec4(position, 1.0), vec4(0, 0, 1 ,-15));
}
