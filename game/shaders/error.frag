
uniform sampler2D albedoSampler;

out vec3 fragment_color;

in vec2 vertex_uv;

void main()
{
	vec4 albedo = texture2D(albedoSampler, vertex_uv);
	
	fragment_color = albedo.rgb;
}
