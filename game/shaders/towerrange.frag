
#include "common.h"

uniform float zrange;
uniform sampler2D albedoSampler;
uniform sampler2D innerSampler;
uniform vec4 diffusemodulation;
uniform float time;

in vec2 vertex_uv;
in float vertex_depth;

out vec4 fragment_color;

void main()
{
	float angle = time * 0.5;
	float c0 = cos(angle);
	float s0 = sin(angle);
	vec2 uv_0 = vertex_uv - vec2(0.5, 0.5);
	uv_0 = vec2(uv_0.x * c0 - uv_0.y * s0, uv_0.x * s0 + uv_0.y * c0);
	
	float angle1 = time * -0.4;
	float c1 = cos(angle1);
	float s1 = sin(angle1);
	vec2 uv_1 = (vertex_uv - vec2(0.5, 0.5)) * 3;
	uv_1 = vec2(uv_1.x * c1 - uv_1.y * s1, uv_1.x * s1 + uv_1.y * c1);
	
	float alpha0 = sin(time * 12) * 0.2 + 0.9;

	fragment_color = texture2D(albedoSampler, uv_0 + 0.5) * alpha0;
	fragment_color += texture2D(innerSampler, uv_1 + 0.5);
	fragment_color *= diffusemodulation;
	
#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = vertex_depth / zrange;
#endif

	//fragment_color = vec4(1,1,0,1);
	//fragment_color = vec4(vertex_uv, 0, 1);
}


