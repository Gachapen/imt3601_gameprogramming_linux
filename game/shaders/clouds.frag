
//COMBO: REFLECTION

#include "common.h"

uniform sampler2D noiseSampler;
uniform sampler2D noise2Sampler;
uniform sampler2D normalSampler;
uniform sampler2D normal2Sampler;

uniform vec3 view_origin;

in vec3 vertex_position;
in vec4 vertex_uvs;
in vec2 vertex_noise;

out vec4 fragment_color;


void main()
{
	vec3 delta = view_origin - vertex_position;
#if REFLECTION
	float deltaZ = 1;
	
	float distSqr = length(delta) / 3000.0;
	distSqr = 1.0 - distSqr;
	distSqr *= 4;
	
	distSqr = clamp(distSqr, 0, 0.55);
	
	distSqr *= distSqr;
#else
	float deltaZ = clamp(abs(delta.z) * 0.001, 0, 1);
	
	float distSqr = length(delta) / 2000.0;
	distSqr = 1.0 - distSqr;
	distSqr *= 3;
	
	distSqr = clamp(distSqr, 0, 0.7);
	
	distSqr *= distSqr;
#endif
	
	vec3 normal = texture2D(normalSampler, vertex_uvs.xy).xyz * 2 - 1;
	normal += texture2D(normalSampler, vertex_uvs.zw).xyz * 2 - 1;
	
	normal = normalize(normal);
	
	float light = dot(vec3(0.707, 0.707, 0.0), normal) * 0.5 + 0.5;
	
	float alpha0 = texture2D(noiseSampler, vertex_uvs.xy).r;
	alpha0 = smoothstep(0.5, 0.7, alpha0);
	
	float alpha1 = texture2D(noise2Sampler, vertex_uvs.zw).r;
	alpha1 = smoothstep(0.5, 0.7, alpha1);
	
	float alphaNoise = texture2D(noiseSampler, vertex_noise).r;
	//alphaNoise = smoothstep(0.6, 0.8, alphaNoise);
	alphaNoise = smoothstep(0.3, 0.5, alphaNoise);
	
	alpha0 = max(alpha1, alpha0);
	
	//float mixedAlpha = abs(alpha0 * 2 - 1);
	//mixedAlpha = smoothstep(0, 0.5, mixedAlpha);
	
	float mixedAlpha = smoothstep(1, 0.4, alpha0) * smoothstep(0, 0.4, alpha0);
	
	alpha0 = mix(alpha0, alphaNoise, mixedAlpha);

	fragment_color = vec4(1,1,1, deltaZ * distSqr * alpha0);
	
	fragment_color.xyz *= light; //vec4(normal, 1);
	
	//fragment_color = vec4(alphaNoise, alphaNoise, alphaNoise, 1);
}