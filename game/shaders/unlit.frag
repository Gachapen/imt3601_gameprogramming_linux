
//COMBO: VERTEXCOLOR
//COMBO: VERTEXALPHA
//COMBO: DIFFUSEMODULATION
//COMBO: TEXTURED

#include "common.h"

uniform float zrange;
uniform vec4 diffusemodulation;

#if VERTEXCOLOR
in vec3 vertex_color;
#elif VERTEXALPHA
in vec4 vertex_color;
#endif

#if TEXTURED
in vec2 vertex_uv;
uniform sampler2D albedoSampler;
#endif

in float vertex_depth;

out vec4 fragment_color;

void main()
{
#if VERTEXCOLOR
	fragment_color = vec4(vertex_color, 1);
#elif VERTEXALPHA
	fragment_color = vertex_color;
#else
	fragment_color = vec4(1, 1, 1, 1);
#endif

#if DIFFUSEMODULATION
	fragment_color *= diffusemodulation;
#endif

#if TEXTURED
	fragment_color *= texture2D(albedoSampler, vertex_uv);
#endif

#if COMBINE_ORTHOGRAPHIC == 0
	gl_FragDepth = vertex_depth / zrange;
#endif
}


