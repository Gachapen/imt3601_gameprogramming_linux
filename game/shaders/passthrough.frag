
uniform sampler2D albedoSampler;

in vec2 vertex_texcoord;

out vec3 fragment_color;

void main()
{
	//fragment_color = texture2D(albedoSampler, vertex_texcoord).rgb;
	fragment_color = texture2D(albedoSampler, vertex_texcoord).rgb;
	//fragment_color = vec3(0,1,0);
}